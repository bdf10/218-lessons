\documentclass[]{bmr}

\title{Eigenvalues}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Eigenvalues}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixlI}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<3->[red]
      }
      , code-after = {
        \begin{tikzpicture}[red, thick, <-, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {$\scriptstyle \alt<7->{7}{t}\cdot I_3$};
        \end{tikzpicture}
      }
      ]
      \alt<7->{7}{t} & 0              & 0 \\
      0              & \alt<7->{7}{t} & 0 \\
      0              & 0              & \alt<7->{7}{t}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<4->
      }
      , code-after = {
        \begin{tikzpicture}[blu, thick, <-, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} 11 &  16 & -8 \\
      {} -4 &  -9 &  8 \\
      {}  0 &   0 &  7
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<5->[grn]
      }
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm]
          \draw[visible on=<5->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {$\scriptstyle X_A(\alt<8->{7}{t})$};
          \node[below right=0.25mm and 4mm, visible on=<10->] at ($ (row-4-|col-1)!0.25!(row-4-|col-4) $)
          {\scriptsize $\lambda=7$ is an eigenvalue of $A$};
        \end{tikzpicture}
      }
      ]
      \alt<9->{-4}{\alt<8->{7}{t}-11} &  -16                            &   8                           \\
      4                               &  \alt<9->{16}{\alt<8->{7}{t}+9} &  -8                           \\
      0                               &    0                            & \alt<9->{0}{\alt<8->{7}{t}-7}
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{characteristic matrix}} of an $n\times n$ matrix $A$ is
    $X_A(t)=t\cdot I_n-A$.
    \[
      \onslide<2->{\MatrixlI-\MatrixA=\MatrixX}
    \]
    \onslide<6->{A scalar $\lambda$ is an \emph{\grn{eigenvalue}} of $A$ if
      $X_A(\lambda)=\lambda\cdot I_n-A$ is \emph{singular}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      80 & -308 & 308 \\
      14 &  -53 &  56 \\
      -7 &   28 & -25
    \end{bNiceMatrix}
  }
  \newcommand{\CharacteristicMatrix}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<4->[grn]}
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm]
          \draw[visible on=<4->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {$\scriptstyle -4\cdot I_3-A$};
        \end{tikzpicture}
      }
      ]
      {} -84 & 308 & -308 \\
      {} -14 &  49 &  -56 \\
      {}   7 & -28 &   21
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<5->{1}{1}
        \hlEntry<6->{2}{2}
        \hlCol<7->[red]{3}
      }
      , code-after = {
        \begin{tikzpicture}[red, thick, <-, shorten <=0.5mm]
          \draw[visible on=<7->] ($ (row-1-|col-3)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle \nullity(-4\cdot I_3-A)=1>0$};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 11 \\
      0 & 1 &  2 \\
      0 & 0 &  0
    \end{bNiceMatrix}
  }
  \begin{block}{Observation}
    A scalar $\lambda$ is an eigenvalue of an $n\times n$ matrix $A$ if and only
    if
    \begin{align*}
      \rank(\lambda\cdot I_n-A)<n && \nullity(\lambda\cdot I_n-A)>0
    \end{align*}
    \onslide<2->{So, to verify if $\lambda$ is an eigenvalue, we need to reduce
      $\lambda\cdot I_n-A$.}
    \begin{align*}
      \onslide<2->{A = \MatrixA} && \onslide<3->{\rref\CharacteristicMatrix=\MatrixR}
    \end{align*}
    \onslide<8->{Here, we have shown that $\lambda=-4$ is an eigenvalue of $A$.}
  \end{block}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      {}  13 & -21 &  1 \\
      {}   1 &  -2 & -2 \\
      {} -15 &   5 &  2
    \end{bNiceMatrix}
  }
  \newcommand{\CharacteristicMatrix}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->[grn]}
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm]
          \draw[visible on=<2->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {$\scriptstyle 3\cdot I_3-A$};
        \end{tikzpicture}
      }
      ]
      {} -10 & 21 & -1 \\
      {}  -1 &  5 &  2 \\
      {}  15 & -5 &  1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<3->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<3->{3}{3}
      }
      ]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1
    \end{bNiceMatrix}
  }
  \begin{example}
    Eigenvalues are rare!
    \begin{align*}
      A = \MatrixA && \rref\CharacteristicMatrix = \MatrixR
    \end{align*}
    \onslide<4->{Here, we have shown that $\lambda=3$ is \emph{not} an
      eigenvalue of $A$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      {} 14 & -32 & -16 \\
      {}  8 & -26 & -16 \\
      {} -8 &  32 &  22
    \end{bNiceMatrix}
  }
  \newcommand{\CharacteristicMatrix}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<2>[grn]
        \hlCol<3->[red]{2}
        \hlCol<4->[red]{3}
      }
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm]
          \draw[visible on=<2->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {$\scriptstyle 6\cdot I_3-A$};
          \draw[red, visible on=<3->] ($ (row-4-|col-2.south)!0.5!(row-4-|col-3.south) $) |- ++(-4mm, -2.5mm)
          node[left] {$\scriptstyle \Col_2=-4\cdot\Col_1$};
          \draw[red, visible on=<4->] ($ (row-4-|col-3.south)!0.5!(row-4-|col-4.south) $) |- ++(4mm, -2.5mm)
          node[right] {$\scriptstyle \Col_3=-2\cdot\Col_1$};
        \end{tikzpicture}
      }
      ]
      {} -8 &  32 &  16 \\
      {} -8 &  32 &  16 \\
      {}  8 & -32 & -16
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      ]
      1 & -4 & -2 \\
      0 &  0 &  0 \\
      0 &  0 &  0
    \end{bNiceMatrix}
  }
  \begin{example}
    Sometimes, we can calculate $\rref(\lambda\cdot I_n-A)$ without row
    reducing.
    \begin{align*}
      A = \MatrixA && \rref\CharacteristicMatrix = \onslide<5->{\MatrixR}
    \end{align*}
    \onslide<6->{Here, $\lambda=6$ is an eigenvalue of $A$ and
      $\nullity(6\cdot I_3-A)=2$.}
  \end{example}

\end{frame}


\section{Properties}
\subsection{Nonsingularity and Eigenvalues}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Suppose that $\lambda=0$ is an eigenvalue of $A$, so that
    \[
      \rank(A) =
      \onslide<2->{\rank(-A) =}
      \onslide<3->{\rank(0\cdot I_n-A)}
      \onslide<4->{< n}
    \]
    \onslide<5->{This means that $A$ is singular!}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<4->
        \hlCol<2-3>[red]{3}
      }
      , code-after = {
        \begin{tikzpicture}[thick, <-, shorten <=0.5mm]
          \draw[blu, visible on=<4->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {\scriptsize$\lambda=0$ is an eigenvalue};
          \draw[red, visible on=<2-3>] ($ (row-4-|col-3.south)!0.5!(row-4-|col-4.south) $) |- ++(-4mm, -2.5mm)
          node[left] {$\scriptstyle \Col_3=3\cdot\Col_1-\Col_2$};
        \end{tikzpicture}
      }
      ]
      {}  3 & 4 &  5 \\
      {} -2 & 1 & -7 \\
      {}  5 & 9 &  6
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[r]
      1 & 0 &  3 \\
      0 & 1 & -1 \\
      0 & 0 &  0
    \end{bNiceMatrix}
  }
  \begin{theorem}
    An $n\times n$ matrix $A$ is singular if and only if $\lambda=0$ is an
    eigenvalue of $A$.
    \[
      \rref\MatrixA = \onslide<3->{\MatrixR}
    \]
    \onslide<5->{Equivalently, $A$ is nonsingular if and only if $\lambda=0$ is
      not an eigenvalue.}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {}   90 &  2 &  236 &   75 &   30 &  105 &  513 &   646 & -1137 &  521 \\
      {}   54 &  8 &  236 &   75 &   75 &  150 &  271 &   715 & -1152 &  446 \\
      {} -142 &  1 & -378 & -120 & -195 & -150 & -286 & -1492 &  2479 & -647 \\
      {} -126 &  1 & -387 & -116 & -195 &  -90 & -437 & -1465 &  2663 & -458 \\
      {}  182 &  8 &  721 &  225 &  109 &  165 & 1890 &  1842 & -3899 &  939 \\
      {}  118 & -1 &  286 &   90 &  135 &  109 &  262 &  1108 & -1846 &  527 \\
      {}   40 &  1 &  144 &   45 &   45 &   45 &  259 &   448 &  -842 &  200 \\
      {}  -22 &  0 &  -48 &  -15 &    0 &    0 & -167 &  -118 &   274 &  -85 \\
      {}  -10 &  1 &    0 &    0 &  -15 &    0 &   68 &   -66 &    73 &  -21 \\
      {}   -4 &  0 &    1 &    0 &    0 &  -15 &   37 &    -7 &   -45 &  -43
    \end{bNiceMatrix}
  }
  \begin{example}
    The eigenvalues of this matrix are $\EVals(A)=\Set{-11, \bxb<3->[red]{0}, 4, 5}$.
    \[
      A = \MatrixA
    \]
    \onslide<2->{Since $\lambda=0$ is an eigenvalue, we know that $A$ is
      singular!}
  \end{example}

\end{frame}


\subsection{Triangular Matrices}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixtI}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->[grn]}
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {$\scriptstyle t\cdot I_3$};
        \end{tikzpicture}
      }
      ]
      t & 0 & 0 \\
      0 & t & 0 \\
      0 & 0 & t
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlUpper<3->}
      , code-after = {
        \begin{tikzpicture}[blu, thick, <-, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      3 & -8 &   9 \\
      0 &  4 &   2 \\
      0 &  0 & -11
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<4->[red]{1}{1}
        \hlEntry<4->[red]{2}{2}
        \hlEntry<4->[red]{3}{3}
      }
      ]
      t-3 &    8 &   -9 \\
      0   &  t-4 &   -2 \\
      0   &    0 & t+11
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the characteristic matrix of this upper triangular $A$.
    \[
      \MatrixtI-\MatrixA=\MatrixX
    \]
    \onslide<5->{The eigenvalues are $\EVals(A)=\Set{3, 4, -11}$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The eigenvalues of a triangular matrix are the diagonal entries.
    \[
      A=
      \begin{bNiceMatrix}[
        , r
        , margin
        , code-before = {\hlLower<2-3>\hlDiag<4->}
        ]
        {} -148 &   0 &    0 & 0 &    0 \\
        {}   35 &  16 &    0 & 0 &    0 \\
        {}  172 &  19 &   57 & 0 &    0 \\
        {} -130 & 371 &   15 & 7 &    0 \\
        {}   48 &  43 & -190 & 1 & -148
      \end{bNiceMatrix}
    \]
    \onslide<3->{Here, the eigenvalues of $A$ are
      $\EVals(A)=\Set{-148, 16, 57, 7}$.}
  \end{theorem}

\end{frame}


\section{Geometric Multiplicity}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

    \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      {} -22 & -20 & -40 \\
      {}  15 &  13 &  30 \\
      {}   5 &   5 &   8
    \end{bNiceMatrix}
  }
  \newcommand{\CharacteristicMatrix}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<4->[grn]}
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm]
          \draw[visible on=<4->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-4mm, 2.5mm)
          node[left] {$\scriptstyle -2\cdot I_3-A$};
        \end{tikzpicture}
      }
      ]
      {}  20 &  20 &  40 \\
      {} -15 & -15 & -30 \\
      {}  -5 &  -5 & -10
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<5->[red]{2}
        \hlCol<5->[red]{3}
      }
      , code-after = {
        \begin{tikzpicture}[red, thick, <-, shorten <=0.5mm]
          \draw[visible on=<5->] ($ (row-1-|col-3)!0.5!(row-1-|col-4) $) |- ++(-8mm, 2.5mm)
          node[left] {$\scriptstyle \nullity(-2\cdot I_3-A)=2$};
          \draw[visible on=<5->] ($ (row-1-|col-2)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm);
        \end{tikzpicture}
      }
      ]
      1 & 1 & 2 \\
      0 & 0 & 0 \\
      0 & 0 & 0
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{geometric multiplicity}} of $\lambda$ is
    $\gm_A(\lambda)=\nullity(\lambda\cdot I_n-A)$.
    \begin{align*}
      \onslide<2->{A=\MatrixA} && \onslide<3->{\rref\CharacteristicMatrix=\MatrixR}
    \end{align*}
    \onslide<6->{Here, we find $\gm_A(-2)=2$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \tikzstyle{th}=[thin, rounded corners=0.5mm]
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      {}  -30 &  -56 & -28 \\
      {}   71 &  119 &  57 \\
      {} -107 & -172 & -81
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixCL}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<3->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[blu, thick, <-, shorten <=0.5mm]
          \draw[visible on=<3->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-8mm, 2.5mm)
          node[left] {$\scriptstyle -2\cdot I_3-A$};
        \end{tikzpicture}
      }
      ]
      {}  28 &   56 &  28 \\
      {} -71 & -121 & -57 \\
      {} 107 &  172 &  79
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixCR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<6->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm]
          \draw[visible on=<6->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-8mm, 2.5mm)
          node[left] {$\scriptstyle 5\cdot I_3-A$};
        \end{tikzpicture}
      }
      ]
      {}  35 &   56 &  28 \\
      {} -71 & -114 & -57 \\
      {} 107 &  172 &  86
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the data
    \begin{align*}
      A = \MatrixA && \EVals(A) = \Set{-2, 5}
    \end{align*}
    \onslide<2->{The geometric multiplicities are}
    \begin{align*}
      \onslide<2->{\gm_A(-2)} &\onslide<2->{= \nullity\MatrixCL} & \onslide<5->{\gm_A(5)} &\onslide<5->{= \nullity\MatrixCR} \\
                              &\onslide<2->{=} \onslide<4->{1}   &                        &\onslide<5->{=} \onslide<7->{1}
    \end{align*}
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $A$ is $n\times n$ with
    $\EVals(A)=\Set{\lambda_1, \lambda_2,\dotsc, \lambda_k}$. Then
    \[
      k\leq \gm_A(\lambda_1)+\gm_A(\lambda_2)+\dotsb+\gm_A(\lambda_k) \leq n
    \]
    In particular, $A$ has at most $n$ eigenvalues.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      {}   7 &   6 & -24 \\
      {} -36 & -23 &  84 \\
      {}  -9 &  -6 &  22
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the data
    \begin{align*}
      A = \MatrixA && \EVals(A)=\Set{-2, 1, 7}
    \end{align*}
    \pause The equation
    \[
      3\leq \gm_A(-2)+\gm_A(1)+\gm_A(7)\leq 3
    \]
    implies that
    \begin{align*}
      \gm_A(-2) = 1 && \gm_A(1) = 1 && \gm_A(7) = 1
    \end{align*}
  \end{example}

\end{frame}

\end{document}
