% \documentclass[12pt]{article}
\documentclass[17pt]{extarticle}

\usepackage{mth}
\usepackage{sgtx}
\usepackage{fullpage}
\usepackage{extarrows}
\usepackage{resizegather}

\theoremstyle{definition}
\newtheorem*{problem}{Problem}

\title{Quadratic Forms Example}
\author{Math 218D-2}
\date{}

\begin{document}

\maketitle

\begin{sagesilent}
  A = matrix(ZZ, [[1, 0, -1], [0, 1, -2], [0, 0, 0], [0, 0, 0]])
  A = matrix(ZZ, [[0, 1, 1], [-1, 0, -1], [0, -1, -1], [1, -1, 0]])
  S = A.T*A
  var('x1 x2 x3 y1 y2 y3')
  x = vector([x1, x2, x3])
  y = vector([y1, y2, y3])
  Ax = A*x
\end{sagesilent}

\begin{problem}
  Consider the real-symmetric matrix $S$ given by
  \[
    \overset{S}{\sage{S}}
    =
    \overset{A^\intercal}{\sage{A.T}}
    \overset{A}{\sage{A}}
  \]
  Write the quadratic form $q(\bv[x])=\inner{\bv[x], S\bv[x]}$ as a linear
  combination of squares to determine the definiteness.
\end{problem}

\begin{proof}[Solution 1]
  One way to do this is to take advantage of the given factorization. Since
  $S=A^\intercal A$, we have
  \begin{align*}
    q(\bv[x])
    &= \inner{\bv[x], S\bv[x]} \\
    &= \inner{\bv[x], A^\intercal A\bv[x]} \\
    &= \inner{A\bv[x], A\bv[x]} \\
    &= \norm{A\bv[x]}^2 \\
    &= (\sage{Ax[0]})^2+(\sage{Ax[1]})^2+(\sage{Ax[2]})^2+(\sage{Ax[3]})^2 \\
    &\geq 0
  \end{align*}
  Of course, this calculation demonstrates that $S$ is \emph{positive
    semidefinite}.

  To determine whether or not $S$ is positive \emph{definite}, note that the
  third column of $A$ is the sum of the first two columns of $A$. This means
  that $\nv{1 1 -1}\in\Null(A)$, which implies
  \[
    q(1, 1, -1)
    = \norm{A\nv{1 1 -1}}^2
    = \norm{\bv[O]}^2
    = 0
  \]
  This means that $S$ is \emph{not} positive definite.
\end{proof}

\begin{proof}[Solution 2]
  Let's pretend that we didn't know $S=A^\intercal A$. We know that ``completing
  the square'' allows us to write $q(\bv[x])$ as a linear combination of
  squares. To do this, we start by calculating
  \begin{sagesilent}
    var('t')
    chi = t*identity_matrix(S.nrows())-S
    from functools import partial
    e = partial(elementary_matrix, S.nrows())
    chi1 = e(row1=2, row2=1, scale=1)*chi
    chi2 = e(row1=2, scale=1/(t-5))*chi1
    chi3 = e(row1=0, row2=2, scale=1)*e(row1=1, row2=2, scale=2)*chi2
    M12 = chi3.matrix_from_rows_and_columns([0, 1], [0, 1])
  \end{sagesilent}
  \newcommand{\StepA}{
    \begin{NiceArray}[small]{rcrcr}
      \bv[r]_1 &+&       \bv[r]_3 &\to& \bv[r]_1 \\
      \bv[r]_2 &+& 2\cdot\bv[r]_3 &\to& \bv[r]_2
    \end{NiceArray}
  }
  \begin{align*}
    \chi_S(t)
    &= \sage{detmat(chi)} \\
    &\xlongequal{\bv[r]_3 + \bv[r]_2 \to \bv[r]_3} \sage{detmat(chi1)} \\
    &= (t-5)\cdot\sage{detmat(chi2)} \\
    &\xlongequal{\StepA} (t-5)\cdot\sage{detmat(chi3)} \\
    &= (t-5)\cdot\sage{detmat(M12)} \\
    &= (t-5)\cdot\Set{\sage{M12.det()}} \\
    &= (t-5)\cdot\Set{\sage{expand(M12.det())}} \\
    &= (t-5)\cdot\sage{factor(M12.det())}
  \end{align*}
  This shows that $\EVals(S)=\Set{0, 3, 5}$, which immediately implies that $S$
  is positive semidefinite but not positive definite. Nice.

  Next, we calculate bases of the eigenspaces of $S$ by looking for column
  relations of the relevant characteristic matrices
  \begin{align*}
    \mathcal{E}_S(0) &= \Null\overset{0\cdot I_3-S}{\sage{chi(t=0)}}=\Span\Set*{\nvc{1; 1; -1}}  = \Span\Set*{\frac{1}{\sqrt{3}}\nvc{1; 1; -1}} \\
    \mathcal{E}_S(3) &= \Null\overset{3\cdot I_3-S}{\sage{chi(t=3)}}=\Span\Set*{\nvc{-2; 1; -1}} = \Set*{\frac{1}{\sqrt{6}}\nvc{-2; 1; -1}} \\
    \mathcal{E}_S(5) &= \Null\overset{5\cdot I_3-S}{\sage{chi(t=5)}}=\Span\Set*{\nvc{0; 1; 1}}   = \Span\Set*{\frac{1}{\sqrt{2}}\nvc{0; 1; 1}}
  \end{align*}
  This gives our desired spectral factorization
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} \sfrac{ 1}{\sqrt{3}} & \sfrac{-2}{\sqrt{6}} & \sfrac{0}{\sqrt{2}} \\
      {} \sfrac{ 1}{\sqrt{3}} & \sfrac{ 1}{\sqrt{6}} & \sfrac{1}{\sqrt{2}} \\
      {} \sfrac{-1}{\sqrt{3}} & \sfrac{-1}{\sqrt{6}} & \sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixUT}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} \sfrac{ 1}{\sqrt{3}} & \sfrac{1}{\sqrt{3}} & \sfrac{-1}{\sqrt{3}} \\
      {} \sfrac{-2}{\sqrt{6}} & \sfrac{1}{\sqrt{6}} & \sfrac{-1}{\sqrt{6}} \\
      {} \sfrac{ 0}{\sqrt{2}} & \sfrac{1}{\sqrt{2}} & \sfrac{ 1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \begin{sagesilent}
    D = diagonal_matrix([0, 3, 5])
  \end{sagesilent}
  \[
    \overset{S}{\sage{S}}
    =
    \overset{U}{\MatrixU}
    \overset{D}{\sage{D}}
    \overset{U^\intercal}{\MatrixUT}
  \]

  Now, by putting $\bv[y]=U^\intercal\bv[x]$, we have
  \begin{align*}
    q(\bv[x])
    & = 0\cdot y_1^2 + 3\cdot y_2^2 + 5\cdot y_3^2 \\
    & =
    0\cdot \Set*{\frac{1}{\sqrt{3}}\cdot(x_1+x_2-x_3)}^2\\
    &+ 3\cdot \Set*{\frac{1}{\sqrt{6}}\cdot(-2\,x_1+x_2-x_3)}^2\\
    &+ 5\cdot \Set*{\frac{1}{\sqrt{2}}\cdot(x_2+x_3)}^2\qedhere
  \end{align*}
\end{proof}

\end{document}
