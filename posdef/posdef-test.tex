\documentclass[]{bmr}

\title{Definiteness}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Quadratic Forms}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\VectorX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[x]$};
        \end{tikzpicture}
      }
      ]
      x \\
      y
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorXT}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[x]^\intercal$};
        \end{tikzpicture}
      }
      ]
      x & y
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixH}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<5-6>[grn]
        \hlEntry<7>[grn]{1}{1}
        \hlEntry<8>[grn]{2}{2}
        \hlEntry<9>[grn]{1}{2}
        \hlEntry<9>[grn]{2}{1}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=<5->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle S$};
        \end{tikzpicture}
      }
      ]
      {}  3 & -7 \\
      {} -7 &  5
    \end{bNiceMatrix}
  }
  \begin{definition}
    A \emph{\grn{quadratic form}} on $\mathbb{R}^n$ is an expression given by
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(%
          q(\subnode{x942h}{\textcolor<2->{blu}{\bv[x]}})
          =
          \inner{\bv[x], \subnode{S942h}{\textcolor<3->{red}{S}}\bv[x]}
          \)};

        \draw[<-, thick, blu, overlay, visible on=<2->]
        (x942h.south) |- ++(-3mm, -2.5mm) node[left, scale=0.75]
        {$\bv[x]=\begin{bNiceMatrix}x_1 & x_2 & \Cdots & x_n\end{bNiceMatrix}^\intercal$};

        \draw[<-, thick, red, overlay, visible on=<3->]
        (S942h.south) |- ++(3mm, -2.5mm) node[right, scale=0.75] {real-symmetric $S^\intercal=S$};
      \end{tikzpicture}
    \]
    \onslide<4->{For example, consider}
    \[
      \begin{tikzpicture}[remember picture, visible on=<4->]
        \node{\(
          q(x, y)
          =
          \VectorXT<6->
          \MatrixH
          \VectorX<6->
          =
          \subnode{oe28}{3\,x^2}+\subnode{njlau}{5\,y^2}\subnode{knhou}{-14\,xy}
          \)};

        \hlNode<7>[grn]{oe28}
        \hlNode<8>[grn]{njlau}
        \hlNode<9>[grn]{knhou}

      \end{tikzpicture}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}<+->
    $q(x, y)=-16\,x^2-2\,y^2+5\,xy$
  \end{example}

  \begin{example}<+->
    $q(x, y, z)=5\,x^2+y^2-z^2+2\,xy-xz+7\,yz$
  \end{example}

  \begin{example}<+->
    $q(x_1, x_2, x_3, x_4)=x_1^2+x_2^2-x_4^2$
  \end{example}

  \begin{example}<+->
    $q(x, y)=x^2-y^2+1$ is \emph{not} a quadratic form
  \end{example}

\end{frame}

\subsection{Symmetric Representations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixH}{
    \begin{bNiceMatrix}[
      , r
      , first-row
      , first-col
      ]
      {}        & x_1               & x_2               & \Cdots & x_n               \\
      {} x_1    & c_{11}            & \sfrac{c_{12}}{2} & \Cdots & \sfrac{c_{1n}}{2} \\
      {} x_2    & \sfrac{c_{12}}{2} & c_{22}            & \Cdots & \sfrac{c_{2n}}{2} \\
      {} \Vdots & \Vdots            & \Vdots            & \Ddots & \Vdots            \\
      {} x_n    & \sfrac{c_{1n}}{2} & \sfrac{c_{2n}}{2} & \Cdots & c_{nn}
    \end{bNiceMatrix}
  }
  \begin{theorem}
    For $q(x_1, x_2,\dotsc, x_n)=\sum c_{ij}\cdot x_ix_j$ define
    \[
      S = \MatrixH
    \]
    Then $S$ is real-symmetric and $q(\bv[x])=\inner{\bv[x], S\bv[x]}$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixH}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , first-row
      , first-col
      , code-before = {
        \hlEntry<3-4>[red]{1}{1}
        \hlEntry<5-6>[red]{1}{2}
        \hlEntry<7-8>[red]{2}{1}
        \hlEntry<9-10>[red]{2}{2}
        \hlEntry<11-12>[red]{1}{3}
        \hlEntry<13-14>[red]{3}{1}
        \hlEntry<15-16>[red]{2}{3}
        \hlEntry<17-18>[red]{3}{2}
        \hlEntry<19-20>[red]{3}{3}
      }
      ]
      {}                                         & \textcolor<3-4,7-8,13-14>{red}{x_1} & \textcolor<5-6,9-10,17-18>{red}{x_2} & \textcolor<11-12,15-16,19-20>{red}{x_3} \\
      {} \textcolor<3-4,5-6,11-12>{red}{x_1}     & \onslide<4->{-6}                    & \onslide<6->{ -5}                    & \onslide<12->{ 1}            \\
      {} \textcolor<7-8,9-10,15-16>{red}{x_2}    & \onslide<8->{-5}                    & \onslide<10->{-10}                   & \onslide<16->{-3}            \\
      {} \textcolor<13-14,17-18,19-20>{red}{x_3} & \onslide<14->{ 1}                   & \onslide<18->{ -3}                   & \onslide<20->{ 2}
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the quadratic form given by
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          q(x_1, x_2, x_3)
          =
          \subnode{x1x1}{-6\,x_1^2}
          \subnode{x1x2}{-10\,x_1x_2}
          \subnode{x2x2}{-10\,x_2^2}
          \subnode{x1x3}{+2\,x_1x_3}
          \subnode{x2x3}{-6\,x_2x_3}
          \subnode{x3x3}{+2\,x_3^2}
          \)};

        \hlNode<3-4>[red]{x1x1}
        \hlNode<5-8>[red]{x1x2}
        \hlNode<9-10>[red]{x2x2}
        \hlNode<11-14>[red]{x1x3}
        \hlNode<15-18>[red]{x2x3}
        \hlNode<19-20>[red]{x3x3}
      \end{tikzpicture}
    \]
    \onslide<2->{Then $q(\bv[x])=\inner{\bv[x], S\bv[x]}$ where}
    \[
      \onslide<2->{S = \MatrixH}
    \]
    \onslide<21->{Quadratic forms and real-symmetric matrices are linked via
      this process.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} \lambda_1 &           &           \\
      {}           & \lambda_2 &           \\
      {}           &           & \lambda_3
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider $q(\bv[x])=\inner{\bv[x], D\bv[x]}$ where $D$ is diagonal.
    \begin{align*}
      D=\MatrixD && q(x_1, x_2, x_3)=\lambda_1\, x_1^2+\lambda_2\, x_2^2+\lambda_3\, x_3^2
    \end{align*}
    \onslide<2->{Diagonal quadratic forms have no ``cross-terms.''}
  \end{example}

\end{frame}


\section{Definiteness}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Quadratic forms are classified according to their values at
    $\bv[x]\neq\bv[O]$.
    \begin{description}[<+->][negative semidefinite]
    \item[positive definite] $q(\bv[x])>0$
    \item[positive semidefinite] $q(\bv[x])\geq0$
    \item[negative definite] $q(\bv[x])<0$
    \item[negative semidefinite] $q(\bv[x])\leq0$
    \item[indefinite] $q(\bv[x])>0$ and $q(\bv[x])<0$ (and possibly $q(\bv[x])=0$)
    \end{description}
    \onslide<+->{These terms are also used to classify real-symmetric matrices.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    \begin{tikzpicture}[remember picture, baseline=(j4.base), inner sep=0pt, outer sep=0pt]
      \node (j4) {\(q(x, y) = x^2+y^2 \subnode{m80j}{\onslide<2->{>}} \onslide<2->{0}\)};

      \draw[<-, thick, red, overlay, visible on=<3->] (m80j.south) |- ++(3mm, -2.5mm)
      node[right] {\scriptsize \emph{positive definite} (also \emph{positive semidefinite})};
    \end{tikzpicture}
  \end{example}

  \begin{example}<4->
    \begin{tikzpicture}[baseline=(j49.base), inner sep=0pt, outer sep=0pt]
      \node (j49) {\(q(x, y) = x^2-y^2\)};

      \draw[<-, thick, red, overlay, visible on=<5->] (j49.east) -- ++(3mm, 0)
      node[right] {\scriptsize \emph{indefinite} ($q(1, 0)>0$ and $q(0, 1)<0$)};
    \end{tikzpicture}
  \end{example}

  \begin{example}<6->
    \begin{tikzpicture}[remember picture, baseline=(j493.base), inner sep=0pt, outer sep=0pt]
      \node (j493) {\(q(x, y) = x^2+2\,xy+y^2 \onslide<7->{= (x+y)^2}\subnode{joie}{\onslide<8->{\geq}}\onslide<8->{0}\)};

      \draw[<-, thick, red, shorten <=0.5mm, overlay, visible on=<8->] (joie.south) |- ++(3mm, -2.5mm)
      node[right] {\scriptsize \emph{positive semidefinite} (\emph{not} positive definite)};
    \end{tikzpicture}
  \end{example}

  \begin{example}<9->
    \begin{tikzpicture}[baseline=(ex.base), inner sep=0pt, outer sep=0pt]
      \node (ex) {$q(x, y, z)=9\,x^2-32\,xy+10\,xz-7y^2-4\,yz+z^2$};

      \node[red, below=3mm, overlay, visible on=<10->] (ex.south) {\scriptsize finding definiteness is \emph{hard}!};
    \end{tikzpicture}
  \end{example}

\end{frame}


\subsection{Gramians}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}<+->
    Every Gramian $A^\intercal A$ is positive semidefinite.
  \end{theorem}
  \begin{proof}<+->
    $
    q(\bv[x]) =
    \onslide<+->{\inner{\bv[x], A^\intercal A\bv[x]} =}
    \onslide<+->{\inner{A\bv[x], A\bv[x]} =}
    \onslide<+->{\norm{A\bv[x]}^2 \geq 0\qedhere}
    $
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<4-6>[grn]
        \hlRow<7->[grn]{1}
        \hlRow<9->[grn]{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      3 & -5 & 9 \\
      2 &  7 & 6
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixAT}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle A^\intercal$};
        \end{tikzpicture}
      }
      ]
      {}  3 & 2 \\
      {} -5 & 7 \\
      {}  9 & 6
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixH}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle S$};
        \end{tikzpicture}
      }
      ]
      13 & -1 &  39 \\
      -1 & 74 &  -3 \\
      39 & -3 & 117
    \end{bNiceMatrix}
  }
  \begin{example}
    Let $q(\bv[x])=\inner{\bv[x], S\bv[x]}$ where
    \[
      \MatrixH<2->
      =
      \MatrixAT<3->
      \MatrixA
    \]
    \onslide<5->{Our quadratic form is positive semidefinite and may be written
      as}
    \[
      \begin{tikzpicture}[remember picture, visible on=<5->]
        \node {\(
          q(x, y, z)
          =
          (\subnode{mapoo}{3\,x-5\,y+9\,z})^2
          +
          (\subnode{tofu}{2\,x+7\,y+6\,z})^2
          \)};

        \hlNode<6->[grn]{mapoo}
        \hlNode<8->[grn]{tofu}

        \draw[<-, thick, grn, shorten <=0.5mm, overlay, visible on=<10->] (tofu.south) |- ++(10mm, -2.5mm)
        node[right] (uiha) {$\scriptstyle\norm{A\bv[x]}^2$};

        \draw[<-, thick, grn, shorten <=0.5mm, overlay, visible on=<10->] (mapoo.south) |- (uiha);
      \end{tikzpicture}
    \]
    \onslide<11->{Note that $q$ is \emph{\red{not positive definite}} because
      $\rank(S)=\rank(A)=2<3$.}
  \end{example}

\end{frame}


\section{Completing the Square}
\subsection{Method}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider a spectral factorization $S=UDU^\intercal$ so that
    \[
      \begin{tikzpicture}[remember picture, inner sep=0pt, outer sep=0pt, visible on=<2->]
        \node {\(
          \begin{aligned}
            q(\bv[x])
            &= \inner{\bv[x], S\bv[x]}                                                                                                                   \\
            &= \onslide<3->{\inner{\bv[x], UDU^\intercal\bv[x]}}                                                                                              \\
            &\onslide<3->{=} \onslide<4->{\inner{\subnode{djie}{\textcolor<5->{red}{U^\intercal\bv[x]}}, D\subnode{joae}{\textcolor<5->{red}{U^\intercal\bv[x]}}}} \\
            &\onslide<4->{=} \onslide<6->{\inner{\textcolor<6->{red}{\bv[y]}, D\textcolor<6->{red}{\bv[y]}}}                                             \\
            &\onslide<6->{=} \onslide<7->{\lambda_1\,y_1^2+\lambda_2\,y_2^2+\dotsb+\lambda_n\,y_n^2}
          \end{aligned}
          \)};

        \draw[<-, thick, red, overlay, visible on=<5>] (joae.south) |- ++(3mm, -2.5mm)
        node[right] (dlkj) {\scriptsize define $\bv[y]=U^\intercal\bv[x]$};

        \draw[<-, thick, red, overlay, visible on=<5>] (djie.south) |- (dlkj);
      \end{tikzpicture}
    \]
    \onslide<8->{This method is called \emph{\grn{completing the square}}.}
  \end{definition}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixH}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      {}  4 & -2 & 1 \\
      {} -2 &  4 & 1 \\
      {}  1 &  1 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      {}  \sfrac{1}{\sqrt{2}} & \sfrac{1}{\sqrt{3}} &  \sfrac{1}{\sqrt{6}} \\
      {} -\sfrac{1}{\sqrt{2}} & \sfrac{1}{\sqrt{3}} &  \sfrac{1}{\sqrt{6}} \\
      {}                    0 & \sfrac{1}{\sqrt{3}} & -\sfrac{2}{\sqrt{6}}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      {} 6 &   &   \\
      {}   & 3 &   \\
      {}   &   & 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixUT}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlRow<4>[red, th]{1}
        \hlRow<6>[red, th]{2}
        \hlRow<8>[red, th]{3}
      }
      ]
      {} \sfrac{1}{\sqrt{2}} & -\sfrac{1}{\sqrt{2}} & 0                    \\
      {} \sfrac{1}{\sqrt{3}} & \sfrac{1}{\sqrt{3}}  & \sfrac{1}{\sqrt{3}}  \\
      {} \sfrac{1}{\sqrt{6}} &  \sfrac{1}{\sqrt{6}} & -\sfrac{2}{\sqrt{6}}
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the spectral factorization
    \[
      \overset{S}{\MatrixH}
      =
      \overset{U}{\MatrixU}
      \overset{D}{\MatrixD}
      \overset{U^\intercal}{\MatrixUT}
    \]
    \onslide<2->{To complete the square, we write}
    \[
      \begin{tikzpicture}[remember picture, inner sep=0pt, outer sep=0pt, visible on=<2->]
        \node {\(
          \begin{aligned}
            q(\bv[x])
            &= 6\,\subnode{kjae4}{\textcolor<3-4>{red}{y_1}}^2+3\,\subnode{uhor}{\textcolor<5-6>{red}{y_2}}^2+0\,\subnode{nhu43}{\textcolor<7-8>{red}{y_3}}^2                                                                    \\
            &= \onslide<9->{6\,\Set*{\frac{1}{\sqrt{2}}\cdot(x_1-x_2)}^2+3\,\Set*{\frac{1}{\sqrt{3}}\cdot(x_1+x_2+x_3)}^2} \\
            &\onslide<9->{=} \onslide<10->{3\,(x_1-x_2)^2+(x_1+x_2+x_3)^2}
          \end{aligned}
          \)};

        \draw[<-, thick, red, overlay, visible on=<3-4>] (kjae4.south) |- ++(3mm, -3mm)
        node[right] {$\scriptstyle y_1=\frac{1}{\sqrt{2}}x_1-\frac{1}{\sqrt{2}}x_2$};

        \draw[<-, thick, red, overlay, visible on=<5-6>] (uhor.south) |- ++(3mm, -3mm)
        node[right] {$\scriptstyle y_2=\frac{1}{\sqrt{3}}x_1+\frac{1}{\sqrt{3}}x_2+\frac{1}{\sqrt{3}}x_3$};

        \draw[<-, thick, red, overlay, visible on=<7-8>] (nhu43.south) |- ++(3mm, -3mm)
        node[right] {$\scriptstyle y_3=\frac{1}{\sqrt{6}}x_1+\frac{1}{\sqrt{6}}x_2-\frac{2}{\sqrt{6}}x_3$};
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\section{Eigenvalues}
\subsection{Definiteness}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose we complete the square to write
    \[
      \begin{tikzpicture}[remember picture, inner sep=0pt, outer sep=0pt]
        \node {\(
          q(\bv[x])
          =
          \subnode{kji1}{\textcolor<2->{red}{\lambda_1}}\,y_1^2+
          \subnode{kji2}{\textcolor<2->{red}{\lambda_2}}\,y_2^2+\dotsb+
          \subnode{kji3}{\textcolor<2->{red}{\lambda_n}}\,y_n^2
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (kji3.south) |- ++(7mm, -2.5mm)
        node[right] (state) {\scriptsize eigenvalues control definiteness};

        \draw[<-, thick, red, overlay, visible on=<2->] (kji1.south) |- (state);
        \draw[<-, thick, red, overlay, visible on=<2->] (kji2.south) |- (state);
      \end{tikzpicture}
    \]
    \onslide<3->{The signs of the eigenvalues determine the definiteness.}
    \begin{description}[<+(3)->][negative semidefinite]
    \item[positive definite] eigenvalues $>0$
    \item[positive semidefinite] eigenvalues $\geq0$
    \item[negative definite] eigenvalues $<0$
    \item[negative semidefinite] eigenvalues $\leq0$
    \item[indefinite] eigenvalues $>0$ and $<0$ (and possibly $=0$)
    \end{description}
  \end{theorem}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixH}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-4-|col-1)!0.5!(row-4-|col-4) $) |- ++(3mm, -2.5mm) node[right] {$\scriptstyle\chi_S(t)=t\cdot (t+2)^2$};
        \end{tikzpicture}
      }
      ]
      {} -1 &  1 &  0 \\
      {}  1 & -1 &  0 \\
      {}  0 &  0 & -2
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider $q(\bv[x])=\inner{\bv[x], S\bv[x]}$ where
    \begin{align*}
      S=\MatrixH<2-> && q(\bv[x])=-x_1^2+2\,x_1x_2-x_2^2-2\,x_3^2
    \end{align*}
    \onslide<3->{The quadratic form is \emph{\red{negative semidefinite}}
      (\emph{not} negative definite).}
  \end{example}

\end{frame}

\end{document}
