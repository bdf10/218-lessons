\documentclass[]{bmr}

\title{Digraphs}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Digraphs}
\subsection{Definition}
\begin{frame}{\secname}{\subsecname}

  \begin{definition}
    A \emph{\grn{directed graph}} is a collection of \emph{\grn{nodes}} and
    \emph{\grn{arrows}}.
    \[
      \begin{tikzpicture}[grph, visible on=<2->]

        \node[state] (v1)                     {$v_1$};
        \node[state] (v2) [right=of v1]       {$v_2$};
        \node[state] (v3) [above right=1.5cm and 2cm of v2] {$v_3$};
        \node[state] (v4) [below right=1.5cm and 2cm of v2] {$v_4$};
        \node[state] (v5) [above right=1.5cm and 2cm of v4] {$v_5$};
        \node[state] (v6) [above right=1.5cm and 2cm of v5] {$v_6$};

        \draw[->, bend left]  (v1) edge node {$a_1$} (v2);
        \draw[->, bend left]  (v2) edge node {$a_2$} (v1);
        \draw[->]             (v2) edge node {$a_3$} (v4);
        \draw[->]             (v3) edge node {$a_4$} (v2);
        \draw[->, bend left]  (v4) edge node {$a_5$} (v5);
        \draw[->, bend right] (v4) edge node {$a_6$} (v5);
        \draw[->]             (v5) edge node {$a_7$} (v6);

      \end{tikzpicture}
    \]
    \onslide<3->{This digraph has \emph{\grn{six nodes}} and \emph{\grn{seven
          arrows}}.}
  \end{definition}

\end{frame}


\subsection{Applications}
\begin{frame}{\secname}{\subsecname}

  \begin{block}{Observation}
    Digraphs describe \emph{\grn{relationships}} between \emph{\grn{objects}}.
    \[
      \begin{tikzpicture}[grph, visible on=<2->]
        \node (wf)                           {\includegraphics[scale=.15]{wf.png}};
        \node (chase) [below right=2cm and 4cm of wf]    {\includegraphics[scale=.15]{chase.png}};
        \node (citi)  [above right=2cm and 4cm of chase] {\includegraphics[scale=.15]{citi.png}};

        \draw[->, bend right] (wf)    edge node {$t_1$} (chase);
        \draw[->, bend right] (chase) edge node {$t_2$} (wf);
        \draw[->, bend left]  (wf)    edge node {$t_3$} (citi);
        \draw[->, bend right] (chase) edge node {$t_4$} (citi);
        \draw[->, bend left]  (chase) edge node {$t_5$} (citi);
      \end{tikzpicture}
    \]
    \onslide<3->{Here we are modeling \emph{\grn{transactions}} between
      \emph{\grn{financial institutions}}.}
  \end{block}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Search engines use \emph{\grn{hyperlink data}} to model the internet.
    \[
      \begin{tikzpicture}[grph, node distance=4cm, scale=0.9, transform shape, visible on=<2->]
        \node (wiki) {\includegraphics[scale=.15]{wiki.png}};
        \node (amazon) [below left=of wiki] {\includegraphics[scale=.15]{amazon.png}};
        \node (youtube) [below=of wiki] {\includegraphics[scale=.15]{youtube.png}};
        \node (netflix) [below right=of wiki] {\includegraphics[scale=.15]{netflix.png}};

        \draw[->] (wiki)    edge [in=90, out=180]  node {$\ell_1$} (amazon);
        \draw[->] (wiki)    edge [in=45, out=-135] node {$\ell_2$} (amazon);
        \draw[->] (amazon)  edge [in=-90, out=0]   node {$\ell_3$} (wiki);
        \draw[->] (amazon)  edge [in=180, out=-45] node {$\ell_4$} (youtube);
        \draw[->] (wiki)    edge [in=90, out=-60]  node {$\ell_5$} (youtube);
        \draw[->] (youtube) edge [in=180, out=45]  node {$\ell_6$} (netflix);
        \draw[->] (netflix) edge [in=0, out=-90]   node {$\ell_7$} (youtube);
        \draw[->] (wiki)    edge                   node {$\ell_8$} (netflix);
        \draw[->] (wiki)    edge [in=90, out=0]    node {$\ell_9$} (netflix);
      \end{tikzpicture}
    \]
    \onslide<3->{Here we are modeling \emph{\grn{hyperlinks}} between
      \emph{\grn{webpages}}.}
  \end{example}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Digraphs can model \emph{\grn{social networks}}.
    \[
      \begin{array}{c}
        \includegraphics[width=0.75\textwidth]{fb-graph.png} \\
        \blu{\text{\tiny\url{https://medium.com/@johnrobb/facebook-the-complete-social-graph-b58157ee6594}}}
      \end{array}
    \]
  \end{example}

\end{frame}


\section{Properties}
\subsection{Euler Characteristic}
\begin{frame}{\secname}{\subsecname}

  \begin{definition}
    The \emph{\grn{Euler characteristic}} of a digraph $G$ is
    \[
      \chi(G)=(\text{number of nodes})-(\text{number of arrows})
    \]
    \onslide<2->{This number is suprisingly interesting.}
    \[
      \begin{tikzpicture}[grph, visible on=<3->]
        \node[state] (v1) {};
        \node[state] (v2) [right=of v1] {};
        \node[state] (v3) [right=of v2] {};

        \draw[->, bend left] (v1) edge (v2);
        \draw[->, bend left] (v2) edge (v3);
        \draw[->, bend right] (v1) edge (v2);
        \draw[->, bend right] (v2) edge (v3);
        \draw[->] (v2) edge (v1);
      \end{tikzpicture}
    \]
    \onslide<4->{Here, $\chi(G)=3-5=-2$.}
  \end{definition}

\end{frame}


\subsection{Connected Components}
\begin{frame}{\secname}{\subsecname}

  \begin{definition}
    The ``islands'' in a digraph are called \emph{\grn{connected components}}.
    \[
      \begin{tikzpicture}[grph, visible on=<2->]
        \node[state] (v1) {};
        \node[state] (v2) [right=of v1] {};
        \node[state] (v3) [right=of v2] {};
        \node[state] (v4) [above right=1cm and 2cm of v3] {};
        \node[state] (v5) [below right=1cm and 2cm of v3] {};

        \draw[->, bend left] (v1) edge (v2);
        \draw[->, bend right] (v1) edge (v2);

        \draw[->] (v3) edge (v4);
        \draw[->] (v4) edge (v5);
        \draw[->] (v5) edge (v3);

      \end{tikzpicture}
    \]
    \onslide<3->{This digraph has \emph{\grn{two connected components}}.}
  \end{definition}

\end{frame}


\subsection{Paths and Cycles}
\begin{frame}{\secname}{\subsecname}

  \begin{definition}
    A \emph{\grn{path}} is a sequence of distinct arrows joining consecutive
    nodes.
    \[
      \begin{tikzpicture}[grph, visible on=<2->]
        \node[state] (v1) {$v_1$};
        \node[state] (v2) [below right=1cm and 3cm of v1] {$v_2$};
        \node[state] (v3) [above right=1cm and 3cm of v1] {$v_3$};
        \node[state] (v4) [right=3cm of v2] {$v_4$};
        \node[state] (v5) [right=3cm of v3] {$v_5$};
        \node[state] (v6) [right=3cm of v5] {$v_6$};

        \draw[->]             (v1) edge node {$a_1$} (v3);
        \draw[->]             (v1) edge node {$a_2$} (v2);
        \draw[->]             (v4) edge node {$a_3$} (v2);
        \draw[->]             (v4) edge node {$a_4$} (v3);
        \draw[->]             (v5) edge node {$a_5$} (v3);
        \draw[->, bend left]  (v5) edge node {$a_6$} (v6);
        \draw[->, bend left]  (v6) edge node {$a_7$} (v5);

        \node[state, fill=grnbg, visible on=<4->] (v1) {$v_1$};
        \draw[->, grn, visible on=<4->]             (v1) edge node {$a_1$} (v3);

        \draw[opacity=0, visible on=<4->] (v1) -- (v3)
        node[midway, above=2.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};

        \node[state, fill=grnbg, visible on=<4->] (v3) [above right=1cm and 3cm of v1] {$v_3$};

        \draw[->, grn, visible on=<5->]             (v5) edge node {$a_5$} (v3);
        \node[state, fill=grnbg, visible on=<5->] (v5) [right=3cm of v3] {$v_5$};

        \draw[opacity=0, visible on=<5->] (v3) -- (v5)
        node[midway, above=2.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};

        \draw[->, bend left, grn, visible on=<6->]  (v6) edge node {$a_7$} (v5);
        \node[state, fill=grnbg, visible on=<6->] (v6) [right=3cm of v5] {$v_6$};

        \draw[opacity=0, bend left, visible on=<6->] (v6) -- (v5)
        node[midway, below=7.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};
      \end{tikzpicture}
    \]
    \onslide<3->{The path $(a_1, a_5, a_7)$ connects $v_1$ to $v_6$.}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \begin{definition}
    A \emph{\grn{cycle}} is a path that starts and ends at the same node.
    \[
      \begin{tikzpicture}[grph, visible on=<2->]
        \node[state] (v1) {$v_1$};
        \node[state] (v2) [below right=1cm and 3cm of v1] {$v_2$};
        \node[state] (v3) [above right=1cm and 3cm of v1] {$v_3$};
        \node[state] (v4) [right=3cm of v2] {$v_4$};
        \node[state] (v5) [right=3cm of v3] {$v_5$};
        \node[state] (v6) [right=3cm of v5] {$v_6$};

        \draw[->]             (v1) edge node {$a_1$} (v3);
        \draw[->]             (v1) edge node {$a_2$} (v2);
        \draw[->]             (v4) edge node {$a_3$} (v2);
        \draw[->]             (v4) edge node {$a_4$} (v3);
        \draw[->]             (v5) edge node {$a_5$} (v3);
        \draw[->, bend left]  (v5) edge node {$a_6$} (v6);
        \draw[->, bend left]  (v6) edge node {$a_7$} (v5);

        \node[state, fill=grnbg, visible on=<4->] (v1) {$v_1$};
        \draw[->, grn, visible on=<4->]             (v1) edge node {$a_1$} (v3);
        \node[state, fill=grnbg, visible on=<4->] (v3) [above right=1cm and 3cm of v1] {$v_3$};

        \draw[opacity=0, visible on=<4->] (v1) -- (v3)
        node[midway, above=2.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};

        \draw[->, grn, visible on=<5->]             (v4) edge node {$a_4$} (v3);
        \node[state, fill=grnbg, visible on=<5->] (v4) [right=3cm of v2] {$v_4$};

        \draw[opacity=0, visible on=<5->] (v3) -- (v4)
        node[midway, above=2.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};

        \draw[->, grn, visible on=<6->]             (v4) edge node {$a_3$} (v2);
        \node[state, fill=grnbg, visible on=<6->] (v2) [below right=1cm and 3cm of v1] {$v_2$};

        \draw[opacity=0, visible on=<6->] (v2) -- (v4)
        node[midway, below=2.5mm, sloped, grn, opacity=1, fill=none] {$\leftarrow$};

        \draw[->, grn, visible on=<7->]             (v1) edge node {$a_2$} (v2);

        \draw[opacity=0, visible on=<7->] (v1) -- (v2)
        node[midway, below=2.5mm, sloped, grn, opacity=1, fill=none] {$\leftarrow$};
      \end{tikzpicture}
    \]
    \onslide<3->{The cycle $(a_1, a_4, a_3, a_2)$ starts and ends at $v_1$.}
  \end{definition}

\end{frame}

\subsection{Circuit Rank}
\begin{frame}{\secname}{\subsecname}

  \begin{definition}
    The \emph{\grn{circuit rank}} is the minimum number of arrows that must be
    removed to break all cycles.
    \[
      \begin{tikzpicture}[grph, visible on=<2->]
        \node[state] (v1) {$v_1$};
        \node[state] (v2) [below right=1cm and 3cm of v1] {$v_2$};
        \node[state] (v3) [above right=1cm and 3cm of v1] {$v_3$};
        \node[state] (v4) [right=3cm of v2] {$v_4$};
        \node[state] (v5) [right=3cm of v3] {$v_5$};
        \node[state] (v6) [right=3cm of v5] {$v_6$};

        \draw[->, visible on=<1-2>]             (v1) edge node {$a_1$} (v3);
        \draw[->]                               (v1) edge node {$a_2$} (v2);
        \draw[->]                               (v4) edge node {$a_3$} (v2);
        \draw[->]                               (v4) edge node {$a_4$} (v3);
        \draw[->]                               (v5) edge node {$a_5$} (v3);
        \draw[->, bend left, visible on=<1-3>]  (v5) edge node {$a_6$} (v6);
        \draw[->, bend left]                    (v6) edge node {$a_7$} (v5);

        \draw[->, red, visible on=<3-4>]             (v1) edge node {$a_1$} (v3);
        \draw[->, bend left, red, visible on=<4>]  (v5) edge node {$a_6$} (v6);
      \end{tikzpicture}
    \]
    \onslide<6->{The circuit rank of this digraph is \emph{\grn{two}}.}
  \end{definition}

\end{frame}


\section{Weighted Digraphs}
\subsection{Definition}
\begin{frame}{\secname}{\subsecname}

  \begin{definition}
    It is often useful to assign \emph{\grn{weights}} to arrows.
    \[
      \begin{tikzpicture}[grph, visible on=<2->]
        \node (wf)                           {\includegraphics[scale=.15]{wf.png}};
        \node (chase) [below right=2cm and 4cm of wf]    {\includegraphics[scale=.15]{chase.png}};
        \node (citi)  [above right=2cm and 4cm of chase] {\includegraphics[scale=.15]{citi.png}};

        \draw[->, bend right] (wf)    edge node {$t_1[\usd{13}]$} (chase);
        \draw[->, bend right] (chase) edge node {$t_2[\usd{9}]$} (wf);
        \draw[->, bend left]  (wf)    edge node {$t_3[\usd{137}]$} (citi);
        \draw[->, bend right] (chase) edge node {$t_4[\usd{3}]$} (citi);
        \draw[->, bend left]  (chase) edge node {$t_5[\usd{95}]$} (citi);
      \end{tikzpicture}
    \]
    \onslide<3->{We call these \emph{\grn{weighted digraphs}}.}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \begin{example}
    Weights can encode the data of a cycle.
    \[
      \begin{tikzpicture}[grph, visible on=<2->]
        \node[state] (v1) {$v_1$};
        \node[state] (v2) [below right=1cm and 3cm of v1] {$v_2$};
        \node[state] (v3) [above right=1cm and 3cm of v1] {$v_3$};
        \node[state] (v4) [right=3cm of v2] {$v_4$};
        \node[state] (v5) [right=3cm of v3] {$v_5$};
        \node[state] (v6) [right=3cm of v5] {$v_6$};

        \draw[->]             (v1) edge node {$a_1[1]$} (v3);
        \draw[->]             (v1) edge node {$a_2[-1]$} (v2);
        \draw[->]             (v4) edge node {$a_3[1]$} (v2);
        \draw[->]             (v4) edge node {$a_4[-1]$} (v3);
        \draw[->]             (v5) edge node {$a_5[0]$} (v3);
        \draw[->, bend left]  (v5) edge node {$a_6[0]$} (v6);
        \draw[->, bend left]  (v6) edge node {$a_7[0]$} (v5);

        \node[state, fill=grnbg, visible on=<3->] (v1) {$v_1$};
        \draw[->, grn, visible on=<3->]             (v1) edge node {$a_1[1]$} (v3);
        \node[state, fill=grnbg, visible on=<3->] (v3) [above right=1cm and 3cm of v1] {$v_3$};

        \draw[opacity=0, visible on=<3->] (v1) -- (v3)
        node[midway, above=2.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};

        \draw[->, grn, visible on=<4->]             (v4) edge node {$a_4[-1]$} (v3);
        \node[state, fill=grnbg, visible on=<4->] (v4) [right=3cm of v2] {$v_4$};

        \draw[opacity=0, visible on=<4->] (v3) -- (v4)
        node[midway, above=2.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};

        \draw[->, grn, visible on=<5->]             (v4) edge node {$a_3[1]$} (v2);
        \node[state, fill=grnbg, visible on=<5->] (v2) [below right=1cm and 3cm of v1] {$v_2$};

        \draw[opacity=0, visible on=<5->] (v2) -- (v4)
        node[midway, below=2.5mm, sloped, grn, opacity=1, fill=none] {$\leftarrow$};

        \draw[->, grn, visible on=<6->]             (v1) edge node {$a_2[-1]$} (v2);

        \draw[opacity=0, visible on=<6->] (v1) -- (v2)
        node[midway, below=2.5mm, sloped, grn, opacity=1, fill=none] {$\leftarrow$};
      \end{tikzpicture}
    \]
    \onslide<7->{Negative weights are perfectly acceptable!}
  \end{example}

\end{frame}


\subsection{Net Flow}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\myMoney}{
    \begin{array}{c}
      \begin{tikzpicture}[grph, visible on=<3->]
        \node (wf)                           {\includegraphics[scale=.15]{wf.png}};
        \node (chase) [below right=2cm and 4cm of wf]    {\includegraphics[scale=.15]{chase.png}};
        \node (citi)  [above right=2cm and 4cm of chase] {\includegraphics[scale=.15]{citi.png}};

        \draw[->, bend right] (wf)    edge node {$t_1[\usd{13}]$} (chase);
        \draw[->, bend right] (chase) edge node {$t_2[\usd{9}]$} (wf);
        \draw[->, bend left]  (wf)    edge node {$t_3[\usd{137}]$} (citi);
        \draw[->, bend right] (chase) edge node {$t_4[\usd{3}]$} (citi);
        \draw[->, bend left]  (chase) edge node {$t_5[\usd{95}]$} (citi);
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myWeights}{
    \begin{aligned}
      \varphi(\begin{array}{c}\includegraphics[height=12pt]{wf.png}\end{array})    &= (\usd{9})-(\usd{13}+\usd{137})        \\
                                                                                   &= \red{-\usd{141}}                      \\
      \varphi(\begin{array}{c}\includegraphics[height=12pt]{chase.png}\end{array}) &= (\usd{13})-(\usd{9}+\usd{3}+\usd{95}) \\
                                                                                   &= \red{-\usd{94}}                       \\
      \varphi(\begin{array}{c}\includegraphics[height=12pt]{citi.png}\end{array})  &= \usd{137}+\usd{95}+\usd{3}            \\
                                                                                   &= \grn{\usd{235}}
    \end{aligned}
  }

  \begin{definition}
    The \emph{\grn{net flow}} through a node $v$ is given by the formula
    \[
      \varphi(v) = (\text{weights in})-(\text{weights out})
    \]
    \onslide<2->{In our financial network, money ``flows through''
      institutions.}
    \begin{gather*}
      \begin{align*}
        \myMoney && \onslide<4->{\myWeights}
      \end{align*}
    \end{gather*}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\myG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]

        \node[state] (v1)                     {$v_1$};
        \node[state] (v2) [right=of v1]       {$v_2$};
        \node[state] (v3) [below right=2cm and 1.5cm of v1] {$v_3$};

        \draw[->, bend left]  (v1) edge node {$a_1[6]$}  (v2);
        \draw[->, bend left]  (v2) edge node {$a_2[-7]$} (v1);
        \draw[->, bend right] (v1) edge node {$a_3[3]$}  (v3);
        \draw[->, bend right] (v3) edge node {$a_4[-9]$} (v2);

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myWeights}{
    \begin{aligned}
      \varphi(v_1) &= (-7)-(3+6) \\
                   &= -16        \\
      \varphi(v_2) &= (6-9)-(-7) \\
                   &= 4          \\
      \varphi(v_3) &= (3)-(-9)   \\
                   &= 12
    \end{aligned}
  }
  \begin{example}
    Negative weights are accounted for in the net flow formula.
    \begin{align*}
      \myG && \myWeights
    \end{align*}
  \end{example}

\end{frame}


\section{Linear Algebra}
\subsection{Incidence Matrices}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\myG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]

        \node[state] (v1)                     {$v_1$};
        \node[state] (v2) [below=of v1]       {$v_2$};
        \node[state] (v3) [right=2cm of v1]   {$v_3$};
        \node[state] (v4) [right=2cm of v2]   {$v_4$};
        \node[state] (v5) [right=1.5cm of v4] {$v_5$};

        \draw[->] (v1) edge [bend right] node {$a_1$} (v2);
        \draw[->] (v2) edge [bend right] node {$a_2$} (v1);
        \draw[->] (v3) edge [bend right] node {$a_3$} (v4);
        \draw[->] (v3) edge [bend left]  node {$a_4$} (v4);
        \draw[->] (v4) edge              node {$a_6$} (v3);
        \draw[->] (v3) edge [bend left]  node {$a_5$} (v5);

        \node[state, fill=grnbg, visible on=<{2,4}>] (v1)                           {$v_1$};
        \node[state, fill=grnbg, visible on=<{2,4}>] (v2) [below=of v1]             {$v_2$};
        \node[state, fill=grnbg, visible on=<{6,8,10,12}>] (v3) [right=2cm of v1]   {$v_3$};
        \node[state, fill=grnbg, visible on=<{6,8,12}>] (v4) [right=2cm of v2]   {$v_4$};
        \node[state, fill=grnbg, visible on=<10>] (v5) [right=1.5cm of v4] {$v_5$};

        \draw[->, grn, visible on=<2>] (v1) edge [bend right]  node {$a_1$} (v2);
        \draw[->, grn, visible on=<4>] (v2) edge [bend right]  node {$a_2$} (v1);
        \draw[->, grn, visible on=<6>] (v3) edge [bend right]  node {$a_3$} (v4);
        \draw[->, grn, visible on=<8>] (v3) edge [bend left]   node {$a_4$} (v4);
        \draw[->, grn, visible on=<10>] (v3) edge [bend left]  node {$a_5$} (v5);
        \draw[->, grn, visible on=<12>] (v4) edge              node {$a_6$} (v3);

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myA}{
    \begin{bNiceMatrix}[
      , r
      , first-row
      , first-col
      , margin
      , code-before={
        \hlCol<2>[grn]{1}
        \hlCol<4>[grn]{2}
        \hlCol<6>[grn]{3}
        \hlCol<8>[grn]{4}
        \hlCol<10>[grn]{5}
        \hlCol<12>[grn]{6}
      }
      ]
      & a_1 & a_2 & a_3 & a_4 & a_5 & a_6 \\
      v_1 &  -1 &   1 &   0 &   0 &   0 &   0 \\
      v_2 &   1 &  -1 &   0 &   0 &   0 &   0 \\
      v_3 &   0 &   0 &  -1 &  -1 &  -1 &   1 \\
      v_4 &   0 &   0 &   1 &   1 &   0 &  -1 \\
      v_5 &   0 &   0 &   0 &   0 &   1 &   0 \\
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{incidence matrix}} encodes all of the data in a digraph.
    \begin{align*}
      \myA && \myG
    \end{align*}
    \onslide<14->{Each column gives information about an arrow.}
    \begin{align*}
      \onslide<15->{\text{\grn{\emph{source} node}}\rightsquigarrow -1} && \onslide<16->{\text{\grn{\emph{target} node}}\rightsquigarrow 1} && \onslide<17->{\text{\grn{other nodes}}\rightsquigarrow 0}
    \end{align*}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\myG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]

        \node[state, visible on=<3->] (v1)                                     {$v_1$};
        \node[state, visible on=<5->] (v2) [above right=1.5cm and 2.5cm of v1] {$v_2$};
        \node[state, visible on=<7->] (v3) [below right=1.5cm and 2.5cm of v1] {$v_3$};

        \draw[->, visible on=<11->]                (v1) edge node {$a_1$} (v2);
        \draw[->, visible on=<14->]                (v1) edge node {$a_2$} (v3);
        \draw[->, visible on=<17->]                (v2) edge node {$a_3$} (v3);
        \draw[->, bend right=40, visible on=<20->] (v2) edge node {$a_4$} (v1);

        \node[state, fill=grnbg, visible on=<{10,13,19}>] (v1)                                     {$v_1$};
        \node[state, fill=grnbg, visible on=<{10,16,19}>] (v2) [above right=1.5cm and 2.5cm of v1] {$v_2$};
        \node[state, fill=grnbg, visible on=<{13,16}>] (v3) [below right=1.5cm and 2.5cm of v1] {$v_3$};

        \draw[->, grn, visible on=<10>]                (v1) edge node {$a_1$} (v2);
        \draw[->, grn, visible on=<13>]                (v1) edge node {$a_2$} (v3);
        \draw[->, grn, visible on=<16>]                (v2) edge node {$a_3$} (v3);
        \draw[->, bend right=40, grn, visible on=<19>] (v2) edge node {$a_4$} (v1);

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myA}{
    \begin{bNiceMatrix}[
      , r
      , first-row
      , first-col
      , margin
      , code-before={
        \hlCol<9-10>[grn]{1}
        \hlCol<12-13>[grn]{2}
        \hlCol<15-16>[grn]{3}
        \hlCol<18-19>[grn]{4}
      }
      ]
      {}             & a_1 & a_2 & a_3 & a_4 \\
      \bxb<2-7>{v_1} &  -1 &  -1 &   0 &   1 \\
      \bxb<4-7>{v_2} &   1 &   0 &  -1 &  -1 \\
      \bxb<6-7>{v_3} &   0 &   1 &   1 &   0 \\
    \end{bNiceMatrix}
  }
  \begin{block}{Observation}
    Every digraph $G$ is defined by its incidence matrix.
    \begin{align*}
      A=\myA && \myG
    \end{align*}
    \onslide<21->{Geometric properties of $G$ translate into \emph{algebraic}
      properties of $A$.}
  \end{block}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \begin{block}{Observation}
    A digraph with $m$ nodes and $n$ arrows has an incidence matrix of the form
    \[
      A=
      \begin{bNiceMatrix}[first-row, first-col]
        & a_1    & a_2    & \Cdots & a_n    \\
        v_1    & \ast   & \ast   & \Cdots & \ast   \\
        v_2    & \ast   & \ast   & \Cdots & \ast   \\
        \Vdots & \Vdots & \Vdots & \Ddots & \Vdots \\
        v_m    & \ast   & \ast   & \Cdots & \ast
      \end{bNiceMatrix}
    \]
    \onslide<2->{The incidence matrix has $m$ rows and $n$ columns, so
      $A\in\mathbb{R}^{m\times n}$.}
  \end{block}

\end{frame}


\subsection{Weight Vectors}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\myG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]

        \node[state] (v1)                                     {$v_1$};
        \node[state] (v2) [above right=1.5cm and 3cm of v1] {$v_2$};
        \node[state] (v3) [below right=1.5cm and 3cm of v1] {$v_3$};

        \draw[->]                (v1) edge node {$a_1[-3]$} (v2);
        \draw[->]                (v1) edge node {$a_2[7]$}  (v3);
        \draw[->]                (v2) edge node {$a_3[14]$} (v3);
        \draw[->, bend right=45] (v2) edge node {$a_4[9]$}  (v1);

        \draw[->, visible on=<2>]                (v1) edge node[draw, rounded corners, fill=grnbg] {$a_1[-3]$} (v2);
        \draw[->, visible on=<4>]                (v1) edge node[draw, rounded corners, fill=grnbg] {$a_2[7]$}  (v3);
        \draw[->, visible on=<6>]                (v2) edge node[draw, rounded corners, fill=grnbg] {$a_3[14]$} (v3);
        \draw[->, bend right=45, visible on=<8>] (v2) edge node[draw, rounded corners, fill=grnbg] {$a_4[9]$}  (v1);

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myW}{
    \begin{bNiceMatrix}[
      , r
      , first-col
      , code-before={
        \hlRow<2>[grn]{1}
        \hlRow<4>[grn]{2}
        \hlRow<6>[grn]{3}
        \hlRow<8>[grn]{4}
      }
      ]
      a_1 & -3 \\
      a_2 &  7 \\
      a_3 & 14 \\
      a_4 &  9 \\
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{weight vector}} of a weighted digraph organizes the weights.
    \begin{align*}
      \myG && \bv[w]=\myW
    \end{align*}
    \onslide<10->{Note that $\bv[w]\in\mathbb{R}^4$ because this digraph has
      four arrows.}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\myG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph, visible on=<2->]

        \node[state] (v1)                                   {$v_1$};
        \node[state] (v2) [above right=1.5cm and 3cm of v1] {$v_2$};
        \node[state] (v3) [below right=1.5cm and 3cm of v1] {$v_3$};

        \draw[->]                (v1) edge node {$a_1[1]$}  (v2);
        \draw[->]                (v1) edge node {$a_2[0]$}  (v3);
        \draw[->]                (v2) edge node {$a_3[-1]$} (v3);
        \draw[->, bend right=45] (v2) edge node {$a_4[1]$}  (v1);

        \node[state, fill=grnbg, visible on=<3->] (v2) [above right=1.5cm and 3cm of v1] {$v_2$};
        \draw[->, grn, visible on=<3->]                (v2) edge node {$a_3[-1]$} (v3);
        \node[state, fill=grnbg, visible on=<3->] (v3) [below right=1.5cm and 3cm of v1] {$v_3$};

        \draw[opacity=0, visible on=<3->] (v2) -- (v3)
        node[midway, above=5mm, sloped, grn, opacity=1, fill=none] {$\leftarrow$};

        \draw[->, bend right=45, grn, visible on=<4->] (v2) edge node {$a_4[1]$}  (v1);
        \node[state, fill=grnbg, visible on=<4->] (v1)                                   {$v_1$};

        \draw[opacity=0, bend right=45, visible on=<4->] (v2) -- (v1)
        node[midway, above=10mm, sloped, grn, opacity=1, fill=none] {$\leftarrow$};

        \draw[->, grn, visible on=<5->]                (v1) edge node {$a_1[1]$}  (v2);

        \draw[opacity=0, visible on=<5->] (v1) -- (v2)
        node[midway, below=2.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myW}{
    \begin{bNiceMatrix}[
      , r
      , first-col
      ]
      a_1 &  1 \\
      a_2 &  0 \\
      a_3 & -1 \\
      a_4 &  1 \\
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{path vector}} of a path $p$ encodes the data of the path.
    \begin{align*}
      \myG && \onslide<6->{\bv_p=\myW}
    \end{align*}
    \onslide<7->{Here, the path vector of $p=(a_3, a_4, a_1)$ is
      $\bv_p=\nv{1 0 -1 1}$.}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\myG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph, visible on=<2->]

        \node[state] (v1)                                   {$v_1$};
        \node[state] (v2) [above right=1.5cm and 3cm of v1] {$v_2$};
        \node[state] (v3) [below right=1.5cm and 3cm of v1] {$v_3$};

        \draw[->]                (v1) edge node {$a_1$}  (v2);
        \draw[->]                (v1) edge node {$a_2$}  (v3);
        \draw[->]                (v2) edge node {$a_3$} (v3);
        \draw[->, bend right=45] (v2) edge node {$a_4$}  (v1);


        \node[state, fill=grnbg, visible on=<3->] (v1)                                   {$v_1$};
        \draw[->, grn, visible on=<3->]                (v1) edge node {$a_2$}  (v3);
        \node[state, fill=grnbg, visible on=<3->] (v3) [below right=1.5cm and 3cm of v1] {$v_3$};

        \draw[opacity=0, visible on=<3->] (v1) -- (v3)
        node[midway, below=2.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};

        \draw[->, grn, visible on=<4->]                (v2) edge node {$a_3$} (v3);
        \node[state, fill=grnbg, visible on=<4->] (v2) [above right=1.5cm and 3cm of v1] {$v_2$};

        \draw[opacity=0, visible on=<4->] (v3) -- (v2)
        node[midway, below=2.5mm, sloped, grn, opacity=1, fill=none] {$\rightarrow$};

        \draw[->, grn, visible on=<5->]                (v1) edge node {$a_1$}  (v2);

        \draw[opacity=0, visible on=<5->] (v1) -- (v2)
        node[midway, above=2.5mm, sloped, grn, opacity=1, fill=none] {$\leftarrow$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myW}{
    \begin{bNiceMatrix}[
      , r
      , first-col
      ]
      a_1 & -1 \\
      a_2 &  1 \\
      a_3 & -1 \\
      a_4 &  0 \\
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{cycle vector}} of a cycle $c$ is the associated path vector.
    \begin{align*}
      \myG && \onslide<6->{\bv_c=\myW}
    \end{align*}
    \onslide<7->{Here, the cycle vector of $c=(a_2, a_3, a_1)$ is
      $\bv_c=\nv{-1 1 -1 0}$.}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\myA}{
    \begin{bNiceMatrix}[first-row, first-col]
      & a_1    & a_2    & \Cdots & a_n    \\
      v_1    & \ast   & \ast   & \Cdots & \ast   \\
      v_2    & \ast   & \ast   & \Cdots & \ast   \\
      \Vdots & \Vdots & \Vdots & \Ddots & \Vdots \\
      v_m    & \ast   & \ast   & \Cdots & \ast   \\
    \end{bNiceMatrix}
  }
  \newcommand{\myW}{
    \begin{bNiceMatrix}[first-col]
      a_1    & \ast   \\
      a_2    & \ast   \\
      \Vdots & \Vdots \\
      a_n    & \ast   \\
    \end{bNiceMatrix}
  }
  \begin{block}{Observation}
    The incidence matrix and weight vector are of the form
    \begin{align*}
      A=\myA && \bv[w]=\myW
    \end{align*}
    \onslide<2->{So then $\mathbb{R}^n\xrightarrow{A}\mathbb{R}^m$ and
      $\bv[w]\in\mathbb{R}^n$. } \onslide<3->{The product $A\bv[w]$ makes
      sense!}
  \end{block}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\myG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]

        \node[state] (v1)                                   {$v_1$};
        \node[state] (v2) [above right=1.5cm and 3cm of v1] {$v_2$};
        \node[state] (v3) [below right=1.5cm and 3cm of v1] {$v_3$};

        \draw[->]                (v1) edge node {$a_1[-3]$} (v2);
        \draw[->]                (v1) edge node {$a_2[7]$}  (v3);
        \draw[->]                (v2) edge node {$a_3[14]$} (v3);
        \draw[->, bend right=45] (v2) edge node {$a_4[9]$}  (v1);

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myA}{
    \begin{bNiceMatrix}[r, light-syntax]
      -1 -1  0  1 ;
      1   0 -1 -1 ;
      0   1  1  0 ;
    \end{bNiceMatrix}
  }
  \newcommand{\myW}{
    \begin{bNiceMatrix}[r, light-syntax]
      -3 ;
      7 ;
      14 ;
      9 ;
    \end{bNiceMatrix}
  }
  \newcommand{\myAw}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlRow<2->[red]{1}
        \hlRow<3->[red]{2}
        \hlRow<4->[red]{3}
      }
      , code-after = {
        \begin{tikzpicture}[red]
          \draw<2->[<-, thick, shorten <=0.5mm]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ++(5mm, 1.5mm)
          node[above] {$\varphi(v_1)$};
          \draw<3->[<-, thick, shorten <=0.5mm]
          ($ (row-2-|col-2)!0.5!(row-3-|col-2) $) -- ++(5mm, 0mm)
          node[right] {$\varphi(v_2)$};
          \draw<4->[<-, thick, shorten <=0.5mm]
          ($ (row-3-|col-2)!0.5!(row-4-|col-2) $) -| ++(5mm, -1.5mm)
          node[below] {$\varphi(v_3)$};
        \end{tikzpicture}
      }
      ]
      5\\ -26\\ 21
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following weighted digraph.
    \begin{align*}
      \myG && \overset{A}{\myA}\overset{\bv[w]}{\myW}=\myAw
    \end{align*}
    \onslide<5->{The matrix-vector product $A\bv[w]$ calculates net flow!}
  \end{example}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\myA}{
    \begin{tikzcd}[ampersand replacement=\&]
      \mathbb{R}^n\ar[r, "A"] \& \mathbb{R}^m
    \end{tikzcd}
  }
  \begin{theorem}
    The incidence matrix $A$ and weight vector $\bv[w]$ of a weighted digraph
    satisfy
    \begin{align*}
      \myA && \bv[w]\in\mathbb{R}^n
    \end{align*}
    The $i$th coordinate of $A\bv[w]$ is the net flow through the $i$th node.
  \end{theorem}

\end{frame}


\end{document}