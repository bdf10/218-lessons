\documentclass[]{bmr}

\usetikzlibrary{
  , decorations
  , decorations.markings
  , decorations.pathmorphing
  , decorations.pathreplacing
  , decorations.text
}

\title{The Gram-Schmidt Algorithm}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Motivation}
\subsection{Visual Description}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MutuallyOrthogonal}{
    \begin{tikzpicture}[anchor=base, baseline, inner sep=0]
      \node (w) {$\color<13->{red}\Set{\bv[w]_1,\bv[w]_2}$};

      \draw[<-, thick, red, overlay, visible on=<13->] (w.south) |- ++(-3mm, -2mm)
      node[left] {\scriptsize\textnormal{mutually orthogonal}};
    \end{tikzpicture}
  }
  \newcommand{\Orthonormal}{
    \begin{tikzpicture}[anchor=base, baseline, inner sep=0]
      \node (q) {$\color<14->{grn}\Set{\bv[q]_1,\bv[q]_2}$};

      \draw[<-, thick, grn, overlay, visible on=<14->] (w.south) |- ++(3mm, -2mm)
      node[right] {\scriptsize\textnormal{orthonormal}};
    \end{tikzpicture}
  }
  \begin{example}
    Can a basis $\Set{\bv_1, \bv_2}$ of $V$ be converted to an
    \emph{\grn{orthonormal basis}} $\Set{\bv[q]_1, \bv[q]_2}$?
    \[
      \begin{tikzpicture}[
        , scale=1
        , line join=round
        , line cap=round
        , ultra thick
        , ->
        , transform shape
        , rotate=15
        ]
        \coordinate (O) at (0, 0);

        \coordinate (v1) at (4, 0);
        \coordinate (v2) at (3, 2);

        \coordinate (w1) at (v1);
        \coordinate (w2) at ($ (v2)-(O)!(v2)!(v1) $);

        \coordinate (q1) at ($ (O)!1cm!(w1) $);
        \coordinate (q2) at ($ (O)!1cm!(w2) $);

        \draw[blu, visible on=<2-3>]
        (O) -- (v1) node[midway, sloped, below] {$\scriptstyle \bv_1$};

        \draw[blu, visible on=<3->]
        (O) -- (v2) node[midway, sloped, above] {$\scriptstyle \bv_2$};

        \pgfmathsetmacro{\Perp}{0.25}
        \draw[visible on=<7->]
        (O) rectangle (\Perp, \Perp);

        \draw[visible on=<5-6>]
        ($ (O)!(v2)!(v1) $) rectangle ++(\Perp, \Perp);

        \draw[red, visible on=<5-6>]
        ($ (O)!(v2)!(v1) $) -- (v2)
        node[midway, right] {$\scriptstyle\bv[w]_2=\onslide<6->{\bv_2-\proj_{\bv[w]_1}(\bv_2)}$};

        \draw[red, visible on=<4->]
        (O) -- (w1) node[right] {$\scriptstyle\bv[w]_1=\bv_1$};

        \draw[red, visible on=<7->]
        (O) -- (w2) node[midway, left] {$\scriptstyle\bv[w]_2=\bv_2-\proj_{\bv[w]_1}(\bv_2)$};

        \draw[grn, visible on=<8->]
        (O) -- (q1) node[below] {$\scriptstyle\bv[q]_1=\onslide<9->{\sfrac{\bv[w]_1}{\norm{\bv[w]_1}}}$};

        \draw[grn, visible on=<10->]
        (O) -- (q2) node[midway, left] {$\scriptstyle\bv[q]_2=\onslide<11->{\sfrac{\bv[w]_2}{\norm{\bv[w]_2}}}$};
      \end{tikzpicture}
    \]
    \onslide<12->{The Gram-Schmidt algorithm produces bases \MutuallyOrthogonal\
      and \Orthonormal.}
  \end{example}

\end{frame}

\section{Projections and Components}
\subsection{Vector Projections}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Figure}{
    \begin{array}{c}
      \begin{tikzpicture}[ultra thick, line join=round, line cap=round, rotate=15, transform shape]

        \coordinate (O) at (0, 0);
        \coordinate (v1) at (1, 0);
        \coordinate (p) at (1.75, 0);
        \coordinate (v) at ($ (p)+(0,1.25) $);

        \draw[visible on=<5->] ($ (p)+(0,2mm) $) -| ++(2mm, -2mm);
        \draw[red, dashed, visible on=<5->] (p) -- (v);

        \draw[grn, very thick, <->, visible on=<3->] ($ -0.5*(v1) $) -- ($ 2.5*(v1) $)
        node[right, overlay] {$\scriptstyle V$};

        \draw[blu, ->, visible on=<2->] (O) -- (v1)
        node[midway, below, sloped] {$\scriptstyle \bv_1$};

        \draw[->, red, visible on=<6->] (O) -- (p)
        node[below, sloped] {$\scriptstyle \proj_{\bv_1}(\bv)$};

        \draw[->, blu, visible on=<4->] (O) -- (v)
        node[midway, above, sloped] {$\scriptstyle\bv$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Projection}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture, visible on=<7->]

        \node {
          \(
          \displaystyle
          \proj_{\bv_1}(\bv)
          = \bv_1(\subnode{norm}{\color<8->{blu}\bv_1^\intercal\bv_1})^{-1}\subnode{inner}{\color<9->{red}\bv_1^\intercal\bv}
          \onslide<10->{= \frac{\inner{\bv_1, \bv}}{\inner{\bv_1, \bv_1}}\bv_1}
          \)
        };

        \draw[<-, thick, blu, overlay, visible on=<8->] (norm.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle \bv_1^\intercal\bv_1=\inner{\bv_1, \bv_1}$};

        \draw[<-, thick, red, overlay, visible on=<9->] (inner.south) |- ++(-1mm, -6mm)
        node[left] {$\scriptstyle \bv_1^\intercal\bv=\inner{\bv_1, \bv}$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    Suppose $V$ is one-dimensional so $V=\Span\Set{\bv_1}$.
    \begin{align*}
      \Figure && \Projection
    \end{align*}
    \onslide<11->{So, for $\bv_1=\nv{3 1 5}$ and $\bv=\nv{-7 9 1}$ the projection is}
    \[
      \onslide<11->{
        \proj_{\bv_1}(\bv)
        = \frac{
          \inner{\nv[small]{3 1 5}, \nv[small]{-7 9 1}}
        }{
          \inner{\nv[small]{3 1 5}, \nv[small]{3 1 5}}
        }
        \nvc{3; 1; 5}
        =
        \nvc{{-\sfrac{3}{5}}; {-\sfrac{1}{5}}; {-1}}
      }
    \]
  \end{definition}

\end{frame}


\subsection{Vector Components}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{component}} of $\bv$ in the direction of $\bv_1$ is
    \[
      \comp_{\bv_1}(\bv)=\frac{\inner{\bv_1, \bv}}{\norm{\bv_1}}
    \]
    \pause So, for $\bv_1=\nv{3 1 5}$ and $\bv=\nv{-7 9 1}$ we have
    \[
      \comp_{\bv_1}(\bv)
      = \frac{\inner{\nv[small]{3 1 5}, \nv[small]{-7 9 1}}}{\norm{\nv[small]{3 1 5}}}
      = \frac{-7}{\sqrt{35}}
    \]
    \pause Note that $\comp_{\bv_1}(\bv)$ is a \emph{scalar}.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\ProjComp}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<2->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\proj_{\bv_1}(\bv)=\comp_{\bv_1}(\bv)\cdot\dfrac{\bv_1}{\norm{\bv_1}}$};
        % \node[above, grn] at (text.north) {\scriptsize Spanning Axiom};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\CompLen}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<3->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\norm{\proj_{\bv_1}(\bv)}=\abs{\comp_{\bv_1}(\bv)}$};
        % \node[above, blu] at (text.north) {\scriptsize Linear Independence Axiom};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\PositiveComponent}{
    \begin{tikzpicture}[
      , rotate=-10
      , transform shape
      , line join=round
      , line cap=round
      , ultra thick
      , visible on=<5->
      ]

      \pgfmathsetmacro{\myc}{0.25}
      \coordinate (O) at (0, 0);
      \coordinate (e1) at (1, 0);
      \coordinate (e2) at (0, 1);
      \coordinate (e1c) at ($ (O)!\myc cm!(e1) $);
      \coordinate (e2c) at ($ (O)!\myc cm!(e2) $);
      \coordinate (v) at (5/2, 0);
      \coordinate (F) at (3/2, 2);
      \coordinate (p) at ($ (O)!(F)!(v) $);

      \draw[<->, grn, visible on=<6->] ($ -.25*(v) $) -- ($ 1.25 *(v) $);

      \draw[visible on=<7->] ($ (p)+(e2c) $) -- ($ (p)+(e1c)+(e2c) $) -- ($ (p)+(e1c) $);
      \draw[dashed, visible on=<7->] (F) -- (p);

      \draw[->, red] (O) -- (F) node[midway, above, sloped] {$\scriptstyle\bv$};
      \draw[->, blu] (O) -- (v) node[below] {$\scriptstyle\bv_1$};

      \draw[red, visible on=<8->] (O) -- (p);

      \draw [
      , red
      , decorate
      , decoration={brace, mirror, amplitude=8pt}
      , visible on=<9->
      ]
      (O) -- (p)
      node[midway, below=2mm] {$\scriptstyle\comp_{\bv_1}(\bv)>0$};

    \end{tikzpicture}
  }
  \newcommand{\NegativeComponent}{
    \begin{tikzpicture}[
      , rotate=10
      , transform shape
      , line join=round
      , line cap=round
      , ultra thick
      , visible on=<10->
      ]

      \pgfmathsetmacro{\myc}{0.25}
      \coordinate (O) at (0, 0);
      \coordinate (e1) at (-1, 0);
      \coordinate (e2) at (0, 1);
      \coordinate (e1c) at ($ (O)!\myc cm!(e1) $);
      \coordinate (e2c) at ($ (O)!\myc cm!(e2) $);
      \coordinate (v) at (1, 0);
      \coordinate (F) at (-3/2, 2);
      \coordinate (p) at ($ (O)!(F)!(v) $);

      \draw[<->, grn, visible on=<11->] ($ -2.25*(v) $) -- ($ 1.5*(v) $);

      \draw[visible on=<12->] ($ (p)+(e2c) $) -- ($ (p)+(e1c)+(e2c) $) -- ($ (p)+(e1c) $);
      \draw[dashed, visible on=<12->] (F) -- (p);

      \draw[->, red] (O) -- (F) node[midway, above, sloped] {$\scriptstyle\bv$};
      \draw[->, blu] (O) -- (v) node[below] {$\scriptstyle\bv_1$};

      \draw[red, visible on=<13->] (O) -- (p);

      \draw [
      , red
      , decorate
      , decoration={brace, amplitude=8pt}
      , visible on=<14->
      ]
      (O) -- (p)
      node[midway, below=2mm] {$\scriptstyle\comp_{\bv_1}(\bv)<0$};

    \end{tikzpicture}
  }

  \begin{theorem}
    Vector components are related to projections via the formulas
    \begin{align*}
      \ProjComp && \CompLen
    \end{align*}
    \onslide<4->{Consequently, we have two visual archetypes.}
    \begin{align*}
      \PositiveComponent && \NegativeComponent
    \end{align*}
  \end{theorem}

\end{frame}


\section{Gram-Schmidt Algorithm}
\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MutuallyOrthogonal}{
    \begin{tikzpicture}[anchor=base, baseline, inner sep=0]
      \node (w) {$\color<11->{blu}\Set{\bv[w]_1,\dotsc,\bv[w]_d}$};

      \draw[<-, thick, blu, overlay, visible on=<11->] (w.south) |- ++(3mm, -2mm)
      node[right] {\scriptsize\textnormal{mutually orthogonal}};
    \end{tikzpicture}
  }
  \newcommand{\Orthonormal}{
    \begin{tikzpicture}[anchor=base, baseline, inner sep=0]
      \node (q) {$\color<12->{grn}\Set{\bv[q]_1,\dotsc,\bv[q]_d}$};

      \draw[<-, thick, grn, overlay, visible on=<12->] (w.south) |- ++(3mm, -2mm)
      node[right] {\scriptsize\textnormal{orthonormal}};
    \end{tikzpicture}
  }
  \begin{theorem}[The Gram-Schmidt Algorithm]
    Let $\Set{\bv_1,\dotsc,\bv_d}$ be a basis of
    $V\subset\mathbb{R}^n$. \onslide<2->{Define new vectors}
    \[
      \begin{NiceArray}{rclrcl}%[cell-space-limits = 1pt]
        {} \onslide<2->{\bv[w]_1} &\onslide<2->{=}   & \onslide<2->{\bv_1}                                                                & \onslide<6->{\bv[q]_1} &\onslide<6->{=}     & \onslide<6->{\sfrac{\bv[w]_1}{\norm{\bv[w]_1}}} \\ \\
        {} \onslide<3->{\bv[w]_2} &\onslide<3->{=}   & \onslide<3->{\bv_2-\proj_{\bv[w]_1}(\bv_2)}                                        & \onslide<7->{\bv[q]_2} &\onslide<7->{=}     & \onslide<7->{\sfrac{\bv[w]_2}{\norm{\bv[w]_2}}} \\ \\
        {} \onslide<4->{\bv[w]_3} &\onslide<4->{=}   & \onslide<4->{\bv_3-\proj_{\bv[w]_1}(\bv_3) - \proj_{\bv[w]_2}(\bv_3)}              & \onslide<8->{\bv[q]_3} &\onslide<8->{=}     & \onslide<8->{\sfrac{\bv[w]_3}{\norm{\bv[w]_3}}} \\
        {}                        &\only<5->{\Vdots} &                                                                                    &                        &\only<9->{\Vdots}   &                                                 \\
        {} \onslide<5->{\bv[w]_d} &\onslide<5->{=}   & \onslide<5->{\bv_d-\proj_{\bv[w]_1}(\bv_d) - \dotsb - \proj_{\bv[w]_{d-1}}(\bv_d)} & \onslide<9->{\bv[q]_d} &\onslide<9->{=}     & \onslide<9->{\sfrac{\bv[w]_d}{\norm{\bv[w]_d}}}
      \end{NiceArray}
    \]
    \onslide<10->{Then \MutuallyOrthogonal and \Orthonormal are bases of $V$.}
  \end{theorem}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\VectorVa}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle\bv_1$};
        \end{tikzpicture}
      }
      ]
      0 & -1 & 1 & -1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorVb}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle\bv_2$};
        \end{tikzpicture}
      }
      ]
      1 & -2 & 2 & -2
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorVc}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle\bv_3$};
        \end{tikzpicture}
      }
      ]
      -10 & 14 & -13 & 9
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\ProjWaVb}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle\proj_{\bv[w]_1}(\bv_2)$};
        \end{tikzpicture}
      }
      ]
      0 & -2 & 2 & -2
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\ProjWaVc}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle\proj_{\bv[w]_1}(\bv_3)$};
        \end{tikzpicture}
      }
      ]
      0 & 12 & -12 & 12
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\ProjWbVc}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle\proj_{\bv[w]_2}(\bv_3)$};
        \end{tikzpicture}
      }
      ]
      -10 & 0 & 0 & 0
    \end{bNiceMatrix}^\intercal
  }
  \begin{example}
    Consider the basis $V=\Span\Set{\bv_1, \bv_2, \bv_3}$ where
    \begin{align*}
      \bv_1 = \nv[small]{0 -1 1 -1} && \bv_2 = \nv[small]{1 -2 2 -2} && \bv_3 = \nv[small]{-10 14 -13 9}
    \end{align*}
    \onslide<2->{Applying Gram-Schmidt to this basis gives}
    \begin{align*}
      \onslide<2->{\bv[w]_1} &\onslide<2->{=}  \onslide<2->{\VectorVa<3>}                                      & \onslide<13->{\bv[q]_1} &\onslide<13->{=} \onslide<14->{{\scriptstyle\frac{1}{\sqrt{3}}}\nv[small]{0 -1 1 -1}}  \\[2mm]
      \onslide<4->{\bv[w]_2} &\onslide<4->{=}  \onslide<4->{\VectorVb<5-7>-\ProjWaVb<6-7>}                     & \onslide<15->{\bv[q]_2} &\onslide<15->{=} \onslide<16->{\nv[small]{1 0 0 0}}                                    \\
                             &\onslide<7->{=}  \onslide<7->{\nv[small]{1 0 0 0}}                               &                         &                                                                                       \\[2mm]
      \onslide<8->{\bv[w]_3} &\onslide<8->{=}  \onslide<8->{\VectorVc<9-12>-\ProjWaVc<10-12>-\ProjWbVc<11-12>} & \onslide<17->{\bv[q]_3} &\onslide<17->{=} \onslide<18->{{\scriptstyle\frac{1}{\sqrt{14}}}\nv[small]{0 2 -1 -3}} \\
                             &\onslide<12->{=} \onslide<12->{\nv[small]{0 2 -1 -3}}
    \end{align*}
  \end{example}

\end{frame}


\section{Calculating $A=QR$}
\subsection{Algorithm}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    We can use the Gram-Schmidt algorithm to factor $A$ as
    \[
      \begin{tikzpicture}[remember picture]
        \node {$
          A
          =
          \subnode{MatQ}{\color<2->{blu}Q}
          \subnode{MatR}{\color<3->{red}R}
          $};

        % \draw[<-, grn, thick, overlay, visible on=<2->] (MatA.south) |- ++(-3mm, -2.5mm)
        % node[left] {\scriptsize\textnormal{$m\times n$ with rank $r$}};

        \draw[<-, blu, thick, overlay, visible on=<2->] (MatQ.south) |- ++(1.5mm, -8mm)
        node[right] {\stackanchor{\scriptsize\textnormal{columns of $Q$ obtained by applying}}{\scriptsize\textnormal{Gram-Schmidt to pivot columns of $A$}}};

        \draw[<-, red, thick, overlay, visible on=<3->] (MatR.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize\textnormal{define $R=Q^\intercal A$}};

      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {}  0 &  1 & -10 \\
      {} -1 & -2 &  14 \\
      {}  1 &  2 & -13 \\
      {} -1 & -2 &   9
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixQ}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle Q$};
        \end{tikzpicture}
      }
      ]
      {}  0                   & 1 &  0                    \\
      {} -\sfrac{1}{\sqrt{3}} & 0 &  \sfrac{2}{\sqrt{14}} \\
      {}  \sfrac{1}{\sqrt{3}} & 0 & -\sfrac{1}{\sqrt{14}} \\
      {} -\sfrac{1}{\sqrt{3}} & 0 & -\sfrac{3}{\sqrt{14}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixQT}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle Q^\intercal$};
        \end{tikzpicture}
      }
      ]
      0 & -\sfrac{1}{\sqrt{3}} & \sfrac{1}{\sqrt{3}}   & -\sfrac{1}{\sqrt{3}}  \\
      1 & 0                    & 0                     & 0                     \\
      0 & \sfrac{2}{\sqrt{14}} & -\sfrac{1}{\sqrt{14}} & -\sfrac{3}{\sqrt{14}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle R$};
        \end{tikzpicture}
      }
      ]
      \onslide####1{\sqrt{3}} & \onslide####1{2\,\sqrt{3}} & \onslide####1{-12\,\sqrt{3}} \\
      \onslide####1{0}        & \onslide####1{1}           & \onslide####1{-10}           \\
      \onslide####1{0}        & \onslide####1{0}           & \onslide####1{\sqrt{14}}
    \end{bNiceMatrix}
  }

  \begin{example}
    Previously, we applied Gram-Schmidt to the columns of $A$ to obtain $Q$.
    \begin{align*}
      \MatrixA<2-> && \MatrixQ<3->
    \end{align*}
    \onslide<4->{We then define $R=Q^\intercal A$.}
    \[
      \onslide<4->{\MatrixR<7->=\MatrixQT<5->\MatrixA<6->}
    \]
    \onslide<8->{This gives $A=QR$.}
  \end{example}

\end{frame}

\end{document}