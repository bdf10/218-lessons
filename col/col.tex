\documentclass[]{bmr}

\title{Column Spaces}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Column Spaces}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixAv}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , code-before = {
        \hlPortion<6->[blu, th]{1}{1}{4}{3}
        \hlCol<6->[grn, th]{4}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[blu, visible on=<6->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
          \draw[grn, visible on=<6->] ($ (row-1-|col-4)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \bv$};
          \draw[grn, visible on=<8->] ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-3mm, -2.5mm)
          node[left] {$\scriptstyle \bv\in\Col(A)$};
        \end{tikzpicture}
      }
      ]
      {}  1 & -6 & -8 &  1 \\
      {} -3 &  1 &  9 & -1 \\
      {} -2 & -1 &  2 & 83 \\
      {}  0 & -2 & -2 &  8
    \end{bNiceArray}
  }
  \newcommand{\MatrixAvR}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , code-before = {
        \hlEntry<7->[blu, th]{1}{1}
        \hlEntry<7->[blu, th]{2}{2}
        \hlEntry<7->[blu, th]{3}{3}
        \hlCol<7->[grn, th]{4}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, visible on=<7->]
          \draw[grn] ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-3mm, -2.5mm)
          node[left] {\scriptsize no pivot!};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 0 & -89 \\
      0 & 1 & 0 &  29 \\
      0 & 0 & 1 & -33 \\
      0 & 0 & 0 &   0
    \end{bNiceArray}
  }
  \newcommand{\MatrixAw}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , code-before = {
        \hlPortion<10->[blu, th]{1}{1}{4}{3}
        \hlCol<10-11>[grn, th]{4}
        \hlCol<12>[red, th]{4}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[blu, visible on=<10->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
          \draw[alt=<12->{red}{grn}, visible on=<10->] ($ (row-1-|col-4)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \bv[w]$};
          \draw[red, visible on=<12->] ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-3mm, -2.5mm)
          node[left] {$\scriptstyle \bv[w]\notin\Col(A)$};
        \end{tikzpicture}
      }
      ]
      {}  1 & -6 & -8 &  14 \\
      {} -3 &  1 &  9 &   2 \\
      {} -2 & -1 &  2 &   4 \\
      {}  0 & -2 & -2 & -43
    \end{bNiceArray}
  }
  \newcommand{\MatrixAwR}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , code-before = {
        \hlEntry<11->[blu, th]{1}{1}
        \hlEntry<11->[blu, th]{2}{2}
        \hlEntry<11->[blu, th]{3}{3}
        \hlEntry<11->[red, th]{4}{4}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, visible on=<11->]
          \draw[red] ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-3mm, -2.5mm)
          node[left] {\scriptsize pivot!};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 0 & 0 \\
      0 & 1 & 0 & 0 \\
      0 & 0 & 1 & 0 \\
      0 & 0 & 0 & 1 
    \end{bNiceArray}
  }
  \begin{definition}
    The \emph{\grn{column space}} of an $m\times n$ matrix $A$ is
    \[
      \begin{tikzpicture}[thick, remember picture]
        \node {$\Col(A) = \Set{\subnode{all}{\color<2->{blu}\bv\in\mathbb{R}^m}\subnode{st}{\color<3->{red}\given}\subnode{null}{\color<4->{grn}\rank(A)=\rank[A\mid\bv]}}$};
        \draw[<-, overlay, blu, visible on=<2->] (all.south) |- ++(-5mm, -2mm) node[left] {\scriptsize all $\bv\in\mathbb{R}^m$};
        \draw[<-, overlay, red, visible on=<3->] (st.north) |- ++(5mm, 2mm) node[right] {\scriptsize that satisfy};
        \draw[<-, overlay, grn, visible on=<4->] (null.south) |- ++(5mm, -2mm) node[right] {\scriptsize this equation};
      \end{tikzpicture}
    \]
    \onslide<5->{We write $\bv\in\Col(A)$ to indicate that
      $\rank(A)=\rank[A\mid\bv]$.}
    \begin{align*}
      \onslide<5->{\rref\MatrixAv=\MatrixAvR} && \onslide<9->{\rref\MatrixAw=\MatrixAwR}
    \end{align*}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixAv}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , code-before = {
        \hlPortion<2->[blu, th]{1}{1}{4}{3}
        \hlCol<2-3>[grn, th]{4}
        \hlCol<4->[red, th]{4}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[blu, visible on=<2->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
          \draw[alt=<4->{red}{grn}, visible on=<2->] ($ (row-1-|col-4)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \bv$};
          \draw[red, visible on=<4->] ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-3mm, -2.5mm)
          node[left] {$\scriptstyle \bv\notin\Col(A)$};
        \end{tikzpicture}
      }
      ]
      {} -5 & 1 &  7 &  2 \\
      {}  1 & 0 & -2 &  4 \\
      {} -3 & 1 &  3 & -2 \\
      {} -4 & 4 & -4 &  0
    \end{bNiceArray}
  }
  \newcommand{\MatrixAvR}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , code-before = {
        \hlEntry<3->[blu, th]{1}{1}
        \hlEntry<3->[blu, th]{2}{2}
        \hlEntry<3->[red, th]{3}{4}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[red, visible on=<3->] ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-3mm, -2.5mm)
          node[left] {\scriptsize inconsistent!};
        \end{tikzpicture}
      }
      ]
      1 & 0 & -2 & 0 \\
      0 & 1 & -3 & 0 \\
      0 & 0 &  0 & 1 \\
      0 & 0 &  0 & 0
    \end{bNiceArray}
  }
  \newcommand{\MatrixAw}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , code-before = {
        \hlPortion<2->[blu, th]{1}{1}{4}{3}
        \hlCol<2->[grn, th]{4}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[blu, visible on=<2->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
          \draw[grn, visible on=<2->] ($ (row-1-|col-4)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \bv[w]$};
          \draw[grn, visible on=<6->] ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-3mm, -2.5mm)
          node[left] {$\scriptstyle \bv[w]\in\Col(A)$};
        \end{tikzpicture}
      }
      ]
      {} -5 & 1 &  7 & -1 \\
      {}  1 & 0 & -2 &  0 \\
      {} -3 & 1 &  3 & -1 \\
      {} -4 & 4 & -4 & -4
    \end{bNiceArray}
  }
  \newcommand{\MatrixAwR}{
    \begin{bNiceArray}{rrr|r}[ 
      , small
      , code-before = {
        \hlEntry<5->[blu, th]{1}{1}
        \hlEntry<5->[blu, th]{2}{2}
        \hlCol<5->[grn, th]{4}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[grn, visible on=<5->] ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-3mm, -2.5mm)
          node[left] {\scriptsize consistent!};
        \end{tikzpicture}
      }
      ]
      1 & 0 & -2 &  0 \\
      0 & 1 & -3 & -1 \\
      0 & 0 &  0 &  0 \\
      0 & 0 &  0 &  0
    \end{bNiceArray}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {} -5 & 1 &  7 \\
      {}  1 & 0 & -2 \\
      {} -3 & 1 &  3 \\
      {} -4 & 4 & -4
    \end{bNiceMatrix}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[r, small]
      1\\ -3\\ 2
    \end{bNiceMatrix}
  }
  \newcommand{\VectorY}{
    \begin{bNiceMatrix}[r, small]
      -2\\ 5\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorAX}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<8->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[grn, visible on=<8->] ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 3mm)
          node[left] {$\scriptstyle A\bv_1\in\Col(A)$};
        \end{tikzpicture}
      }
      ]
      6\\ -3\\ 0\\ -24
    \end{bNiceMatrix}
  }
  \newcommand{\VectorAY}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<9->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[grn, visible on=<9->] ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 3mm)
          node[left] {$\scriptstyle A\bv_2\in\Col(A)$};
        \end{tikzpicture}
      }
      ]
      22\\ -4\\ 14\\ 24
    \end{bNiceMatrix}
  }
  \begin{example}
    Whether or not $\bv\in\Col(A)$ hinges on whether or not $A\bv[x]=\bv$ is
    \emph{consistent}.
    \begin{align*}
      \rref\MatrixAv=\MatrixAvR && \rref\MatrixAw=\MatrixAwR
    \end{align*}
    \onslide<7->{We can easily produce examples of $\bv\in\Col(A)$ by
      multiplying $A$ by vectors.}
    \begin{align*}
      \onslide<7->{\overset{A}{\MatrixA}\overset{\bv[v]_1}{\VectorX}=\VectorAX} && \onslide<7->{\overset{A}{\MatrixA}\overset{\bv_2}{\VectorY}=\VectorAY}
    \end{align*}
    \onslide<10->{The vectors in $\Col(A)$ are the linear combinations of the
      columns of $A$!}
  \end{example}

\end{frame}


\section{Properties}
\subsection{Subset Language}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat<4->}
      , code-after = {
        \begin{tikzpicture}[<-, blu, thick, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \Col(A)\subset\mathbb{R}^5$};
        \end{tikzpicture}
      }
      ]
      \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat<5->[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<5->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-7) $) |- ++(-3mm, 3mm)
          node[left] {$\scriptstyle \Col(B)\subset\mathbb{R}^3$};
        \end{tikzpicture}
      }
      ]
      \ast & \ast & \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast & \ast & \ast
    \end{bNiceMatrix}
  }
  \begin{definition}
    If $A$ is $m\times n$, then $\rank(A)=\rank[A\mid\bv]$ only makes sense if
    $\bv\in\mathbb{R}^m$.
    \[
      \onslide<2->{\mathbb{R}^n\xrightarrow{A}\mathbb{R}^m\supset\Col(A)}
    \]
    \onslide<2->{Writing $\Col(A)\subset\mathbb{R}^m$ indicates that vectors in
      $\Col(A)$ have $m$ coordinates.}
    \begin{align*}
      \onslide<3->{A = \MatrixA} && \onslide<3->{B = \MatrixB}
    \end{align*}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlCol<2->[grn, th]{2}}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-5-|col-2)!0.5!(row-5-|col-3) $) |- ++(3mm, -3mm)
          node[right] {$\scriptstyle \Col_2=-2\cdot\Col_1$};
        \end{tikzpicture}
      }
      ]
      {} -3 &  6 \\
      {}  2 & -4 \\
      {} -1 &  2 \\
      {} -2 &  4
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixANaked}{
    \begin{bNiceMatrix}[r, small]
      {} -3 &  6 \\
      {}  2 & -4 \\
      {} -1 &  2 \\
      {} -2 &  4
    \end{bNiceMatrix}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[r, small]
      c_1\\ c_2
    \end{bNiceMatrix}
  }
  \newcommand{\ColAa}{
    \begin{bNiceMatrix}[r, small]
      {} -3 \\
      {}  2 \\
      {} -1 \\
      {} -2
    \end{bNiceMatrix}
  }
  \newcommand{\ColAb}{
    \begin{bNiceMatrix}[r, small]
      {}  6 \\
      {} -4 \\
      {}  2 \\
      {}  4
    \end{bNiceMatrix}
  }
  \begin{example}
    This matrix $A$ is $4\times 2$, so $\Col(A)\subset\mathbb{R}^4$.
    \begin{align*}
      A=\MatrixA && \onslide<3->{\MatrixANaked\VectorX = c_1\cdot\ColAa+c_2\cdot\ColAb = c\cdot\ColAa}
    \end{align*}
    \onslide<4->{The vectors in $\Col(A)$ are the scalar multiples of
      $\nv{-3 2 -1 -2}$.}
    \[
      \begin{tikzpicture}[ultra thick, line join=round, line cap=round, scale=0.75, visible on=<2->]
        \coordinate (v) at (3, 1);
        \draw[<->, grn, visible on=<6->] ($ -1.5*(v) $) -- ($ 1.5*(v)  $) node[right, overlay] {$\Col(A)$};
        \draw[->, blu, visible on=<5->] (0, 0) -- (v) node[midway, sloped, above] {$\nv[small]{-3 2 -1 -2}$};
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\subsection{Net Flow in Digraphs}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\DigraphG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]
        \node[state] (v1)                                      {$v_1$};
        \node[state] (v2) [above right=1cm and 2.5cm of v1]    {$v_2$};
        \node[state] (v3) [below right=0.75cm and 2.5cm of v1] {$v_3$};

        \draw[->, bend left]     (v1) edge node {$\scriptstyle a_1[7]$}  (v2);
        \draw[->, bend left]     (v2) edge node {$\scriptstyle a_2[-3]$} (v1);
        \draw[->, bend right]    (v1) edge node {$\scriptstyle a_3[1]$}  (v3);
        \draw[->, bend right=66] (v3) edge node {$\scriptstyle a_4[9]$}  (v2);
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<2->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} -1 &  1 & -1 &  0 \\
      {}  1 & -1 &  0 &  1 \\
      {}  0 &  0 &  1 & -1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorW}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<3->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, blu, thick, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \bv[w]$};
        \end{tikzpicture}
      }
      ]
      7\\ -3\\ 1\\ 9
    \end{bNiceMatrix}
  }
  \newcommand{\VectorAW}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<4->[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, red, thick, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(3mm, 2.5mm)
          node[right] {\scriptsize net flow};
          \draw[visible on=<5->] ($ (row-4-|col-1)!0.5!(row-4-|col-2) $) |- ++(3mm, -2.5mm)
          node[right] {\scriptsize $A\bv[w]\in\Col(A)$};
        \end{tikzpicture}
      }
      ]
      -11\\ 19\\ -8
    \end{bNiceMatrix}
  }
  \begin{block}{Recall}
    Net flow through nodes is calculated with matrix-vector products.
    \begin{align*}
      \DigraphG && \MatrixA\VectorW = \VectorAW
    \end{align*}
    \onslide<6->{The vectors in $\Col(A)$ are the possible net flow
      configurations.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\DigraphG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]
        \node[state] (v1)                                      {$v_1$};
        \node[state] (v2) [above right=1cm and 2.5cm of v1]    {$v_2$};
        \node[state] (v3) [below right=0.75cm and 2.5cm of v1] {$v_3$};

        \draw[->, bend left]     (v1) edge node {$\scriptstyle a_1$}  (v2);
        \draw[->, bend left]     (v2) edge node {$\scriptstyle a_2$} (v1);
        \draw[->, bend right]    (v1) edge node {$\scriptstyle a_3$}  (v3);
        \draw[->, bend right=66] (v3) edge node {$\scriptstyle a_4$}  (v2);
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\MatrixAv}{
    \begin{bNiceArray}{rrrr|r}[
      , small
      , code-before = {
        \hlPortion<2->[blu, th]{1}{1}{3}{4}
        \hlCol<2-3>[grn, th]{5}
        \hlCol<4->[red, th]{5}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[blu, visible on=<2->] ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
          \draw[alt=<4->{red}{grn}, visible on=<2->] ($ (row-1-|col-5)!0.5!(row-1-|col-6) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \bv$};
          \draw[red, visible on=<4->] ($ (row-4-|col-5)!0.5!(row-4-|col-6) $) |- ++(-3mm, -2.5mm)
          node[left] {\scriptsize\textnormal{impossible net flow!}};
        \end{tikzpicture}
      }
      ]
      {} -1 &  1 & -1 &  0 &  7 \\
      {}  1 & -1 &  0 &  1 & -2 \\
      {}  0 &  0 &  1 & -1 &  9
    \end{bNiceArray}
  }
  \newcommand{\MatrixAvR}{
    \begin{bNiceArray}{rrrr|r}[
      , small
      , code-before = {
        \hlEntry<3->[blu, th]{1}{1}
        \hlEntry<3->[blu, th]{2}{3}
        \hlEntry<3->[red, th]{3}{5}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[red, visible on=<3->] ($ (row-4-|col-5)!0.5!(row-4-|col-6) $) |- ++(-3mm, -2.5mm)
          node[left] {\scriptsize\textnormal{inconsistent!}};
        \end{tikzpicture}
      }
      ]
      1 & -1 & 0 &  1 & 0 \\
      0 &  0 & 1 & -1 & 0 \\
      0 &  0 & 0 &  0 & 1
    \end{bNiceArray}
  }
  \begin{theorem}
    Let $A$ be the incidence matrix of a digraph.
    \begin{align*}
      \DigraphG && \rref\MatrixAv=\MatrixAvR
    \end{align*}
    Then $\bv\in\Col(A)$ means there is a weight configuration $\bv[w]$ with
    $A\bv[w]=\bv$.
  \end{theorem}

\end{frame}


\section{Spans}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{span}} of $\bv_1,\bv_2,\dotsc,\bv_n\in\mathbb{R}^m$ is
    \[
      \Span\Set{\bv_1,\bv_2,\dotsc,\bv_n}=\textnormal{all linear combinations of }\Set{\bv_1,\bv_2,\dotsc,\bv_n}
    \]
    So, $\bv\in\Span\Set{\bv_1,\bv_2,\dotsc,\bv_n}$ means
    $\bv=c_1\cdot\bv_1+c_2\cdot\bv_2+\dotsb+c_n\cdot\bv_n$.

  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\VectorVa}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      ]
      5\\ 2\\ 1
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVb}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      ]
      2\\ 9\\ 3
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVc}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      ]
      4\\ 4\\ 1
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      ]
      31\\ 8\\ 2
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVar}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1[blu]}
      ]
      5& 2& 1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorVbr}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1[blu]}
      ]
      2& 9& 3
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorVcr}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1[blu]}
      ]
      4& 4& 1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorVr}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1[grn]}
      ]
      31 & 8 & 2
    \end{bNiceMatrix}^\intercal
  }
  \begin{example}
    Consider the equation
    \[
      3\cdot\VectorVa<3->-2\cdot\VectorVb<3->+5\cdot\VectorVc<3->=\VectorV<2->
    \]
    Here,
    $\VectorVr<2->\in\Span\Set{\VectorVar<3->, \VectorVbr<3->, \VectorVcr<3->}$.
  \end{example}

\end{frame}


\subsection{Column Spaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\VectorVT}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before={\hlMat####1[grn, th]}
      ]
      2 & -3 & 9 & 1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorVaT}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before={\hlMat####1[blu, th]}
      ]
      1 & 0 & 2 & 0
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorVbT}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before={\hlMat####1[blu, th]}
      ]
      0 & 3 & 1 & 2
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorVcT}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before={\hlMat####1[blu, th]}
      ]
      0 & 0 & 2 & 1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before={\hlMat####1[grn, th]}
      ]
      2\\ -3\\ 9\\ 1
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVa}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before={\hlMat####1[blu, th]}
      ]
      1\\ 0\\ 2\\ 0
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVb}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before={\hlMat####1[blu, th]}
      ]
      0\\ 3\\ 1\\ 2
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVc}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before={\hlMat####1[blu, th]}
      ]
      0\\ 0\\ 2\\ 1
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before={
        \hlCol####1[blu, th]{1}
        \hlCol####1[blu, th]{2}
        \hlCol####1[blu, th]{3}
      }
      ]
      1 & 0 & 0 \\
      0 & 3 & 0 \\
      2 & 1 & 2 \\
      0 & 2 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      2\\ -1\\ 3
    \end{bNiceMatrix}
  }
  \begin{block}{Observation}
    These equations show that
    $\VectorVT<3->\in\Span\Set{\VectorVaT<2->, \VectorVbT<2->, \VectorVcT<2->}$.
    \begin{align*}
      2\cdot\VectorVa<2->-\VectorVb<2->+3\cdot\VectorVc<2->=\VectorV<3-> && \leftrightsquigarrow && \MatrixA<2->\VectorX=\VectorV<3->
    \end{align*}
    \onslide<4->{Linear combinations are matrix-vector products, so}
    \[
      \onslide<4->{\Span\Set*{\VectorVa<5->, \VectorVb<5->, \VectorVc<5->} =
        \Col\MatrixA<5->}
    \]
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}\bv_1 & \bv_2 & \Cdots & \bv_n\end{bNiceMatrix}
  }
  \newcommand{\MatrixAv}{
    \begin{bNiceArray}{rrrr|r}
      \bv_1 & \bv_2 & \Cdots & \bv_n & \bv
    \end{bNiceArray}
  }
  \begin{theorem}
    Suppose $\bv_1,\bv_2,\dotsc,\bv_n\in\mathbb{R}^m$. Then
    \[
      \Span\Set{\bv_1,\bv_2,\dotsc,\bv_n} = \Col\MatrixA
    \]
    Consequently, $\bv\in\Span\Set{\bv_1,\bv_2,\dotsc,\bv_n}$ if and only if
    \[
      \rank\MatrixA=\rank\MatrixAv
    \]
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixAv}{
    \begin{bNiceArray}{rr|r}[
      , small
      , code-before = {
        \hlCol<6->[blu, th]{1}
        \hlCol<6->[blu, th]{2}
        \hlCol<5->[grn, th]{3}
      }
      ]
      1 &  3 & -13 \\
      4 &  1 &   3 \\
      3 & -5 &  31
    \end{bNiceArray}
  }
  \newcommand{\MatrixAvR}{
    \begin{bNiceArray}{rr|r}[
      , small
      , code-before = {\hlEntry<2->[grn, th]{3}{3}}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, visible on=<2->]
          \draw[grn] ($ (row-4-|col-3)!0.5!(row-4-|col-4) $) |- ++(-3mm, -2.5mm)
          node[left] {\scriptsize consistent!};
        \end{tikzpicture}
      }
      ]
      1 & 0 &  2 \\
      0 & 1 & -5 \\
      0 & 0 &  0
    \end{bNiceArray}
  }
  \newcommand{\MatrixAw}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , code-before = {
        \hlCol<8->[blu, th]{1}
        \hlCol<8->[blu, th]{2}
        \hlCol<8->[blu, th]{3}
        \hlCol<7->[grn, th]{4}
      }
      ]
      {}  2 &  6 &  12 & -22 \\
      {}  5 & 21 &  36 &   4 \\
      {} -1 & -2 &  -5 &   0 \\
      {} -2 & -4 & -10 & -12
    \end{bNiceArray}
  }
  \newcommand{\MatrixAwR}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , code-before = {\hlEntry<3->[red, th]{3}{4}}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, visible on=<3->]
          \draw[red] ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-3mm, -2.5mm)
          node[left] {\scriptsize inconsistent!};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 3 & 0 \\
      0 & 1 & 1 & 0 \\
      0 & 0 & 0 & 1 \\
      0 & 0 & 0 & 0
    \end{bNiceArray}
  }
  \newcommand<>{\Vector}[2][blu, th]{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####3[####1]}
      ]
      ####2
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following calculations.
    \begin{align*}
      \rref\MatrixAv = \MatrixAvR && \rref\MatrixAw = \MatrixAwR
    \end{align*}
    \onslide<4->{These calculations show that}
    \begin{align*}
      \onslide<4->{\Vector<5->[grn, th]{-13\\ 3\\ 31}\in\Span\Set*{\Vector<6->{1\\ 4\\ 3}, \Vector<6->{3\\ 1\\ -5}}} &&
      \onslide<4->{\Vector<7->[grn, th]{-22\\ 4\\ 0\\ -12}\notin\Span\Set*{\Vector<8->{2\\ 5\\ -1\\ -2}, \Vector<8->{6\\ 21\\ -2\\ -4}, \Vector<8->{12\\ 36\\ -5\\ -10}}}
    \end{align*}
  \end{example}

\end{frame}


\end{document}