\documentclass[]{bmr}

\title{Cholesky Factorizations}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{$LDL^\intercal$-Factorizations}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    An \emph{\grn{$LDL^\intercal$-factorization}} is an equation of the form
    \[
      \begin{tikzpicture}[remember picture]
        \node {$
          \subnode{MatS}{\color<5->{grn}S}
          =
          \subnode{MatL}{\color<2->{blu}L}
          \subnode{MatD}{\color<3->{red}D}
          \subnode{MatLT}{L^\intercal}
          $};

        \node[below=2mm of MatL] {$\phantom{n}$};

        \draw[<-, grn, thick, overlay, visible on=<5->] (MatS.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize\textnormal{$n\times n$ \emph{real-symmetric}}};

        \draw[<-, blu, thick, overlay, visible on=<2->] (MatL.south) |- ++(1.5mm, -6mm)
        node[right] {\scriptsize\textnormal{$n\times n$ \emph{real lower-unitriangular} ($1$'s on diagonal)}};

        \draw[<-, red, thick, overlay, visible on=<3->] (MatD.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize\textnormal{$n\times n$ real-diagonal}};
      \end{tikzpicture}
    \]
    \onslide<4->{The calculation
      $S^\intercal=(LDL^\intercal)^\intercal=(L^\intercal)^\intercal D^\intercal
      L^\intercal=LDL^\intercal=S$ shows that\ldots}
  \end{definition}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat<2->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$S$};
        \end{tikzpicture}
      }
      ]
      {}  3 &  -9 &   6 \\
      {} -9 &  26 & -23 \\
      {}  6 & -23 &  -8
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixL}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlLower<3->[red]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<3->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$L$};
        \end{tikzpicture}
      }
      ]
      {}  1 & 0 & 0 \\
      {} -3 & 1 & 0 \\
      {}  2 & 5 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlDiag<4->[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<4->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$D$};
        \end{tikzpicture}
      }
      ]
      {} 3 &  0 & 0 \\
      {} 0 & -1 & 0 \\
      {} 0 &  0 & 5
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixLT}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlUpper<5->[red]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<5->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$L^\intercal$};
        \end{tikzpicture}
      }
      ]
      {} 1 & -3 & 2 \\
      {} 0 &  1 & 5 \\
      {} 0 &  0 & 1
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the factorization
    \[
      \MatrixS
      =
      \MatrixL
      \MatrixD
      \MatrixLT
    \]
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The quadratic form defined by $S=LDL^\intercal$ is
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          q(\bv[x]) =
          \onslide<2->{\inner{\bv[x], LDL^\intercal\bv[x]} =}
          \onslide<3->{\inner{\subnode{l}{\textcolor<4->{red}{L^\intercal\bv[x]}}, D\subnode{r}{\textcolor<4->{red}{L^\intercal\bv[x]}}} =}
          \onslide<5->{\inner{\bv[y], D\bv[y]} =}
          \onslide<6->{d_1\,y_1^2+\dotsb+d_n\,y_n^2}
          \)};

        \draw[<-, thick, red, overlay, visible on=<4->] (l.south) |- ++(-3mm, -2.5mm)
        node[left] {$\bv[y]$};

        \draw[<-, thick, red, overlay, visible on=<4->] (r.south) |- ++(-3mm, -2.5mm)
        node[left] {$\bv[y]$};
      \end{tikzpicture}
    \]
    \onslide<7->{The diagonal entries of $D$ control the definiteness of
      $q(\bv[x])$.}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      % , margin
      , r
      % , code-before = {
      % \hlMat<2->
      % }
      %   , code-after = {
      %   \begin{tikzpicture}
      %     \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %     ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
      %     node[left] {$S$};
      %   \end{tikzpicture}
      % }
      ]
      {}  3 &  -9 &    6 \\
      {} -9 &  20 &  -53 \\
      {}  6 & -53 & -158
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixL}{
    \begin{bNiceMatrix}[
      % , margin
      , r
      % , code-before = {
      % \hlLower<2->[red]
      % }
      %   , code-after = {
      %   \begin{tikzpicture}
      %     \draw[<-, red, thick, shorten <=0.5mm, visible on=<2->]
      %     ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
      %     node[left] {$L$};
      %   \end{tikzpicture}
      % }
      ]
      {}  1 & 0 & 0 \\
      {} -3 & 1 & 0 \\
      {}  2 & 5 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<3>[grn]{1}{1}
        \hlEntry<5>[grn]{2}{2}
        \hlEntry<7>[grn]{3}{3}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, grn, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$D$};
      % \end{tikzpicture}
      % }
      ]
      {} 3 &  0 & 0 \\
      {} 0 & -7 & 0 \\
      {} 0 &  0 & 5
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixLT}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlRow<4>[red]{1}
        \hlRow<6>[red]{2}
        \hlRow<8>[red]{3}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, red, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$L^\intercal$};
      % \end{tikzpicture}
      % }
      ]
      {} 1 & -3 & 2 \\
      {} 0 &  1 & 5 \\
      {} 0 &  0 & 1
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the factorization
    \[
      \overset{S}{\MatrixS}
      =
      \overset{L}{\MatrixL}
      \overset{D}{\MatrixD}
      \overset{L^\intercal}{\MatrixLT}
    \]
    \onslide<2->{The quadratic form $q(\bv[x])=\inner{\bv[x], S\bv[x]}$ is given
      by}
    \[
      \begin{tikzpicture}[remember picture, visible on=<2->]
        \node {\(
          q(\bv[x])
          =
          \subnode{d1}{\textcolor<3->{grn}{3}}\,(\subnode{y1}{\textcolor<4->{red}{x_1-3\,x_2+2\,x_3}})^2
          \subnode{d2}{\textcolor<5->{grn}{-7}}\,(\subnode{y2}{\textcolor<6->{red}{x_2+5\,x_3}})^2
          +
          \subnode{d3}{\textcolor<7->{grn}{5}}\,\subnode{y3}{\textcolor<8->{red}{x_3}}^2
          \)};

        \draw[<-, grn, thick, overlay, visible on=<3->] (d1.south) |- ++(-3mm, -2.5mm)
        node[left] {$d_1$};

        \draw[<-, grn, thick, overlay, visible on=<5->] (d2.south) |- ++(-3mm, -2.5mm)
        node[left] {$d_2$};

        \draw[<-, grn, thick, overlay, visible on=<7->] (d3.south) |- ++(-3mm, -2.5mm)
        node[left] {$d_3$};

        \draw[<-, red, thick, overlay, visible on=<4->] (y1.south) |- ++(-3mm, -2.5mm)
        node[left] {$y_1$};

        \draw[<-, red, thick, overlay, visible on=<6->] (y2.south) |- ++(-3mm, -2.5mm)
        node[left] {$y_2$};

        \draw[<-, red, thick, overlay, visible on=<8->] (y3.south) |- ++(3mm, -2.5mm)
        node[right] {$y_3$};
      \end{tikzpicture}
    \]
    \onslide<9->{This quadratic form is \emph{indefinite} ($d_1,d_3>0$ and
      $d_2<0$).}
  \end{example}


\end{frame}



\section{$LDL^\intercal$ Algorithm}
\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      , r
      ]
      {}   4 & -12 &   16 \\
      {} -12 &  31 &  -83 \\
      {}  16 & -83 & -175
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixL}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlCol<2>{1}
        \hlCol<3>{2}
        \hlCol<4>{3}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      {}  1 & 0 & 0 \\
      {} -3 & 1 & 0 \\
      {}  4 & 7 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixLb}{
    \begin{bNiceMatrix}[
      % , margin
      , r
      % , code-before = {
      % \hlCol<2>{1}
      % \hlCol<3>{2}
      % \hlCol<4>{3}
      % }
      %   , code-after = {
      %   \begin{tikzpicture}
      %     \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %     ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %     node[left] {$A$};
      %   \end{tikzpicture}
      % }
      ]
      {}  1 & 0 & 0 \\
      {} -3 & 1 & 0 \\
      {}  4 & 7 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlRow<2>{1}
        \hlRow<3>{2}
        \hlRow<4>{3}
        \hlDiag<7->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2>]
          ($ (row-1-|col-4)!0.5!(row-2-|col-4) $) -| ++(3mm, 5mm)
          node[above] {$4\cdot\begin{bmatrix} 1 & -3 & 4\end{bmatrix}$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<3>]
          ($ (row-2-|col-4)!0.5!(row-3-|col-4) $) -| ++(3mm, 9mm)
          node[above] {$-5\cdot\begin{bmatrix} 0 & 1 & 7\end{bmatrix}$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<4>]
          ($ (row-3-|col-4)!0.5!(row-4-|col-4) $) -| ++(3mm, 13mm)
          node[above] {$6\cdot\begin{bmatrix} 0 & 0 & 1\end{bmatrix}$};
        \end{tikzpicture}
      }
      ]
      {} 4 & -12 &  16 \\
      {} 0 &  -5 & -35 \\
      {} 0 &   0 &   6
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlDiag<7->
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      {} 4 &  0 & 0 \\
      {} 0 & -5 & 0 \\
      {} 0 &  0 & 6
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixLT}{
    \begin{bNiceMatrix}[
      % , margin
      , r
      % , code-before = {
      % \hlDiag<2->
      % }
      %   , code-after = {
      %   \begin{tikzpicture}
      %     \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %     ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %     node[left] {$A$};
      %   \end{tikzpicture}
      % }
      ]
      1 & -3 & 4 \\
      0 &  1 & 7 \\
      0 &  0 & 1
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following $S=LU$ factorization
    \[
      \overset{S}{\MatrixS}
      =
      \overset{L}{\MatrixL}
      \overset{U}{\MatrixU}
    \]
    \onslide<5->{Then $U=DL^\intercal$ where $D=\operatorname{diag}(4, -5, 6)$,
      which gives}
    \[
      \onslide<6->{
        \overset{S}{\MatrixS}
        =
        \overset{L}{\MatrixLb}
        \overset{D}{\MatrixD}
        \overset{L^\intercal}{\MatrixLT}
      }
    \]
    \onslide<8->{Note that $S$ is \emph{indefinite} ($d_1,d_3>0$ and $d_2<0$).}
  \end{example}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat<3->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<3->]
          ($ (row-3-|col-1)!0.5!(row-3-|col-3) $) |- ++(2mm, -2.5mm)
          node[right] {indefinite!};
        \end{tikzpicture}
      }
      ]
      0 & a \\
      a & b
    \end{bNiceMatrix}
  }
  \newcommand{\Definiteness}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture]
        \node {\(
          \subnode{l1}{\textcolor<2->{red}{\lambda_1\cdot\lambda_2}} = \det(S) = -a^2 < 0
          \)};

        \draw[<-, red, thick, overlay, visible on=<2->] (l1.south) |- ++(3mm, -2.5mm)
        node[right] {one is $<0$ and one is $>0$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Suppose $a\neq0$ and consider the real-symmetric $S$ below.
    \begin{align*}
      S = \MatrixS && \Definiteness
    \end{align*}
    \onslide<4->{Note that the first step to row-reduce $S$ is a row-swap.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algorithm}
    The following procedure calculates $S=LDL^\intercal$ if the factorization
    exists.
    \begin{description}
    \item[Step 1] Perform $PS=LU$ (striving only for upper-triangular $U$).
    \item[Step 2] Necessary row-swaps imply $S$ is \emph{indefinite} and
      $S\neq LDL^\intercal$.
    \item[Step 3] With $S=LU$, define $D=\operatorname{diag}(U)$ to obtain $S=LDL^\intercal$.
    \end{description}
    We can identify the definiteness of $S$ without calculating eigenvalues!
  \end{block}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      , r
      ]
      {}  -2 &   4 &  -10 \\
      {}   4 & -11 &    8 \\
      {} -10 &   8 & -102
    \end{bNiceMatrix}
  }
  \newcommand{\StepA}{
    \begin{NiceArray}[small]{rcrcr}
      \bv[r]_2 &+& 2\cdot\bv[r]_1 &\to& \bv[r]_2 \\
      \bv[r]_3 &-& 5\cdot\bv[r]_1 &\to& \bv[r]_3
    \end{NiceArray}
  }
  \newcommand{\MatrixSa}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} -2 &   4 & -10 \\
      {}  0 &  -3 & -12 \\
      {}  0 & -12 & -52
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlDiag<2->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {negative definite!};
        \end{tikzpicture}
      }
      ]
      {} -2 &  4 & -10 \\
      {}  0 & -3 & -12 \\
      {}  0 &  0 &  -4
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixL}{
    \begin{bNiceMatrix}[
      , r
      ]
      {}  1 & 0 & 0 \\
      {} -2 & 1 & 0 \\
      {}  5 & 4 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlDiag<4->
      }
      ]
      {} -2 &  0 &  0 \\
      {}  0 & -3 &  0 \\
      {}  0 &  0 & -4
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixLT}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} 1 & -2 & 5 \\
      {} 0 &  1 & 4 \\
      {} 0 &  0 & 1
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following row-reductions.
    \begin{gather*}
      \overset{S}{\MatrixS}
      \xrightarrow{\StepA}\MatrixSa
      \xrightarrow{\bv[r]_3-4\cdot\bv[r]_2\to\bv[r]_3}
      \overset{U}{\MatrixU}
    \end{gather*}
    \onslide<3->{This gives the $S=LDL^\intercal$ factorization}
    \[
      \onslide<3->{
        \overset{S}{\MatrixS}
        =
        \overset{L}{\MatrixL}
        \overset{D}{\MatrixD}
        \overset{L^\intercal}{\MatrixLT}
      }
    \]
  \end{example}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      , r
      ]
      {}  1 &  3 & 11 \\
      {}  3 &  9 & 39 \\
      {} 11 & 39 & 60
    \end{bNiceMatrix}
  }
  \newcommand{\StepA}{
    \begin{NiceArray}[small]{rcrcr}
      \bv[r]_2 &-&  3\cdot\bv[r]_1 &\to& \bv[r]_2 \\
      \bv[r]_3 &-& 11\cdot\bv[r]_1 &\to& \bv[r]_3
    \end{NiceArray}
  }
  \newcommand{\MatrixSa}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlPortion<2->[red]{2}{2}{3}{2}
      }
      ]
      {} 1 & 3 &  11 \\
      {} 0 & 0 &   6 \\
      {} 0 & 6 & -61
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following row-reductions.
    \[
      \overset{S}{\MatrixS}
      \xrightarrow{\StepA}\MatrixSa
    \]
    \onslide<3->{A row-swap is required, so $S$ is \emph{indefinite}.}
  \end{example}

\end{frame}


\section{Cholesky Factorizations}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{\grn{Cholesky factorization}} is an equation of the form
    \[
      \begin{tikzpicture}[remember picture]
        \node {$
          \subnode{S}{\color<3->{blu}S}
          =
          R^\intercal
          \subnode{R}{\color<2->{red}R}
          $};

        % \node[below=2mm of MatL] {$\phantom{n}$};

        \draw[<-, red, thick, overlay, visible on=<2->] (R.south) |- ++(3mm, -2.5mm)
        node[right] {\stackanchor{$n\times n$ upper-triangular}{(nonnegative diagonal)}};

        \draw[<-, blu, thick, overlay, visible on=<3->] (S.south) |- ++(-3mm, -2.5mm)
        node[left] {$n\times n$ positive semidefinite};
      \end{tikzpicture}
    \]
    \onslide<4->{The quadratic form defined by $S$ is}
    \[
      \onslide<4->{q(\bv[x])
        = \inner{\bv[x], S\bv[x]}
        = \inner{\bv[x], R^\intercal R\bv[x]}
        = \inner{R\bv[x], R\bv[x]}
        = \norm{R\bv[x]}^2
      }
    \]
    \onslide<5->{The matrix $S$ is positive definite if and only if $R$ is
      nonsingular.}
  \end{definition}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlUpper<2->[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$R$};
        \end{tikzpicture}
      }
      ]
      {} 3 & -2 & 5 \\
      {} 0 &  1 & 8 \\
      {} 0 &  0 & 6
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixRT}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlLower<2->[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$R^\intercal$};
        \end{tikzpicture}
      }
      ]
      {}  3 & 0 & 0 \\
      {} -2 & 1 & 0 \\
      {}  5 & 8 & 6
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat<2->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$S$};
        \end{tikzpicture}
      }
      ]
      {}  9 & -6 &  15 \\
      {} -6 &  5 &  -2 \\
      {} 15 & -2 & 125
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following Cholesky factorization.
    \[
      \MatrixS
      =
      \MatrixRT
      \MatrixR
    \]
    \onslide<3->{The quadratic form defined by $S$ is}
    \[
      \onslide<3->{q(\bv[x]) = (3\,x_1-2\,x_2+5\,x_3)^2+(x_2+8\,x_3)^2+(6\,x_3)^2}
    \]
    \onslide<4->{Note that $S$ is \emph{\grn{positive definite}} because $R$ is
      nonsingular.}
  \end{example}

\end{frame}


\section{Cholesky Algorithm}
\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algorithm}
    Suppose that $S$ is real-symmetric.
    \begin{description}
    \item[Step 1] Perform $PS=LU$ (striving only for upper-triangular $U$).
      \begin{description}
      \item[\textcolor{red}{not positive semidefinite}] if a row-swap is required.
      \item[\textcolor{red}{not positive semidefinite}] if $U$ has a negative pivot.
      \end{description}
    \item[Step 2] Factor $S$ as $S=LDL^\intercal$.
    \item[Step 3] Define $R=\sqrt{D} L^\intercal$.
    \end{description}
    This gives a Cholesky factorization $S=R^\intercal R$.
  \end{block}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      , r
      ]
      {}  9 & 3 & 20 \\
      {}  3 & 2 &  3 \\
      {} 20 & 3 & 58
    \end{bNiceMatrix}
  }
  \newcommand{\StepA}{
    \begin{NiceArray}[small]{rcrcr}
      \bv[r]_2 &-& (\sfrac{1}{3})\cdot\bv[r]_1 &\to& \bv[r]_2 \\
      \bv[r]_3 &-& (\sfrac{20}{9})\cdot\bv[r]_1 &\to& \bv[r]_3
    \end{NiceArray}
  }
  \newcommand{\MatrixSa}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} 9 &             3 &            20 \\
      {} 0 &             1 & -\sfrac{11}{3} \\
      {} 0 & -\sfrac{11}{3} & \sfrac{122}{9}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlDiag<2->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {positive-definite!};
        \end{tikzpicture}
      }
      ]
      {} 9 & 3 &            20 \\
      {} 0 & 1 & -\sfrac{11}{3} \\
      {} 0 & 0 &  \sfrac{1}{9}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixSqrtD}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlDiag<4->
      }
      ]
      {} 3 & 0 &           0 \\
      {} 0 & 1 &           0 \\
      {} 0 & 0 & \sfrac{1}{3}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixLT}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} 1 & \sfrac{1}{3} &  \sfrac{20}{9} \\
      {} 0 &           1 & -\sfrac{11}{3} \\
      {} 0 &           0 &             1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} 3 & 1 &  \sfrac{20}{3} \\
      {} 0 & 1 & -\sfrac{11}{3} \\
      {} 0 & 0 &   \sfrac{1}{3}
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following row-reductions.
    \begin{gather*}
      \overset{S}{\MatrixS}
      \xrightarrow{\StepA}\MatrixSa
      \xrightarrow{\bv[r]_3+(\sfrac{11}{3})\cdot\bv[r]_2\to\bv[r]_3}\overset{U}{\MatrixU}
    \end{gather*}
    \onslide<3->{This gives the Cholesky factorization $S=R^\intercal R$ where}
    \[
      \onslide<3->{\overset{R}{\MatrixR}
        =
        \overset{\sqrt{D}}{\MatrixSqrtD}
        \overset{L^\intercal}{\MatrixLT}
      }
    \]

  \end{example}

\end{frame}





\end{document}
