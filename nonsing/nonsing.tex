\documentclass[]{bmr}

\title{Nonsingular Matrices}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Matrix Inverses}
\subsection{Definition and Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{NiceArray}{rcrcrcr}
      {}    x_1 &+& 3\,x_2 &+& 9\,x_3 &=&  2 \\
      {}2 \,x_1 &+& 8\,x_2 &-& 8\,x_3 &=&  7 \\
      {}4 \,x_1 &+& 7\,x_2 &-& 5\,x_3 &=&  9
    \end{NiceArray}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, light-syntax]
      1 3 9; 2 8 -8; 4 7 -5
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{bNiceMatrix}[r, light-syntax]
      {x_1}; {x_2}; {x_3}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceMatrix}[r, light-syntax]
      2; 7; 9
    \end{bNiceMatrix}
  }
  \begin{block}{Question}
    Every system can be written as a matrix equation $A\bv[x]=\bv[b]$.
    \begin{gather*}
      \begin{align*}
        \System && A=\MatrixA && \bv[x]=\MatrixX && \bv[b]=\MatrixB
      \end{align*}
    \end{gather*}
    \onslide<2->{Why not just divide $A\bv[x]=\bv[b]$ by $A$ to obtain
      $\bv[x]=\sfrac{\bv[b]}{A}$?}
  \end{block}

  \begin{block}{Answer}<3->
    We don't have a theory for matrix division!
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\LeftInverse}{
    \begin{NiceArray}{c}
      LA=I_n \\
      \textnormal{\scriptsize{\grn{Left Inverse $L$}}}
    \end{NiceArray}
  }
  \newcommand{\RightInverse}{
    \begin{NiceArray}{c}
      AR=I_m \\
      \textnormal{\scriptsize{\blu{Right Inverse $R$}}}
    \end{NiceArray}
  }
  \begin{definition}
    An $m\times n$ matrix $A$ might have \emph{\grn{left inverses}} or
    \emph{\blu{right inverses}}.
    \begin{align*}
      \LeftInverse && \RightInverse
    \end{align*}
    \pause If $A$ has both a left inverse $L$ and a right inverse $R$, then
    \[
      L = LI_m = L(AR) = (LA)R = I_nR = R
    \]
    \pause Here, $A$ has \emph{\grn{exactly one two-sided inverse}} denoted by
    $A^{-1}$ and
    \[
      m
      = \trace(I_m)
      = \trace(AA^{-1})
      = \trace(A^{-1}A)
      = \trace(I_n)
      = n
    \]
    \pause This $A^{-1}$ \emph{might} only exist if \emph{\grn{$A$ is square}}.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}[2]{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlMat<####1>[grn]}
      , code-after={
        \begin{tikzpicture}[thick, ->]
          \node[above=2mm] at (1-2) {$\scriptstyle A$};
          \node[below right=3mm and 2mm, grn, visible on=<####2>]
          (A) at (3-2) {$\scriptstyle A$ is \emph{invertible}};
          \draw[grn, shorten >=2mm, visible on=<####2>] (A) -| (3-2.south);
        \end{tikzpicture}
      }
      ]
      {}  9 & -7 &  8 \\
      {}  4 & -3 &  3 \\
      {} -3 &  3 & -5
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAi}[2]{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlMat<####1>}
      , code-after={
        \begin{tikzpicture}[visible on=<####2>, blu, ->, shorten >=2mm, thick]
          \node[below right=3mm and 2mm] (Ai) at (3-2) {$\scriptstyle A^{-1}$ exists!};
          \draw (Ai) -| (3-2.south);
        \end{tikzpicture}
      }
      ]
      {}  6 & -11 & 3 \\
      {} 11 & -21 & 5 \\
      {}  3 &  -6 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixI}[1]{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlMat<####1>[red]}
      , code-after={
        \begin{tikzpicture}[]
          \node[above=2mm] at (1-2) {$\scriptstyle I_3$};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1
    \end{bNiceMatrix}
  }
  \begin{definition}
    We call $A$ \emph{\grn{invertible}} if $A^{-1}$ exists.
    \begin{gather*}
      \begin{align*}
        \MatrixAi{3-}{6-}\MatrixA{2-}{5-}=\MatrixI{4-} && \MatrixA{2-}{5-}\MatrixAi{3-}{6-}=\MatrixI{4-}
      \end{align*}
    \end{gather*}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\NotInvertible}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<6->[red]}
      , code-after = {
        \begin{tikzpicture}
          \node[red, below right=3mm and 2mm, visible on=<6->] (A) at (3-2) {$\scriptstyle A$ is \emph{not invertible}!};
          \draw[red, ->, shorten >=2mm, thick, visible on=<6->] (A) -| (3-2.south);
        \end{tikzpicture}
      }
      ]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\NotInverse}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<5->[red]}
      , code-after = {
        \begin{tikzpicture}[thick, red]
          \draw[<-, visible on =<5->, shorten <=1mm]
          (row-1-|col-2.north) |- ++(0.5, 3mm) node[right] {$\neq A^{-1}$};
        \end{tikzpicture}
      }
      ]
      x_{11} & x_{12} & x_{13} \\
      x_{21} & x_{22} & x_{23} \\
      x_{31} & x_{32} & x_{33}
    \end{bNiceMatrix}
  }
  \newcommand{\NotIdentity}{
    \begin{bNiceMatrix}[
      , c
      , margin
      , code-before = {
        \hlEntry<3->[red]{3}{3}
        \hlMat<4->[red]
      }
      , code-after = {
        \begin{tikzpicture}[thick, red]
          \draw[<-, visible on=<3->] ($ (row-4-|col-4)!0.5!(row-3-|col-4) $) -- ++(5mm, 0) node[right] {$\neq1$};
          \draw[<-, visible on =<4->, shorten <=1mm]
          (row-1-|col-2.north) |- ++(0.5, 3mm) node[right] {$\neq I_3$};
        \end{tikzpicture}
      }
      ]
      x_{11} & x_{12} & x_{13} \\
      x_{21} & x_{22} & x_{23} \\
      0      & 0      & 0
    \end{bNiceMatrix}
  }
  \begin{alertblock}{Warning}
    Many matrices are \emph{\red{not invertible}}.
    \[
      \onslide<2->{\overset{A}{\NotInvertible}\NotInverse=\NotIdentity}
    \]
  \end{alertblock}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<3-5>}
      ]
      {}  3 &  4 & -9 \\
      {} -1 & -1 &  3 \\
      {} -1 & -2 &  4
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAi}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<7-8>}
      ]
      2 & 2 & 3 \\
      1 & 3 & 0 \\
      1 & 2 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAiLower}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<7-8>}
      ]
      2 & 2 & 3 \\
      1 & 3 & 0 \\
      1 & 2 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorB}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlMat<4-5,8>[grn]}
      ]
      2\\ 0\\ 2
    \end{bNiceMatrix}
  }
  \newcommand{\VectorBLower}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlMat<8>[grn]}
      ]
      2\\ 0\\ 2
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAb}{
    \begin{bNiceArray}{rrr|r}[
      , margin
      , code-before = {
        \hlPortion<3-5>{1}{1}{3}{3}
        \hlCol<4-5>[grn]{4}
      }
      ]
      {}  3 &  4 & -9 & 2 \\
      {} -1 & -1 &  3 & 0 \\
      {} -1 & -2 &  4 & 2
    \end{bNiceArray}
  }
  \newcommand{\MatrixAbR}{
    \begin{bNiceArray}{rrr|r}[
      , margin
      , code-before ={\hlCol<10->{4}}
      ]
      1 & 0 & 0 & 10 \\
      0 & 1 & 0 &  2 \\
      0 & 0 & 1 &  4
    \end{bNiceArray}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before ={\hlMat<10->}
      ]
      10\\ 2\\ 4
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXT}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before ={\hlMat<10->}
      ]
      10 & 2 & 4
    \end{bNiceMatrix}
  }
  \newcommand<>{\Axb}{
    \begin{tikzpicture}[baseline, remember picture]
      \node[anchor=base, inner sep=0] (Axb) {\(A\subnode{bvx}{\color####1{grn}\bv[x]}=\bv[b]\)};

      \draw[thick, <-, shorten <=1.5mm, grn, overlay, visible on=####1]
      (bvx.south) |- ++(3mm, -3.5mm) node[right, scale=0.75]
      {\(A(A^{-1}\bv[b])=I_3\bv[b]=\bv[b]\)};
    \end{tikzpicture}
  }
  \begin{example}
    Consider the matrix $A$, its inverse $A^{-1}$, and the vector $\bv[b]$ given
    by
    \begin{align*}
      A=\MatrixA && A^{-1}=\MatrixAi && \bv[b]=\VectorB
    \end{align*}
    % \onslide<2->{We could solve $A\bv[x]=\bv[b]$ with Gau\ss-Jordan}
    \onslide<2->{We could solve \Axb<5-> with Gau\ss-Jordan}
    \onslide<6->{or use $\bv[x]=A^{-1}\bv[b]$.}
    \begin{gather*}
      \begin{align*}
        \onslide<2->{\rref\MatrixAb=\MatrixAbR} && \onslide<6->{\overset{A^{-1}}{\MatrixAiLower}\overset{\bv[b]}{\VectorBLower}=\overset{\bv[x]}{\VectorX}}
      \end{align*}
    \end{gather*}
    \onslide<9->{Either way, the only solution is
      $\bv[x]={\VectorXT}^\intercal$.}
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      -2 &  -7 & 4 \\
      -3 & -11 & 7 \\
      -1 &  -3 & 2
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<3->[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=<3->]
          \draw (row-4-|col-2.south) |- ++(5mm, -2mm) node[right]
          {\textnormal{\scriptsize verifying $BA=I_3$ is unnecessary}};
        \end{tikzpicture}
      }
      ]
      -1 & 2 & -5 \\
      -1 & 0 &  2 \\
      -2 & 1 &  1
    \end{bNiceMatrix}
  }
  \begin{theorem}
    Suppose $A$ is a square $n\times n$ matrix. If one of the equations
    \begin{align*}
      AB=I_n && BA=I_n
    \end{align*}
    holds, then so does the other.
    \[
      \onslide<2->{\overset{A}{\MatrixA}\overset{B\onslide<4->{=A^{-1}}}{\MatrixB}=\overset{I_3}{\Identity{3}}}
    \]
    \onslide<5->{Checking if $B=A^{-1}$ is as simple as calculating one matrix
      product.}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Properties of Inverses}
    The inversion operation satisfies the following properties.
    \begin{description}[<+->][transposition rule]
    \item[order-reversing] $(AB)^{-1}=B^{-1}A^{-1}$ (as long as $A^{-1}$ and $B^{-1}$ exist!)
    \item[involution] $(A^{-1})^{-1}=A$
    \item[scaling rule] $(c\cdot A)^{-1}=(\sfrac{1}{c})\cdot A^{-1}$
    \item[transposition rule] $(A^\intercal)^{-1}=(A^{-1})^\intercal$
    \end{description}
  \end{block}

\end{frame}


\section{Calculating Inverses}
\subsection{Augmentation Method}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Algorithm}
    These steps will calculate $A^{-1}$ or determine that $A^{-1}$ does not
    exist.
    \begin{description}[<+->]
    \item[Step 1] Form an augmented matrix $[A\mid I_n]$.
    \item[Step 2] Row-reduce $[A\mid I_n]\rightsquigarrow[\rref(A)\mid B]$.
    \item[Step 3] If $\rref(A)\neq I_n$, then $A$ is not invertible.
    \item[Step 4] If $\rref(A)=I_n$, then $B=A^{-1}$.
    \end{description}
    \onslide<+->{\emph{Producing} $A^{-1}$ is very different from
      \emph{verifying} if $B=A^{-1}$.}
  \end{block}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceArray}{rr|rr}[
      , margin
      , code-before = {
        \hlPortion<2->{1}{1}{2}{2}
        \hlPortion<3->[grn]{1}{3}{2}{4}
      }
      , code-after = {
        \begin{tikzpicture}[thick, ->]
          \node[below left=1mm and 3mm, blu, visible on=<2->]
          (A) at (row-3-|col-2) {$\scriptstyle A$};
          \node[below right=1mm and 3mm, grn, visible on=<3->]
          (I) at (row-3-|col-4) {$\scriptstyle I_2$};
          \draw[blu, shorten >=1mm, visible on=<2->] (A) -| (row-3-|col-2.south);
          \draw[grn, shorten >=1mm, visible on=<3->] (I) -| (row-3-|col-4.south);
        \end{tikzpicture}
      }
      ]
      -1 & -1 & 1 & 0 \\
      -1 &  2 & 0 & 1
    \end{bNiceArray}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceArray}{rr|rr}
      {} 1 & 1 & -1 & 0 \\
      {}-1 & 2 &  0 & 1
    \end{bNiceArray}
  }
  \newcommand{\MatrixC}{
    \begin{bNiceArray}{rr|rr}
      1 & 1 & -1 & 0 \\
      0 & 3 & -1 & 1
    \end{bNiceArray}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceArray}{rr|rr}
      1 & 1 & -1            & 0            \\
      0 & 1 & -\sfrac{1}{3} & \sfrac{1}{3}
    \end{bNiceArray}
  }
  \newcommand{\MatrixE}{
    \begin{bNiceArray}{rr|rr}[
      , margin
      , code-before = {
        \hlPortion<8->[grn]{1}{1}{2}{2}
        \hlPortion<9->{1}{3}{2}{4}
      }
      , code-after = {
        \begin{tikzpicture}[thick, ->]
          \node[below left=1mm and 3mm, grn, visible on=<8->]
          (A) at (row-3-|col-2) {$\scriptstyle \rref(A)=I_2$};
          \node[below right=1mm and 3mm, blu, visible on=<9->]
          (I) at (row-3-|col-4) {$\scriptstyle A^{-1}$};
          \draw[grn, shorten >=1mm, visible on=<8->]
          (A) -| (row-3-|col-2.south);
          \draw[blu, shorten >=1mm, visible on=<9->]
          (I) -| (row-3-|col-4.south);
        \end{tikzpicture}
      }
      ]
      1 & 0 & -\sfrac{2}{3} & -\sfrac{1}{3} \\
      0 & 1 & -\sfrac{1}{3} &  \sfrac{1}{3}
    \end{bNiceArray}
  }
  \begin{example}
    Consider the following row-reductions.
    \begin{align*}
      \MatrixA
      \onslide<4->{\xrightarrow{-\bv[r]_1\to\bv[r]_1}                   \MatrixB} \\
      \onslide<5->{\xrightarrow{\bv[r]_2+\bv[r]_1\to\bv[r]_2}           \MatrixC} \\
      \onslide<6->{\xrightarrow{(\sfrac{1}{3})\cdot\bv[r]_2\to\bv[r]_2} \MatrixD} \\
      \onslide<7->{\xrightarrow{\bv[r]_1-\bv[r]_2\to\bv[r]_1}           \MatrixE}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceArray}{rrr|rrr}[
      , margin
      , code-before = {
        \hlPortion<2->{1}{1}{3}{3}
        \hlPortion<3->[grn]{1}{4}{3}{6}
      }
      , code-after = {
        \begin{tikzpicture}[thick, ->]
          \node[above left=2.5mm and 3mm, blu, visible on=<2->]
          (A) at (1-2) {$\scriptstyle A$};
          \node[above left=2.5mm and 3mm, grn, visible on=<3->]
          (I) at (1-5) {$\scriptstyle I_3$};
          \draw[blu, shorten >=1.5mm, visible on=<2->] (A) -| (1-2.north);
          \draw[grn, shorten >=1.5mm, visible on=<3->] (I) -| (1-5.north);
        \end{tikzpicture}
      }
      ]
      0 &  2 & -9  & 1 & 0 & 0 \\
      1 & -1 &  2  & 0 & 1 & 0 \\
      1 &  0 &  1  & 0 & 0 & 1
    \end{bNiceArray}
  }
  \newcommand{\MatrixAR}{
    \begin{bNiceArray}{rrr|rrr}[
      , margin
      , code-before = {
        \hlPortion<4->[grn]{1}{1}{3}{3}
        \hlPortion<5->{1}{4}{3}{6}
      }
      , code-after = {
        \begin{tikzpicture}[thick, ->]
          \node[above right=2.5mm and 3mm, grn, visible on=<4->]
          (A) at (1-2) {$\scriptstyle \rref(A)=I_3$};
          \node[above right=2.5mm and 3mm, blu, visible on=<5->]
          (I) at (1-5) {$\scriptstyle A^{-1}$};
          \draw[grn, shorten >=1.5mm, visible on=<4->] (A) -| (1-2.north);
          \draw[blu, shorten >=1mm, visible on=<5->] (I) -| (1-5.north);
        \end{tikzpicture}
      }
      ]
      1 & 0 & 0 &  \sfrac{1}{7} &  \sfrac{2}{7} & \sfrac{5}{7} \\
      0 & 1 & 0 & -\sfrac{1}{7} & -\sfrac{9}{7} & \sfrac{9}{7} \\
      0 & 0 & 1 & -\sfrac{1}{7} & -\sfrac{2}{7} & \sfrac{2}{7}
    \end{bNiceArray}
  }
  \newcommand{\CheckA}{
    \begin{bNiceMatrix}[r]
      0 &  2 & -9  \\
      1 & -1 &  2  \\
      1 &  0 &  1
    \end{bNiceMatrix}
  }
  \newcommand{\CheckAi}{
    \begin{bNiceMatrix}[r]
      {}  \sfrac{1}{7} &  \sfrac{2}{7} & \sfrac{5}{7} \\
      {} -\sfrac{1}{7} & -\sfrac{9}{7} & \sfrac{9}{7} \\
      {} -\sfrac{1}{7} & -\sfrac{2}{7} & \sfrac{2}{7}
    \end{bNiceMatrix}
  }
  \begin{example}
    Applying the Gau\ss-Jordan algorithm gives
    \[
      \rref\MatrixA=\MatrixAR
    \]
    \onslide<6->{We can verify our calculation by checking $A^{-1}A=I_3$.}
    \[
      \onslide<6->{\overset{A^{-1}}{\CheckAi}\overset{A}{\CheckA} =
        \overset{I_3}{\Identity{3}}}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceArray}{rrrr|rrrr}[
      , margin
      , code-before = {
        \hlPortion<2->{1}{1}{4}{4}
        \hlPortion<3->[grn]{1}{5}{4}{8}
      }
      , code-after = {
        \begin{tikzpicture}[thick, ->]
          \node[above left=1mm and 2mm, blu, visible on=<2->]
          (A) at (row-1-|col-3) {$\scriptstyle A$};
          \node[above left=1mm and 2mm, grn, visible on=<3->]
          (I) at (row-1-|col-7) {$\scriptstyle I_4$};
          \draw[blu, shorten >=1.5mm, visible on=<2->]
          (A) -| (row-1-|col-3.north);
          \draw[grn, shorten >=1.5mm, visible on=<3->]
          (I) -| (row-1-|col-7.north);
        \end{tikzpicture}
      }
      ]
      {} 6 & -28 &  51 &  152 & 1 & 0 & 0 & 0 \\
      {} 5 & -24 &  44 &  131 & 0 & 1 & 0 & 0 \\
      {}-5 &  23 & -42 & -125 & 0 & 0 & 1 & 0 \\
      {} 4 & -19 &  35 &  104 & 0 & 0 & 0 & 1
    \end{bNiceArray}
  }
  \newcommand{\MatrixAR}{
    \begin{bNiceArray}{rrrr|rrrr}[
      , margin
      , code-before = {
        \hlPortion<4->[red]{1}{1}{4}{4}
      }
      , code-after = {
        \begin{tikzpicture}[thick, ->, red, shorten >=1.5mm, visible on=<4->]
          \node[above right=1mm and 3mm] (A) at (row-1-|col-3) {$\scriptstyle \rref(A)\neq I_4$};
          \draw (A) -| (row-1-|col-3.north);
        \end{tikzpicture}
      }
      ]
      1 & 0 & 0 & -1 & 0 & -7 & -4 &  4 \\
      0 & 1 & 0 & -2 & 0 & -7 &  1 & 10 \\
      0 & 0 & 1 &  2 & 0 & -3 &  1 &  5 \\
      0 & 0 & 0 &  0 & 1 & -1 &  1 &  1
    \end{bNiceArray}
  }
  \newcommand{\CheckA}{
    \begin{bNiceMatrix}[r]
      1 &  2 & -9  \\
      1 & -1 &  2  \\
      1 &  0 &  1
    \end{bNiceMatrix}
  }
  \newcommand{\CheckAi}{
    \begin{bNiceMatrix}[r]
      {}  \sfrac{1}{7} &  \sfrac{2}{7} & \sfrac{5}{7} \\
      {} -\sfrac{1}{7} & -\sfrac{9}{7} & \sfrac{9}{7} \\
      {} -\sfrac{1}{7} & -\sfrac{2}{7} & \sfrac{2}{7}
    \end{bNiceMatrix}
  }
  \begin{example}
    Applying the Gau\ss-Jordan algorithm here gives
    \begin{gather*}
      \rref\MatrixA=\MatrixAR
    \end{gather*}
    \onslide<5->{This matrix $A$ is not invertible.}
  \end{example}

\end{frame}


\section{Nonsingularity}
\subsection{Nonsingular Means Invertible}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceArray}{rrr|rrr}[
      , margin
      , code-before = {
        \hlPortion<3->{1}{1}{3}{3}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<3->]
          \draw (row-1-|col-3.north) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      1 & -2 & -2 & 1 & 0 & 0 \\
      0 &  1 &  1 & 0 & 1 & 0 \\
      1 & -2 & -1 & 0 & 0 & 1
    \end{bNiceArray}
  }
  \newcommand{\MatrixAR}{
    \begin{bNiceArray}{rrr|rrr}[
      , margin
      , code-before = {
        \hlPortion<4->[red]{1}{1}{3}{3}
        \hlPortion<5->[grn]{1}{4}{3}{6}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm]
          \draw[visible on=<4->] (row-1-|col-3.north) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle\rref(A)=I_3$};
          \draw[grn, visible on=<5->] (row-1-|col-5.north) |- ++(3mm, 2.5mm)
          node[right] {\scriptsize$A^{-1}$ exists!};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 0 &  1 & 2 &  0 \\
      0 & 1 & 0 &  1 & 1 & -1 \\
      0 & 0 & 1 & -1 & 0 &  1
    \end{bNiceArray}
  }
  \begin{block}{Observation}
    The existance of $A^{-1}$ hinges on whether or not $\rref(A)=I_n$.
    \begin{align*}
      \onslide<2->{\rref\MatrixA=\MatrixAR}
    \end{align*}
    \onslide<6->{This is exactly what it means for $A$ to be
      \emph{nonsingular}.}
  \end{block}

\end{frame}


\subsection{The Invertible Matrix Theorem}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Invertible Matrix Theorem (Partial)}
    For an $n\times n$ matrix $A$, each of the following statements is
    equivalent.
    \begin{itemize}[<+->]
    \item $A$ is nonsingular
    \item $\rank(A)=n$
    \item $\nullity(A)=0$
    \item $A$ is invertible
    \item $A^{-1}$ exists
    \item $\rref(A)=I_n$
    \item $A\bv[x]=\bv[O]$ has only the ``trivial solution'' $\bv[x]=\bv[O]$
    \item $\rank(A)=\rank[A\mid\bv[b]]$ for every $\bv[b]\in\mathbb{R}^n$
    \end{itemize}
  \end{block}

\end{frame}


\end{document}
