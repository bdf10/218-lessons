\documentclass[]{bmr}

\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{patchplots}

\title{The Hessian}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Second Derivatives}
\subsection{Concavity}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\ConcaveUp}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , <->
        , blu
        , visible on=####1
        ]

        \draw[domain=-sqrt(2):sqrt(2), smooth, variable=\x]
        plot ({\x, \x*\x});

        \draw[black, fill=black] (0, 0) circle (3pt);

        \node[above, grn] at (current bounding box.north) {Concave Up};

        \node[below, black, font=\scriptsize] at (current bounding box.south)
        {\stackanchor{$f^{\prime\prime}(p)>0$}{(sufficient, unnecessary)}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand<>{\ConcaveDown}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , <->
        , blu
        , visible on=####1
        ]

        \draw[domain=-sqrt(2):sqrt(2), smooth, variable=\x]
        plot ({\x, -\x*\x});

        \draw[black, fill=black] (0, 0) circle (3pt);

        \node[above, grn] at (current bounding box.north) {Concave Down};

        \node[below, black, font=\scriptsize] at (current bounding box.south)
        {\stackanchor{$f^{\prime\prime}(p)<0$}{(sufficient, unnecessary)}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand<>{\InflectionPoint}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , <->
        , blu
        , visible on=####1
        ]

        \draw[domain=-1:1, smooth, variable=\x]
        plot ({\x, \x*\x*\x});

        \draw[black, fill=black] (0, 0) circle (3pt);

        \node[above, grn] at (current bounding box.north) {Inflection Point};

        \node[below, black, font=\scriptsize] at (current bounding box.south)
        {\stackanchor{$f^{\prime\prime}(p)=0$}{(insufficient, necessary)}};

      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    In single-variable calculus, the second derivative can measure
    \emph{\grn{concavity}}.
    \begin{align*}
      \ConcaveUp<2-> && \ConcaveDown<3-> && \InflectionPoint<4->
    \end{align*}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{second degree Taylor polynomial}} of $f(x)$ at $x=p$ is
    \[
      \begin{tikzpicture}[remember picture]
        \node{\(\displaystyle
          T_2(x)
          = f(p)
          + \subnode{fp}{\textcolor<2->{red}{f^\prime(p)}}\cdot(x-p)
          + \frac{1}{2!}\subnode{fpp}{\textcolor<3->{grn}{f^{\prime\prime}(p)}}\cdot(x-p)^2
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (fp.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize controls \emph{variation}};

        \draw[<-, thick, grn, overlay, visible on=<3->] (fpp.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize controls \emph{concavity}};
      \end{tikzpicture}
    \]
    \onslide<4->{This quadratic is used to approximate values of $f(x)$ ``near'' $x=p$.}
    \[
      \onslide<4->{f(p+\Delta p) \approx f(p)+f^\prime(p)\cdot\Delta p+\frac{1}{2!}f^{\prime\prime}(p)\cdot{\Delta p}^2}
    \]
    \onslide<5->{When $f^{\prime\prime}(p)=0$, we can use \emph{\grn{higher
          degree Taylor polynomials}}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $f(x)=4\,e^{\sfrac{(x-1)}{2}}+2$ at $x=1$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \pgfmathsetmacro{\ys}{0.25}

        \draw[<->, blu, yscale=\ys] plot[domain=-2:3]
        ({\x}, {4*exp((\x-1)/2)+2}) node[above] {$f(x)$};

        \draw[<->, red, thick, yscale=\ys, visible on=<2->] plot[domain=-1:3]
        ({\x}, {2*\x+4}) node[right, overlay] {$\scriptstyle T_1(x)=6+2\cdot(x-1)$};

        \draw[<->, grn, thick, yscale=\ys, visible on=<3->] plot[domain=-2:3]
        ({\x}, {(\x-1)*(\x-1)/2+2*\x+4}) node[right, overlay]
        {$\scriptstyle T_2(x)=6+2\cdot(x-1)+\frac{1}{2!}\cdot(x-1)^2$};

        % T1=2*x + 4
        % T2=1/2*(x - 1)^2 + 2*x + 4

        \draw[fill=black] (1, {\ys*6}) circle(2pt);

        \draw[red, fill=red, visible on=<6->] (5/2, {\ys*9}) circle(2pt);
        \draw[grn, fill=grn, visible on=<8->] (5/2, {\ys*81/8}) circle(2pt);

      \end{tikzpicture}
    \]
    \onslide<4->{We can approximate $f(\sfrac{5}{2})$ by using
      $\Delta p=\sfrac{5}{2}-1=\sfrac{3}{2}$.}
    \begin{align*}
      \onslide<5->{\textcolor{red}{f(\sfrac{5}{2}) \approx 6+2\cdot(\sfrac{3}{2}) = 9}} && \onslide<7->{\textcolor{grn}{f(\sfrac{5}{2}) \approx 6+2\cdot(\sfrac{3}{2})+\frac{1}{2!}\cdot(\sfrac{3}{2})^2 = \sfrac{81}{8}}}
    \end{align*}
  \end{example}

\end{frame}

\subsection{Error Orientation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\ConcaveUp}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , <->
        , blu
        , visible on=####1
        ]

        \draw[domain=-sqrt(2):sqrt(2), smooth, variable=\x]
        plot ({\x, \x*\x});

        \draw[red] ({-sqrt(2)}, 0) -- ({sqrt(2)}, 0);

        \draw[black, fill=black] (0, 0) circle (3pt);

        \node[above, grn] at (current bounding box.north) {Concave Up};

        \node[below, black, font=\scriptsize] at (current bounding box.south)
        {$L_p(x)$ \emph{underestimate}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand<>{\ConcaveDown}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , <->
        , blu
        , visible on=####1
        ]

        \draw[domain=-sqrt(2):sqrt(2), smooth, variable=\x]
        plot ({\x, -\x*\x});

        \draw[red] ({-sqrt(2)}, 0) -- ({sqrt(2)}, 0);

        \draw[black, fill=black] (0, 0) circle (3pt);

        \node[above, grn] at (current bounding box.north) {Concave Down};

        \node[below, black, font=\scriptsize] at (current bounding box.south)
        {$L_p(x)$ \emph{overestimate}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand<>{\InflectionPoint}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , <->
        , blu
        , visible on=####1
        ]

        \draw[domain=-1:1, smooth, variable=\x]
        plot ({\x, \x*\x*\x});

        \draw[red] ({-sqrt(2)}, 0) -- ({sqrt(2)}, 0);

        \draw[black, fill=black] (0, 0) circle (3pt);

        \node[above, grn] at (current bounding box.north) {Inflection Point};

        \node[below, black, font=\scriptsize] at (current bounding box.south)
        {direction matters};

      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    Concavity helps us understand the error in a linear approximation.
    \begin{align*}
      \ConcaveUp<2-> && \ConcaveDown<3-> && \InflectionPoint<4->
    \end{align*}
  \end{block}

\end{frame}


\section{Second-Order Partials}
\subsection{Definition}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{second order partial derivatives}} of
    $f\in\mathscr{C}(\mathbb{R}^n)$ are
    \begin{align*}
      \frac{\partial^2f}{\partial x_i\partial x_j} = \onslide<2->{\frac{\partial}{\partial x_i}\frac{\partial f}{\partial x_j}} && \onslide<5->{=} && \onslide<3->{f_{x_jx_i} =} \onslide<4->{(f_{x_j})_{x_i}}
    \end{align*}
    \onslide<6->{First differentiate with respect to $x_j$. Then differentiate
      with respect to $x_i$.}
  \end{definition}

\end{frame}


\subsection{Clairaut's Theorem}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\SecondOrder}{
    \begin{tikzpicture}[visible on=####1]
      \node {\(
        \begin{aligned}
          f_{xx} &= \frac{\partial}{\partial x} f_x                & \onslide<6->{f_{yy}} &\onslide<6->{= \frac{\partial}{\partial y} f_y}                    & \onslide<7->{f_{xy}} &\onslide<7->{= \frac{\partial}{\partial y} f_x}                & \onslide<8->{f_{yx}} &\onslide<8->{= \frac{\partial}{\partial x} f_y}                    \\
          {}     &= \frac{\partial}{\partial x} \Set{\cos(x-3\,y)} &                      &\onslide<6->{= \frac{\partial}{\partial y} \Set{-3\,\cos(x-3\,y)}} &                      &\onslide<7->{= \frac{\partial}{\partial y} \Set{\cos(x-3\,y)}} &                      &\onslide<8->{= \frac{\partial}{\partial x} \Set{-3\,\cos(x-3\,y)}} \\
          {}     &= -\sin(x-3\,y)                                  &                      &\onslide<6->{= -9\,\sin(x-3\,y)}                                   &                      &\onslide<7->{= 3\,\sin(x-3\,y)}                                &                      &\onslide<8->{= 3\,\sin(x-3\,y)}
        \end{aligned}
        \)};
    \end{tikzpicture}
  }

  \begin{example}
    The first-order partial derivatives of $f(x, y)=\sin(x-3\,y)$ are
    \begin{align*}
      f_x &= \onslide<2->{\cos(x-3\,y)} & \onslide<3->{f_y} &\onslide<3->{=} \onslide<4->{-3\,\cos(x-3\,y)}
    \end{align*}
    \onslide<5->{The second-order partial derivatives of $f$ are}
    \begin{gather*}
      \SecondOrder<5->
    \end{gather*}
    \onslide<9->{Note that $f_{xy}=f_{yx}$!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Clairaut's Theorem]
    $\displaystyle
    \frac{\partial^2 f}{\partial x_i\partial x_j}
    = \frac{\partial^2 f}{\partial x_j\partial x_i}$%
    \onslide<2->{\textcolor{red}{$\leftarrow$ order does not matter!}}
  \end{theorem}

\end{frame}


\section{The Hessian}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\HEnt}[2]{f_{x_{####1} x_{####2}}}
  \begin{definition}
    The \emph{\grn{Hessian}} of $f\in\mathscr{C}(\mathbb{R}^n)$ is
    \[
      Hf
      =
      \begin{bNiceMatrix}[
        , margin
        , r
        , code-before = {
          \hlEntry<2->[red]{1}{2}
          \hlEntry<3->[red]{2}{1}
          \hlEntry<4->[blu]{1}{4}
          \hlEntry<5->[blu]{4}{1}
          \hlEntry<6->[grn]{2}{4}
          \hlEntry<7->[grn]{4}{2}
        }
        % , code-after = {
        % \begin{tikzpicture}
        %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
        %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
        %   node[left] {$A$};
        % \end{tikzpicture}
        % }
        ]
        \HEnt{1}{1} & \HEnt{1}{2} & \Cdots & \HEnt{1}{n} \\
        \HEnt{2}{1} & \HEnt{2}{2} & \Cdots & \HEnt{2}{n} \\
        \Vdots      & \Vdots      & \Ddots & \Vdots      \\
        \HEnt{n}{1} & \HEnt{n}{2} & \Cdots & \HEnt{n}{n}
      \end{bNiceMatrix}
    \]
    \onslide<8->{Clairaut's Theorem implies that $Hf$ is
      \emph{\grn{real-symmetric}}.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Df}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlCol<2->{1}
        \hlCol<3->[grn]{2}
        \hlCol<4->[red]{3}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-2-|col-1)!0.5!(row-2-|col-2) $) |- ++(-2mm, -2.5mm)
          node[left] {$f_x$};
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<3->]
          ($ (row-2-|col-2)!0.5!(row-2-|col-3) $) |- ++(-2mm, -2.5mm)
          node[left] {$f_y$};
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<4->]
          ($ (row-2-|col-3)!0.5!(row-2-|col-4) $) |- ++(-2mm, -2.5mm)
          node[left] {$f_z$};
        \end{tikzpicture}
      }
      ]
      2\,xz & 3\,\sfrac{y^2}{z} & x^2-\sfrac{y^3}{z^2}
    \end{bNiceMatrix}
  }
  \newcommand{\HEnt}[2]{f_{####1 ####2}}
  \newcommand{\HfFormula}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<6-7>{1}{1}
        \hlEntry<8-9>[grn]{2}{2}
        \hlEntry<10-11>[red]{3}{3}
        \hlEntry<12-13>{1}{2}
        \hlEntry<12-13>{2}{1}
        \hlEntry<14-15>{1}{3}
        \hlEntry<14-15>{3}{1}
        \hlEntry<16-17>[grn]{2}{3}
        \hlEntry<16-17>[grn]{3}{2}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      \HEnt{x}{x} & \HEnt{x}{y} & \HEnt{x}{z} \\
      \HEnt{y}{x} & \HEnt{y}{y} & \HEnt{y}{z} \\
      \HEnt{z}{x} & \HEnt{z}{y} & \HEnt{z}{z}
    \end{bNiceMatrix}
  }
  \newcommand{\Hf}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<6-7>{1}{1}
        \hlEntry<8-9>[grn]{2}{2}
        \hlEntry<10-11>[red]{3}{3}
        \hlEntry<12-13>{1}{2}
        \hlEntry<12-13>{2}{1}
        \hlEntry<14-15>{1}{3}
        \hlEntry<14-15>{3}{1}
        \hlEntry<16-17>[grn]{2}{3}
        \hlEntry<16-17>[grn]{3}{2}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      \onslide<7->{2\,z} & \onslide<13->{0}                    & \onslide<15->{2\,x}                 \\
      \onslide<13->{0}    & \onslide<9->{6\,\sfrac{y}{z}}      & \onslide<17->{-3\,\sfrac{y^2}{z^2}} \\
      \onslide<15->{2\,x} & \onslide<17->{-3\,\sfrac{y^2}{z^2}} & \onslide<11->{2\,\sfrac{y^3}{z^3}}
    \end{bNiceMatrix}
  }
  \begin{example}
    The Jacobian derivative of $f(x, y, z)=x^2z+\sfrac{y^3}{z}$ is
    \[
      Df = \Df
    \]
    \onslide<5->{The Hessian of $f$ is}
    \[
      \onslide<5->{Hf = \HfFormula = \Hf}
    \]
  \end{example}

\end{frame}


\section{Taylor Approximations}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{second degree Taylor polynomial}} of
    $f\in\mathscr{C}(\mathbb{R}^n)$ at $P$ is
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          T_2(\bv[x])
          = \subnode{LP}{\textcolor<2->{red}{f(P) + Df(P)\Delta\bv[P]}}
          + \frac{1}{2!}\subnode{q}{\textcolor<3->{grn}{\inner{\Delta\bv[P], Hf(P)\Delta\bv[P]}}}
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (LP.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize linear approximation};

        \draw[<-, thick, grn, overlay, visible on=<3->] (q.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize quadratic form!};
      \end{tikzpicture}
    \]
    \onslide<4->{We use $T_2(\bv[x])$ to approximate $f$ when
      $\norm{\Delta\bv[P]}$ is small.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Df}{
    \begin{bNiceMatrix}[r]
      y^2-\sin(x+2\,y) & 2\,xy-2\,\sin(x+2\,y)
    \end{bNiceMatrix}
  }
  \newcommand{\Hf}{
    \begin{bNiceMatrix}[r]
      -\cos(x+2\,y)          & 2\,y-2\,\cos(x+2\,y) \\
      2\,y - 2\,\cos(x+2\,y) & 2\,x-4\,\cos(x+2\,y)
    \end{bNiceMatrix}
  }
  \newcommand{\OversetGrn}[2]{\overset{\textcolor{grn}{####1}}{####2}}
  \begin{example}
    Consider $f(x, y)=xy^2+\cos(x+2\,y)$.
    \begin{gather*}
      \begin{align*}
        Df &= \Df & Hf &= \Hf
      \end{align*}
    \end{gather*}
    \pause To approximate $f(-3, 1)$ using $P(-4, 2)$, we have
    \[
      f(-3, 1)
      \approx
      \OversetGrn{f(-4, 2)}{-15}
      + \OversetGrn{Df(-4, 2)}{\nvc{4 -16}}\OversetGrn{\Delta\bv[P]}{\nvc{1; -1}}
      + \frac{1}{2!}
      \OversetGrn{\Delta\bv[P]^\intercal}{\nvc{1 -1}}
      \OversetGrn{Hf(-4, 2)}{\nvc{-1 2; 2 -12}}
      \OversetGrn{\Delta\bv[P]}{\nvc{1; -1}}
      = -\frac{7}{2}
    \]
  \end{example}

\end{frame}


\subsection{Error Orientation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\ConcaveUp}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , <->
        , visible on=####1
        ]

        \coordinate (O) at (0, 0);

        \draw[fill=blubg, draw=blu, fill opacity=0.6, visible on=####1]
        (-1, 1)
        -- plot[smooth, domain=-1:1] ({\x}, {\x*\x})
        -- (1, 1)
        -- plot[smooth, domain=0:180] ({cos(\x)}, {1+sin(\x)/6})
        -- (-1, 1)
        -- cycle;

        \draw[fill=blubg, draw=blu, fill opacity=0.6, visible on=####1]
        (-1, 1)
        -- plot[smooth, domain=-1:1] ({\x}, {\x*\x})
        -- (1, 1)
        -- plot[smooth, domain=0:-180] ({cos(\x)}, {1+sin(\x)/6})
        -- (-1, 1)
        -- cycle;

        \node[above, grn] at (current bounding box.north) {Concave Up};

        \node[below, black, font=\scriptsize] at (current bounding box.south)
        {\stackanchor{$Hf(P)$ \emph{positive definite}}{($L_P(\bv[x])$ \emph{underestimate})}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand<>{\ConcaveDown}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , <->
        , blu
        , visible on=####1
        ]

        \draw[fill=blubg, draw=blu, fill opacity=0.6, visible on=####1]
        (-1, 0)
        -- plot[smooth, domain=-1:1] ({\x}, {1-\x*\x})
        -- (1, 0)
        -- plot[smooth, domain=0:180] ({cos(\x)}, {sin(\x)/6})
        -- (-1, 0)
        -- cycle;

        \draw[fill=blubg, draw=blu, fill opacity=0.6, visible on=####1]
        (-1, 0)
        -- plot[smooth, domain=-1:1] ({\x}, {1-\x*\x})
        -- (1, 0)
        -- plot[smooth, domain=0:-180] ({cos(\x)}, {sin(\x)/6})
        -- (-1, 0)
        -- cycle;

        \node[above, grn] at (current bounding box.north) {Concave Down};

        \node[below, black, font=\scriptsize] at (current bounding box.south)
        {\stackanchor{$Hf(P)$ \emph{negative definite}}{($L_P(\bv[x])$ \emph{overestimate})}};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand<>{\Saddle}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , <->
        , blu
        , visible on=####1
        ]

        \begin{axis}[domain=-1:1, y domain=-1:1, scale=1/2, hide axis]
          \addplot3[
          , surf
          , samples=20
          , thick
          % , opacity=0.05
          % , color=blue!40!white
          % , faceted color=blue!70!black
          , color=blubg
          , faceted color=blu
          , fill opacity=0.60
          , visible on=####1
          ]
          {x^2-y^2};
          % \node[label={87.5:{\tiny saddle pt}},circle,fill,inner sep=2pt] at (axis cs:0,0,0) {};
        \end{axis}

        \node[above, grn] at (current bounding box.north) {Saddle};

        \node[below, black, font=\scriptsize] at (current bounding box.south)
        {\stackanchor{$Hf(P)$ \emph{indefinite}}{(direction matters)}};

      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    We can use the definiteness of $Hf(P)$ to \emph{\grn{define}} concavity.
    \begin{align*}
      \ConcaveUp<2-> && \ConcaveDown<3-> && \Saddle<4->
    \end{align*}
    \onslide<5->{Further analysis is required if $Hf(P)$ is semidefinite but not
      definite.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\OversetGrn}[2]{\overset{\textcolor{grn}{####1}}{####2}}
  \newcommand{\Hf}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<5->[red]{1}{1}
        \hlEntry<5->[red]{2}{2}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<5->]
          (row-2-|col-3) -| ++(14mm, 1mm)
          node[above] {\stackanchor{\emph{negative definite}}{(pivots $<0$)}};
        \end{tikzpicture}
      }
      ]
      {} -1 &  2 \\
      {}  0 & -8
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the scalar field $f\in\mathscr{C}(\mathbb{R}^2)$ given by
    \[
      f(x, y) = xy^2+\cos(x+2\,y)
    \]
    \onslide<2->{Using $P(-4, 2)$ to linearly approximate
      $f(-\sfrac{15}{4}, \sfrac{9}{5})$ gives}
    \[
      \onslide<2->{
        f(-\sfrac{15}{4}, \sfrac{9}{5})
        \approx
        \OversetGrn{f(P)}{-15}
        + \OversetGrn{Df(P)}{\nvc{4 -16}}\OversetGrn{\Delta\bv[P]}{\nvc{\sfrac{1}{4}; -\sfrac{1}{5}}}
        = -\sfrac{54}{5}
      }
    \]
    \onslide<3->{Here, the Hessian is}
    \[
      \onslide<3->{Hf(-4, 2) = \nvc{-1 2; 2 -12}}
      \onslide<4->{\xrightarrow{\bv[r]_2+2\bv[r]_1\to\bv[r]_2}\Hf}
    \]
    \onslide<6->{We expect our approximation to be an \emph{overestimate}.}
  \end{example}

\end{frame}


\end{document}
