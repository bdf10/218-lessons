\documentclass[]{bmr}

\title{Complex Numbers}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Complex Numbers}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{\grn{complex number}} is an expression of the form
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          z=
          \subnode{rez}{\textcolor<2->{grn}{a}}+
          \subnode{imz}{\textcolor<4->{blu}{b}}\cdot
          \subnode{imag}{\textcolor<3->{red}{i}}\in
          \subnode{CC}{\textcolor<5->{blu}{\mathbb{C}}}
          \)};

        \draw[<-, thick, grn, overlay, visible on=<2->] (rez.south) |- ++(-3mm, -2.5mm)
        node[left] (dummy) {\scriptsize ``real part'' $\Re(z)\in\mathbb{R}$};

        \draw[<-, thick, red, overlay, visible on=<3->] (imag.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize ``imaginary unit'' $i^2=-1$};

        \draw[<-, thick, blu, overlay, visible on=<4->] (imz.south) |- ++(1mm, -8mm)
        node[right] {\scriptsize ``imaginary part'' $\Im(z)\in\mathbb{R}$};

        \draw[<-, thick, blu, overlay, visible on=<5->] (CC.north) |- ++(3mm, 2.5mm)
        node[right] {\scriptsize ``all complex numbers''};

        \node at (dummy.south east) {\phantom{z}};
      \end{tikzpicture}
    \]
    \onslide<6->{For example, $\textcolor{blu}{6-11\,i\in\mathbb{C}}$ with
      $\textcolor{grn}{\Re(6-11\,i)=6}$ and
      $\textcolor{red}{\Im(6-11\,i)=-11}$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Real numbers are complex, but complex numbers are \emph{\red{not necessarily
        real}}.
    \[
      \begin{tikzpicture}[ultra thick, rounded corners]

        \newcommand{\Numbers}{$17, 0, -11, \pi, e, \sfrac{9}{22}, \dotsc$}

        \node[draw, fill=grn, fill opacity=0.45, inner sep=2cm] (complex)
        {\phantom{\Numbers}};

        \node[fill=blu, fill opacity=0.45, draw, text opacity=1.0, inner sep=1cm] (real)
        {\Numbers};

        \node[below right] at (complex.north west) {$\mathbb{C}\not\subset\mathbb{R}$};
        \node[below right] at (real.north west) {$\mathbb{R}\subset\mathbb{C}$};
        \node[above] at (real.north) {$4-3\,i, 7\,i, 19+2\,i$};
      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Addition}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\scriptstyle z_1+z_2=(a_1+a_2)+(b_1+b_2)\,i$};
      \node[above, grn] at (text.north) {\scriptsize Complex Addition};
    \end{tikzpicture}
  }
  \newcommand{\Multiplication}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\scriptstyle z_1\cdot z_2=(a_1a_2-b_1b_2)+(a_1b_2+a_2b_1)\,i$};
      \node[above, blu] at (text.north) {\scriptsize Complex Multiplication};
    \end{tikzpicture}
  }
  \newcommand{\Conjugation}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\overline{z}=\overline{a+b\,i}=a-b\,i$};
      \node[above, grn] at (text.north) {\scriptsize Complex Conjugation};
    \end{tikzpicture}
  }
  \newcommand{\AbsoluteValue}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\abs{z}=\sqrt{z\cdot\overline{z}}=\sqrt{a^2+b^2}$};
      \node[above, blu] at (text.north) {\scriptsize Absolute Value};
    \end{tikzpicture}
  }
  \begin{definition}
    For $z_1=a_1+b_1\,i$ and $z_2=a_2+b_2\,i$ we have \emph{\grn{addition}} and
    \emph{\blu{multiplication}}.
    \begin{align*}
      \Addition && \Multiplication
    \end{align*}
    \onslide<2->{For $z=a+b\,i$, we have \emph{\grn{conjugation}} and
      \emph{\blu{absolute value}}.}
    \begin{align*}
      \Conjugation && \AbsoluteValue
    \end{align*}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Additive}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\overline{z_1+z_2}=\overline{z_1}+\overline{z_2}$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Additive}};
    \end{tikzpicture}
  }
  \newcommand{\Multiplicative}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\overline{z_1\cdot z_2}=\overline{z_1}\cdot\overline{z_2}$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Multiplicative}};
    \end{tikzpicture}
  }
  \newcommand{\Involutive}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=redbg, draw, rounded corners, ultra thick]
      (text) {$\overline{\overline{z}}=z$};
      \node[above, red] at (text.north) {\scriptsize\textnormal{Involutive}};
    \end{tikzpicture}
  }
  \newcommand{\Reciprocal}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\displaystyle \frac{1}{z}=\frac{\overline{z}}{\abs{z}^2}=\frac{a}{a^2+b^2}-\frac{b}{a^2+b^2}\,i$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Reciprocal Formula}};
    \end{tikzpicture}
  }
  \begin{theorem}
    Complex conjugation is \grn{additive}, \blu{multiplicative}, and
    \red{involutive}.
    \begin{align*}
      \Additive && \Multiplicative && \Involutive
    \end{align*}
    \onslide<2->{The \grn{reciprocal} of $z=a+b\,i\neq0$ is}
    \[
      \Reciprocal
    \]
    \onslide<3->{We can test if $z\in\mathbb{R}$ by checking $\overline{z}=z$.}
  \end{theorem}

\end{frame}


\section{Complex Vectors}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\VectorV}{
    \begin{bNiceMatrix}
      1-i\\ 3+2\,i
    \end{bNiceMatrix}
  }
  \newcommand{\VectorW}{
    \begin{bNiceMatrix}[r]
      6\\ -5\\ 2
    \end{bNiceMatrix}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}
      3-i\\ 0\\ 2-6\,i\\ 88
    \end{bNiceMatrix}
  }
  \begin{definition}
    A \emph{\grn{vector}} in $\mathbb{C}^n$ is a list of $n$ scalars organized
    vertically into a list.
    \begin{align*}
      \bv=\VectorV\in\mathbb{C}^2 && \bv[w]=\VectorW\in\mathbb{C}^3 && \bv[x]=\VectorX\in\mathbb{C}^4
    \end{align*}
    \onslide<2->{Note that $\mathbb{R}^n\subset\mathbb{C}^n$ but
      $\mathbb{C}^n\not\subset\mathbb{R}^n$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\VectorV}{
    \begin{bNiceMatrix}
      v_1 & v_2 & \Cdots & v_n
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\ConjugateV}{
    \begin{bNiceMatrix}
      \overline{v_1} & \overline{v_2} & \Cdots & \overline{v_n}
    \end{bNiceMatrix}^\intercal
  }
  \begin{definition}
    The \emph{\grn{conjugate}} of $\bv=\VectorV$ is
    $\overline{\bv}=\ConjugateV$.
  \end{definition}

  \begin{example}<2->
    $\overline{\nv{{3-2\,i} {9\,i} {11}}}=\nv{{3+2\,i} {-9\,i} {11}}$
  \end{example}

\end{frame}


\subsection{Inner Products}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{inner product}} of $\bv,\bv[w]\in\mathbb{C}^n$ is
    \[
      \inner{\bv, \bv[w]}
      =
      \overline{v_1}\cdot w_1+\overline{v_2}\cdot w_2+\dotsb+\overline{v_n}\cdot w_n
    \]
    \onslide<2->{So, for $\bv=\nv{{7-3\,i} {4+i}}$ and $\bv[w]=\nv{2 {5+2\,i}}$
      we have}
    \begin{align*}
      \onslide<2->{\inner{\bv, \bv[w]}}
      &\onslide<2->{=} \onslide<2->{(\overline{7-3\,i})\cdot(2)+(\overline{4+i})\cdot(5+2\,i)} \\
      &\onslide<2->{=} \onslide<3->{(7+3\,i)\cdot(2)+(4-i)\cdot(5+2\,i)}                       \\
      &\onslide<3->{=} \onslide<4->{(14+6\,i)+(22+3\,i)}                                       \\
      &\onslide<4->{=} \onslide<5->{36+9\,i}
    \end{align*}
    \onslide<6->{Note that $\inner{\bv, \bv[w]}\in\mathbb{C}$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\ComplexValued}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\inner{\bv,\bv[w]}\in\mathbb{C}$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Complex-Valued}};
    \end{tikzpicture}
  }
  \newcommand{\ConjugateSymmetric}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\inner{\bv, \bv[w]}=\overline{\inner{\bv[w], \bv}}$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Conjugate-Symmetric}};
    \end{tikzpicture}
  }
  \newcommand{\Left}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\scriptstyle\inner{c_1\cdot\bv_1+c_2\cdot\bv_2,\bv[w]}=\overline{c_1}\cdot\inner{\bv_1,\bv[w]}+\overline{c_2}\cdot\inner{\bv_2,\bv[w]}$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Left Conjugate-Linear}};
    \end{tikzpicture}
  }
  \newcommand{\Right}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\scriptstyle\inner{\bv, c_1\cdot\bv[w]_1+c_2\cdot\bv[w]_2}=c_1\cdot\inner{\bv,\bv[w]_1}+c_2\cdot\inner{\bv,\bv[w]_2}$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Right Linear}};
    \end{tikzpicture}
  }
  \begin{theorem}
    The inner product on $\mathbb{C}^n$ is \grn{complex valued} and
    \blu{conjugate-symmetric}.
    \begin{align*}
      \ComplexValued && \ConjugateSymmetric
    \end{align*}
    \onslide<2->{We also have \grn{left conjugate-linearity} and
      \blu{right-linearity} laws.}
    \begin{align*}
      \Left && \Right
    \end{align*}
    \onslide<3->{In particular,
      $\inner{c\cdot\bv,\bv[w]}=\overline{c}\cdot\inner{\bv, \bv[w]}$ and
      $\inner{\bv, c\cdot\bv[w]}=c\cdot\inner{\bv, \bv[w]}$.}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\VectorV}{
    \begin{bNiceMatrix}
      a_1+b_1\,i & \Cdots & a_n+b_n\,i
    \end{bNiceMatrix}^\intercal
  }
  \begin{definition}
    For $\bv=\VectorV$ we have
    \begin{align*}
      \inner{\bv, \bv}
      &= (\overline{a_1+b_1\,i})\cdot(a_1+b_1\,i)+\dotsb+(\overline{a_n+b_n\,i})\cdot(a_n+b_n\,i) \\
      &= (a_1-b_1\,i)\cdot(a_1+b_1\,i)+\dotsb+(a_n-b_n\,i)\cdot(a_n+b_n\,i)                       \\
      &= a_1^2+b_1^2+\dotsb+a_n^2+b_n^2
    \end{align*}
    \pause We thus define the \emph{\grn{length}} of $\bv\in\mathbb{C}^n$ as
    \[
      \norm{\bv}=\sqrt{\inner{\bv, \bv}}=\sqrt{a_1^2+b_1^2+\dotsb+a_n^2+b_n^2}
    \]
    \pause So, for $\bv=\nv{{1-i} {-3+2\,i} {7}}$ we have
    \[
      \norm{\bv}
      = \sqrt{1^2+(-1)^2+(-3)^2+2^2+7^2}
      = \sqrt{64}
    \]
  \end{definition}

\end{frame}


\section{Complex Matrices}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , c
      , margin
      , code-before = {
        \hlRow<3>{1}
        \hlRow<3>{2}
        \hlRow<3>{3}
        \hlCol<4>[grn]{1}
        \hlCol<4>[grn]{2}
        \hlCol<4>[grn]{3}
        \hlCol<4>[grn]{4}
      }
      ]
      3-9\,i & 7   & 14+4\,i & -5\,i   \\
      4+5\,i & 5+i & 3-7\,i  & 6+19\,i \\
      4\,i   & 11  & 14+3\,i & 5\,i
    \end{bNiceMatrix}
  }
  \begin{definition}
    An $m\times n$ \emph{\grn{complex matrix}} is an $m\times n$ array of
    complex numbers.
    \[
      A = \MatrixA
    \]
    \onslide<2->{Here, we write $A\in\mathbb{C}^{3\times 4}$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}
      \bv[a]_1 & \bv[a]_2 & \Cdots & \bv[a]_n
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}
      v_1 & v_2 & \Cdots & v_n
    \end{bNiceMatrix}^\intercal
  }
  \begin{definition}
    Suppose that $A\in\mathbb{C}^{m\times n}$ and $\bv\in\mathbb{C}^n$ are of
    the form
    \begin{align*}
      A=\MatrixA && \bv=\VectorV
    \end{align*}
    \pause The \emph{\grn{matrix-vector product}} $A\bv$ is given by the linear
    combination
    \[
      A\bv
      = v_1\cdot\bv[a]_1+v_2\cdot\bv[a]_2+\dotsb+v_n\cdot\bv[a]_n
    \]
    \pause This gives our familiar interpretation
    $\mathbb{C}^n\xrightarrow{A}\mathbb{C}^m$.
  \end{definition}

\end{frame}


% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \newcommand{\MatrixA}{
%   \begin{bNiceMatrix}[r]
%     {}  i & -1-i    \\
%     {} -i &  1+i    \\
%     {}  i & -1+2\,i
%   \end{bNiceMatrix}
% }
%   \newcommand{\VectorV}{
%   \begin{bNiceMatrix}
%     1+i \\
%     i
%   \end{bNiceMatrix}
% }
%   \begin{example}
%     $
%     \begin{aligned}
%       \MatrixA\VectorV
%       &= (1+i)\nvc{i; -i; i}+i\nvc{-1-i; 1+i; -1+2\,i} \\
%       &= \nvc{-1+i; 1-i; -1+i}+\nvc{1-i; -1+i; -2-i}     \\
%       &= \nvc{0; 0; -3}
%     \end{aligned}
%     $
%   \end{example}

% \end{frame}


\subsection{Conjugate Transposition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<2-3>{1}
        \hlRow<4-5>{2}
        \hlRow<6-7>{3}
      }
      ]
      2+i     & 1       \\
      2+17\,i & -4-9\,i \\
      -i      & 2
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAast}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<2-3>{1}
        \hlCol<4-5>{2}
        \hlCol<6-7>{3}
      }
      ]
      \onslide<3->{2-i} & \onslide<5->{2-17\,i} & \onslide<7->{i} \\
      \onslide<3->{1}   & \onslide<5->{-4+9\,i} & \onslide<7->{2}
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{conjugate transpose}} of $A$ is
    $A^\ast=\overline{A^\intercal}$.
    \[
      \MatrixA^\ast=\MatrixAast
    \]
    \onslide<8->{Matrices over $\mathbb{R}$ satisfy $A^\ast=A^\intercal$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{alertblock}{Warning}
    Some authors write $A^\dagger$ or $A^H$ instead of $A^\ast$.
  \end{alertblock}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\OrderReversing}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$(AB)^\ast=B^\ast A^\ast$};
      % \node[above, grn] at (text.north) {\scriptsize\textnormal{Order-Reversing}};
    \end{tikzpicture}
  }
  \newcommand{\Involution}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$(A^\ast)^\ast=A$};
      % \node[above, blu] at (text.north) {\scriptsize\textnormal{Involution}};
    \end{tikzpicture}
  }
  \newcommand{\ConjugateLinear}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$(c_1\cdot A_1+c_2\cdot A_2)^\ast=\overline{c_1}\cdot A_1^\ast+\overline{c_2}\cdot A_2^\ast$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Conjugate-Linear}};
    \end{tikzpicture}
  }
  \newcommand{\Inverse}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$(A^\ast)^{-1}=(A^{-1})^\ast$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Inverse Formula}};
    \end{tikzpicture}
  }
  \newcommand{\Trace}{
    \begin{tikzpicture}[visible on=<3->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\trace(A^\ast)=\overline{\trace(A)}$};
      % \node[above, grn] at (text.north) {\scriptsize\textnormal{Trace Formula}};
    \end{tikzpicture}
  }
  \newcommand{\Rank}{
    \begin{tikzpicture}[visible on=<3->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\rank(A^\ast)=\rank(A)$};
      % \node[above, blu] at (text.north) {\scriptsize\textnormal{Rank Formula}};
    \end{tikzpicture}
  }
  \newcommand{\Det}{
    \begin{tikzpicture}[visible on=<3->]
      \node[fill=redbg, draw, rounded corners, ultra thick]
      (text) {$\det(A^\ast)=\overline{\det(A)}$};
      % \node[above, red] at (text.north) {\scriptsize\textnormal{Determinant Formula}};
    \end{tikzpicture}
  }
  \begin{theorem}
    Conjugate transposition is an \grn{order-reversing} \blu{involution}.
    \begin{align*}
      \OrderReversing && \Involution
    \end{align*}
    \onslide<2->{The operation is \grn{conjugate-linear} and \blu{respects inversion}.}
    \begin{align*}
      \ConjugateLinear && \Inverse
    \end{align*}
    \onslide<3->{The operation cooperates with \grn{trace}, \blu{rank}, and
      \red{determinants}.}
    \begin{align*}
      \Trace && \Rank && \Det
    \end{align*}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{theorem}[Adjoint Formula]
    $\inner{A\bv, \bv[w]}=\inner{\bv,A^\ast\bv[w]}$
  \end{theorem}
  \begin{proof}<2->
    $\inner{A\bv, \bv[w]}=(A\bv)^\ast\bv[w]=(\bv^\ast A^\ast)\bv[w]=\bv^\ast(A^\ast\bv[w])=\inner{\bv, A^\ast\bv[w]}$
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The eigenvalues of $A^\ast$ are the conjugates of the eigenvalues of $A$.
  \end{theorem}
  \begin{proof}<2->
    This follows from
    \[
      \rank(\lambda\cdot I_n-A)
      = \rank(\lambda\cdot I_n-A)^\ast
      = \rank(\overline{\lambda}\cdot I_n-A^\ast)
    \]
    So $\overline{\lambda}$ is an eigenvalue of $A^\ast$ when $\lambda$ is an
    eigenvalue of $A$.
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Projections}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\scriptstyle X(X^\intercal X)^{-1}X^\intercal\leftrightsquigarrow X(X^\ast X)^{-1}X^\ast$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Projections}};
    \end{tikzpicture}
  }
  \newcommand{\LeastSquares}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\scriptstyle A^\intercal A\widehat{\bv[x]}=A^\intercal\bv[b]\leftrightsquigarrow A^\ast A\widehat{\bv[x]}=A^\ast\bv[b]$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Least-Squares}};
    \end{tikzpicture}
  }
  \newcommand{\OrthonormalColumns}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=redbg, draw, rounded corners, ultra thick]
      (text) {$\scriptstyle Q^\intercal Q=I_n \leftrightsquigarrow Q^\ast Q=I_n$};
      \node[above, red] at (text.north) {\scriptsize\textnormal{Orthonormal Cols}};
    \end{tikzpicture}
  }
  \begin{block}{Paradigm}
    The following replacements allow us to do linear algebra over $\mathbb{C}$.
    \begin{align*}
      \mathbb{R}\leftrightsquigarrow\mathbb{C} && \inner{\bv,\bv[w]}=\bv^\intercal\bv[w]\leftrightsquigarrow\inner{\bv,\bv[w]}=\bv^\ast\bv[w] && A^\intercal\leftrightsquigarrow A^\ast
    \end{align*}
    \onslide<2->{These replacements give ``complex'' analogues of ``real'' concepts.}
    \begin{align*}
      \Projections && \LeastSquares && \OrthonormalColumns
    \end{align*}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Symmetric}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$S^\intercal=S$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Symmetric}};
    \end{tikzpicture}
  }
  \newcommand{\Hermitian}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$H^\ast=H$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Hermitian}};
    \end{tikzpicture}
  }
  \newcommand{\Orthogonal}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$Q^\intercal=Q^{-1}$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Orthogonal}};
    \end{tikzpicture}
  }
  \newcommand{\Unitary}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$Q^\ast=Q^{-1}$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Unitary}};
    \end{tikzpicture}
  }
  \begin{definition}
    \emph{\grn{Symmetric matrices}} are analogous to \emph{\blu{Hermitian
        matrices}}.
    \begin{align*}
      \Symmetric && \Hermitian
    \end{align*}
    \onslide<2->{\emph{\grn{Orthogonal matrices}} are analogous to
      \emph{\blu{unitary matrices}}.}
    \begin{align*}
      \Orthogonal && \Unitary
    \end{align*}
    \onslide<3->{Some adjectives change when we pass from $\mathbb{R}$ to
      $\mathbb{C}$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixH}{
    \begin{bNiceMatrix}[
      , small
      ]
      3   & 1-i    & 5 \\
      1+i & -7     & 6-2\,i \\
      5   & 6+2\,i & 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixHast}{
    \begin{bNiceMatrix}[
      , small
      ]
      3   & 1-i    & 5 \\
      1+i & -7     & 6-2\,i \\
      5   & 6+2\,i & 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixQ}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      0 & 1 & 0 \\
      i & 0 & 0 \\
      0 & 0 & -i
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixQast}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      0 & -i & 0 \\
      1 &  0 & 0 \\
      0 &  0 & i
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the equations
    \begin{align*}
      \overset{H}{\MatrixH^\ast}=\MatrixHast && \overset{Q^\ast}{\MatrixQast}\overset{Q}{\MatrixQ}=\Identity[small]{3}
    \end{align*}
    \onslide<2->{Here, $H$ is \emph{\grn{Hermitian}} ($H^\ast=H$) and $Q$ is
      \emph{\blu{unitary}} ($Q^\ast=Q^{-1}$).}
  \end{example}

\end{frame}


\section{Complex Vector Spaces}
\subsection{Fundamental Subspaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{four fundamental subspaces}} of a complex matrix $A$ are
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\PerpLength}{0.25}
        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        ] {$\scriptstyle\underset{\rank(A)}{\Col(A^\ast)}$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        ] {$\scriptstyle\underset{\nullity(A)}{\Null(A)}$};
        \node[right] (Rn) at (Row.east) {$\mathbb{C}^n$};

        \draw[LeftSpace] (0, 0) rectangle (-\PerpLength,\PerpLength);

        \begin{scope}[xshift=4.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          ] {$\scriptstyle\underset{\rank(A)}{\Col(A)}$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          ] {$\scriptstyle\underset{\nullity(A^\ast)}{\Null(A^\ast)}$};

          \draw[RightSpace] (0, 0) rectangle (\PerpLength,\PerpLength);

          \node[left] (Rm) at (Col.west) {$\mathbb{C}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A$};

      \end{tikzpicture}
    \]
    \onslide<2->{This is the same as before with $A^\intercal$ replaced with
      $A^\ast$.}
  \end{definition}

\end{frame}


\subsection{Eigenspaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      0 & -1 \\
      1 &  0
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXa}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle -i\cdot I_2-A$};
        \end{tikzpicture}
      }
      ]
      -i &  1 \\
      -1 & -i
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXb}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, visible on=####1]
          \draw ($ (row-3-|col-1)!0.5!(row-3-|col-3) $) |- ++(3mm, -2.5mm)
          node[right] {$\scriptstyle i\cdot I_2-A$};
        \end{tikzpicture}
      }
      ]
      {}  i & 1 \\
      {} -1 & i
    \end{bNiceMatrix}
  }
  \begin{example}
    This matrix has non-real complex eigenvalues!
    \begin{align*}
      A=\MatrixA && \EVals(A)=\Set{-i, i}
    \end{align*}
    \onslide<2->{The eigenspaces are}
    \begin{align*}
      \onslide<2->{\mathcal{E}_A(-i)} &\onslide<2->{=} \onslide<2->{\Null\MatrixXa<3->=\Span\Set{\nv{-i 1}}} \\
      \onslide<4->{\mathcal{E}_A(i)}  &\onslide<4->{=} \onslide<4->{\Null\MatrixXb<5->=\Span\Set{\nv{i 1}}}
    \end{align*}
  \end{example}

\end{frame}

\end{document}
