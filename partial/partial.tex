\documentclass[]{bmr}

\usepackage{booktabs}
\sisetup{quotient-mode=fraction,per-mode=symbol}


\title{Partial Derivatives}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Scalar Fields}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\ScalarFieldf}{
    \begin{tikzpicture}[visible on=####1]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$f(x, y)=x^3-7\,xy^2$};
      \node[above, grn] at (text.north) {\scriptsize$f\in\mathscr{C}(\mathbb{R}^2)$};
    \end{tikzpicture}
  }
  \newcommand<>{\ScalarFieldT}{
    \begin{tikzpicture}[visible on=####1]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$T(x_1, x_2)=17$};
      \node[above, blu] at (text.north) {\scriptsize$T\in\mathscr{C}(\mathbb{R}^2)$};
    \end{tikzpicture}
  }
  \newcommand<>{\ScalarFieldphi}{
    \begin{tikzpicture}[visible on=####1]
      \node[fill=redbg, draw, rounded corners, ultra thick]
      (text) {$\phi(x, y, z)=xy-\cos(yz)$};
      \node[above, red] at (text.north) {\scriptsize$\phi\in\mathscr{C}(\mathbb{R}^3)$};
    \end{tikzpicture}
  }

  \begin{definition}
    A \emph{\grn{scalar field on $\mathbb{R}^n$}} is a function
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(f:
          \subnode{Rn}{\textcolor<2->{red}{\mathbb{R}^n}}
          \to
          \subnode{R}{\textcolor<3->{blu}{\mathbb{R}}}\)};

        \draw[<-, thick, red, overlay, visible on=<2->] (Rn.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize ``domain''};

        \draw[<-, thick, blu, overlay, visible on=<3->] (R.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize ``target''};
      \end{tikzpicture}
    \]
    \onslide<4->{We write $f\in\mathscr{C}(\mathbb{R}^n)$ to indicate that $f$
      is a scalar field.}
    \begin{align*}
      \ScalarFieldf<5-> && \ScalarFieldT<6-> && \ScalarFieldphi<7->
    \end{align*}
    \onslide<8->{Scalar fields associate a single \emph{scalar output} to every
      \emph{vector input}.}
  \end{definition}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Theme}
    Scalar fields are useful for measuring \emph{scalar quantities in space}.
    \begin{center}
      \begin{tabular}{lr}
        \toprule
        \multicolumn{1}{c}{Measurement} & \multicolumn{1}{c}{Units} \\
        \midrule
        Temperature         & \SI{}{\degreeCelsius} \\
        Mass                & \SI{}{\kilogram} \\
        Force               & \SI{}{\newton} \\
        Electric Current    & \SI{}{\ampere} \\
        Time                & \SI{}{\second} \\
        Amount of Substance & \SI{}{\mole} \\
        Length              & \SI{}{\metre} \\
        \bottomrule
      \end{tabular}
    \end{center}
  \end{block}

\end{frame}

\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that temperature in $\mathbb{R}^2$ is given by
    $T(x, y)=x^2-2\,y\,\si{\degreeCelsius}$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \draw[<->] (-4,  0) -- (4, 0);
        \draw[<->] ( 0, -2) -- (0, 2);

        \coordinate (P1) at ( 1,  1);
        \coordinate (P2) at ( 2, -1);
        \coordinate (P3) at ( 0, -1);
        \coordinate (P4) at (-3,  1);

        \fill[blu] (P1) circle (3pt);
        \fill[blu] (P2) circle (3pt);
        \fill[blu] (P3) circle (3pt);
        \fill[blu] (P4) circle (3pt);

        \draw[overlay, red, <-, thick, shorten <=4pt, visible on=<2->]
        (P1.south) |- ++(3mm, -4mm) node[right]
        {$T(1, 1)=-1\,\si{\degreeCelsius}$};

        \draw[overlay, red, <-, thick, shorten <=4pt, visible on=<3->]
        (P2.south) |- ++(3mm, -4mm) node[right]
        {$T(2, -1)=6\,\si{\degreeCelsius}$};

        \draw[overlay, red, <-, thick, shorten <=4pt, visible on=<4->]
        (P3.west) -- ++(-5mm, 0) node[left]
        {$T(0, -1)=2\,\si{\degreeCelsius}$};

        \draw[overlay, red, <-, thick, shorten <=4pt, visible on=<5->]
        (P4.south) |- ++(-3mm, -4mm) node[left]
        {$T(-3, 1)=7\,\si{\degreeCelsius}$};
      \end{tikzpicture}
    \]
    \onslide<6->{What rate of change occurs when traveling northwest from
      $(1, 1)$?}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose the depth of a lake is $f(x, y)=-2\,x^2-y^2+400\,\si{\metre}$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        ]

        \filldraw [fill=blubg] plot [smooth cycle] coordinates
        {(-2,-2) (-2, 2) (1,2) (3,1) (3,0) (2,-1)};

        \coordinate (P1) at ( 0,  0);
        \coordinate (P2) at ( 1,  1);
        \coordinate (P3) at (-1,  2);
        \coordinate (P4) at (-1, -1);
        \coordinate (P5) at ( 1,  0);

        \fill[blu] (P1) circle (3pt);
        \fill[blu] (P2) circle (3pt);
        \fill[blu] (P3) circle (3pt);
        \fill[blu] (P4) circle (3pt);
        \fill[blu] (P5) circle (3pt);

        \draw[overlay, red, <-, thick, shorten <=4pt, visible on=<2->]
        (P1.south) |- ++(-3mm, -4mm) node[left]
        {$\scriptstyle f(0, 0)=400\,\si{\metre}$};

        \draw[overlay, red, <-, thick, shorten <=4pt, visible on=<3->]
        (P2.south) |- ++(2mm, -4mm) node[right]
        {$\scriptstyle f(1, 1)=397\,\si{\metre}$};

        \draw[overlay, red, <-, thick, shorten <=4pt, visible on=<4->]
        (P3.south) |- ++(3mm, -4mm) node[right]
        {$\scriptstyle f(-1, 2)=394\,\si{\metre}$};

        \draw[overlay, red, <-, thick, shorten <=4pt, visible on=<5->]
        (P4.south) |- ++(-16mm, -4mm) node[left]
        {$\scriptstyle f(-1, -1)=397\,\si{\metre}$};

        \draw[overlay, red, <-, thick, shorten <=4pt, visible on=<6->]
        (P5.south) |- ++(22mm, -4mm) node[right]
        {$\scriptstyle f(1, 0)=398\,\si{\metre}$};
      \end{tikzpicture}
    \]
    \onslide<7->{Which direction from $(1, 1)$ is depth increasing most
      rapidly?}
  \end{example}

\end{frame}


\section{Derivatives}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{derivative}} of $f:\mathbb{R}\to\mathbb{R}$ is
    \[
      f^\prime(x) = \lim_{h\to0}\frac{f(x+h)-f(x)}{h}
    \]
    provided this limit exists.
  \end{definition}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\ProductRule}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\displaystyle\Set{f\cdot g}^\prime=f^\prime\cdot g+f\cdot g^\prime$};
        \node[above, grn] at (text.north) {\scriptsize Product Rule};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\QuotientRule}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\displaystyle\Set*{\frac{f}{g}}^\prime=\frac{g\cdot f^\prime-f\cdot g^\prime}{g^2}$};
        \node[above, blu] at (text.north) {\scriptsize Quotient Rule};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\ChainRule}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=redbg, draw, rounded corners, ultra thick]
        (text) {$\displaystyle\frac{d}{dx}\Set*{f(g(x))}=f^\prime(g(x))\cdot g^\prime(x)$};
        \node[above, red] at (text.north) {\scriptsize Chain Rule};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\PowerRule}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\displaystyle\frac{d}{dx}x^a=ax^{a-1}$};
        \node[above, grn] at (text.north) {\scriptsize Power Rule};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\ExponentialRule}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\displaystyle\frac{d}{dx} b^x=b^x\ln(b)$};
        \node[above, blu] at (text.north) {\scriptsize Exponential Rule};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\LogRule}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=redbg, draw, rounded corners, ultra thick]
        (text) {$\displaystyle\frac{d}{dx}\log(x)=\frac{1}{x}$};
        \node[above, red] at (text.north) {\scriptsize Log Rule};
      \end{tikzpicture}
    \end{array}
  }
  \begin{theorem}[Differentiation Rules]
    Differentiation obeys the \grn{product}, \blu{quotient}, and \red{chain}
    rules
    \begin{gather*}
      \begin{align*}
        \ProductRule && \QuotientRule && \ChainRule
      \end{align*}
    \end{gather*}
    \pause Differentiation also obeys the \grn{power}, \blu{exponential}, and \red{log}
    rules
    \begin{gather*}
      \begin{align*}
        \PowerRule && \ExponentialRule && \LogRule
      \end{align*}
    \end{gather*}
    \pause It is common to write $\log(x)$ in place of $\ln(x)$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Trig Rules]
    Derivatives of common trig functions are given by
    \begin{gather*}
      \begin{align*}
        \frac{d}{dx}\sin(x) &= \cos(x)        & \frac{d}{dx}\cos(x) &= -\sin(x)        & \frac{d}{dx}\tan(x) &= \sec^2(x) \\
        \frac{d}{dx}\sec(x) &= \sec(x)\tan(x) & \frac{d}{dx}\csc(x) &= -\csc(x)\cot(x) & \frac{d}{dx}\cot(x) &= -\csc^2(x)
      \end{align*}
    \end{gather*}
    \pause Derivatives of a few inverse trig functions are given by
    \begin{gather*}
      \begin{align*}
        \frac{d}{dx}\arcsin(x) &= \frac{1}{\sqrt{1-x^2}} & \frac{d}{dx}\arccos(x) &= -\frac{1}{\sqrt{1-x^2}} & \frac{d}{dx}\arctan(x) &= \frac{1}{1+x^2}
      \end{align*}
    \end{gather*}
    \pause These identities follow from applying the chain rule to
    $f(f^{-1}(x))=x$.
  \end{theorem}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Interpretation}
    The scalar $f^\prime(a)$ is the slope of the line tangent to $f(x)$ at
    $x=a$.
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , visible on=<2->
        ]

        \coordinate (p) at (3*pi/4, {sqrt(2)/2+3/2});

        \draw[<->] ({0-1},0) -- ({2*pi+1},0) node [right] {$x$};
        \draw (3*pi/4, 3pt) -- (3*pi/4, -3pt) node[below] {$a$};

        \draw[blu, domain=0:2*pi, smooth, <->]
        plot (\x, {sin(\x r)+3/2}) node [above] {$f(x)$};

        \draw[red, ->, visible on=<4->] (p) -- ++($ 1.5*(1, {-sqrt(2)/2}) $)
        node[above, sloped, pos=0] {$\scriptscriptstyle L_a(x)=f(a)+f^\prime(a)\cdot(x-a)$};

        \draw[red, ->, visible on=<4->] (p) -- ++($ -1.5*(1, {-sqrt(2)/2}) $);

        \fill[blu, visible on=<3->] (3*pi/4, {sqrt(2)/2+3/2}) circle (3pt);

      \end{tikzpicture}
    \]
    \onslide<5->{The line $L_a(x)$ is called the \emph{\grn{local
          linearization}} of $f(x)$ at $x=a$.}
  \end{block}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The equation $L_a(x)=f(a)+f^\prime(a)\cdot(x-a)$ gives the approximation
    \[
      f(a+1)
      \approx L_a(a+1)
      = f(a)+f^\prime(a)\cdot(a+1-a)
      = f(a)+f^\prime(a)
    \]
    \onslide<2->{Increasing $a$ by one approximately increases $f(a)$ by
      $f^\prime(a)$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose temperature $t$ hours past \DTMdisplaytime{12}{00}{00} is
    $T(t)=3\,t^2+4\,t\,\si{\degreeCelsius}$.
    \begin{align*}
      T(2) &= 20\,\si{\degreeCelsius} & T^\prime(t) &= 6\,t+4\,\si{\celsius\per\hour} & T^\prime(2) &= 16\,\si{\celsius\per\hour}
    \end{align*}
    \pause From \DTMdisplaytime{14}{00}{00} to \DTMdisplaytime{15}{00}{00}
    temperature approximately \emph{\grn{increases}} by $16\,\si{\celsius}$.
  \end{example}

\end{frame}


\section{Partial Derivatives}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $T\in\mathscr{C}(\mathbb{R}^2)$ given by
    $T(x, y)=y^3+x^2-2\,xy\,\si{\degreeCelsius}$.
    \[
      \begin{tikzpicture}[
        , line cap=round
        , line join=round
        , ultra thick
        ]

        \coordinate (O) at (0, 0);
        \coordinate (p) at (3, 1);

        \draw[<->] (-1/2,  0) -- (6, 0) node[right] {$x$};
        \draw[<->] ( 0, -1/2) -- (0, 3) node[above] {$y$};

        \draw (3, 3pt) -- (3, -3pt) node[below] {$3$};
        \draw (3pt, 1) -- (-3pt, 1) node[left] {$1$};

        \draw[blu, ->, visible on=<4->] (p) -- ++(1, 0) node[right] {$?\,\si{\celsius\per\metre}$};
        \draw[grn, ->, visible on=<5->] (p) -- ++(0, 1) node[above] {$?\,\si{\celsius\per\metre}$};

        \node[circle, inner sep=0pt, outer sep=0pt, fill=black, minimum size=4pt] (c) at (p) {};

        \draw[<-, thick, red, overlay, visible on=<2->] (c.south) |- ++(-3mm, -2.5mm)
        node[left] {$T(3, 1)=4\,\si{\degreeCelsius}$};

      \end{tikzpicture}
    \]
    \onslide<3->{What is the rate of change of temperature due East or due
      North?}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{partial derivative}} of $f\in\mathscr{C}(\mathbb{R}^n)$ with
    respect to $x_i$ is
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , thick
        , remember picture
        ]
        \node {\(\displaystyle
          \subnode{def}{\textcolor<3->{red}{\frac{\partial f}{\partial x_i}}}
          = \lim_{h\to0} \frac{f(\bv[x]+h\cdot\subnode{ei}{\textcolor<2->{red}{\bv[e]_i}})-f(\bv[x])}{h}
          \)};

        \draw[<-, thick, red, overlay, visible on=<3->] (def.west) -- ++(-3mm, 0)
        node[left] {\scriptsize also written as $\frac{\partial f}{\partial x_i}=f_{x_i}$};

        \draw[<-, thick, red, overlay, visible on=<2->] (ei.north) |- ++(3mm, 2.5mm)
        node[right] {\scriptsize $i$th column of $I_n$};
      \end{tikzpicture}
    \]
    \onslide<4->{To calculate $f_{x_i}$, treat variables $\neq x_i$ as constants
      and differentiate.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The partial derivatives of $f(x_1, x_2)=x_1^2+x_2^2$ are
    \begin{align*}
      \frac{\partial f}{\partial x_1} &= 2\,x_1 & \frac{\partial f}{\partial x_2} &= 2\,x_2
    \end{align*}
    \pause The partial derivatives of $g(x, y, z)=x\sin(xyz)$ are
    \begin{align*}
      g_x &= xyz\cos(xyz)+\sin(xyz) & g_y &= x^2z\cos(xyz) & g_z &= x^2y\cos(xyz)
    \end{align*}
    \pause The partial derivatives of $h(r, s, t)=re^{r^2t}$ are
    \begin{align*}
      h_r &= 2\,r^2te^{r^2t}+e^{r^2t} & h_s &= 0 & h_t &= r^3e^{r^2t}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    The partial derivatives of $f(x, y, z)=yz\ln(1+x^2)$ are
    \begin{align*}
      f_x &= \frac{2\,xyz}{1+x^2} & f_y &= z\ln(1+x^2) & f_z &= y\ln(1+x^2)
    \end{align*}
    \pause The partial derivatives of $g(x, y)=(1+x^2)^{1+y^2}$ are
    \begin{align*}
      g_x &= (1+y^2)(1+x^2)^{y^2}(2\,x) & g_y &= (1+x^2)^{1+y^2}\ln(1+x^2)(2\,y)
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $T\in\mathscr{C}(\mathbb{R}^2)$ given by
    $T(x, y)=y^3+x^2-2\,xy\,\si{\degreeCelsius}$.
    \[
      \begin{tikzpicture}[
        , line cap=round
        , line join=round
        , ultra thick
        , scale=2/3
        ]

        \coordinate (O) at (0, 0);
        \coordinate (p) at (3, 1);

        \draw[<->] (-3/4,  0) -- (6, 0) node[right] {$x$};
        \draw[<->] ( 0, -3/4) -- (0, 3) node[above] {$y$};

        \draw (3, 3pt) -- (3, -3pt) node[below] {$3$};
        \draw (3pt, 1) -- (-3pt, 1) node[left] {$1$};

        \draw[blu, ->, visible on=<4->] (p) -- ++(1, 0) node[right] {$T_x(3, 1)=4\,\si{\celsius\per\metre}$};
        \draw[grn, ->, visible on=<5->] (p) -- ++(0, 1) node[above] {$T_y(3, 1)=-3\,\si{\celsius\per\metre}$};

        \node[circle, inner sep=0pt, outer sep=0pt, fill=black, minimum size=4pt] (c) at (p) {};

        \draw[<-, thick, red, overlay, visible on=<2->] (c.south) |- ++(-3mm, -3mm)
        node[left] {$\scriptstyle T(3, 1)=4\,\si{\degreeCelsius}$};

      \end{tikzpicture}
    \]
    \onslide<3->{The partial derivatives of $T$ are}
    \begin{align*}
      \onslide<3->{T_x} &\onslide<3->{=} \onslide<3->{2\,x-2\,y\,\si{\celsius\per\metre}} & \onslide<3->{T_y} &\onslide<3->{=} \onslide<3->{3\,y^2-2\,x\,\si{\celsius\per\metre}}
    \end{align*}
    \onslide<6->{Temperature is \emph{\blu{increasing}} due East and
      \emph{\grn{decreasing}} due North.}
  \end{example}

\end{frame}





\end{document}
