\documentclass[]{bmr}

\title{Linear Independence}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Linear Dependence}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We call a list of vectors $\ell=\Set{\bv_1,\bv_2,\dotsc,\bv_n}$
    \emph{\red{linearly dependent}} if
    \[
      c_1\cdot\bv_1+c_2\cdot\bv_2+\dotsc+c_n\cdot\bv_n=\bv[O]
    \]
    where at least one $c_i\neq0$.
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\Vector}[2][blu]{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat####3[####1]
      }
      ]
      ####2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol####1{1}
        \hlCol####1{2}
        \hlCol####1{3}
        \hlCol####1{4}
      }
      , code-after = {
        \begin{tikzpicture}[blu, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-5mm, 3mm) node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {}  0 &  0 & -5 &  0 \\
      {}  3 & -2 &  1 &  0 \\
      {} -1 & -1 & -2 & -2
    \end{bNiceMatrix}
  }
  \begin{example}
    This linear combination produces the zero vector
    \[
      \bxb<4->[grn]{4}\cdot\Vector<3->{0\\3\\-1}+\bxb<4->[grn]{6}\cdot\Vector<3->{0\\-2\\-1}+\bxb<4->[grn]{0}\cdot\Vector<3->{-5\\1\\-2}\bxb<4->[grn]{-5}\cdot\Vector<3->{0\\0\\-2}=\Vector<5->[red]{0\\0\\0}
    \]
    \onslide<2->{We could more elegantly write this equation as a matrix-vector
      product.}
    \[
      \onslide<2->{\MatrixA<3->\Vector<4->[grn]{4\\6\\0\\-5}=\Vector<5->[red]{0\\0\\0}}
    \]
    \onslide<6->{Here, $A$ has \emph{linearly dependent} columns and
      $\nv[margin, code-before = {\hlMat<7->[grn]}]{4 6 0 -5}\in\Null(A)$.}
  \end{example}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\VectorVa}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[blu, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv_1$};
        \end{tikzpicture}
      }
      ]
      -1\\ 3\\ 12
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVb}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv_2$};
        \end{tikzpicture}
      }
      ]
      -19\\ 17\\ 43
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVc}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[red, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv_3$};
        \end{tikzpicture}
      }
      ]
      -11\\ 9\\ 21
    \end{bNiceMatrix}
  }
  \newcommand{\VectorZ}{
    \begin{bNiceMatrix}[r]
      0\\ 0\\ 0
    \end{bNiceMatrix}
  }
  \begin{example}
    The list
    $\Set{\nv[small]{-1 3 12}, \nv[small]{-19 17 43}, \nv[small]{-11 9 21}}$ is
    linearly dependent because
    \[
      2\cdot\VectorVa<3->-3\cdot\VectorVb<4->+5\cdot\VectorVc<5->=\VectorZ
    \]
    \onslide<2->{We can view this dependence geometrically.}
    \[
      \begin{tikzpicture}[line cap=round, ->, rotate=-25, transform shape, visible on=<2->]
        \coordinate (O) at (0, 0);
        \coordinate (v1) at (0:1);
        \coordinate (v3) at (22:2);
        \coordinate (2v1) at ($ 2*(v1) $);
        \coordinate (5v3) at ($ 5*(v3) $);

        \draw[grn, ultra thick, visible on=<4->] (5v3) -- ($ (5v3)!0.333!(2v1) $)
        node[midway, below, sloped] {$\scriptstyle \bv_2$};

        \draw[blu, thick] (O) -- ($ 2*(v1) $)
        node[pos=0.75, below, sloped] {$\scriptstyle 2\cdot\bv_1$};

        \draw[grn, thick] (5v3) -- (2v1)
        node[pos=0.75, below, sloped] {$\scriptstyle 3\cdot\bv_2$};

        \draw[red, thick] (5v3) -- (O)
        node[pos=0.75, above, sloped] {$\scriptstyle 5\cdot\bv_3$};

        \draw[blu, ultra thick, visible on=<3->] (O) -- (v1)
        node[midway, below, sloped] {$\scriptstyle \bv_1$};

        \draw[red, ultra thick, visible on=<5->] (5v3) -- ++($ -1*(v3) $)
        node[midway, above, sloped] {$\scriptstyle \bv_3$};

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}

\section{Linear Independence}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We call a list of vectors $\ell=\Set{\bv_1,\bv_2,\dotsc,\bv_n}$
    \emph{\grn{linearly independent}} if
    \[
      c_1\cdot\bv_1+c_2\cdot\bv_2+\dotsc+c_n\cdot\bv_n=\bv[O]
    \]
    only works when $c_1=c_2=\dotsb=c_n=0$.
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{NiceArray}{rcrcrcr}[
      , small
      , code-before = {
        \hlCol<3>[blu, th]{1}
        \hlCol<4>[blu, th]{3}
        \hlCol<5>[blu, th]{5}
        \hlPortion<7>[blu, th]{1}{1}{4}{5}
      }
      ]
      {} 6\,c_1 &+& 8\,c_2 &+& 9\,c_3 &=& 0 \\
      {} 3\,c_1 &+& 3\,c_2 &+& 6\,c_3 &=& 0 \\
      {} 5\,c_1 &+& 4\,c_2 &+& 2\,c_3 &=& 0 \\
      {} 7\,c_1 &+& 9\,c_2 &+& 2\,c_3 &=& 0
    \end{NiceArray}
  }
  \newcommand{\MatrixM}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , first-row
      , code-before = {
        \hlPortion<7,10>[blu, th]{1}{1}{4}{3}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, blu, visible on=<10->]
          \draw ($ (row-5-|col-1)!0.5!(row-5-|col-4) $) |- ++(3mm, -2.5mm)
          node[right] {$\scriptstyle\Null(A)=\Set{\bv[O]}$};
        \end{tikzpicture}
      }
      ]
      c_1 & c_2 & c_3 &   \\
      6   &   8 &   9 & 0 \\
      3   &   3 &   6 & 0 \\
      5   &   4 &   2 & 0 \\
      7   &   9 &   2 & 0
    \end{bNiceArray}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , first-row
      ]
      c_1 & c_2 & c_3 &   \\
      1   &   0 &   0 & 0 \\
      0   &   1 &   0 & 0 \\
      0   &   0 &   1 & 0 \\
      0   &   0 &   0 & 0
    \end{bNiceArray}
  }
  \newcommand<>{\VectorV}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1}
      ]
      6 & 3 & 5 & 7
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorW}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1}
      ]
      8 & 3 & 4 & 9
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorX}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1}
      ]
      9 & 6 & 2 & 2
    \end{bNiceMatrix}^\intercal
  }
  \begin{example}
    Let's determine if $\ell=\Set{\bv_1,\bv_2,\bv_3}$ is linearly independent
    where
    \begin{align*}
      \bv_1 &= \VectorV<3> & \bv_2 &= \VectorW<4> & \bv_3 &= \VectorX<5>
    \end{align*}
    \onslide<2->{The equation $c_1\cdot\bv_1+c_2\cdot\bv_2+c_3\cdot\bv_3=\bv[O]$ gives a system}
    \begin{align*}
      \onslide<2->{\System} && \onslide<8->{\rref}\onslide<6->{\MatrixM}\onslide<8->{=\MatrixR}
    \end{align*}
    \onslide<9->{This forces $c_1=c_2=c_3=0$, so $\ell$ is linearly
      independent!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{bNiceArray}{rrr|r}[small, first-row]
      {} c_1 & c_2 & c_3 &   \\
      {}   1 &   0 &   1 & 0 \\
      {}  -1 &   1 &   0 & 0 \\
      {}   0 &  -1 &   1 & 0
    \end{bNiceArray}
  }
  \newcommand{\SystemR}{
    \begin{bNiceArray}{rrr|r}[
      , small
      , first-row
      , code-before = {
        \hlRow<12->[red, th]{1}
        \hlRow<12->[red, th]{2}
        \hlRow<12->[red, th]{3}
      }
      , code-after = {
        \begin{tikzpicture}[red, thick, <-, shorten <=0.5mm, visible on=<12->]
          \draw ($ (row-2-|col-5)!0.5!(row-3-|col-5) $) -- ++(7.5mm, 0)
          node[right] (eq) {$\scriptstyle c_1=c_2=c_3=0$};
          \draw ($ (row-1-|col-5)!0.5!(row-2-|col-5) $) -| (eq.north west);
          \node[below] (text) at (eq) {\scriptsize ($\ell$ is independent!)};
          \draw ($ (row-3-|col-5)!0.5!(row-4-|col-5) $) -- (text);
        \end{tikzpicture}
      }
      ]
      {} c_1 & c_2 & c_3 &   \\
      {}   1 &   0 &   0 & 0 \\
      {}   0 &   1 &   0 & 0 \\
      {}   0 &   0 &   1 & 0
    \end{bNiceArray}
  }
  \begin{example}
    Suppose $\Set{\bv_1,\bv_2,\bv_3}$ is independent and consider
    $\ell=\Set{\bv[w]_1,\bv[w]_2,\bv[w]_3}$ where
    \begin{align*}
      {\color<3->{blu}\bv[w]_1=\bv_1-\bv_2} && {\color<4->{red}\bv[w]_2=\bv_2-\bv_3} && {\color<5->{grn}\bv[w]_3=\bv_3+\bv_1}
    \end{align*}
    Is $\ell$ is independent? \onslide<2->{Consider
      $c_1\cdot{\color<3->{blu}\bv[w]_1}+c_2\cdot{\color<4->{red}\bv[w]_2}+c_3\cdot{\color<5->{grn}\bv[w]_3}=\bv[O]$,
      which is}
    \[
      \begin{tikzpicture}[thick, <-, shorten <=0.5mm, remember picture, visible on=<2->]
        \node {
          $
          \begin{aligned}
            c_1\cdot(\subnode{w1}{\color<3->{blu}\bv_1-\bv_2})+
            c_2\cdot(\subnode{w2}{\color<4->{red}\bv_2-\bv_3})+
            c_3\cdot(\subnode{w3}{\color<5->{grn}\bv_3+\bv_1}) =\bv[O] \\
            \onslide<6->{
              (\subnode{eq1}{\color<8->{blu}c_1+c_3})\cdot\bv_1+
              (\subnode{eq2}{\color<9->{red}-c_1+c_2})\cdot\bv_2+
              (\subnode{eq3}{\color<10->{grn}-c_2+c_3})\cdot\bv_3 = \bv[O]
            }
          \end{aligned}
          $
        };

        \draw[blu, overlay, visible on=<3->] (w1.north) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[w]_1$};
        \draw[red, overlay, visible on=<4->] (w2.north) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[w]_2$};
        \draw[grn, overlay, visible on=<5->] (w3.north) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[w]_3$};

        \draw[blu, overlay, visible on=<8->] (eq1.south) |- ++(-3mm, -2.5mm) node[left] {$\scriptstyle c_1+c_3=0$};
        \draw[red, overlay, visible on=<9->] (eq2.south) |- ++(-3mm, -2.5mm) node[left] {$\scriptstyle -c_1+c_2=0$};
        \draw[grn, overlay, visible on=<10->] (eq3.south) |- ++(-3mm, -2.5mm) node[left] {$\scriptstyle -c_2+c_3=0$};
      \end{tikzpicture}
    \]
    \onslide<7->{Since $\Set{\bv_1,\bv_2,\bv_3}$ is independent, these
      coefficients must equal zero.}
    \[
      \onslide<11->{\rref\System=\SystemR}
    \]
  \end{example}

\end{frame}


\section{Rank and Independence}
\subsection{Theorem Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlCol<3->[blu, th]{1}
        \hlCol<3->[blu, th]{2}
        \hlCol<3->[blu, th]{3}
      }
      , code-after = {
        \begin{tikzpicture}[blu, <-, thick, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-5-|col-3)!0.5!(row-5-|col-4) $) |- ++(3mm, -2.5mm)
          node[right] (text) {\scriptsize\textnormal{independent columns!}};
          \draw ($ (row-5-|col-2)!0.5!(row-5-|col-3) $) |- (text);
          \draw ($ (row-5-|col-1)!0.5!(row-5-|col-2) $) |- (text);
          \node[below] at (text) {$\scriptstyle (\nullity=0)$};
        \end{tikzpicture}
      }
      ]
      {} -3 &  1 & -1 \\
      {}  1 &  2 & -6 \\
      {} -2 &  0 &  1 \\
      {}  3 & -1 &  8
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlEntry<2->[blu, th]{1}{1}
        \hlEntry<2->[blu, th]{2}{2}
        \hlEntry<2->[blu, th]{3}{3}
      }
      ]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1 \\
      0 & 0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlCol<6->[red, th]{1}
        \hlCol<6->[red, th]{2}
        \hlCol<6->[red, th]{3}
      }
      , code-after = {
        \begin{tikzpicture}[red, <-, thick, shorten <=0.5mm, visible on=<6->]
          \draw ($ (row-5-|col-3)!0.5!(row-5-|col-4) $) |- ++(3mm, -2.5mm)
          node[right] (text) {\scriptsize\textnormal{dependent columns!}};
          \draw ($ (row-5-|col-2)!0.5!(row-5-|col-3) $) |- (text);
          \draw ($ (row-5-|col-1)!0.5!(row-5-|col-2) $) |- (text);
          \node[below] at (text) {$\scriptstyle (\nullity=1>0)$};
        \end{tikzpicture}
      }
      ]
      {} -14 & 14 & -28 \\
      {}  -5 &  6 & -11 \\
      {}  -5 &  5 & -10 \\
      {}   3 & -4 &   7
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixBR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlEntry<4->[blu, th]{1}{1}
        \hlEntry<4->[blu, th]{2}{2}
        \hlCol<5->[red, th]{3}
      }
      , code-after = {
        \begin{tikzpicture}[red, <-, thick, shorten <=0.5mm, visible on=<5->]
          \draw ($ (row-1-|col-3)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \Col_1-\Col_2-\Col_3=\bv[O]$};
        \end{tikzpicture}
      }
      ]
      1 & 0 &  1 \\
      0 & 1 & -1 \\
      0 & 0 &  0 \\
      0 & 0 &  0
    \end{bNiceMatrix}
  }
  \begin{theorem}[The Linear Independence Test]
    The columns of $A$ are linearly independent if and only if $\nullity(A)=0$.
    \begin{align*}
      \rref\MatrixA=\MatrixAR && \rref\MatrixB=\MatrixBR \\
      \phantom{foo bar!}
    \end{align*}
    \onslide<7->{Equivalently, having independent columns means having full
      column rank.}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Matrix}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<2>{1}
        \hlCol<2>{2}
        \hlCol<2>{3}
        \hlCol<2>{4}
        \hlRow<4->[red]{1}
        \hlRow<4->[red]{2}
        \hlRow<4->[red]{3}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=1.5mm, visible on=<4->]
          \draw ($ (row-2-|col-5)!0.5!(row-3-|col-5) $) -- ++(5mm, 0)
          node[right] (rank) {$\scriptstyle\rank\leq 3$};
          \draw ($ (row-1-|col-5)!0.5!(row-2-|col-5) $) -| ($ (rank.north west)!0.25!(rank.north east) $);
          \draw ($ (row-3-|col-5)!0.5!(row-4-|col-5) $) -| ($ (rank.south west)!0.25!(rank.south east) $);
        \end{tikzpicture}
      }
      ]
      4 & 8 & 2 & 7 \\
      5 & 4 & 2 & 1 \\
      1 & 2 & 2 & 9
    \end{bNiceMatrix}
  }
  \begin{example}
    Let's determine if $\ell=\Set{\bv_1, \bv_2, \bv_3, \bv_4}$ is linearly independent where
    \begin{align*}
      \bv_1=\nv{4 5 1} && \bv_2=\nv{8 4 2} && \bv_3=\nv{2 2 2} && \bv_4=\nv{7 1 9}
    \end{align*}
    \onslide<2->{To do so, we can insert these vectors into the columns of a
      matrix}
    \[
      \onslide<2->{\Matrix}
    \]
    \onslide<5->{This matrix is \emph{not full column rank}, so $\ell$ is
      dependent!}
  \end{example}

\end{frame}


\subsection{Consequences}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlCol<3->[blu, th]{1}
        \hlCol<3->[blu, th]{2}
        \hlCol<3->[blu, th]{4}
        \hlCol<3->[blu, th]{6}
        \hlCol<3->[blu, th]{7}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-7-|col-7)!0.5!(row-7-|col-8) $) |- ++(3mm, -2.5mm)
          node[right] (text) {\scriptsize \textnormal{linearly independent!}};
          \draw ($ (row-7-|col-1)!0.5!(row-7-|col-2) $) |- (text);
          \draw ($ (row-7-|col-2)!0.5!(row-7-|col-3) $) |- (text);
          \draw ($ (row-7-|col-4)!0.5!(row-7-|col-5) $) |- (text);
          \draw ($ (row-7-|col-6)!0.5!(row-7-|col-7) $) |- (text);
        \end{tikzpicture}
      }
      ]
      {} 11 &  63 & -208 &  9 & -111 &  329 & -197 & -475 \\
      {}  3 &  16 &  -52 &  3 &  -29 &   86 &  -53 & -120 \\
      {} -5 & -29 &   96 & -4 &   51 & -151 &   90 &  219 \\
      {} -4 & -16 &   48 & -8 &   36 & -103 &   72 &  118 \\
      {}  4 &  17 &  -52 &  7 &  -36 &  106 &  -70 & -131 \\
      {}  4 &  17 &  -52 &  0 &  -22 &   76 &  -54 & -103
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlEntry<2->[blu, th]{1}{1}
        \hlEntry<2->[blu, th]{2}{2}
        \hlEntry<2->[blu, th]{3}{4}
        \hlEntry<2->[blu, th]{4}{6}
        \hlEntry<2->[blu, th]{5}{7}
      }
      ]
      1 & 0 &  4 & 0 &  3 & 0 & 0 & -2 \\
      0 & 1 & -4 & 0 & -2 & 0 & 0 & -3 \\
      0 & 0 &  0 & 1 & -2 & 0 & 0 &  0 \\
      0 & 0 &  0 & 0 &  0 & 1 & 0 & -2 \\
      0 & 0 &  0 & 0 &  0 & 0 & 1 & -2 \\
      0 & 0 &  0 & 0 &  0 & 0 & 0 &  0
    \end{bNiceMatrix}
  }
  \begin{theorem}
    The pivot columns of a matrix are linearly independent.
    \[
      \rref\MatrixA=\MatrixR
    \]
    \onslide<5->{The rank is the max number of independent vectors among the
      columns.}
  \end{theorem}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlCol<2->[red]{3}}
      , code-after = {
        \begin{tikzpicture}[thick, <-, red, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-3)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \Col_3=\Col_1+\Col_2$};
        \end{tikzpicture}
      }
      ]
      1 & 3 & 4 \\
      9 & 0 & 9 \\
      4 & 2 & 6 \\
      1 & 6 & 7
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAp}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-after = {\trCol<5->{3}}
      ]
      1 & 3 & 4 \\
      9 & 0 & 9 \\
      4 & 2 & 6 \\
      1 & 6 & 7
    \end{bNiceMatrix}
  }
  \newcommand{\Figure}{
    \begin{tikzpicture}[
      , anchor=base
      , baseline
      , line cap=round
      , ->
      , ultra thick
      , rotate=-25
      , transform shape
      , visible on=<6->
      ]

      \pgfmathsetmacro{\vnorm}{1.5}
      \pgfmathsetmacro{\wnorm}{\vnorm * 1.421}
      \coordinate (O) at (0, 0);
      \coordinate (v1) at (0:\vnorm);
      \coordinate (v2) at (75:\wnorm);
      \coordinate (v3) at ($ (v1)+(v2) $);

      \draw[blu] (O) -- (v1) node[midway, below, sloped] {$\scriptstyle \Col_1$};
      \draw[grn] (v1) -- ++(v2) node[midway, below, sloped] {$\scriptstyle \Col_2$};
      \draw[red, visible on=<7->] (O) -- (v3) node[midway, above, sloped] {$\scriptstyle \Col_3$};

    \end{tikzpicture}
  }
  \begin{example}
    The nonpivot columns \emph{depend} on the independent pivot columns.
    \begin{align*}
      \onslide<4->{\Col}\MatrixA\onslide<4->{=\Col\MatrixAp} && \Figure
    \end{align*}
    \onslide<3->{Deleting the nonpivot columns does not change the column
      space!}
  \end{example}

\end{frame}

\end{document}
