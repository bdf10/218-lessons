\documentclass[]{bmr}

\title{Linear Systems}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Basic Vocabulary}
\subsection{Systems and Consistency}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{NiceArray}{rcrcccrcr}
      a_{11}\,x_1 &+& a_{12}\,x_2 &+& \Cdots &+& a_{1n}\,x_n &=& b_1    \\
      a_{21}\,x_1 &+& a_{22}\,x_2 &+& \Cdots &+& a_{2n}\,x_n &=& b_2    \\
      \Vdots      & & \Vdots      & & \Ddots & & \Vdots      & & \Vdots \\
      a_{m1}\,x_1 &+& a_{m2}\,x_2 &+& \Cdots &+& a_{mn}\,x_n &=& b_m
    \end{NiceArray}
  }
  \newcommand{\Solution}{
    \begin{bNiceMatrix}
      x_1\\ x_2\\ \Vdots\\ x_n
    \end{bNiceMatrix}
  }
  \begin{definition}
    Consider a system of \emph{\grn{$m$ linear equations}} with \emph{\grn{$n$
        variables}}.
    \begin{align*}
      \System && \onslide<2->{\bv[x]=\Solution}
    \end{align*}
    \onslide<3->{A \emph{\grn{solution}} is a vector $\bv[x]$ whose coordinates
      solve each equation.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{NiceArray}{rcrcrcr}
      x_1     &-& 2\,x_2 &-&    x_3 &=&  1 \\
      -4\,x_1 &+& 9\,x_2 &+& 5\,x_3 &=& -2 \\
      -2\,x_1 &+& 9\,x_2 &+& 7\,x_3 &=&  8 \\
      -x_1    &+& 6\,x_2 &+& 5\,x_3 &=&  7
    \end{NiceArray}
  }
  \newcommand{\SolutionA}{
    \begin{bNiceMatrix}[r]
      5\\ 2\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\SolutionB}{
    \begin{bNiceMatrix}[r]
      6\\ 3\\ -1
    \end{bNiceMatrix}
  }
  \begin{definition}
    A system is \emph{\grn{consistent}} if it has at least one solution. \pause
    \begin{align*}
      \System && \bv[x]_1 = \SolutionA && \bv[x]_2 = \SolutionB
    \end{align*}
    \pause This system is consistent because each of $\bv[x]_1$ and $\bv[x]_2$
    is a solution.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A system is \emph{\red{inconsistent}} if it has no solutions. \pause
    \[
      \begin{NiceArray}{rcrcrcr}
        {} -5\,x_1 &+& 4\,x_2 &-& x_3 &=&  3 \\
        {} -4\,x_1 &+&    x_2 &-& x_3 &=& -1 \\
        {}  2\,x_1 &-& 2\,x_2 &+& x_3 &=&  2 \\
        {}  7\,x_1 &-& 3\,x_2 &+& x_3 &=& -3
      \end{NiceArray}
    \]
    Summing these equations gives $0=1$, so this system is inconsistent!
  \end{definition}

\end{frame}


\subsection{Matrix Representations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{NiceArray}{rcrcrcr}[
      , margin
      , code-before = {
        \hlPortion<3->{1}{1}{4}{5}
        \hlCol<6->[red]{7}
      }
      ]
      {}  3\,x_1 &+& 7\,x_2 &-& 4\,x_3  &=& 11\phantom{.} \\
      {}  6\,x_1 & &        &+& 9\,x_3  &=& -3\phantom{.} \\
      {}  2\,x_1 &+& 9\,x_2 & &         &=&  2\phantom{.} \\
      {} -7\,x_1 &+& 4\,x_2 &-& 12\,x_3 &=&  7\phantom{.}
    \end{NiceArray}
  }
  \newcommand{\Augment}{
    \begin{bNiceArray}{rrr|r}[
      , first-row
      , margin
      , code-before = {
        \hlPortion<2->{1}{1}{4}{3}
        \hlCol<5->[red]{4}
      }
      , code-after = {
        \tikz\draw[<-, thick, blu, shorten <=6pt, visible on=<2->]
        (4-2.south) |- ++(-1,-5mm) node[left] {\emph{coefficient matrix}};
        \tikz\draw[<-, thick, red, shorten <=6pt, visible on=<5->]
        (4-4.south) |- ++(-0.125,-10mm) node[left] {\emph{augmented column}};
      }
      ]
      {} \onslide<4->{x_1} & \onslide<4->{x_2} & \onslide<4->{x_3} &    \\
      {}   3 &   7 &  -4 & 11 \\
      {}   6 &   0 &   9 & -3 \\
      {}   2 &   9 &   0 &  2 \\
      {}  -7 &   4 & -12 &  7
    \end{bNiceArray}
  }
  \begin{definition}
    Every system is represented by an \emph{\grn{augmented matrix}}.
    \begin{align*}
      \System && \leftrightsquigarrow && \Augment
    \end{align*}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{NiceArray}{rcrcr}[
      , margin
      , code-before = {
      \hlPortion<3->{1}{1}{4}{3}
      \hlCol<5->[red]{5}
      }
      ]
      {}  7\,x_1 &+& 9\,x_2 &=& 11\phantom{.} \\
      {} -3\,x_1 &-& 3\,x_2 &=&  2\phantom{.} \\
      {} 12\,x_1 &-& 7\,x_2 &=&  8\phantom{.} \\
      {} -5\,x_1 &+& 2\,x_2 &=& 14\phantom{.}
    \end{NiceArray}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->}
      ]
      {}  7 &  9  \\
      {} -3 & -3  \\
      {} 12 & -7  \\
      {} -5 &  2 
    \end{bNiceMatrix}
  }
  \newcommand{\Vectorx}{
    \begin{bNiceMatrix}[r]
      x_1\\ x_2
    \end{bNiceMatrix}
  }
  \newcommand{\Vectorb}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<4->[red]}
      ]
      11\\ 2\\ 8\\ 14
    \end{bNiceMatrix}
  }
  \begin{definition}
    We can also represent a system with a \emph{\grn{matrix equation}}
    $A\bv[x]=\bv[b]$.
    \begin{align*}
      \System && \leftrightsquigarrow && \overset{A}{\MatrixA}\overset{\bv[x]}{\Vectorx}=\overset{\bv[b]}{\Vectorb}
    \end{align*}
  \end{definition}

\end{frame}


\section{Reduced Systems}
\subsection{Variable Types and Consistency}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider an augmented matrix in \emph{\grn{reduced row echelon form}}.
    \[
      \begin{bNiceArray}{rrrrr|r}[
        , margin
        , first-row
        , code-before = {
          \hlEntry<2->{1}{1}
          \hlEntry<3->{2}{2}
          \hlEntry<4->{3}{5}
          \hlCol<8->[grn]{3}
          \hlCol<8->[grn]{4}
        }
        , code-after = {
          \tikz\draw[<-, thick, blu, shorten <=4mm, visible on=<6->]
          (1-1.north) |- ++(4, 7mm) node[right] {$\text{\emph{dependent}}=\Set{x_1, x_2, x_5}$};
          \tikz\draw[<-, thick, blu, shorten <=4mm, visible on=<6->]
          (1-2.north) |- ++(3, 7mm);
          \tikz\draw[<-, thick, blu, shorten <=4mm, visible on=<6->]
          (1-5.north) |- ++(0.5, 7mm);
          \tikz\draw[<-, thick, grn, shorten <=4mm, visible on=<8->]
          (4-3.north) |- ++(3, -6mm) node[right] {$\text{\emph{free}}=\Set{x_3, x_4}$};
          \tikz\draw[<-, thick, grn, shorten <=4mm, visible on=<8->]
          (4-4.north) |- ++(1, -6mm);
        }
        ]
        x_1 & x_2 & x_3 & x_4 & x_5 &    \\
        1   &   0 &   5 &  -9 &   0 &  8 \\
        0   &   1 &  -3 &  10 &   0 & -5 \\
        0   &   0 &   0 &   0 &   1 &  7 \\
        0   &   0 &   0 &   0 &   0 &  0
      \end{bNiceArray}
    \]
    \onslide<5->{Variables in pivot columns are called \emph{\blu{dependent}}.}
    \onslide<7->{The others are called \emph{\grn{free}}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    A system is \emph{\red{inconsistent}} if the augmented column contains a
    pivot.
    \[
      \begin{bNiceArray}{rrrr|r}[
        , margin
        , first-row
        , code-before = {
          \hlEntry<2->{1}{1}
          \hlEntry<3->{2}{2}
          \hlEntry<4->[red]{3}{5}
          \hlRow<6->[red]{3}
        }
        , code-after = {
          \tikz \draw[<-, thick, red, shorten <=5pt, visible on=<5->]
          (3-5) -- ++(1.0, 0) node[right] {\emph{\red{inconsistent}}};
          \tikz \draw[<-, thick, red, shorten <=5mm, visible on=<6->]
          (3-1) -| ++(-3.0, 2mm) node[above] {this equation is $0=1$};
        }
        ]
        x_1 & x_2 & x_3 & x_4 &   \\
        1   &   0 &   3 &  -4 & 0 \\
        0   &   1 &  -7 &  -2 & 0 \\
        0   &   0 &   0 &   0 & 1 \\
        0 & 0 & 0 & 0 & 0
      \end{bNiceArray}
    \]
    \onslide<7->{A system is \emph{\grn{consistent}} if the augmented column
      contains no pivot.}
  \end{block}

\end{frame}


\subsection{Rouch\'e-Capelli Theorem}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{bNiceArray}{rrrrr|r}[
      , first-row
      , margin
      , code-before = {
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<4->{3}{5}
        \hlCol<7->[grn]{3}
        \hlCol<7->[grn]{4}
      }
      , code-after = {
        \tikz \draw[<-, thick, blu, shorten <=4mm, visible on=<5->]
        (1-1.north) |- ++(4, 7mm) node[below right=-4mm and 1mm]
        {$
          \begin{aligned}
            \text{\emph{dependent}} &= \Set{x_1, x_2, x_5} \\
            \rank(A) &= 3
          \end{aligned}
          $};
        \tikz \draw[<-, thick, blu, shorten <=4mm, visible on=<5->]
        (1-2.north) |- ++(1, 7mm);
        \tikz \draw[<-, thick, blu, shorten <=4mm, visible on=<5->]
        (1-5.north) |- ++(1, 7mm);
        \tikz \draw[<-, thick, grn, shorten <=2.5mm, visible on=<7->]
        (3-3.south) |- ++(2, -5mm) node[below right=-4mm and -10mm]
        {$
          \begin{aligned}
            \text{\emph{free}} &= \Set{x_3,x_4} \\
            \nullity(A) &= 2
          \end{aligned}
          $};
        \tikz \draw[<-, thick, grn, shorten <=2.5mm, visible on=<7->]
        (3-4.south) |- ++(1.0, -5mm);
      }
      ]
      x_1 & x_2 & x_3 & x_4 & x_5 &    \\
      1   &   0 &   3 &  -5 &   0 &  2 \\
      0   &   1 &  -4 &  -9 &   0 &  7 \\
      0   &   0 &   0 &   0 &   1 &  1
    \end{bNiceArray}
  }
  \begin{block}{Observation}
    The \emph{\blu{rank}} of the coefficient matrix is the \emph{\blu{number of
        dependent variables}}.
    \[
      \System
      \vspace{10mm}
    \]
    \onslide<6->{The \emph{\grn{nullity}} is the number of free variables.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\Inconsistent}{
    \onslide####1{
      \begin{array}{c}
        \stackanchor{\textnormal{\scriptsize\red{inconsistent}}}{\textnormal{\scriptsize\red{(no solutions)}}} \\
        \begin{bNiceArray}{rrr|r}[
          , margin
          , code-before = {
            \hlEntry{1}{1}
            \hlEntry{2}{2}
            \hlEntry[red]{3}{4}
          }
          ]
          1 & 0 & 3 & 0 \\
          0 & 1 & 2 & 0 \\
          0 & 0 & 0 & 1
        \end{bNiceArray} \\
        {\scriptstyle\color{white}foo}
      \end{array}
    }
  }
  \newcommand<>{\ConsistentOne}{
    \onslide####1{
      \begin{array}{c}
        \stackanchor{\textnormal{\scriptsize\grn{consistent}}}{\textnormal{\scriptsize\grn{(one solution)}}} \\
        \begin{bNiceArray}{rrr|r}[
          , margin
          , code-before = {
            \hlEntry{1}{1}
            \hlEntry{2}{2}
            \hlEntry{3}{3}
          }
          ]
          1 & 0 & 0 & 2 \\
          0 & 1 & 0 & 7 \\
          0 & 0 & 1 & 1
        \end{bNiceArray} \\
        \onslide<6->{{\scriptstyle\grn{\nullity(A)=0}}}
      \end{array}
    }
  }
  \newcommand<>{\ConsistentInfinite}{
    \onslide####1{
      \begin{array}{c}
        \stackanchor{\textnormal{\scriptsize\blu{consistent}}}{\textnormal{\scriptsize\blu{(infinite solutions)}}} \\
        \begin{bNiceArray}{rrr|r}[
          , margin
          , code-before = {
            \hlEntry{1}{1}
            \hlEntry{2}{3}
          }
          ]
          1 & 3 & 0 & 2 \\
          0 & 0 & 1 & 7 \\
          0 & 0 & 0 & 0
        \end{bNiceArray} \\
        \onslide<7->{{\scriptstyle\blu{\nullity(A) > 0}}}
      \end{array}
    }
  }
  \begin{theorem}[Rouch\'e-Capelli Theorem]
    Every rref system falls into one of the following three categories.
    \begin{align*}
      \Inconsistent<2-> && \ConsistentOne<3-> && \ConsistentInfinite<4->
    \end{align*}
    \onslide<5->{The number of solutions to a consistent system is controlled by
      the nullity.}
  \end{theorem}

\end{frame}



\section{Solving Reduced Systems}
\subsection{Procedure}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Solving Reduced Systems}
    Suppose $R\bv[x]=\bv[b]$ is reduced and consistent.
    \begin{description}[<+->]
    \item[Step 1] Set the free variables equal to scalars $c_1,c_2,\dotsc, c_k$.
    \item[Step 2] Write each dependent variable in terms of
      $c_1, c_2,\dotsc, c_k$.
    \item[Step 3] Express the full solution as
      $\bv[x]=\bv[x]_p+c_1\cdot\bv[x]_1+\dotsb+c_k\cdot\bv[x]_k$.
    \end{description}
  \end{block}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{bNiceArray}{rrrr|r}[
      , first-row
      , margin
      , code-before = {
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{3}
      }
      ]
      x_1 & x_2 & x_3 & x_4 &   \\
      1   &   3 &   0 &   6 & 7 \\
      0   &   0 &   1 &  -5 & 1 \\
    \end{bNiceArray}
  }
  \newcommand{\Equations}{
    \setlength{\arraycolsep}{0.5mm}
    \begin{NiceArray}{rcl}[
      , code-before = {
        \hlEntry<14-15>[grn]{1}{3}
        \hlEntry<20-21>[grn]{2}{3}
      }
      ]
      x_1 + 3\,\alt<6->{c_1}{x_2} + 6\,\alt<7->{c_2}{x_4} = 7 & \onslide<9->{ \to} & \onslide<9->{ x_1=7-3\,c_1-6\,c_2} \\
      x_3 - 5\,\alt<8->{c_2}{x_4}                         = 1 & \onslide<10->{\to} & \onslide<10->{x_3=1+5\,c_2}
    \end{NiceArray}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<13-15>[grn]{1}
        \hlRow<16-18>[grn]{2}
        \hlRow<19-21>[grn]{3}
        \hlRow<22-24>[grn]{4}
      }
      ]
      x_1\\ x_2\\ x_3\\ x_4
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXs}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<13-15>[grn]{1}
        \hlRow<16-18>[grn]{2}
        \hlRow<19-21>[grn]{3}
        \hlRow<22-24>[grn]{4}
      }
      ]
      \onslide<15->{7-3\,c_1-6\,c_2} \\
      \onslide<18->{c_1}             \\
      \onslide<21->{1+5\,c_2}        \\
      \onslide<24->{c_2}
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXp}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlCol<26-27>[grn]{1}}
      ]
      \onslide<27->{7}\\ \onslide<27->{0}\\ \onslide<27->{1}\\ \onslide<27->{0}
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXa}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlCol<28-29>[grn]{1}}
      ]
      \onslide<29->{-3}\\ \onslide<29->{1}\\ \onslide<29->{0}\\ \onslide<29->{0}
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXb}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlCol<30-31>[grn]{1}}
      ]
      \onslide<31->{-6}\\ \onslide<31->{0}\\ \onslide<31->{5}\\ \onslide<31->{1}
    \end{bNiceMatrix}
  }
  \begin{example}
    In this system, $\Set{x_1, x_3}$ are dependent and $\Set{x_2,x_4}$ are free.
    \begin{align*}
      \System && \onslide<4->{\Equations}
    \end{align*}
    \onslide<5->{To solve the system, start with $\bxb<17-18>[grn]{x_2=c_1}$ and
      $\bxb<23-24>[grn]{x_4=c_2}$.}  \onslide<11->{The full solution is}
    \[
      \onslide<11->{\bv[x] = \VectorX }
      \onslide<12->{       = \VectorXs}
      \onslide<25->{       = \overset{\bv[x]_p}{\VectorXp} + c_1\cdot\overset{\bv[x]_1}{\VectorXa} + c_2\cdot\overset{\bv[x]_2}{\VectorXb}}
    \]
    \onslide<32->{This system has \emph{infinitely many solutions}.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\System}{
    \begin{bNiceArray}{rrr|r}[
      , first-row
      , margin
      , code-before = {
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<4->{3}{3}
      }
      ]
      x & y & z &    \\
      1 & 0 & 0 & -3 \\
      0 & 1 & 0 &  2 \\
      0 & 0 & 1 &  9 \\
      0 & 0 & 0 &  0
    \end{bNiceArray}
  }
  \begin{example}
    This reduced row echelon form system has no free variables.
    \begin{align*}
      \System && \onslide<5->{\bv[x] = \nvc{x; y; z} = \nvc{-3; 2; 9}}
    \end{align*}
    \onslide<6->{Our system is consistent with \emph{\grn{exactly one solution}}.}
  \end{example}

\end{frame}




\section{Row Echelon Form Systems}
\subsection{Rouch\'e-Capelli Theorem}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Consider an augmented matrix in \emph{\grn{row echelon form}}.
    \[
      \begin{bNiceArray}{rrrrr|r}[
        , margin
        , first-row
        , code-before = {
          \hlEntry<2->{1}{1}
          \hlEntry<3->{2}{2}
          \hlEntry<4->{3}{5}
          \hlCol<7->[grn]{3}
          \hlCol<7->[grn]{4}
          \hlCol<8->[red]{6}
        }
        , code-after = {
          \tikz\draw[<-, thick, blu, shorten <=4mm, visible on=<6->]
          (1-1.north) |- ++(4, 7mm) node[right] {$\text{\emph{dependent}}=\Set{x_1, x_2, x_5}$};
          \tikz\draw[<-, thick, blu, shorten <=4mm, visible on=<6->]
          (1-2.north) |- ++(3, 7mm);
          \tikz\draw[<-, thick, blu, shorten <=4mm, visible on=<6->]
          (1-5.north) |- ++(0.5, 7mm);
          \tikz\draw[<-, thick, grn, shorten <=4mm, visible on=<7->]
          (4-3.north) |- ++(3, -6mm) node[right] {$\text{\emph{free}}=\Set{x_3, x_4}$};
          \tikz\draw[<-, thick, grn, shorten <=4mm, visible on=<7->]
          (4-4.north) |- ++(1, -6mm);
          \tikz\draw[<-, thick, red, shorten <=2mm, visible on=<8->]
          (4-6.east) -- ++(1, 0) node[right] {\emph{consistent}};
        }
        ]
        x_1 & x_2 & x_3 & x_4 & x_5 &    \\
        7   &  -3 &   5 &  -9 &   1 &  8 \\
        0   &   4 &  -3 &  10 &   2 & -5 \\
        0   &   0 &   0 &   0 &   8 &  7 \\
        0   &   0 &   0 &   0 &   0 &  0
      \end{bNiceArray}
    \]
    \onslide<5->{Our language of \emph{\blu{dependent}}, \emph{\grn{free}}, and
      \emph{\red{consistency}} directly carries over.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Inconsistent}{
    \begin{array}{c}
      \stackanchor{\textnormal{\scriptsize\red{inconsistent}}}{\textnormal{\scriptsize\red{(no solutions)}}} \\
      \begin{bNiceArray}{rrr|r}[
        , margin
        , code-before = {
          \hlEntry{1}{1}
          \hlEntry{2}{2}
          \hlEntry[red]{3}{4}
        }
        ]
        9 & 4 & 3 & 1 \\
        0 & 7 & 2 & 5 \\
        0 & 0 & 0 & 6
      \end{bNiceArray} \\
      {\scriptstyle\color{white}foo}
    \end{array}
  }
  \newcommand{\ConsistentOne}{
    \begin{array}{c}
      \stackanchor{\textnormal{\scriptsize\grn{consistent}}}{\textnormal{\scriptsize\grn{(one solution)}}} \\
      \begin{bNiceArray}{rrr|r}[
        , margin
        , code-before = {
          \hlEntry{1}{1}
          \hlEntry{2}{2}
          \hlEntry{3}{3}
        }
        ]
        4 & 9 & 1 & 2 \\
        0 & 7 & 2 & 7 \\
        0 & 0 & 8 & 1
      \end{bNiceArray} \\
      \onslide<3->{{\scriptstyle\grn{\nullity(A)=0}}}
    \end{array}
  }
  \newcommand{\ConsistentInfinite}{
    \begin{array}{c}
      \stackanchor{\textnormal{\scriptsize\blu{consistent}}}{\textnormal{\scriptsize\blu{(infinite solutions)}}} \\
      \begin{bNiceArray}{rrr|r}[
        , margin
        , code-before = {
          \hlEntry{1}{1}
          \hlEntry{2}{3}
        }
        ]
        5 & 3 & 7 & 2 \\
        0 & 0 & 2 & 7 \\
        0 & 0 & 0 & 0
      \end{bNiceArray} \\
      \onslide<4->{{\scriptstyle\blu{\nullity(A) > 0}}}
    \end{array}
  }
  \begin{theorem}[Rouch\'e-Capelli Theorem]
    The Rouch\'e-Capelli Theorem applies to systems in row echelon form.
    \begin{align*}
      \Inconsistent && \ConsistentOne && \ConsistentInfinite
    \end{align*}
    \onslide<2->{The number of solutions to a consistent system is controlled by
      the nullity.}
  \end{theorem}

\end{frame}


\subsection{Back Substitution}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

    \newcommand{\System}{
    \begin{bNiceArray}{rrr|r}[
      , first-row
      , margin
      , code-before = {
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
      }
      ]
      {} x_1 & x_2 & x_3 &    \\
      {}  -2 &  -7 &   9 & 12 \\
      {}   0 &   3 & -15 &  6
    \end{bNiceArray}
  }
  \newcommand{\Equations}{
    \setlength{\arraycolsep}{0.5mm}
    \begin{NiceArray}{rcrcrcrcl}[
      , code-before = {
        \hlEntry<19-20>[grn]{1}{9}
        \hlEntry<16-17>[grn]{2}{9}
        }
      ]
      {} -2\,x_1 &-& 7\,x_2 &+&  9\,\alt<6->{c_1}{x_3} &=& 12 &\onslide<9->{\to}& \onslide<9->{x_1=\frac{12+7\cdot(2+5\,c_1)-9\,c_1}{-2}} \\
      {}         & & 3\,x_2 &-& 15\,\alt<7->{c_1}{x_3} &=&  6 &\onslide<8->{\to}& \onslide<8->{x_2=2+5\,c_1}
    \end{NiceArray}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<18-20>[grn]{1}
        \hlRow<15-17>[grn]{2}
        \hlRow<12-14>[grn]{3}
      }
      ]
      x_1\\ x_2\\ x_3
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXs}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<18-20>[grn]{1}
        \hlRow<15-17>[grn]{2}
        \hlRow<12-14>[grn]{3}
      }
      ]
      \onslide<20->{-13-13\,c_1}\\ \onslide<17->{2+5\,c_1}\\ \onslide<14->{c_1}
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXp}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlCol<22-23>[grn]{1}}
      ]
      \onslide<23->{-13}\\ \onslide<23->{2}\\ \onslide<23->{0}
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXa}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlCol<24-25>[grn]{1}}
      ]
      \onslide<25->{-13}\\ \onslide<25->{5}\\ \onslide<25->{1}
    \end{bNiceMatrix}
  }
  \begin{example}
    Here, $\Set{x_1, x_2}$ are dependent and $x_3$ is free.
    \begin{gather*}
      \begin{align*}
        \System && \onslide<4->{\Equations}
      \end{align*}
    \end{gather*}
    \onslide<5->{To solve the system, start with $\bxb<13-14>[grn]{x_3=c_1}$.}
    \onslide<10->{The full solution is then}
    \[
      \onslide<10->{\bv[x] = \VectorX}
      \onslide<11->{= \VectorXs}
      \onslide<21->{= \overset{\bv[x]_p}{\VectorXp} + c_1\cdot\overset{\bv[x]_1}{\VectorXa}}
    \]
    \onslide<26->{This method is called \emph{back substitution}.}
  \end{example}

\end{frame}

\end{document}
