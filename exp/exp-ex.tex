\documentclass[]{article}

\usepackage{mth}
\usepackage{sgtx}
\usepackage{fullpage}
\usepackage{extarrows}
\usepackage{resizegather}

\theoremstyle{definition}
\newtheorem*{problem}{Problem}

\title{Matrix Exponentials Example}
\author{Math 218D-2}
\date{}

\begin{document}

\maketitle

\begin{sagesilent}
  A = matrix(ZZ, [[-4, -1, -1], [11, 1, 2], [15, 5, 4]])
  u0 = vector([2, 0, -10])
\end{sagesilent}

\begin{problem}
  Consider the linear system of ordinary differential equations given by
  \[
    \begin{NiceArray}{rcrcrcrcrcr}
      u_1^\prime &=& -4\,u_1 &-&    u_2 &-&    u_3 && u_1(0) &=& \sage{u0[0]} \\
      u_2^\prime &=& 11\,u_1 &+&    u_2 &+& 2\,u_3 && u_2(0) &=& \sage{u0[1]} \\
      u_3^\prime &=& 15\,u_1 &+& 5\,u_2 &+& 4\,u_3 && u_3(0) &=& \sage{u0[2]}
    \end{NiceArray}
  \]
  Find the solution $\bv[u](t)=\nv{u_1(t) u_2(t) u_3(t)}$ to this initial value
  problem.
\end{problem}

\begin{proof}[Solution]
  This initial value problem is of the form $\bv[u]^\prime=A\bv[u]$ with
  $\bv[u](0)=\bv[u]_0$ where
  \begin{align*}
    A &= \sage{A} & \bv[u]_0 &= \sage{u0.column()}
  \end{align*}
  The solution is given by $\bv[u](t)=\exp(At)\bv[u]_0$. To find this solution,
  we hope that $A$ diagonalizes as $A=XDX^{-1}$, which would allow us to write
  $\bv[u](t)=X\exp(Dt)X^{-1}\bv[u]_0$.

  To diagonalize $A$, we start by calculating the characteristic polynomial
  \begin{sagesilent}
    var('t')
    chi = t*identity_matrix(A.nrows())-A
    from functools import partial
    e = partial(elementary_matrix, chi.nrows())
    chi1 = e(row1=2, row2=0, scale=5)*chi
    chi2 = expand(e(row1=2, scale=1/(t+1))*chi1)
    chi3 = e(row1=0, row2=2, scale=-1)*e(row1=1, row2=2, scale=2)*chi2
    chi4 = chi3.matrix_from_rows_and_columns([0, 1], [0, 1])
  \end{sagesilent}
  \newcommand{\StepA}{
    \begin{NiceArray}{rcrcr}[small]
      \bv[r]_1 &-&       \bv[r]_3 &\to& \bv[r]_1 \\
      \bv[r]_2 &+& 2\cdot\bv[r]_3 &\to& \bv[r]_1 \\
    \end{NiceArray}
  }
  \begin{align*}
    \chi_A(t)
    = \sage{detmat(chi)}                                                \\
    \xlongequal{\bv[r]_3+5\cdot\bv[r]_1\to\bv[r]_3} \sage{detmat(chi1)} \\
    = (t+1)\cdot\sage{detmat(chi2)}                                     \\
    \xlongequal{\StepA}(t+1)\cdot\sage{detmat(chi3)}                    \\
    = (t+1)\cdot\sage{detmat(chi4)}                                     \\
    = (t+1)\cdot(\sage{expand(chi4.det())})                             \\
    = (t+1)(t-(1+i))(t-(1-i))
  \end{align*}
  This factorization tells us that $\EVals(A)=\Set{-1, 1+i, 1-i}$.

  For $\lambda=-1$, the eigenspace is
  \begin{sagesilent}
    v1 = vector([0, 1, -1])
  \end{sagesilent}
  \[
    \mathcal{E}_A(-1)
    = \Null\overset{-I_3-A}{\sage{chi(t=-1)}}
    = \Span\Set*{\sage{v1.column()}}
  \]
  For $\lambda=1+i$, the eigenspace is
  \begin{sagesilent}
    Null1 = e(row1=1, row2=0, scale=-i)*e(row1=2, row2=0, scale=5)*chi(t=1+I)
    Null2 = e(row1=2, scale=1/(I+2))*e(row1=1, scale=-1/(I+2))*Null1
    v2 = vector([1, -i, -5])
    v3 = v2.conjugate()
  \end{sagesilent}
  \newcommand{\StepB}{
    \begin{NiceArray}{rcrcr}[small]
      \bv[r]_2 &-& i\cdot\bv[r]_1 &\to& \bv[r]_2 \\
      \bv[r]_3 &+& 5\cdot\bv[r]_1 &\to& \bv[r]_3
    \end{NiceArray}
  }
  \newcommand{\StepC}{
    \begin{NiceArray}{rcr}[small]
      \sfrac{-1}{(i+2)}\cdot\bv[r]_2 &\to& \bv[r]_2 \\
      \sfrac{ 1}{(i+2)}\cdot\bv[r]_3 &\to& \bv[r]_3
    \end{NiceArray}
  }
  \begin{align*}
    \mathcal{E}_A(1+i)
    &= \Null\overset{(1+i)\cdot I_3-A}{\sage{chi(t=1+i)}} \\
    &\xlongequal{\StepB} \Null\sage{Null1}                \\
    &\xlongequal{\StepC} \Null\sage{Null2}                \\
    &= \Span\Set*{\sage{v2.column()}}
  \end{align*}
  The last eigenspace is then
  $\mathcal{E}_A(1-i)=\overline{\mathcal{E}_A(1+i)}=\Span\Set{\sage{v3}}$.

  Now, we know that $A$ diagonalizes as $A=XDX^{-1}$ where
  \begin{sagesilent}
    X = matrix.column([v1, v2, v3])
    X1 = e(row1=2, row2=1, scale=1)*X
    C = lambda t: detmat(X.matrix_from_rows_and_columns([i for i in range(X.nrows()) if i != t[0]], [i for i in range(X.nrows()) if i != t[1]]))
  \end{sagesilent}
  \begin{align*}
    X &= \sage{X} & \det(X) &\xlongequal{\bv[r]_3+\bv[r]_2\to\bv[r]_3}\sage{detmat(X1)} = \sage{X.det()}
  \end{align*}
  The cofactor matrix of $X$ is
  \newcommand{\Cofactors}{
    \left[
      \begin{array}{rrr}
        {}  \sage{C((0, 0))} & -\sage{C((0, 1))} &  \sage{C((0, 2))} \\[1em]
        {} -\sage{C((1, 0))} &  \sage{C((1, 1))} & -\sage{C((1, 2))} \\[1em]
        {}  \sage{C((2, 0))} & -\sage{C((2, 1))} &  \sage{C((2, 2))}
      \end{array}
    \right]
  }
  \[
    C
    = \Cofactors
    = \sage{X.adjugate().T}
  \]
  Our diagonalization is then
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[r]
      {} -1 &     &     \\
      {}    & 1+i &     \\
      {}    &     & 1-i
    \end{bNiceMatrix}
  }
  \[
    \overset{A}{\sage{A}}
    =
    \overset{X}{\sage{X}}
    \overset{D}{\MatrixD}
    \overset{X^{-1}}{\frac{1}{\sage{X.det()}}\sage{X.adjugate()}}
  \]
  The solution to our initial value problem is then
  \newcommand{\ExpDt}{
    \begin{bNiceMatrix}
      {} e^{-t} &                                     & \\
      {}        & e^t\cdot\Set{\cos(t)+i\cdot\sin(t)} & \\
      {}        &                                     & e^t\cdot\Set{\cos(t)-i\cdot\sin(t)}
    \end{bNiceMatrix}
  }
  \begin{sagesilent}
    f1 = exp(-t)
    f2 = exp(t)*(cos(t)+I*sin(t))
    f3 = exp(t)*(cos(t)-I*sin(t))
    ExpD = diagonal_matrix([f1, f2, f3])
  \end{sagesilent}
  \begin{gather*}
    \begin{align*}
      \bv[u](t)
      &= \overset{X}{\sage{X}}\overset{\exp(D\,t)}{\ExpDt}\overset{X^{-1}}{\frac{1}{\sage{X.det()}}\sage{X.adjugate()}}\sage{u0.column()} \\
      &= \sage{X}\ExpDt\sage{X.inverse()*u0.column()} \\
      &= \sage{X}\sage{ExpD*X.inverse()*u0.column()} \\
      &= \sage{expand(X*ExpD*X.inverse()*u0.column())}\qedhere
    \end{align*}
  \end{gather*}

\end{proof}

\end{document}
