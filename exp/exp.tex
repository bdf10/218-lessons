\documentclass[]{bmr}

\title{Matrix Exponentials}

\DeclareSIUnit\salt{salt}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Linear ODEs}
\subsection{Tank Problems}

\newcommand{\myTank}[3]{
  \begin{array}{c}
    \begin{tikzpicture}[line join=round, line cap=round]

      \node[
      , cylinder
      , ultra thick
      , shape border rotate=90
      , draw
      , aspect=.4
      , minimum height=#2cm
      , minimum width=#3cm
      , cylinder uses custom fill
      , cylinder body fill=blue!30
      , cylinder end fill=blue!10
      ]
      (A) {\SI{#1}{\liter}};

    \end{tikzpicture}
  \end{array}
}
\newcommand{\mySystem}{
  \begin{tikzcd}[column sep=large]
    \arrow[blue, rightsquigarrow]{r}[black]{\SI[per-mode=repeated-symbol]{10}{\liter\per\second}}
    \pgfmatrixnextcell\myTank{20}{1}{2}
    \arrow[blue, rightsquigarrow]{r}[black]{\SI[per-mode=repeated-symbol]{10}{\liter\per\second}}
    \pgfmatrixnextcell\myTank{40}{2}{2}
    \arrow[blue, rightsquigarrow]{r}[black]{\SI[per-mode=repeated-symbol]{10}{\liter\per\second}}
    \pgfmatrixnextcell\phantom{j}
  \end{tikzcd}
}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider a cascade of salt water tanks.
    \[
      \mySystem
    \]
    \onslide<+->{Initially, the salt content is \SI{10}{\kg} in Tank 1 and
      \SI{30}{\kg} in Tank 2.}
    \begin{enumerate}[<+->]
    \item Pure water is pumped into Tank 1 at a rate of
      \SI[per-mode=repeated-symbol]{10}{\liter\per\second}.
    \item Content in Tank 1 is mixed and pumped into Tank 2 at
      \SI[per-mode=repeated-symbol]{10}{\liter\per\second}.
    \item Content in Tank 2 is mixed and pumped out at
      \SI[per-mode=repeated-symbol]{10}{\liter\per\second}.
    \end{enumerate}
    \onslide<+->{How much salt is in each tank at time $t$?}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\mySI}[2]{\SI[per-mode=repeated-symbol,
    parse-numbers=false, number-math-rm=\ensuremath]{####1}{####2}}
  \begin{exampleblock}{Chemical Balance Law}
    $\textnormal{rate of change}=\textnormal{input rate}-\textnormal{output rate}$
    \[
      \mySystem
    \]
    \onslide<2->{Let $u_i(t)$ be the mass of salt in the $i$th tank at time
      $t$.}
    \[
      \begin{array}{rclclcl}
        \onslide<3->{u_1^\prime} &\onslide<3->{=}& \onslide<3->{\frac{\SI{0}{\kg\salt}}{\SI{}{\liter}}\cdot\mySI{10}{\liter\per\second}}       &\onslide<3->{-}& \onslide<3->{\frac{\mySI{u_1}{\kg\salt}}{\SI{20}{\liter}}\cdot\mySI{10}{\liter\per\second}} &\onslide<3->{=}& \onslide<3->{-\frac{1}{2}u_1}                          \\ \\
        \onslide<4->{u_2^\prime} &\onslide<4->{=}& \onslide<4->{\frac{\mySI{u_1}{\kg\salt}}{\SI{20}{\liter}}\cdot\mySI{10}{\liter\per\second}} &\onslide<4->{-}& \onslide<4->{\frac{\mySI{u_2}{\kg\salt}}{\SI{40}{\liter}}\cdot\mySI{10}{\liter\per\second}} &\onslide<4->{=}& \onslide<4->{\phantom{-}\frac{1}{2}u_1-\frac{1}{4}u_2}
      \end{array}
    \]
    \onslide<5->{This is a \emph{linear system of ordinary differential equations}.}
  \end{exampleblock}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\UPrime}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {
        \hlMat<3->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<3->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv[u]^\prime$};
        \end{tikzpicture}
      }
      ]
      u_1^\prime\\ u_2^\prime
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r][
      , margin
      , code-before = {
        \hlMat<4->[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<4->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
        \end{tikzpicture}
      }
      ]
      {} -\sfrac{1}{2} &             0 \\
      {}  \sfrac{1}{2} & -\sfrac{1}{4}
    \end{bNiceMatrix}
  }
  \newcommand{\U}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {
        \hlMat<4->[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<4->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv[u]$};
        \end{tikzpicture}
      }
      ]
      u_1\\ u_2
    \end{bNiceMatrix}
  }
  \newcommand{\UOfZero}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {
        \hlMat<5->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<5->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv[u](0)$};
        \end{tikzpicture}
      }
      ]
      u_1(0)\\ u_2(0)
    \end{bNiceMatrix}
  }
  \newcommand{\UZero}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {
        \hlMat<6->[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<6->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv[u]_0$};
        \end{tikzpicture}
      }
      ]
      10\\ 30
    \end{bNiceMatrix}
  }

  \begin{block}{Observation}
    Our system is of the form
    \[
      \begin{NiceArray}{rcrcrcrcl}[
        , margin
        , code-before = {
          \hlPortion<3->{1}{1}{2}{1}
          \hlPortion<4->[grn]{1}{3}{2}{5}
          \hlPortion<5->{1}{7}{2}{7}
          \hlPortion<6->[grn]{1}{9}{2}{9}
        }
        ]
        u_1^\prime &=& -\frac{1}{2}\,u_1 &&&& u_1(0) &=& 10 \\[1em]
        u_2^\prime &=& \frac{1}{2}\,u_1 &-& \frac{1}{4}\,u_2 && u_2(0) &=& 30 \\
      \end{NiceArray}
    \]
    \onslide<2->{Defining $\bv[u](t)=\nv{u_1(t) u_2(t)}$ allows us to write}
    \begin{align*}
      \onslide<2->{\UPrime} &\onslide<2->{= \MatrixA\U} & \onslide<2->{\UOfZero} &\onslide<2->{= \UZero}
    \end{align*}
    \onslide<7->{How do we solve this?}
  \end{block}

\end{frame}


\subsection{Single-Variable Calc}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Recall}
    The \emph{\grn{initial value problem}}
    \begin{align*}
      y^\prime &= k\,y & y(0) &= y_0
    \end{align*}
    is uniquely solved by $y(t)=\onslide<2->{y_0\,e^{kt}}$\onslide<2->{.}
  \end{block}

  \begin{block}{Question}<3->
    Can exponentials help us solve $\bv[u]^\prime=A\bv[u]$?
  \end{block}

\end{frame}

\section{Matrix Exponentials}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    Recall that the \emph{\grn{exponential function}} $e^t=\exp(t)$ may be
    written as
    \[
      \begin{tikzpicture}[]

        \node (exp) {$\displaystyle\exp(t) =
          \textcolor<2->{grn}{\sum_{k=0}^\infty\frac{1}{k!}t^k}
          $};

        \draw[overlay, <-, thick, grn, visible on=<2->]
        (exp.east) -- ++(0.666cm, 0) node[right] {\small\emph{Taylor series representation}};
      \end{tikzpicture}
    \]
    \onslide<3->{The \emph{\grn{exponential}} of a square matrix $A$ is}
    \[
      \onslide<3->{
      \exp(A)
      = \sum_{k=0}^\infty\frac{1}{k!}A^k
      = \onslide<4->{I+A+\frac{1}{2}\,A^2+\frac{1}{3!}A^3+\dotsb}
    }
    \]
    \onslide<5->{The exponential $\exp(A)$ is a square matrix with the same size
      as $A$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Matrix exponentials satisfy the following properties.
    \begin{description}[<+->][multiplicative rule]
    \item[identity rule] $\exp(\bv[O]_n)=I_n$
    \item[transposition rule] $\exp(A^\intercal)=\exp(A)^\intercal$
    \item[similarity rule] $\exp(XAX^{-1})=X\exp(A)X^{-1}$
    \item[multiplicative rule] $e^{A}e^{B}=e^{A+B}$ (provided $AB=BA$)
    \item[inverse rule] $\exp(A)^{-1}=\exp(-A)$
    \item[determinant rule] $\det(e^A)=e^{\trace(A)}$
    \item[diagonal rule] $e^{\operatorname{diag}(d_1,\dotsc,d_n)}=\operatorname{diag}(e^{d_1},\dotsc, e^{d_n})$
    \end{description}
  \end{theorem}

\end{frame}

\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
        \end{tikzpicture}
      }
      ]
      {} 20 &  54 \\
      {} -9 & -25
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixX}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1[red]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$X$};
        \end{tikzpicture}
      }
      ]
      {} -2 & -3 \\
      {}  1 &  1
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixD}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before={
        \hlDiag####1[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$D$};
        \end{tikzpicture}
      }
      ]
      {} -7 & 0 \\
      {}  0 & 2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXi}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1[red]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$X^{-1}$};
        \end{tikzpicture}
      }
      ]
      {}  1 &  3 \\
      {} -1 & -2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixExpD}{
    \begin{bNiceMatrix}[
      , margin
      , c
      , code-before={
        \hlDiag####1[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$\exp(D)$};
        \end{tikzpicture}
      }
      ]
      {} e^{-7} & 0 \\
      {} 0      & e^2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixExpA}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat<2->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$\exp(A)$};
        \end{tikzpicture}
      }
      ]
      {} \onslide####1{3\,e^2-2\,e^{-7}} & \onslide####1{6\,e^2-6\,e^{-7}} \\
      {} \onslide####1{-e^2+e^{-7}}      & \onslide####1{-2\,e^2+3\,e^{-7}}
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the diagonalization
    \[
      \MatrixA<2->
      =
      \MatrixX<4->
      \MatrixD<3->
      \MatrixXi<4->
    \]
    \onslide<5->{The matrix exponential $\exp(A)$ is then}
    \[
      \onslide<5->{\MatrixExpA<8->
        =
        \MatrixX<7->
        \MatrixExpD<6->
        \MatrixXi<7->
      }
    \]
  \end{example}

\end{frame}


\section{Solving ODEs}
\subsection{Formula}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The IVP $\bv[u]^\prime(t) = A\bv[u]$ with $\bv[u](0) = \bv[u]_0$ has a
    unique solution given by
    \[
      \bv[u](t)=\exp(At)\bv[u]_0
    \]
    \pause Moreover, if $A$ diagonalizes as $A=XDX^{-1}$, then
    \[
      \bv[u](t)=X\exp(Dt)X^{-1}\bv[u]_0
    \]
    \pause Exponentials solve systems of ODEs!
  \end{theorem}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
        \end{tikzpicture}
      }
      ]
      {} 20 &  54 \\
      {} -9 & -25
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixX}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1[red]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$X$};
        \end{tikzpicture}
      }
      ]
      {} -2 & -3 \\
      {}  1 &  1
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixD}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before={
        \hlDiag####1[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$D$};
        \end{tikzpicture}
      }
      ]
      {} -7 & 0 \\
      {}  0 & 2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXi}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1[red]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$X^{-1}$};
        \end{tikzpicture}
      }
      ]
      {}  1 &  3 \\
      {} -1 & -2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixExpDt}{
    \begin{bNiceMatrix}[
      , margin
      , c
      , code-before={
        \hlDiag####1[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$\exp(D\,t)$};
        \end{tikzpicture}
      }
      ]
      {} e^{-7\,t} & 0 \\
      {} 0      & e^{2\,t}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixSol}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat<2->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv[u](t)$};
        \end{tikzpicture}
      }
      ]
      {} \onslide####1{-3\,e^{2\,t}} \\
      {} \onslide####1{e^{2\,t}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorU}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv[u]_0$};
        \end{tikzpicture}
      }
      ]
      {} -3\\ 1
    \end{bNiceMatrix}
  }

  \begin{example}
    Consider the diagonalization
    \[
      \MatrixA<2->
      =
      \MatrixX<4->
      \MatrixD<3->
      \MatrixXi<4->
    \]
    \onslide<5->{The solution to $\bv[u]^\prime=A\bv[u]$ with
      $\bv[u]_0=\nv{-3 1}$ is}
    \[
      \onslide<5->{
        \MatrixSol<9->
        =
        \MatrixX<8->
        \MatrixExpDt<7->
        \MatrixXi<8->
        \VectorU<6->
      }
    \]
  \end{example}

\end{frame}

\section{Complex Eigenvalues}
\subsection{Euler's Formula}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Euler's Formula]
    $
    \begin{aligned}
      e^{a+b\,i}
      &= \onslide<2->{e^a\cdot e^{b\,i}} \\
      &\onslide<2->{=} \onslide<3->{e^a\cdot\sum_{k=0}^\infty \frac{1}{k!}b^ki^k} \\
      &\onslide<3->{=} \onslide<4->{e^a\cdot\Set*{\sum_{k\textnormal{ even}}\frac{1}{k!}b^ki^k+\sum_{k\textnormal{ odd}}\frac{1}{k!}b^ki^k}} \\
      &\onslide<4->{=} \onslide<5->{e^a\cdot\Set*{\sum_{k=0}^\infty\frac{1}{(2\,k)!}b^{2\,k}i^{2\,k}+\sum_{k=0}^\infty\frac{1}{(2\,k+1)!}b^{2\,k+1}i^{2\,k+1}}} \\
      &\onslide<5->{=} \onslide<6->{e^a\cdot\Set*{\sum_{k=0}^\infty\frac{(-1)^k}{(2\,k)!}b^{2\,k}+i\sum_{k=0}^\infty\frac{(-1)^{k}}{(2\,k+1)!}b^{2\,k+1}}} \\
      &\onslide<6->{=} \onslide<7->{e^a\cdot\Set{\cos(b)+i\sin(b)}}
    \end{aligned}
    $
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{corollary}[Parity Formula]
    $
    \begin{aligned}
      e^{a-b\,i}
      &= e^a\cdot\Set{\cos(-b)+i\cdot\sin(-b)} \\
      &= e^a\cdot\Set{\cos(b)-i\cdot\sin(b)}
    \end{aligned}
    $
  \end{corollary}

  \begin{corollary}[Euler's Identity]<2->
    $
    \begin{aligned}
      e^{i\,\pi}
      &= e^0\cdot\Set{\cos(\pi)+i\cdot\sin(\pi)} \\
      &= -1
    \end{aligned}
    $
  \end{corollary}

\end{frame}

\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
        \end{tikzpicture}
      }
      ]
      {} 1 & -1 \\
      {} 1 &  1
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixX}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1[red]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$X$};
        \end{tikzpicture}
      }
      ]
      {} i & -i \\
      {} 1 &  1
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixD}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before={
        \hlDiag####1[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$D$};
        \end{tikzpicture}
      }
      ]
      {} 1+i & 0 \\
      {}   0 & 1-i
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXi}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1[red]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$X^{-1}$};
        \end{tikzpicture}
      }
      ]
      {} -\sfrac{i}{2} & \sfrac{1}{2} \\
      {}  \sfrac{i}{2} & \sfrac{1}{2}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixExpDt}{
    \begin{bNiceMatrix}[
      , margin
      , c
      , code-before={
        \hlDiag####1[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$\exp(D\,t)$};
        \end{tikzpicture}
      }
      ]
      {} e^{t}\cdot\Set{\cos(t)+i\cdot\sin(t)} & 0 \\
      {} 0                                     & e^{t}\cdot\Set{\cos(t)-i\cdot\sin(t)}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixSol}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat<2->
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv[u](t)$};
        \end{tikzpicture}
      }
      ]
      {} \onslide####1{-2\,e^t\sin(t)} \\
      {} \onslide####1{ 2\,e^t\cos(t)}
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorU}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1[grn]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv[u]_0$};
        \end{tikzpicture}
      }
      ]
      {} 0\\ 2
    \end{bNiceMatrix}
  }

  \begin{example}
    Consider the diagonalization
    \[
      \MatrixA<2->
      =
      \MatrixX<4->
      \MatrixD<3->
      \MatrixXi<4->
    \]
    \onslide<5->{The solution to $\bv[u]^\prime=A\bv[u]$ with $\bv[u]_0=\nv{0 2}$ is}
    \begin{gather*}
      \onslide<5->{
        \MatrixSol<9->
        =
        \MatrixX<8->
        \MatrixExpDt<7->
        \MatrixXi<8->
        \VectorU<6->
      }
    \end{gather*}
  \end{example}

\end{frame}



\end{document}
