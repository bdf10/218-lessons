\documentclass[]{bmr}

\title{The Four Fundamental Subspaces}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Fundamental Subspaces}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    For an $m\times n$ matrix $A$, we have $\Null(A)\subset\mathbb{R}^n$ and
    $\Col(A)\subset\mathbb{R}^m$.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm        
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<6->
        ] {$\scriptstyle\Col(A^\intercal)$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<2->
        ] {$\scriptstyle\Null(A)$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^n$};

        \begin{scope}[xshift=5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<3->
          ] {$\scriptstyle\Col(A)$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<5->
          ] {$\scriptstyle\Null(A^\intercal)$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A$};

        \draw[->, thick, visible on=<4->] (Rm.south west) to[bend left]
        node[midway, below] {$\scriptstyle A^\intercal$} (Rn.south east);

        \draw[Point, blu, visible on=<9->]
        (Col.north) |- ++(5mm, 2.5mm)
        node[right] {$\scriptstyle\rank(A)=\rank[A\mid\bv]$};

        \draw[Point, blu, visible on=<11->]
        (Row.north) |- ++(-5mm, 2.5mm)
        node[left] {$\scriptstyle\rank(A^\intercal)=\rank[A^\intercal\mid\bv]$};

        \draw[Point, red, visible on=<10->]
        (LNull.north) |- ++(5mm, 2.5mm)
        node[right] {$\scriptstyle A^\intercal\bv=\bv[O]$};

        \draw[Point, grn, visible on=<8->]
        (Null.north) |- ++(-5mm, 2.5mm)
        node[left] {$\scriptstyle A\bv=\bv[O]$};

      \end{tikzpicture}
    \]
    \onslide<7->{These are called the \emph{\grn{four fundamental subspaces}} of
      $A$.}
  \end{definition}

\end{frame}

\subsection{Terminology}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Writing $\bv\in\Null(A^\intercal)$ means
    $A^\intercal\bv=\bv[O]$. \onslide<2->{Transposing this equation gives}
    \[
      \only<2>{(A^\intercal\bv)^\intercal=\bv[O]^\intercal}
      \only<3>{\bv^\intercal (A^\intercal)^\intercal=\bv[O]^\intercal}
      \only<4->{\bv^\intercal A=\bv[O]^\intercal}
    \]
    \onslide<5->{Consequently, $\Null(A^\intercal)$ is often called the
      \emph{\grn{left null space}} of $A$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The vectors in $\Col(A^\intercal)$ are the linear combinations of the
    columns of $A^\intercal$.
    \[
      \onslide<2->{
        \Col(A^\intercal) = \Span\Set{\textnormal{columns of }A^\intercal}
      }
      \onslide<3->{= \Span\Set{\textnormal{rows of }A}}
    \]
    \onslide<4->{Consequently, $\Col(A^\intercal)$ is often called the
      \emph{\grn{row space}} of $A$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Terminology}
    We now have terminology to describe all four fundamental subspaces.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm        
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        ] {$\scriptstyle\Col(A^\intercal)$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        ] {$\scriptstyle\Null(A)$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^n$};

        \begin{scope}[xshift=5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          ] {$\scriptstyle\Col(A)$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          ] {$\scriptstyle\Null(A^\intercal)$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A$};

        % \draw[->, thick, visible on=<4->] (Rm.south west) to[bend left]
        % node[midway, below] {$\scriptstyle A^\intercal$} (Rn.south east);

        \draw[Point, blu, visible on=<3->]
        (Col.north) |- ++(5mm, 2.5mm)
        node[right] {\scriptsize column space};

        \draw[Point, blu, visible on=<5->]
        (Row.north) |- ++(-5mm, 2.5mm)
        node[left] {\scriptsize row space};

        \draw[Point, red, visible on=<4->]
        (LNull.north) |- ++(5mm, 2.5mm)
        node[right] {\scriptsize left null space};

        \draw[Point, grn, visible on=<2->]
        (Null.north) |- ++(-5mm, 2.5mm)
        node[left] {\scriptsize null space};

      \end{tikzpicture}
    \]
  \end{block}

\end{frame}


\section{Examples}
\subsection{Verification Problems}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      1 & 3 & 2 \\
      2 & 6 & 4
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAT}{
    \begin{bNiceMatrix}[r, small]
      1 & 2 \\
      3 & 6 \\
      2 & 4
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAv}{
    \begin{bNiceArray}{rrr|r}[small]
      1 & 3 & 2 & 1 \\
      2 & 6 & 4 & 3
    \end{bNiceArray}
  }
  \newcommand{\MatrixAvR}{
    \begin{bNiceArray}{rrr|r}[small]
      1 & 3 & 2 & 0 \\
      0 & 0 & 0 & 1
    \end{bNiceArray}
  }
  \newcommand{\MatrixATv}{
    \begin{bNiceArray}{rr|r}[small]
      1 & 2 & 3 \\
      3 & 6 & 9 \\
      2 & 4 & 6
    \end{bNiceArray}
  }
  \newcommand{\MatrixATvR}{
    \begin{bNiceArray}{rr|r}[small]
      1 & 2 & 3 \\
      0 & 0 & 0 \\
      0 & 0 & 0
    \end{bNiceArray}
  }
  \begin{example}
    It is generally easier to \emph{verify} if vectors live in $\Null(A)$ or
    $\Null(A^\intercal)$.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm        
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<1->
        ] {$\scriptstyle \Col(A^\intercal)$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<1->
        ] {$\scriptstyle\Null(A)$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^3$};

        \begin{scope}[xshift=5.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<1->
          ] {$\scriptstyle\Col(A)$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<1->
          ] {$\scriptstyle\Null(A^\intercal)$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^2$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A=\MatrixA$};

        % \draw[->, thick, visible on=<1->] (Rm.south west) to[bend left]
        % node[midway, below] {$\scriptstyle A^\intercal$} (Rn.south east);

        \draw[Point, blu, visible on=<7->]
        (Col.north) |- ++(5mm, 2.5mm)
        node[right] (rrefAv) {$\scriptstyle\rref\MatrixAv=\MatrixAvR$};
        \node[blu, overlay, below=2.75mm, scale=0.65, visible on=<8->]
        at (rrefAv) {$\nv{1 3}\notin\Col(A)$};

        \draw[Point, blu, visible on=<9->]
        (Row.north) |- ++(-5mm, 2.5mm)
        node[left] (rrefATv) {$\scriptstyle\rref\MatrixATv=\MatrixATvR$};
        \node[blu, overlay, below=3.25mm, scale=0.65, visible on=<10->]
        at (rrefATv) {$\nv{3 9 6}\in\Col(A^\intercal)$};

        \draw[Point, red, visible on=<4->]
        (LNull.north) |- ++(5mm, 2.5mm)
        node[right] (ATv) {$\scriptstyle\MatrixAT\nvc[small]{2; -1}=\nvc[small]{0; 0; 0}$};
        \node[red, overlay, below=3.25mm, scale=0.65, visible on=<5->]
        at (ATv) {$\nv{2 -1}\in\Null(A^\intercal)$};

        \draw[Point, grn, visible on=<2->]
        (Null.north) |- ++(-5mm, 2.5mm)
        node[left] (Av) {$\scriptstyle\MatrixA\nvc[small]{1; 1; 1}=\nvc[small]{6; 12}$};
        \node[grn, overlay, below=3.25mm, scale=0.65, visible on=<3->]
        at (Av) {$\nv{1 1 1}\notin\Null(A)$};

      \end{tikzpicture}
    \]
    \onslide<6->{It is generally harder to verify if vectors live in $\Col(A)$
      or $\Col(A^\intercal)$.}
  \end{example}

\end{frame}


\subsection{Production Problems}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      1 & 3 & 2 \\
      2 & 6 & 4
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAT}{
    \begin{bNiceMatrix}[r, small]
      1 & 2 \\
      3 & 6 \\
      2 & 4
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAR}{
    \begin{bNiceMatrix}[r, small]
      1 & 3 & 2 \\
      0 & 0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixATR}{
    \begin{bNiceMatrix}[r, small]
      1 & 2 \\
      0 & 0 \\
      0 & 0
    \end{bNiceMatrix}
  }
  \begin{example}
    It is generally easier to \emph{produce} vectors in $\Col(A)$ and
    $\Col(A^\intercal)$.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm        
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<1->
        ] {$\scriptstyle \Col(A^\intercal)$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<1->
        ] {$\scriptstyle\Null(A)$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^3$};

        \begin{scope}[xshift=5.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<1->
          ] {$\scriptstyle\Col(A)$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<1->
          ] {$\scriptstyle\Null(A^\intercal)$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^2$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A=\MatrixA$};

        % \draw[->, thick, visible on=<1->] (Rm.south west) to[bend left]
        % node[midway, below] {$\scriptstyle A^\intercal$} (Rn.south east);

        \draw[Point, blu, visible on=<2->]
        (Col.north) |- ++(5mm, 2.5mm)
        node[right] (rrefAv) {$\scriptstyle\MatrixA\nvc[small]{3; 1; -2}=\nvc[small]{2; 4}$};
        \node[blu, overlay, below=3.25mm, scale=0.65, visible on=<3->]
        at (rrefAv) {$\nv{2 4}\in\Col(A)$};

        \draw[Point, blu, visible on=<4->]
        (Row.north) |- ++(-10mm, 2.5mm)
        node[left] (rrefATv) {$\scriptstyle\MatrixAT\nvc[small]{1; -3}=\nvc[small]{-5; -15; -10}$};
        \node[blu, overlay, below=3.25mm, scale=0.65, visible on=<5->]
        at (rrefATv) {$\nv{-5 -15 -10}\in\Col(A^\intercal)$};

        \draw[Point, red, visible on=<9->]
        (LNull.north) |- ++(5mm, 2.5mm)
        node[right] (ATv) {$\scriptstyle\rref\MatrixAT=\MatrixATR$};
        \node[red, overlay, below=3.25mm, scale=0.65, visible on=<10->]
        at (ATv) {$\nv{2 -1}\in\Null(A^\intercal)$};

        \draw[Point, grn, visible on=<7->]
        (Null.north) |- ++(-5mm, 2.5mm)
        node[left] (Av) {$\scriptstyle\rref\MatrixA=\MatrixAR$};
        \node[grn, overlay, below=3.25mm, scale=0.65, visible on=<8->]
        at (Av) {$\nv{-3 1 0}\in\Null(A)$};

      \end{tikzpicture}
    \]
    \onslide<6->{It is generally harder to produce vectors in $\Null(A)$ or
      $\Null(A^\intercal)$.}
  \end{example}

\end{frame}




\section{Vector Spaces}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We call $V\subset\mathbb{R}^n$ a \emph{\grn{vector space}} if $\bv[O]\in V$
    and
    \[
      c_1\cdot\bv_1+c_2\cdot\bv_2\in V
    \]
    whenever $c_1, c_2\in\mathbb{R}$ and $\bv_1, \bv_2\in V$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the ``upper half space'' $V=\Set{\nv{x y}\in\mathbb{R}^2\given y\geq 0}$.
    \[
      \begin{tikzpicture}[ultra thick]
        \node[fill=blubg, minimum width=10cm, minimum height=2cm, anchor=south] (space) at (0, 0) {};
        \draw[<->] (-5, 0) -- (5, 0);
        \draw[<->] (0, -1) -- (0, 2);

        \draw[->, blu, visible on=<3->] (0, 0) -- (-2, 1) node[left] {$\bv\in V$};
        \draw[->, red, visible on=<4->] (0, 0) -- (2, -1) node[right, overlay] {$-\bv\notin V$};

        \node[below left, alt=<5->{red}] (V) at (space.north east) {$V$};
        \node[above left=2mm and 5mm, red, overlay, visible on=<5->]
        (message) at (V) {\scriptsize \emph{not} a vector space!};
        \draw[->, thick, red, overlay, visible on=<5->] (message.east) -| (V);
      \end{tikzpicture}
    \]
    \onslide<2->{Note that $\bv=\nv{-2 1}\in V$ but $-\bv=\nv{2 -1}\notin V$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider $V=\Set{\bv\in\mathbb{R}^2\given\bv=\nv{x
        -2\,x}}$. \onslide<2->{Then $\bv_1,\bv_2\in V$ implies}
    \[
      \begin{tikzpicture}[remember picture, thick, <-, visible on=<2->]
        \node {$
          \begin{aligned}
            c_1\cdot\subnode{v1}{\color<3->{blu}\bv_1}+c_2\cdot\subnode{v2}{\color<4->{grn}\bv_2}
            &= \onslide<5->{c_1\cdot\nv{x_1 -2\,x_1}+c_2\cdot\nv{x_2 -2\,x_2}} \\
            &\onslide<5->{=} \onslide<6->{\nv{c_1x_1+c_2x_2 -2\,c_1x_1-2\,c_2x_2}}           \\
            &\onslide<6->{=} \onslide<7->{\nv{{\subnode{x}{\color<8->{blu}c_1x_1+c_2x_2}} {\subnode{y}{\color<9->{grn}-2\,(c_1x_1+c_2x_2)}}}} \\
            &\onslide<10->{\in V}
          \end{aligned}
          $};

        \draw[blu, overlay, visible on=<3->] (v1) |- ++(-5mm, -5mm) node[left, scale=0.75] {$\nv{x_1 -2\,x_1}$};
        \draw[grn, overlay, visible on=<4->] (v2) |- ++(-5mm, -8mm) node[left, scale=0.75] {$\nv{x_2 -2\,x_2}$};
        \draw[blu, overlay, visible on=<8->] (x) |- ++(5mm, -5mm) node[right] {$x$};
        \draw[grn, overlay, visible on=<9->] (y) |- ++(5mm, -5mm) node[right] {$-2\,x$};
      \end{tikzpicture}
    \]
    \onslide<11->{This shows that $V$ is a vector space!}
  \end{example}

\end{frame}

\subsection{Fundamental Subspaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\Null(A)$ is a vector space.
  \end{theorem}
  \begin{proof}<2->
    First, $\bv[O]\in\Null(A)$ since $A\bv[O]=\bv[O]$. \onslide<3->{Then, $\bv_1,\bv_2\in\Null(A)$ implies}
    \[
      \begin{tikzpicture}[remember picture, thick, <-, visible on=<3->]
        \node {$
          A(c_1\cdot\bv_1+c_2\cdot\bv_2)
          = \onslide<4->{c_1\cdot \subnode{Av1}{\color<5->{blu}A\bv[v]_1}+c_2\cdot \subnode{Av2}{\color<6->{grn}A\bv[v]_2}}
          \onslide<4->{=} \onslide<7->{c_1\cdot\bv[O]+c_2\cdot\bv[O]}
          \onslide<7->{=}\onslide<8->{\bv[O]}
          $};

        \draw[blu, overlay, visible on=<5->] (Av1) |- ++(-5mm, 5mm) node[left] {$\scriptstyle A\bv_1=\bv[O]$};
        \draw[grn, overlay, visible on=<6->] (Av2) |- ++(-1mm, 5mm) node[left] {$\scriptstyle A\bv_2=\bv[O]$};
      \end{tikzpicture}
    \]
    \onslide<9->{This shows that $c_1\cdot\bv_1+c_2\cdot\bv_2\in\Null(A)$.\qedhere}
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\Col(A)$ is a vector space.
  \end{theorem}
  \begin{proof}<2->
    Suppose $\bv_1,\bv_2\in\Col(A)$. \onslide<3->{Then}
    \[
      \begin{tikzpicture}[remember picture, thick, <-, visible on=<3->]
        \node {$
          c_1\cdot\subnode{v1}{\color<4->{blu}\bv_1}+c_2\cdot\subnode{v2}{\color<5->{grn}\bv_2}
          = \onslide<6->{c_1\cdot A\bv[x]_1+c_2\cdot A\bv[x]_2}
          \onslide<6->{=} \onslide<7->{A(c_1\cdot\bv[x]_1+c_2\cdot\bv[x]_2)}
          \onslide<8->{\in\Col(A)}
          $};

        \draw[blu, overlay, visible on=<4->] (v1) |- ++(-5mm, 5mm) node[left] {$\scriptstyle\bv_1=A\bv[x]_1$};
        \draw[grn, overlay, visible on=<5->] (v2) |- ++(-1mm, 5mm) node[left] {$\scriptstyle\bv_2=A\bv[x]_2$};
      \end{tikzpicture}
    \]
    \onslide<9->{Also, $\bv[O]\in\Col(A)$ because $A\bv[O]=\bv[O]$.\qedhere}
  \end{proof}

\end{frame}


\section{Converting Spaces}
\subsection{$\Null(A)$ into $\Col(B)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1.north)!0.5!(row-1-|col-6.north) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {}  3 & -6 &  1 &  18 & -23 \\
      {} -2 &  4 &  0 & -14 &  10 \\
      {}  4 & -8 & -5 &  43 &  20
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlEntry<3->[blu, th]{1}{1}
        \hlEntry<3->[blu, th]{2}{3}
      }
      ]
      1 & -2 & 0 &  7 & -5 \\
      0 &  0 & 1 & -3 & -8 \\
      0 &  0 & 0 &  0 &  0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[r, small]
      v_1\\ v_2\\ v_3\\ v_4\\ v_5
    \end{bNiceMatrix}
  }
  \newcommand{\VectorVs}{
    \begin{bNiceMatrix}[r, small]
      2\,c_1-7\,c_2+5\,c_3\\ c_1\\ 3\,c_2+8\,c_3\\ c_2\\ c_3
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVa}{
    \begin{bNiceMatrix}[r, small, code-before = {\hlMat####1[grn, th]}]
      2\\ 1\\ 0\\ 0\\ 0
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVb}{
    \begin{bNiceMatrix}[r, small, code-before = {\hlMat####1[grn, th]}]
      -7\\ 0\\ 3\\ 1\\ 0
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorVc}{
    \begin{bNiceMatrix}[r, small, code-before = {\hlMat####1[grn, th]}]
      5\\ 0\\ 8\\ 0\\ 1
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixB}{
    \begin{bNiceMatrix}[
      ,r
      , small
      , code-before = {
        \hlCol####1[grn, th]{1}
        \hlCol####1[grn, th]{2}
        \hlCol####1[grn, th]{3}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1.north)!0.5!(row-1-|col-4.north) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle B$};
        \end{tikzpicture}
      }
      ]
      2 & -7 & 5 \\
      1 &  0 & 0 \\
      0 &  3 & 8 \\
      0 &  1 & 0 \\
      0 &  0 & 1
    \end{bNiceMatrix}
  }
  \begin{example}
    To describe all vectors in $\Null(A)$, we start by row-reducing.
    \[
      \rref\MatrixA<2->=\MatrixR
    \]
    \onslide<4->{The vectors $\bv\in\Null(A)$ satisfy $A\bv=\bv[O]$, and may be
      written as}
    \[
      \onslide<4->{\bv =}
      \onslide<5->{\VectorV =}
      \onslide<6->{\VectorVs =}
      \onslide<7->{c_1\cdot\VectorVa<9->+c_2\cdot\VectorVb<9->+c_3\cdot\VectorVc<9->}
    \]
    \onslide<8->{This shows that
      $\Null(A)=\Span\Set{
        \nv[small, code-before = {\hlMat<9->[grn, th]}]{2 1 0 0 0},
        \nv[small, code-before = {\hlMat<9->[grn, th]}]{-7 0 3 1 0},
        \nv[small, code-before = {\hlMat<9->[grn, th]}]{5 0 8 0 1}
      }$, so}
    \[
      \onslide<10->{\Null\MatrixA<11-> = \Col\MatrixB<12->}
    \]
  \end{example}

\end{frame}


\subsection{$\Col(A)$ into $\Null(B)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceArray}{rrrrr|r}[
      ,small
      , code-before = {
        \hlPortion####1[blu, th]{1}{1}{4}{5}
        \hlCol####1[grn, th]{6}
      }
      , code-after = {
        \begin{tikzpicture}[thick, shorten <=0.5mm, <-, visible on=####1]
          \draw[blu] ($ (row-1-|col-1)!0.5!(row-1-|col-6) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
          \draw[grn] ($ (row-1-|col-6)!0.5!(row-1-|col-7) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \bv$};
        \end{tikzpicture}
      }
      ]
      {}  1 &   3 & -11 &   5 &   2 & v_1 \\
      {}  0 &   0 &   1 &   8 &   5 & v_2 \\
      {} -7 & -21 &  77 & -35 & -14 & v_3 \\
      {}  0 &   0 &   6 &  48 &  30 & v_4
    \end{bNiceArray}
  }
  \newcommand<>{\MatrixR}{
    \begin{bNiceArray}{rrrrr|r}[
      , small
      , code-before = {
        \hlEntry####1[red, th]{3}{6}
        \hlEntry####1[red, th]{4}{6}
      }
      , code-after = {
        \begin{tikzpicture}[red, thick, shorten <=0.5mm, <-, visible on=####1]
          \draw ($ (row-4-|col-7)!0.5!(row-5-|col-7) $) -| ++(12mm, 4mm) node[above]
          (label) {\scriptsize must equal zero};
          \draw ($ (row-3-|col-7)!0.5!(row-4-|col-7) $) -| ($ (label.south west)!0.40!(label.south east) $);
        \end{tikzpicture}
      }
      ]
      1 & 3 & -11 & 5 & 2 & v_{1} \\
      0 & 0 &   1 & 8 & 5 & v_{2} \\
      0 & 0 &   0 & 0 & 0 & 7 \, v_{1} + v_{3} \\
      0 & 0 &   0 & 0 & 0 & -6 \, v_{2} + v_{4}
    \end{bNiceArray}
  }
  \newcommand<>{\System}{
    \onslide####1{
      \begin{NiceArray}{rcrcr}
        {}  7\,v_1 &+& v_3 &=& 0 \\
        {} -6\,v_2 &+& v_4 &=& 0
      \end{NiceArray}
    }
  }
  \newcommand<>{\MatrixB}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat####1
      }
      , code-after = {
        \begin{tikzpicture}[thick, shorten <=0.5mm, <-, visible on=####1]
          \draw[blu] ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle B$};
        \end{tikzpicture}
      }
      ]
      7 &  0 & 1 & 0 \\
      0 & -6 & 0 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[small]
      v_1\\ v_2\\ v_3\\ v_4
    \end{bNiceMatrix}
  }
  \newcommand{\VectorO}{
    \begin{bNiceMatrix}
      0\\ 0
    \end{bNiceMatrix}
  }
  \begin{example}
    To describe all vectors $\bv\in\Col(A)$, we start by row-reducing $[A\mid\bv]$.
    \[
      \MatrixA<2->\rightsquigarrow\MatrixR<3->
    \]
    \onslide<4->{Every vector $\bv\in\Col(A)$ must satisfy}
    \begin{align*}
      \System<4-> &&\onslide<5->{\leftrightsquigarrow}&& \onslide<6->{\MatrixB<7->\VectorV=\VectorO}
    \end{align*}
    \onslide<8->{This shows that $\Col(A)=\Null(B)$.}
  \end{example}

\end{frame}


\end{document}