\documentclass[]{bmr}

\title{Vector Geometry}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Geometric Vectors}
\subsection{Arrows in $\mathbb{R}^2$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}  

  \begin{definition}
    We view $\bv\in\mathbb{R}^2$ as an \emph{\grn{arrow in the $xy$-plane}}.
    \[
      \begin{tikzpicture}[fig]

        \pgfmathsetmacro{\px}{3}
        \pgfmathsetmacro{\py}{1}
        \pgfmathsetmacro{\qx}{8}
        \pgfmathsetmacro{\qy}{2}
        \pgfmathtruncatemacro{\vx}{\qx-\px}
        \pgfmathtruncatemacro{\vy}{\qy-\py}

        \draw[<->] (-0.5,  0.0) -- (9, 0) node[right] {$x$};
        \draw[<->] ( 0.0, -0.5) -- (0, 3) node[above] {$y$};

        \foreach \x in {1,...,8} {
          \draw (\x, 3pt) -- (\x, -3pt) node[below] {$\x$};
        }

        \foreach \y in {1,...,2} {
          \draw (3pt, \y) -- (-3pt, \y) node[left] {$\y$};
        }
        
        \node[dot, visible on=<3->] (P) at (\px, \py) {};
        \node[dot, visible on=<3->] (Q) at (\qx, \qy) {};

        \draw[->, blu] (P) -- (Q) node[midway, sloped, above]
        {$\bv\onslide<4->{=\vv{PQ}=}\onslide<5->{\nvc{\qx-\px; \qy-\py}=\nvc{\vx; \vy}}$};

        \node[left, visible on=<3->] at (P.west)
        {$\begin{array}{c}P(\px, \py)\\ \text{``tail''}\end{array}$};
        \node[right, overlay, visible on=<3->] at (Q.east)
        {$\begin{array}{c}Q(\qx, \qy)\\ \text{``tip''}\end{array}$};
      \end{tikzpicture}
    \]
    \onslide<2->{Coordinates are given by \emph{\grn{tail-to-tip
          displacement}}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}  

  \begin{block}{Observation}
    Vectors represent displacement, \emph{not} location.
    \[
      \begin{tikzpicture}[fig]

        \pgfmathtruncatemacro{\vx}{2}
        \pgfmathtruncatemacro{\vy}{1}

        \draw[<->] (-0.5,  0.0) -- (9, 0) node[right] {$x$};
        \draw[<->] ( 0.0, -0.5) -- (0, 3) node[above] {$y$};

        \foreach \x in {1,...,8} {
          \draw (\x, 3pt) -- (\x, -3pt) node[below] {$\x$};
        }

        \foreach \y in {1,...,2} {
          \draw (3pt, \y) -- (-3pt, \y) node[left] {$\y$};
        }

        \draw[->, blu] (1, 1) -- ++(\vx, \vy)
        node[midway, sloped, above]
        {$\bv\onslide<2->{=\nv{{\vx} {\vy}}}$};

        \draw[->, blu] (2, 0) -- ++(\vx, \vy)
        node[midway, sloped, above]
        {$\bv[w]\onslide<2->{=\nv{{\vx} {\vy}}}$};

        \draw[->, blu] (5, 1) -- ++(\vx, \vy)
        node[midway, sloped, above]
        {$\bv[x]\onslide<2->{=\nv{{\vx} {\vy}}}$};

        \draw[->, blu] (6, 0) -- ++(\vx, \vy)
        node[midway, sloped, above]
        {$\bv[y]\onslide<2->{=\nv{{\vx} {\vy}}}$};

      \end{tikzpicture}
    \]
    \onslide<3->{These are all the same vector!}
  \end{block}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{length}} of $\bv=\nv{v_1 v_2}$ is
    $\norm{\bv}=\sqrt{v_1^2+v_2^2}$.
    \[
      \begin{tikzpicture}[fig]

        \pgfmathsetmacro{\px}{3}
        \pgfmathsetmacro{\py}{1}
        \pgfmathsetmacro{\qx}{8}
        \pgfmathsetmacro{\qy}{2}
        \pgfmathtruncatemacro{\vx}{\qx-\px}
        \pgfmathtruncatemacro{\vy}{\qy-\py}

        \draw[<->] (-0.5,  0.0) -- (9, 0) node[right] {$x$};
        \draw[<->] ( 0.0, -0.5) -- (0, 3) node[above] {$y$};

        \foreach \x in {1,...,8} {
          \draw (\x, 3pt) -- (\x, -3pt) node[below] {$\x$};
        }

        \foreach \y in {1,...,2} {
          \draw (3pt, \y) -- (-3pt, \y) node[left] {$\y$};
        }
        
        \node[dot] (P) at (\px, \py) {};
        \node[dot] (Q) at (\qx, \qy) {};

        \draw[->, blu] (P) -- (Q) node[midway, sloped, above]
        {$\onslide<3->{\norm{\bv}=\sqrt{{\vx}^2+{\vy}^2}=\sqrt{26}}$};

        \draw[dashed, grn, visible on=<2->] (P) -- (\qx, \py)
        node[midway, below] {$v_1=\vx$};

        \draw[dashed, grn, visible on=<2->] (\qx, \py) -- (Q)
        node[midway, right] {$v_2=\vy$};

        \node[left] at (P.west) {$P(\px, \py)$};
        \node[right, overlay] at (Q.east) {$Q(\qx, \qy)$};
      \end{tikzpicture}
    \]
    \onslide<4->{This formula is inspired by the \emph{\grn{Pythagorean
          theorem}}.}
  \end{definition}

\end{frame}



\subsection{Arrows in $\mathbb{R}^3$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We view $\bv\in\mathbb{R}^3$ as an \emph{\grn{arrow in $xyz$-space}}.
    \[
      \begin{tikzpicture}[3dfig]
        \pgfmathsetmacro{\xmax}{5}
        \pgfmathsetmacro{\xmin}{-\xmax}
        \pgfmathsetmacro{\ymax}{\xmax-1}
        \pgfmathsetmacro{\ymin}{\xmin+1}
        \pgfmathsetmacro{\zmax}{\xmax-3}
        \pgfmathsetmacro{\zmin}{\xmin+3}

        \newcommand{\tiklen}{.1pt}

        \pgfmathtruncatemacro{\px}{1}
        \pgfmathtruncatemacro{\py}{-3}
        \pgfmathtruncatemacro{\pz}{-1}

        \draw[blu, dashed, visible on=<4>]
        (0, \py, 0) -- (0, \py, \pz)
        (0, \py, 0) -- (\px, \py, 0) -- (\px, 0, 0)
        (\px, \py, \pz) -- (\px, 0, \pz) -- (0, 0, \pz) -- (0, \py, \pz) -- (\px, \py, \pz)
        (\px, \py, 0) -- (\px, \py, \pz)
        (\px, 0, 0) -- (\px, 0, \pz);

        \node[dot, visible on=<3->] (P) at (\px, \py, \pz) {};
        \node[left, overlay, visible on=<3->] at (P.west) {$P(\px, \py, \pz)$};

        \pgfmathtruncatemacro{\qx}{-2}
        \pgfmathtruncatemacro{\qy}{3}
        \pgfmathtruncatemacro{\qz}{1}

        \draw[blu, dashed, visible on=<6>]
        (0, \qy, 0) -- (0, \qy, \qz)
        (0, \qy, 0) -- (\qx, \qy, 0) -- (\qx, 0, 0)
        (\qx, \qy, \qz) -- (\qx, 0, \qz) -- (0, 0, \qz) -- (0, \qy, \qz) -- (\qx, \qy, \qz)
        (\qx, \qy, 0) -- (\qx, \qy, \qz)
        (\qx, 0, 0) -- (\qx, 0, \qz);
        
        \node[dot, visible on=<3->] (Q) at (\qx,  \qy,  \qz) {};
        \node[right, overlay, visible on=<3->] at (Q.east) {$Q(\qx, \qy, \qz)$};

        \draw[blu, ->, visible on=<8->] (P) -- (Q)
        node[sloped, above, pos=1.00, overlay] {$\bv=\vv{PQ}\onslide<10->{=\nv{-3 6 2}}$};

        \draw[<->, visible on=<2->]
        (\xmin,      0,     0) -- (\xmax,     0,     0) node[pos=1.075]{$x$};
        \draw[<->, visible on=<2->]
        (     0, \ymin,     0) -- (    0, \ymax,     0) node[pos=1.050]{$y$};
        \draw[<->, visible on=<2->]
        (     0,     0, \zmin) -- (    0,     0, \zmax) node[pos=1.050]{$z$};

        \pgfmathsetmacro{\start}{\xmin+1}
        \pgfmathsetmacro{\finish}{\xmax-1}
        \foreach \x in {\start,...,\finish} {
          \draw[visible on=<2->] (\x, 0, \tiklen) -- (\x, 0, -\tiklen);
        }

        \pgfmathsetmacro{\start}{\ymin+1}
        \pgfmathsetmacro{\finish}{\ymax-1}
        \foreach \y in {\start,...,\finish} {
          \draw[visible on=<2->] (0, \y, \tiklen) -- (0, \y, -\tiklen);
        }

        \pgfmathsetmacro{\start}{\zmin+1}
        \pgfmathsetmacro{\finish}{\zmax-1}
        \foreach \z in {\start,...,\finish} {
          \draw[visible on=<2->] (\tiklen, 0, \z) -- (-\tiklen, 0, \z);
        }

      \end{tikzpicture}
    \]
    \onslide<9->{Again, coordinates are given by \emph{\grn{tail-to-tip
          displacement}}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{length}} of $\bv=\nv{{v_1} {v_2} {v_3}}$ is
    $\norm{\bv}=\sqrt{v_1^2+v_2^2+v_3^2}$.
    \[
      \begin{tikzpicture}[3dfig]
        \node[dot, visible on=<2->] (P) at (1,  -3,  -1) {};
        \node[left, overlay, visible on=<2->] at (P.west) {$P(1, -3, -1)$};

        \node[dot, visible on=<2->] (Q) at (-2,  3,  1) {};
        \node[right, overlay, visible on=<2->] at (Q.east) {$Q(-2, 3, 1)$};

        \draw[blu, ->, visible on=<2->] (P) -- (Q) node[midway, sloped, above]
        {$\norm{\bv}=\sqrt{(-3)^2+6^2+2^2}=\sqrt{49}=7$};

      \end{tikzpicture}
    \]
    \onslide<3->{Again, $\norm{\bv}$ measures \emph{\grn{distance}} from tail to
      tip.}
  \end{definition}

\end{frame}



\subsection{Arrows in $\mathbb{R}^n$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    We view $\bv\in\mathbb{R}^n$ as an \emph{\grn{arrow in space}}.
    \[
      \begin{tikzpicture}[fig, ->]

        \draw[blu] (0, 0) -- ++(3, 2) node[midway, sloped, above] {$\bv$};
        \draw[grn] (2, 0) -- ++(2, -1) node[midway, sloped, above] {$\bv[w]$};
        \draw[red] (6, -1) -- ++(-3, 2) node[midway, sloped, above] {$\bv[x]$};
        
      \end{tikzpicture}
    \]
    Visible geometry in $\mathbb{R}^3$ \emph{\grn{defines}} geometry in $\mathbb{R}^n$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myv}{{\begin{bNiceMatrix}v_1 & v_2 & \Cdots & v_n\end{bNiceMatrix}}^\intercal}
  \newcommand{\myFigure}{
    \begin{array}{c}
      \begin{tikzpicture}[fig, ->]
        \draw[blu] (0, 0) -- (5, 3) node[midway, sloped, above] {$\bv=\nv{4 0 -2 -5 -1}$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myNorm}{
    \begin{aligned}
      \norm{\bv}
      &= \sqrt{4^2+0^2+(-2)^2+(-5)^2+(-1)^2} \\
      &= \sqrt{16+0+4+25+1}                  \\
      &= \sqrt{46}
    \end{aligned}
  }
  \begin{definition}
    The \emph{length} of $\bv=\myv$ is $\norm{\bv}=\sqrt{v_1^2+v_2^2+\dotsb+v_n^2}$.
    \begin{gather*}
      \begin{align*}
        \myFigure && \myNorm 
      \end{align*}
    \end{gather*}
  \end{definition}

\end{frame}


\section{Arithmetic and Properties}
\subsection{Scaling Arrows}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myv}[4][]{\norm*{####1{\begin{bNiceMatrix}####2 & ####3 & \Cdots & ####4\end{bNiceMatrix}}^\intercal}}
  \begin{theorem}
    $\norm{c\cdot\bv}=\abs{c}\cdot\norm{\bv}$
  \end{theorem}
  \begin{proof}<2->
    $
    \begin{aligned}
      \norm{c\cdot\bv}
      &= \onslide<3->{\myv[c\cdot]{v_1}{v_2}{v_n}}                                              \\
      &\onslide<3->{=} \onslide<4->{\myv{c\cdot v_1}{c\cdot v_2}{c\cdot v_n}}                   \\
      &\onslide<4->{=} \onslide<5->{\sqrt{(c\cdot v_1)^2+(c\cdot v_2)^2+\dotsb+(c\cdot v_n)^2}} \\
      &\onslide<5->{=} \onslide<6->{\sqrt{c^2\cdot(v_1^2+v_2^2+\dotsb+v_n^2)}}                  \\
      &\onslide<6->{=} \onslide<7->{\sqrt{c^2}\cdot\sqrt{v_1^2+v_2^2+\dotsb+v_n^2}}             \\
      &\onslide<7->{=} \onslide<8->{\onslide<2->{\abs{c}\cdot\norm{\bv} \qedhere}}
    \end{aligned}
    $
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \pgfmathsetmacro{\vx}{1.5}
  \pgfmathsetmacro{\vy}{2}
  \newcommand{\myva}{
    \begin{array}{c}
      \begin{tikzpicture}[fig]
        \draw[->, blu] (0, 0) -- (\vx, \vy) node[midway, sloped, above] {$\bv$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myvb}{
    \begin{array}{c}
      \begin{tikzpicture}[fig]
        \draw[->, grn] (0, 0) -- ($ 2*(\vx, \vy) $) node[midway, sloped, above] {$2\cdot\bv$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myvc}{
    \begin{array}{c}
      \begin{tikzpicture}[fig]
        \draw[->, grn] (0, 0) -- ($ 0.5*(\vx, \vy) $) node[midway, sloped, above] {$\sfrac{1}{2}\cdot\bv$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myvd}{
    \begin{array}{c}
      \begin{tikzpicture}[fig]
        \draw[->, red] (0, 0) -- ($ -0.5*(\vx, \vy) $) node[midway, sloped, above] {$-\sfrac{1}{2}\cdot\bv$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\myve}{
    \begin{array}{c}
      \begin{tikzpicture}[fig]
        \draw[->, red] (0, 0) -- ($ -1.0*(\vx, \vy) $) node[midway, sloped, above] {$-1\cdot\bv$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    The product $c\cdot\bv$ \emph{\grn{scales}} and \emph{\grn{orients}} $\bv$.
    \begin{align*}
      \myva && \myvb && \myvc && \myvd && \myve
    \end{align*}
    The magnitude $\abs{c}$ controls \emph{\grn{length}} and the sign controls
    \emph{\grn{direction}}.
  \end{block}

\end{frame}


\subsection{Adding Arrows}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    To interpret $\bv+\bv[w]$ geometrically, start by forming a parallelogram.
    \[
      \begin{tikzpicture}[fig]
        \node (P) [
        , anchor=bottom left corner
        , trapezium
        , trapezium left angle=35
        , trapezium right angle=145
        , minimum height=1.5cm
        , text width=1mm
        , rotate=10
        ] {};

        \coordinate (O) at (0, 0);
        \coordinate (v) at (P.bottom right corner);
        \coordinate (w) at (P.top left corner);
        \coordinate (vpw) at (P.top right corner);

        \draw[blu, ->] (O) -- (v) node[midway, sloped, below] {$\bv$};
        \draw[grn, ->] (O) -- (w) node[midway, sloped, above] {$\bv[w]$};

        \draw[blu, ->, shorten >=3mm, visible on=<2->] (w) -- (vpw) node[midway, sloped, above] {$\bv$};
        \draw[grn, ->, shorten >=3mm, visible on=<2->] (v) -- (vpw) node[midway, below, sloped] {$\bv[w]$};
        
        \draw[red, ->, shorten >=3mm, visible on=<4->] (O) -- (vpw) node[midway, above, sloped] {$\bv+\bv[w]$};
      \end{tikzpicture}
    \]
    \onslide<3->{The sum $\bv+\bv[w]$ is the diagonal of this parallelogram.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Our geometric perspective of sums allows us to interpret \emph{\grn{vector
        diagrams}}.
    \[
      \begin{tikzpicture}[fig, ->]
        \coordinate (O) at (0,0);
        \coordinate (P) at (7,4);
        \coordinate (Q) at (6, 1);
        \coordinate (R) at (-2, 4);


        \draw[blu] (O) -- (P) node[midway, above, sloped] {$\bv$};
        \draw[blu] (Q) -- (P) node[midway, right] {$\bv[w]$};
        \draw[blu] (O) -- (R) node[midway, left] {$\bv[x]$};
        \draw[red] (R) -- (P) node[midway, above, sloped] {$\bv[y]=\onslide<2->{\bv-\bv[x]}$};
        \draw[red] (Q) -- (O) node[midway, below, sloped] {$\bv[z]=\onslide<3->{\bv[w]-\bv}$};
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\subsection{Unit Vectors}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \begin{definition}
    A \emph{\grn{unit vector}} is a vector with length one.
  \end{definition}

\end{frame}



\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\h}{\sfrac{1}{2}}
  \begin{example}
    The length of $\bv[u]=\nv{{\h} {-\h} {-\h} {\h}}$ is 
    \begin{align*}
      \onslide<1->{\norm{\bv[u]}} &\onslide<1->{=} \onslide<2->{\norm*{\nv{{\h} {-\h} {-\h} {\h}}}}    \\
                                  &\onslide<2->{=} \onslide<3->{\norm*{(\h)\cdot\nv{1 -1 -1 1}}}       \\
                                  &\onslide<3->{=} \onslide<4->{\abs{\h}\cdot\norm*{\nv{1 -1 -1 1}}}   \\
                                  &\onslide<4->{=} \onslide<5->{(\h)\cdot\sqrt{1^2+(-1)^2+(-1)^2+1^2}} \\
                                  &\onslide<5->{=} \onslide<6->{(\h)\cdot\sqrt{1+1+1+1}}               \\
                                  &\onslide<6->{=} \onslide<7->{(\h)\cdot\sqrt{4}}                     \\
                                  &\onslide<7->{=} \onslide<8->{(\h)\cdot2}                            \\
                                  &\onslide<8->{=} \onslide<9->{1}
    \end{align*}
    \onslide<10->{This shows that $\bv[u]$ is a unit vector.}
  \end{example}

\end{frame}


\subsection{Normalization}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{normalization}} of $\bv$ is
    $\widehat{\bv}=(\sfrac{1}{\norm{\bv}})\cdot\bv$.
    \[
      \begin{tikzpicture}[fig, ->]
        \draw[blu] (0, 0) -- (4, 2) node[pos=0.75, above, sloped] {$\bv$};
        \draw[red, visible on=<2->] (0, 0) -- (2, 1) node[midway, above, sloped] {$\widehat{\bv}$};
      \end{tikzpicture}
    \]
    \onslide<3->{Calculating $\norm{\widehat{\bv}}$ gives}
    \[
      \onslide<3->{\norm{\widehat{\bv}} =}
      \onslide<4->{\norm*{(\sfrac{1}{\norm{\bv}})\cdot\bv} =}
      \onslide<5->{\abs{\sfrac{1}{\norm{\bv}}}\cdot\norm{\bv} =}
      \onslide<6->{(\sfrac{1}{\norm{\bv}})\cdot\norm{\bv} =}
      \onslide<7->{1}
    \]
    \onslide<8->{This shows that $\widehat{\bv}$ is always a unit vector.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myv}{\nv{3 -8 4 2}}
  \begin{example}
    The length $\norm{\bv}$ and normalization $\widehat{\bv}$ of
    $\bv=\myv$ are
    \begin{align*}
      \norm{\bv} &= \sqrt{93} & \widehat{\bv} &= \frac{1}{\sqrt{93}}\cdot\myv
    \end{align*}
    \onslide<2->{Algebra allows us to speak \emph{\grn{geometrically}} about
      this $\bv\in\mathbb{R}^4$.}
  \end{example}

\end{frame}


\section{The Inner Product}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{inner product}} of $\bv,\bv[w]\in\mathbb{R}^n$ is
    \[
      \inner{\bv, \bv[w]}
      = v_1w_1+v_2w_2+\dotsb+v_nw_n
    \]
    \onslide<2->{This is also called the \emph{\grn{dot product}}
      $\inner{\bv, \bv[w]}=\bv\cdot\bv[w]$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myv}{\nv{{3} {1} {2} {-2}}}
  \newcommand{\myw}{\nv{{-7} {0} {3} {-9}}}
  \begin{example}
    Consider the vectors $\bv$ and $\bv[w]$ given by
    \begin{align*}
      \bv &= \myv & \bv[w] &= \myw
    \end{align*}
    \onslide<2->{The inner product of $\bv$ and $\bv[w]$ is}
    \begin{align*}
      \onslide<2->{\inner{\bv, \bv[w]}} &\onslide<2->{=} \onslide<3->{(3)(-7)+(1)(0)+(2)(3)+(-2)(-9)} \\
                                        &\onslide<3->{=} \onslide<4->{-21+0+6+18}                     \\
                                        &\onslide<4->{=} \onslide<5->{3}
    \end{align*}
    \onslide<6->{Note that $\inner{\bv, \bv[w]}$ is a \emph{\grn{scalar}}.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myAst}[1]{{\bAutoNiceMatrix{####1}{\ast}}^\intercal}
  \newcommand{\BadInnerA}{
    \begin{tikzpicture}
      \node[visible on=<2->, cross out, draw=red, ultra thick] {$\inner{\myAst{1-2}, \myAst{1-3}}$};
      \node {$\inner{\myAst{1-2}, \myAst{1-3}}$};
    \end{tikzpicture}
  }
  \newcommand{\BadInnerB}{
    \begin{tikzpicture}
      \node[visible on=<2->, cross out, draw=red, ultra thick] {$\inner{\myAst{1-4}, \myAst{1-6}}$};
      \node {$\inner{\myAst{1-4}, \myAst{1-6}}$};
    \end{tikzpicture}
  }
  \begin{alertblock}{Warning}
    Inner products $\inner{\bv, \bv[w]}$ only work if $\bv$ and $\bv[w]$
    \emph{have the same shape}.
    \begin{align*}
      \BadInnerA && \BadInnerB
    \end{align*}
  \end{alertblock}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The inner product obeys the following laws.
    \begin{description}[<+->][adjoint formula]
    \item[commutative] $\inner{\bv, \bv[w]}=\inner{\bv[w], \bv}$
    \item[left-linear] $\inner{c_1\cdot\bv_1+c_2\cdot\bv_2, \bv[w]}=c_1\cdot\inner{\bv_1, \bv[w]}+c_2\cdot\inner{\bv_2, \bv[w]}$
    \item[right-linear] $\inner{\bv, c_1\cdot\bv[w]_1+c_2\cdot\bv[w]_2}=c_1\cdot\inner{\bv, \bv[w]_1}+c_2\cdot\inner{\bv, \bv[w]_2}$
    \item[adjoint formula] $\inner{A\bv, \bv[w]}=\inner{\bv, A^\intercal\bv[w]}$
    \item[length formula] $\inner{\bv, \bv}=\norm{\bv}^2$
    \end{description}
  \end{block}

\end{frame}


% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \begin{example}
%     Suppose the following conditions are satisfied.
%     \begin{align*}
%       \inner{\bv_1, A\bv[w]_2} &= -2 & A\bv[w]_1 &= \bv[O] & A^\intercal\bv_2 &= \bv[O]
%     \end{align*}
%     \onslide<2->{We then have}
%     \begin{align*}
%       \onslide<2->{\inner{3\cdot \bv_1+7\cdot\bv_2, A(\bv[w]_1-6\cdot\bv[w]_2)}}
%       &\onslide<2->{=} \onslide<3->{\inner{3\cdot\bv_1+7\cdot\bv_2, -6\cdot A\bv[w]_2}}            \\
%       &\onslide<3->{=} \onslide<4->{\inner{A^\intercal(3\cdot\bv_1+7\cdot\bv_2), -6\cdot\bv[w]_2}} \\
%       &\onslide<4->{=} \onslide<5->{\inner{3\cdot A^\intercal\bv_1, -6\cdot\bv[w]_2}}              \\
%       &\onslide<5->{=} \onslide<6->{3\cdot(-6)\cdot\inner{\bv_1, A\bv[w]_2}}                       \\
%       &\onslide<6->{=} \onslide<7->{3\cdot (-6)\cdot(-2)}                                          \\
%       &\onslide<7->{=} \onslide<8->{36}
%     \end{align*}
%   \end{example}

% \end{frame}



\subsection{Angles}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\TwoVectors}{
    \begin{array}{c}
      \begin{tikzpicture}[fig]
        \coordinate (O) at (0, 0);
        \coordinate (v) at (15:3.5cm);
        \coordinate (w) at (75:2.5cm);

        \draw[->, blu] (O) -- (v) node[midway, sloped, below] {$\bv$};
        \draw[->, blu] (O) -- (w) node[midway, sloped, above] {$\bv[w]$};
        \draw[->, red, visible on=<2->] (w) -- (v) node[midway, sloped, above] {$\bv-\bv[w]$};

        \newcommand{\thetalen}{0.75cm}
        \draw[<->, visible on=<9->] (15:\thetalen) arc (15:75:\thetalen);
        \node[visible on=<9->] at (45:1cm) {$\theta$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\MyNorm}{
    \begin{aligned}
      \onslide<3->{\norm{\bv-\bv[w]}^2}
      &\onslide<3->{=} \onslide<4->{\inner{\bv-\bv[w], \bv-\bv[w]}}                                                  \\
      &\onslide<4->{=} \onslide<5->{\inner{\bv, \bv-\bv[w]}-\inner{\bv[w], \bv-\bv[w]}}                              \\
      &\onslide<5->{=} \onslide<6->{\inner{\bv, \bv}-\inner{\bv, \bv[w]}-\inner{\bv[w], \bv}+\inner{\bv[w], \bv[w]}} \\
      &\onslide<6->{=} \onslide<7->{\norm{\bv}^2+\norm{\bv[w]}^2-2\cdot\inner{\bv, \bv[w]}}
    \end{aligned}
  }
  \begin{example}
    The scalar $\norm{\bv-\bv[w]}^2$ may be written in terms of inner products
    \begin{align*}
      \TwoVectors && \MyNorm
    \end{align*}
    \onslide<8->{We also have the \emph{\grn{law of cosines}}.}
    \[
      \onslide<10->{\norm{\bv-\bv[w]}^2=\norm{\bv}^2+\norm{\bv[w]}^2-2\cdot\norm{\bv}\cdot\norm{\bv[w]}\cdot\cos(\theta)}
    \]
    \onslide<11->{Equating and simplifying gives
      $\inner{\bv, \bv[w]}=\norm{\bv}\cdot\norm{\bv[w]}\cdot\cos(\theta)$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\inner{\bv, \bv[w]}=\norm{\bv}\cdot\norm{\bv[w]}\cdot\cos(\theta)$
  \end{theorem}

  \begin{corollary}[The Cauchy-Schwarz Inequality]<2->
    $\abs{\inner{\bv, \bv[w]}}\leq\norm{\bv}\cdot\norm{\bv[w]}$
  \end{corollary}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myv}{\nv{{1} {-3} {4}}}
  \newcommand{\myw}{\nv{1 1 1}}
  \begin{example}
    For $\bv=\myv$ and $\bv[w]=\myw$ we have
    \[
      \cos(\theta)
      = \frac{\inner{\bv, \bv[w]}}{\norm{\bv}\cdot\norm{\bv[w]}}
      = \frac{\inner{\myv, \myw}}{\norm*{\myv}\cdot\norm*{\myw}}
      = \frac{2}{\sqrt{26}\cdot\sqrt{3}}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\AcuteAngle}{
    \begin{array}{c}
      \grn{\text{acute } \inner{\bv, \bv[w]} > 0} \\
      \begin{tikzpicture}[fig]
        
        \pgfmathsetmacro{\vtheta}{5}
        \pgfmathsetmacro{\wtheta}{65}
        
        \coordinate (O) at (0, 0);
        \coordinate (v) at (\vtheta:2cm);
        \coordinate (w) at (\wtheta:1.5cm);

        \draw[->, blu] (O) -- (v) node[midway, sloped, below] {$\bv$};
        \draw[->, blu] (O) -- (w) node[midway, sloped, above] {$\bv[w]$};

        \newcommand{\thetalen}{0.75cm}
        \draw[<->] (\vtheta:\thetalen) arc (\vtheta:\wtheta:\thetalen);
        \node at ({0.5*\vtheta+0.5*\wtheta}:1cm) {$\theta$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\ObtuseAngle}{
    \begin{array}{c}
      \grn{\text{obtuse } \inner{\bv, \bv[w]} < 0} \\
      \begin{tikzpicture}[fig]
        
        \pgfmathsetmacro{\vtheta}{5}
        \pgfmathsetmacro{\wtheta}{125}
        
        \coordinate (O) at (0, 0);
        \coordinate (v) at (\vtheta:2cm);
        \coordinate (w) at (\wtheta:1.5cm);

        \draw[->, blu] (O) -- (v) node[midway, sloped, below] {$\bv$};
        \draw[->, blu] (O) -- (w) node[midway, sloped, below] {$\bv[w]$};

        \newcommand{\thetalen}{0.75cm}
        \draw[<->] (\vtheta:\thetalen) arc (\vtheta:\wtheta:\thetalen);
        \node at ({0.5*\vtheta+0.5*\wtheta}:1cm) {$\theta$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Orthogonal}{
    \begin{array}{c}
      \grn{\text{orthogonal } \inner{\bv, \bv[w]} = 0} \\
      \begin{tikzpicture}[fig, rotate=10, transform shape]
        
        \pgfmathsetmacro{\vtheta}{0}
        \pgfmathsetmacro{\wtheta}{\vtheta+90}
        
        \coordinate (O) at (0, 0);
        \coordinate (v) at (\vtheta:2cm);
        \coordinate (w) at (\wtheta:1.5cm);

        \draw[->, blu] (O) -- (v) node[midway, sloped, below] {$\bv$};
        \draw[->, blu] (O) -- (w) node[midway, left] {$\bv[w]$};

        \newcommand{\thetalen}{0.25cm}
        \draw (\vtheta:\thetalen) |- (\wtheta:\thetalen);
      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    Any two vectors are configured in one of the following three ways.
    \begin{align*}
      \onslide<2->{\AcuteAngle} && \onslide<3->{\ObtuseAngle} && \onslide<4->{\Orthogonal}
    \end{align*}
    \onslide<5->{For example, $\bv=\nv{3 {-2} 1 4}$ and $\bv[w]=\nv{1 1 7 {-2}}$
      satisfy}
    \[
      \onslide<5->{\inner{\bv, \bv[w]} = (3)(1)+(-2)(1)+(1)(7)+(4)(-2) = 0}
    \]
    \onslide<6->{Whatever $\mathbb{R}^4$ looks like, $\bv$ and $\bv[w]$ are
      configured \emph{\grn{orthogonally}}.}
  \end{definition}

\end{frame}


\subsection{Reinterpreting $A\bv$}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , code-before={
        \hlRow<2>{1}
        \hlRow<3>{2}
        \hlRow<4>{3}
        \hlRow<5->{4}
        }
      ]
      -5 & -8 \\
      2 & -2 \\
      1 & 6 \\
      3 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , code-before={\hlCol<2->[grn]{1}}
      ]
      3 \\ -1
    \end{bNiceMatrix}
  }
  \newcommand{\mv}[1]{{\left[\begin{array}{rr}####1\end{array}\right]}^\intercal}
  \newcommand{\Av}{
    \begin{bNiceMatrix}[
      , r
      , code-before={
        \hlRow<2>[red]{1}
        \hlRow<3>[red]{2}
        \hlRow<4>[red]{3}
        \hlRow<5->[red]{4}
      }
      ]
      -7 \\ 8 \\ -3 \\ 9
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the matrix-vector product.
    \[
      \overset{A}{\MatrixA}
      \overset{\bv}{\VectorV}
      =
      \Av
    \]
    \onslide<6->{The $i$th coordinate of $A\bv$ is the inner product of the
      $i$th row of $A$ and $\bv$.}
  \end{example}

\end{frame}


\end{document}
