\documentclass[]{bmr}

\title{$A=QR$ Factorizations}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Orthonormality}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{\grn{QR-factorization}} is an equation of the form
    \[
      \begin{tikzpicture}[remember picture]
        \node {$
          \subnode{MatA}{\color<2->{grn}A}
          =
          \subnode{MatQ}{\color<3->{blu}Q}
          \subnode{MatR}{\color<4->{red}R}
          $};

        \draw[<-, grn, thick, overlay, visible on=<2->] (MatA.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize\textnormal{$m\times n$ with rank $r$}};

        \draw[<-, blu, thick, overlay, visible on=<3->] (MatQ.south) |- ++(3mm, -5mm)
        node[right] {\scriptsize\textnormal{$m\times r$ with \emph{orthonormal} columns}};

        \draw[<-, red, thick, overlay, visible on=<4->] (MatR.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize\textnormal{$r\times n$ upper-triangular}};

      \end{tikzpicture}
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MutuallyOrthogonal}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\inner{\bv[q]_i, \bv[q]_j}=0$ for $i\neq j$};
      \node[above, grn] at (text.north) {\scriptsize Mutually Orthogonal};
    \end{tikzpicture}
  }
  \newcommand{\UnitVectors}{
    \begin{tikzpicture}[visible on=<3->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\inner{\bv[q]_i, \bv[q]_i}=1$};
      \node[above, blu] at (text.north) {\scriptsize Unit Vectors};
    \end{tikzpicture}
  }
  \begin{definition}
    We call $\Set{\bv[q]_1,\dotsc,\bv[q]_m}$ \emph{\grn{orthonormal}} if
    \begin{align*}
      \MutuallyOrthogonal && \UnitVectors
    \end{align*}
    \onslide<4->{For example, consider}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , scale=2
        , visible on=<4->
        ]

        \coordinate (O) at (0, 0, 0);

        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \draw[->] (O) -- (e1);
        \draw[->] (O) -- (e2);
        \draw[->] (O) -- (e3);

        \draw[->, blu, visible on=<5->] (O) -- ($ 0.75*(e1)$) node[overlay, above left]  {$\scriptstyle \bv[e]_1=\nv[small]{1 0 0}$};
        \draw[->, grn, visible on=<6->] (O) -- ($ 0.75*(e2)$) node[overlay, below]       {$\scriptstyle \bv[e]_2=\nv[small]{0 1 0}$};
        \draw[->, red, visible on=<7->] (O) -- ($ 0.75*(e3)$) node[overlay, right]       {$\scriptstyle \bv[e]_3=\nv[small]{0 0 1}$};
      \end{tikzpicture}
    \]
    \onslide<8->{Then $\Set{\bv[e]_1, \bv[e]_2, \bv[e]_3}$ is orthonormal.}
  \end{definition}

\end{frame}


\subsection{Orthonormal Columns}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixQT}{
    \begin{bNiceMatrix}[
      , margin
      , code-before={
        \hlRow<3-8>{1}
        \hlRow<9-14>{2}
        \hlRow<15-20>{4}
      }
      ]
      \bv[q]_1^\intercal \\ \bv[q]_2^\intercal\\ \Vdots\\ \bv[q]_r^\intercal
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixQ}{
    \begin{bNiceMatrix}[
      , margin
      , code-before={
        \hlCol<3-4,9-10,15-16>{1}
        \hlCol<5-6,11-12,17-18>{2}
        \hlCol<7-8,13-14,19-20>{4}
      }
      ]
      \bv[q]_1 & \bv[q]_2 & \Cdots & \bv[q]_r
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixQTQ}{
    \begin{bNiceMatrix}[
      , margin
      , code-before={
        \hlEntry<3-4>[red]{1}{1}
        \hlEntry<5-6>[red]{1}{2}
        \hlEntry<7-8>[red]{1}{4}
        \hlEntry<9-10>[red]{2}{1}
        \hlEntry<11-12>[red]{2}{2}
        \hlEntry<13-14>[red]{2}{4}
        \hlEntry<15-16>[red]{4}{1}
        \hlEntry<17-18>[red]{4}{2}
        \hlEntry<19-20>[red]{4}{4}
      }
      ]
      \alt<4->{1}{\inner{\bv[q]_1, \bv[q]_1}}   & \alt<6->{0}{\inner{\bv[q]_1, \bv[q]_2}}   & \Cdots & \alt<8->{0}{\inner{\bv[q]_1, \bv[q]_r}}      \\
      \alt<10->{0}{\inner{\bv[q]_2, \bv[q]_1}}   & \alt<12->{1}{\inner{\bv[q]_2, \bv[q]_2}}   & \Cdots & \alt<14->{0}{\inner{\bv[q]_2, \bv[q]_r}}      \\
      \Vdots                       & \Vdots                       & \Ddots & \Vdots                          \\
      \alt<16->{0}{\inner{\bv[q]_{r}, \bv[q]_1}} & \alt<18->{0}{\inner{\bv[q]_{r}, \bv[q]_2}} & \Cdots & \alt<20->{1}{\inner{\bv[q]_r, \bv[q]_r}}
    \end{bNiceMatrix}
  }
  \begin{theorem}
    An $m\times r$ matrix $Q$ has orthonormal columns if and only if
    $Q^\intercal Q=I_r$.
  \end{theorem}
  \begin{proof}<2->
    Note that the $(i, j)$ entry of $Q^\intercal Q$ is
    $\inner{\bv[q]_i, \bv[q]_j}$.
    \[
      \overset{Q^\intercal}{\MatrixQT}
      \overset{Q}{\MatrixQ}
      =
      \overset{\onslide<21->{I_r}}{\MatrixQTQ}
    \]
    \onslide<22->{This proves the theorem. \qedhere}
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixQ}{
    \begin{array}{c}
      \begin{tikzpicture}
        \node[alt=####1{fill=blubg, rounded corners, draw, ultra thick}] (Q)
        {$
          \scriptstyle\frac{1}{\sqrt{7}}
          \begin{bNiceMatrix}[
            , r
            , small
            ]
            {} -2 &  1 & 1 \\
            {} -1 & -2 & 1 \\
            {}  1 & -1 & 1 \\
            {}  1 &  1 & 2
          \end{bNiceMatrix}
          $};

        \draw[<-, blu, thick, shorten <=0.5mm, overlay, visible on=####1]
        (Q.north) |- ++(-2mm, 2.5mm) node[left] {$\scriptstyle Q$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand<>{\MatrixQT}{
    \begin{array}{c}
      \begin{tikzpicture}[]
        \node[alt=####1{fill=grnbg, rounded corners, draw, ultra thick}] (QT)
        {$
          \scriptstyle\frac{1}{\sqrt{7}}
          \begin{bNiceMatrix}[
            , r
            , small
            ]
            {} -2 & -1 &  1 & 1 \\
            {}  1 & -2 & -1 & 1 \\
            {}  1 &  1 &  1 & 2
          \end{bNiceMatrix}
          $};

        \draw[<-, grn, thick, shorten <=0.5mm, overlay, visible on=####1]
        (QT.north) |- ++(-2mm, 2.5mm) node[left] {$\scriptstyle Q^\intercal$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand<>{\MatrixI}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[red, th]####1}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, visible on=####1] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle I_3$};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the data
    \begin{align*}
      \MatrixQ<2-> && \onslide<3->{\MatrixQT<4->\MatrixQ<5->=}\onslide<6->{\MatrixI<7->}
    \end{align*}
    \onslide<8->{This shows that the vectors
      $\Set{\bv[q]_1, \bv[q]_2, \bv[q]_3}$ are orthonormal where}
    \begin{align*}
      \onslide<8->{\bv[q]_1 = \frac{1}{\sqrt{7}}\nv[small]{-2 -1 1 1}} && \onslide<8->{\bv[q]_2 = \frac{1}{\sqrt{7}}\nv[small]{1 -2 -1 1}} && \onslide<8->{\bv[q]_3 = \frac{1}{\sqrt{7}}\nv[small]{1 1 1 2}}
    \end{align*}
    \onslide<9->{Note that $Q^\intercal\neq Q^{-1}$ because $Q$ is not square!}
  \end{example}

\end{frame}


\section{$A=QR$ Factorizations}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[grn, th]<6->}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, visible on=<6->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} -2 &  -2 &  3 \\
      {}  2 &   1 & -3 \\
      {}  1 & -15 &  6 \\
      {}  0 &  -2 &  0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixQ}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[blu, th]<7->}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, visible on=<7->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle Q$};
        \end{tikzpicture}
      }
      ]
      {} -\sfrac{2}{3} & -\sfrac{4}{15}  & -\sfrac{1}{5}   \\
      {}  \sfrac{2}{3} &  \sfrac{1}{5}   & -\sfrac{4}{15}  \\
      {}  \sfrac{1}{3} & -\sfrac{14}{15} &  \sfrac{2}{15}  \\
      {}             0 & -\sfrac{2}{15}  & -\sfrac{14}{15}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[red, th]<8->}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, visible on=<8->] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle R$};
        \end{tikzpicture}
      }
      ]
      3 & -3 & -2 \\
      0 & 15 & -7 \\
      0 &  0 &  1
    \end{bNiceMatrix}
  }
  \begin{definition}
    A \emph{\grn{QR-factorization}} is an equation of the form
    \[
      \begin{tikzpicture}[remember picture]
        \node {$
          \subnode{MatrixA}{\color<2->{grn}A}
          =
          \subnode{MatrixQ}{\color<3->{blu}Q}
          \subnode{MatrixR}{\color<4->{red}R}
          $};

        \draw[<-, grn, thick, overlay, visible on=<2->] (MatrixA.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize\textnormal{$m\times n$ with rank $r$}};

        \draw[<-, blu, thick, overlay, visible on=<3->] (MatrixQ.south) |- ++(3mm, -5mm)
        node[right] {\scriptsize\textnormal{$m\times r$ with $Q^\intercal Q=I_r$}};

        \draw[<-, red, thick, overlay, visible on=<4->] (MatrixR.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize\textnormal{$r\times n$ upper-triangular}};

      \end{tikzpicture}
    \]
    \onslide<5->{For example,}
    \[
      \onslide<5->{\MatrixA=\MatrixQ\MatrixR}
    \]
  \end{definition}

\end{frame}


\subsection{General Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\CommonGramian}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<7->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$A^\intercal A=R^\intercal R$};
        \node[above, grn] at (text.north) {\scriptsize\textnormal{Common Gramian}};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\CommonRank}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<8->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\rank(A)=\rank(R)$};
        \node[above, blu] at (text.north) {\scriptsize\textnormal{Common Rank}};
      \end{tikzpicture}
    \end{array}
  }
  \begin{theorem}
    Suppose $A=QR$ and consider the equation
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          A^\intercal A =
          \onslide<2->{(QR)^\intercal(QR) =}
          \onslide<3->{R^\intercal \subnode{QTQ}{\color<4->{red}Q^\intercal Q}R =}
          \onslide<5->{R^\intercal R}
          \)};

        \draw[<-, red, thick, overlay, visible on=<4->] (QTQ.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle Q^\intercal Q=I_r$};
      \end{tikzpicture}
    \]
    \onslide<6->{This means that $A$ are $R$ have \grn{common Gramians} and
      \blu{common rank}.}
    \begin{align*}
      \CommonGramian && \CommonRank
    \end{align*}
    \onslide<9->{Moreover, $R$ is nonsingular if and only if $A$ has full column
      rank.}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    In $A=QR$, $\Col(A)=\Col(Q)$.
  \end{theorem}
  \begin{proof}<+->%

  \end{proof}

  \begin{corollary}<+->
    In $A=QR$, $\Col(A)=\Col(Q)$.
  \end{corollary}

\end{frame}


\section{Projections}
\subsection{$P_{\Col(A)}=QQ^\intercal$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    In $A=QR$, projection onto $\Col(A)$ is $P_{\Col(A)}=QQ^\intercal$.
  \end{theorem}
  \begin{proof}<2->%
    The columns of $Q$ form a basis of $\Col(A)$, so the projection formula
    gives
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          P_{\Col(A)}
          =
          \onslide<2->{Q(\subnode{QGramian}{\color<3->{red}Q^\intercal Q})^{-1}Q^\intercal =}
          \onslide<4->{QQ^\intercal}
          \)};

        \draw[<-, red, thick, overlay, visible on=<3->] (QGramian.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle Q^\intercal Q=I_r$};
      \end{tikzpicture}
    \]
    \onslide<5->{This proves the theorem.\qedhere}
  \end{proof}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Advantage}
    We now have two projection formulas
    \begin{align*}
      P_{\Col(A)} = X(X^\intercal X)^{-1}X^\intercal && P_{\Col(A)}=QQ^\intercal
    \end{align*}
    We avoid the ``difficult'' inversion $(X^\intercal X)^{-1}$ with $A=QR$.
  \end{block}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[grn, th]####1}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, visible on=####1] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} -2 &  -2 &  3 \\
      {}  2 &   1 & -3 \\
      {}  1 & -15 &  6 \\
      {}  0 &  -2 &  0
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixQ}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[blu, th]####1}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, visible on=####1] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle Q$};
        \end{tikzpicture}
      }
      ]
      {} -\sfrac{2}{3} & -\sfrac{4}{15}  & -\sfrac{1}{5}   \\
      {}  \sfrac{2}{3} &  \sfrac{1}{5}   & -\sfrac{4}{15}  \\
      {}  \sfrac{1}{3} & -\sfrac{14}{15} &  \sfrac{2}{15}  \\
      {}             0 & -\sfrac{2}{15}  & -\sfrac{14}{15}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[red, th]####1}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, visible on=####1] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle R$};
        \end{tikzpicture}
      }
      ]
      3 & -3 & -2 \\
      0 & 15 & -7 \\
      0 &  0 &  1
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixQT}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[blu, th]####1}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, visible on=####1] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle Q^\intercal$};
        \end{tikzpicture}
      }
      ]
      -\sfrac{2}{3}  &  \sfrac{2}{3}  &  \sfrac{1}{3}   & 0               \\
      -\sfrac{4}{15} &  \sfrac{1}{5}  & -\sfrac{14}{15} & -\sfrac{2}{15}  \\
      -\sfrac{1}{5}  & -\sfrac{4}{15} &  \sfrac{2}{15}  & -\sfrac{14}{15}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixP}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[grn, th]####1}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, visible on=####1] ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle P_{\Col(A)}$};
        \end{tikzpicture}
      }
      ]
      {} \onslide####1{\sfrac{5}{9}}  & \onslide####1{-\sfrac{4}{9}} & \onslide####1{0} & \onslide####1{\sfrac{2}{9}} \\
      {} \onslide####1{-\sfrac{4}{9}} & \onslide####1{\sfrac{5}{9}}  & \onslide####1{0} & \onslide####1{\sfrac{2}{9}} \\
      {} \onslide####1{0}             & \onslide####1{0}             & \onslide####1{1} & \onslide####1{0}            \\
      {} \onslide####1{\sfrac{2}{9}}  & \onslide####1{\sfrac{2}{9}}  & \onslide####1{0} & \onslide####1{\sfrac{8}{9}}
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following $QR$-factorization.
    \[
      \MatrixA<2->=\MatrixQ<3->\MatrixR<4->
    \]
    \onslide<5->{Projection onto $\Col(A)$ is given by}
    \[
      \onslide<5->{\MatrixP<8-> = \MatrixQ<6->\MatrixQT<7->}
    \]
  \end{example}


\end{frame}


\section{Least Squares}
\subsection{$R\widehat{\bv[x]}=Q^\intercal\bv[b]$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose $A=QR$ and consider the least squares problem
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \subnode{AGramian}{\alt<6->{\textcolor{red}{Q^\intercal}QR}{\alt<3-5>{QR}{\color<2->{red}A}}}\widehat{\bv[x]}
          =
          \subnode{ATeqRTQT}{\alt<6->{\textcolor{red}{Q^\intercal}QQ^\intercal}{\alt<5>{Q Q^\intercal}{\color<4->{red}P}}}\bv[b]
          \)};

        \draw[<-, red, thick, overlay, visible on=<2>] (AGramian.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle QR$};
        \draw[<-, red, thick, overlay, visible on=<4>] (ATeqRTQT.south) |- ++(3mm, -2.5mm)
        node[right] {$\scriptstyle Q Q^\intercal$};
      \end{tikzpicture}
    \]
    \onslide<7->{The least squares problem reduces to
      $R\widehat{\bv[x]}=Q^\intercal\bv[b]$.}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Advantage}
    We now have two forms of the least squares problem
    \begin{align*}
      A^\intercal A\widehat{\bv[x]}=A^\intercal\bv[b] && R\widehat{\bv[x]}=Q^\intercal\bv[b]
    \end{align*}
    \onslide<2->{The system $R\widehat{\bv[x]}=Q^\intercal\bv[b]$ is quickly
      solved with \emph{\grn{back substitution}}.}
  \end{block}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[grn, th]####1}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, grn, thick, visible on=####1] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} -2 &  -2 &  3 \\
      {}  2 &   1 & -3 \\
      {}  1 & -15 &  6 \\
      {}  0 &  -2 &  0
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixQ}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[blu, th]####1}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, visible on=####1] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle Q$};
        \end{tikzpicture}
      }
      ]
      {} -\sfrac{2}{3} & -\sfrac{4}{15}  & -\sfrac{1}{5}   \\
      {}  \sfrac{2}{3} &  \sfrac{1}{5}   & -\sfrac{4}{15}  \\
      {}  \sfrac{1}{3} & -\sfrac{14}{15} &  \sfrac{2}{15}  \\
      {}             0 & -\sfrac{2}{15}  & -\sfrac{14}{15}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[red, th]####1}
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, visible on=####1] ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle R$};
        \end{tikzpicture}
      }
      ]
      3 & -3 & -2 \\
      0 & 15 & -7 \\
      0 &  0 &  1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorB}{
    \begin{bNiceMatrix}[r, small]
      3\\ 1\\ 7\\ -1
    \end{bNiceMatrix}
  }
  \newcommand{\System}{
    \begin{NiceArray}{rcrcrcrcl}[
      , margin
      , code-before = {
        \hlPortion<6-7>[red]{1}{1}{3}{5}
        \hlCol<7>{7}
        \hlPortion<8-9>{3}{5}{3}{7}
        \hlPortion<12-13>{2}{3}{2}{7}
        \hlPortion<15-16>{1}{1}{1}{7}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<6-7>] ($ (row-1-|col-1)!0.5!(row-1-|col-6) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle R\widehat{\bv[x]}$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<7>] ($ (row-1-|col-7)!0.5!(row-1-|col-8) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle Q^\intercal\bv[b]$};
        \end{tikzpicture}
      }
      ]
      {} 3\,\widehat{x}_1 &-&  3\alt<14->{\cdot0}{\,\widehat{x}_2} &-& 2\alt<11->{\cdot 1}{\,\widehat{x}_3} &=&  1 &\onslide<16->{\to}& \onslide<16->{\widehat{x}_1=\frac{1+3\cdot0+2\cdot1}{3} = 1} \\
      {}                  & & 15\,\widehat{x}_2                    &-& 7\alt<10->{\cdot 1}{\,\widehat{x}_3} &=& -7 &\onslide<13->{\to}& \onslide<13->{\widehat{x}_2=\frac{-7+7\cdot1}{15}    = 0}    \\
      {}                  & &                                      & &                        \widehat{x}_3 &=&  1 &\onslide<9->{ \to}& \onslide<9->{\widehat{x}_3                        = 1}
    \end{NiceArray}
  }
  \begin{example}
    Consider the $A=QR$ factorization and the vector $\bv[b]$ given by
    \begin{align*}
      \MatrixA<2->=\MatrixQ<3->\MatrixR<4-> && \bv[b] = \VectorB
    \end{align*}
    \onslide<5->{The least squares problem
      $A^\intercal A\widehat{\bv[x]}=A^\intercal\bv[b]$ reduces to
      $R\widehat{\bv[x]}=Q^\intercal\bv[b]$.}
    \[
      \onslide<5->{\System}
    \]
    \onslide<17->{Here, we find that $\widehat{\bv[x]}=\nv{1 0 1}$.}
  \end{example}


\end{frame}


\end{document}
