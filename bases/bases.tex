\documentclass[]{bmr}

\title{Bases}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Bases}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\SpanAxiom}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\Span\Set{\bv_1,\dotsc,\bv_d}=V$};
      \node[above, grn] at (text.north) {\scriptsize Spanning Axiom};
    \end{tikzpicture}
  }
  \newcommand{\LinIndAxiom}{
    \begin{tikzpicture}[visible on=<3->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\Set{\bv_1,\dotsc,\bv_d}$ linearly independent};
      \node[above, blu] at (text.north) {\scriptsize Linear Independence Axiom};
    \end{tikzpicture}
  }
  \begin{definition}
    A list of vectors $\beta=\Set{\bv_1,\bv_2,\dotsc,\bv_d}$ in a vector space
    $V$ is a \emph{\grn{basis}} if
    \begin{align*}
      \SpanAxiom && \LinIndAxiom
    \end{align*}
    \onslide<4->{We think of a basis as a \emph{\grn{minimal spanning set}}.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle\bv$};
        \end{tikzpicture}
      }
      ]
      1\\ 2\\ 3
    \end{bNiceMatrix}
  }
  \newcommand{\VectorW}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<3->[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle2\cdot\bv$};
        \end{tikzpicture}
      }
      ]
      2\\ 4\\ 6
    \end{bNiceMatrix}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<4->[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle-\bv$};
        \end{tikzpicture}
      }
      ]
      -1\\ -2\\ -3
    \end{bNiceMatrix}
  }
  \newcommand{\Figure}{
    \begin{tikzpicture}[ultra thick, ->, anchor=base, baseline]
      \coordinate (O) at (0, 0);
      \coordinate (v) at (15: 1);
      \coordinate (w) at ($ 2*(v) $);
      \coordinate (x) at ($ -1*(v) $);

      \draw[thick, blu, <->, visible on=<5->] ($ -1.5*(v) $) -- ($ 2.5*(v) $) node[right] {$\scriptstyle V$};

      \draw[grn, visible on=<3->] (O) -- (w) node[pos=0.75, sloped, above] {$\scriptstyle 2\cdot\bv$};
      \draw[blu, visible on=<2->] (O) -- (v) node[midway, above, sloped] {$\scriptstyle\bv$};
      \draw[red, visible on=<4->] (O) -- (x) node[midway, above, sloped] {$\scriptstyle-\bv$};
    \end{tikzpicture}
  }
  \begin{example}
    Consider the vector space $V\subset\mathbb{R}^3$ given by
    \begin{align*}
      V = \Span\Set*{\VectorV, \VectorW, \VectorX} && \Figure
    \end{align*}
    \onslide<6->{These vectors are all \emph{dependent}.} \onslide<7->{Choosing
      any one gives a basis of $V$.}
    \begin{align*}
      \onslide<7->{V=\Span\Set{\nv[small]{1 2 3}}} && \onslide<7->{V=\Span\Set{\nv[small]{2 4 6}}} && \onslide<7->{V=\Span\Set{\nv[small]{-1 -2 -3}}}
    \end{align*}
    \onslide<8->{Vector spaces have infinitely many bases!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<2->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {}  2 &   1 & 5 & 25 \\
      {} -1 & -12 & 9 & 68 \\
      {}  3 &   5 & 4 & 13
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      1 & 0 &  3 & 16 \\
      0 & 1 & -1 & -7 \\
      0 & 0 &  0 &  0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      -3\,c_1-16\,c_2 \\
      c_1+7\,c_2 \\
      c_1\\
      c_2
    \end{bNiceMatrix}
  }
  \newcommand{\VectorVa}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<5->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, remember picture, visible on=<6->]
          % \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm)
          % node[left] {$\scriptstyle \bv_1$};
          \draw[visible on=<2->] ($ (row-5-|col-1)!0.5!(row-5-|col-2) $) |- ++(-3mm, -2.5mm)
          node[left] {\scriptsize ``pivot solutions'' to $A\bv=\bv[O]$};
        \end{tikzpicture}
      }
      ]
      -3\\ 1\\ 1\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorVb}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<5->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<6->]
          % \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm)
          % node[left] {$\scriptstyle \bv_2$};
          \draw[visible on=<2->] ($ (row-5-|col-1)!0.5!(row-5-|col-2) $) |- ++(-15mm, -2.5mm);
        \end{tikzpicture}
      }
      ]
      -16\\ 7\\ 0\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\Solution}{\scriptstyle\bv=\VectorV=c_1\cdot\VectorVa+c_2\cdot\VectorVb}
  \newcommand{\PivotVa}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat<5->[grn]}
      ]
      -3 & 1 & 1 & 0
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\PivotVb}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat<5->[grn]}
      ]
      -16 & 7 & 0 & 1
    \end{bNiceMatrix}^\intercal
  }
  \begin{example}
    To find vectors $\bv\in\Null(A)$, we must solve $A\bv=\bv[O]$ for $\bv$.
    \begin{align*}
      \rref\MatrixA=\MatrixR && \onslide<3->{\Solution}
    \end{align*}
    \onslide<4->{Here, we have shown that}
    \[
      \onslide<4->{\Null(A)=\Span\Set{\PivotVa, \PivotVb}}
    \]
    \onslide<7->{This is called the \emph{\grn{pivot basis}} of $\Null(A)$.}
  \end{example}

\end{frame}


\section{Fundamental Subspaces}
\subsection{Bases}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    We may systematically construct bases of the four fundamental subspaces.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<1->
        ] {$\scriptstyle\Col(A^\intercal)$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<1->
        ] {$\scriptstyle\Null(A)$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^n$};

        \begin{scope}[xshift=4.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<1->
          ] {$\scriptstyle\Col(A)$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<1->
          ] {$\scriptstyle\Null(A^\intercal)$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A$};

        % \draw[->, thick, visible on=<1->] (Rm.south west) to[bend left]
        % node[midway, below] {$\scriptstyle A^\intercal$} (Rn.south east);

        \draw[Point, blu, visible on=<1->]
        (Col.north) |- ++(3mm, 2.5mm)
        node[right, scale=0.6] {$
          \begin{aligned}
            \Col(A) &= \Span\Set{\textnormal{pivot columns of }A}                 \\
            {}      &= \Span\Set{\textnormal{nonzero rows of }\rref(A^\intercal)}
          \end{aligned}
          $};

        \draw[Point, blu, visible on=<2->]
        (Row.north) |- ++(-3mm, 2.5mm)
        node[left, scale=0.6] {$
          \begin{aligned}
            \Col(A^\intercal) &= \Span\Set{\textnormal{pivot columns of }A^\intercal} \\
            {}                &= \Span\Set{\textnormal{nonzero rows of }\rref(A)}
          \end{aligned}
          $};

        \draw[Point, red, visible on=<4->]
        (LNull.north) |- ++(2mm, 2.5mm)
        node[right, scale=0.55] {$\Null(A^\intercal)=\Span\Set{\textnormal{pivot sols to }A^\intercal\bv=\bv[O]}$};

        \draw[Point, grn, visible on=<3->]
        (Null.north) |- ++(-2mm, 2.5mm)
        node[left, scale=0.55] {$\Null(A)=\Span\Set{\textnormal{pivot sols to }A\bv=\bv[O]}$};

      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}

\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlMat<2-3>[blu, th]
        \hlCol<5-9>[blu, th]{1}
        \hlCol<5-9>[blu, th]{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} -2 & -7 & -4 & -9 \\
      {}  3 & 10 &  6 & 13 \\
      {} -1 & -2 & -2 & -3
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlEntry<5-9>[blu, th]{1}{1}
        \hlEntry<5-9>[blu, th]{2}{2}
        \hlRow<14-15>[blu, th]{1}
        \hlRow<14-15>[blu, th]{2}
        \hlMat<17-18>[grn, th]
      }
      ]
      1 & 0 & 2 & 1 \\
      0 & 1 & 0 & 1 \\
      0 & 0 & 0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAT}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlMat<2-3>[blu, th]
        \hlCol<11-15>[blu, th]{1}
        \hlCol<11-15>[blu, th]{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A^\intercal$};
        \end{tikzpicture}
      }
      ]
      -2 &  3 & -1 \\
      -7 & 10 & -2 \\
      -4 &  6 & -2 \\
      -9 & 13 & -3
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixATR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlRow<8-9>[blu, th]{1}
        \hlRow<8-9>[blu, th]{2}
        \hlEntry<11-15>[blu, th]{1}{1}
        \hlEntry<11-15>[blu, th]{2}{2}
        \hlMat<20-21>[red, th]
      }
      ]
      1 & 0 & -4 \\
      0 & 1 & -3 \\
      0 & 0 &  0 \\
      0 & 0 &  0
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the calculations
    \begin{align*}
      \rref\MatrixA=\MatrixAR && \rref\MatrixAT=\MatrixATR
    \end{align*}
    \onslide<3->{This information gives bases of the four fundamental
      subspaces.}
    \[
      \begin{tikzpicture}[visible on=<3->]

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<1->
        ] {$\scriptstyle\Col(A^\intercal)$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<1->
        ] {$\scriptstyle\Null(A)$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^4$};

        \begin{scope}[xshift=4.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<1->
          ] {$\scriptstyle\Col(A)$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<1->
          ] {$\scriptstyle\Null(A^\intercal)$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^3$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A$};

        % \draw[->, thick, visible on=<1->] (Rm.south west) to[bend left]
        % node[midway, below] {$\scriptstyle A^\intercal$} (Rn.south east);

        \draw[Point, blu, visible on=<4->]
        (Col.north) |- ++(5mm, 2.5mm)
        node[right, scale=0.75] {$
          \begin{aligned}
            \onslide<4->{\beta_1} &\onslide<4->{=} \onslide<6->{\Set{\nv[small]{-2 3 -1}, \nv[small]{-7 10 -2}}} \\
            \onslide<7->{\beta_2} &\onslide<7->{=} \onslide<9->{\Set{\nv[small]{1 0 -4}, \nv[small]{0 1 -3}}}
          \end{aligned}
          $};

        \draw[Point, blu, visible on=<10->]
        (Row.north) |- ++(-5mm, 2.5mm)
        node[left, scale=0.75] {$
          \begin{aligned}
            \onslide<10->{\beta_1} &\onslide<10->{=} \onslide<12->{\Set{\nv[small]{-2 -7 -4 -9}, \nv[small]{3 10 6 13}}} \\
            \onslide<13->{\beta_2} &\onslide<13->{=} \onslide<15->{\Set{\nv[small]{1 0 2 1}, \nv[small]{0 1 0 1}}}
          \end{aligned}
          $};

        \draw[Point, red, visible on=<19->]
        (LNull.north) |- ++(2mm, 2.5mm)
        node[right, scale=0.75] {$\beta=\onslide<21->{\Set{\nv[small]{4 3 1}}}$};

        \draw[Point, grn, visible on=<16->]
        (Null.north) |- ++(-2mm, 2.5mm)
        node[left, scale=0.75] {$\beta=\onslide<18->{\Set{\nv[small]{-2 0 1 0}, \nv[small]{-1 -1 0 1}}}$};

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\section{Digraphs}
\subsection{Bases of $\Null(A)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[first-row]
      a_1 & \color{red}{a_2} & a_3 & a_4 & a_5 \\
      -1  & \color{red}{  1} &   1 &   0 &   0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorW}{
    \begin{bNiceMatrix}[first-row]
      a_1 & a_2 & a_3 & \color{red}{a_4} & a_5 \\
      {}1 &   0 &   0 & \color{red}{  1} &   0
    \end{bNiceMatrix}
  }
  \begin{theorem}
    Purge the minimum number of arrows necessary to break all cycles.
    \[
      \begin{tikzpicture}[grph]
        \node[state, alt=<{6,9}>{fill=grnbg}] (v1)                                 {$v_1$};
        \node[state, alt=<{6,9}>{fill=grnbg}] (v2) [above right=0.85cm and 4cm of v1] {$v_2$};
        \node[state, alt=<6>{fill=grnbg}] (v3) [below right=0.85cm and 3cm of v1] {$v_3$};
        \node[state] (v4) [below right=1cm and 3cm of v2] {$v_4$};

        \draw[->, bend left, alt=<{6,9}>{grn}]
        (v1) edge node {$\scriptstyle a_1$} (v2);

        \draw[->, bend right, alt=<2->{red}, visible on=<{1-2,6}>]
        (v1) edge node {$\scriptstyle a_2$} (v3);

        \draw[->, bend right, alt=<6>{grn}]
        (v3) edge node {$\scriptstyle a_3$} (v2);

        \draw[->, bend left, alt=<2->{red}, visible on=<{1-2,9}>]
        (v2) edge node {$\scriptstyle a_4$} (v1);

        \draw[->, bend left]  (v2) edge node {$\scriptstyle a_5$} (v4);
      \end{tikzpicture}
    \]
    \onslide<4->{Let $\Set{c_1,\dotsc,c_k}$ be cycles, each passing through
      \red{exactly one} purged arrow.}
    \begin{align*}
      \onslide<5->{\bv_{c_1}=\VectorV} && \onslide<8->{\bv_{c_2}=\VectorW}
    \end{align*}
    \onslide<10->{Then $\Set{\bv_{c_1},\dotsc,\bv_{c_k}}$ is a basis of
      $\Null(A)$.} \onslide<11->{Here,
      $\Null(A)=\Span\Set{\bv_{c_1}, \bv_{c_2}}$.}
  \end{theorem}

\end{frame}


\subsection{Bases of $\Null(A^\intercal)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Ga}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]
        \node[state, alt=<4-5>{fill=redbg}] (v1) {$v_1$};
        \node[state, alt=<4-5>{fill=redbg}] (v2) [right=of v1] {$v_2$};
        \node[state, alt=<4-5>{fill=redbg}] (v3) [below right=1.5cm and 1.5cm of v1] {$v_3$};

        \draw[->] (v1) edge (v2);
        \draw[->] (v3) edge (v1);
        \draw[->] (v3) edge (v2);
        \draw[->, bend left] (v2) edge (v3);

        \node[fill=none, overlay, visible on=<2->] at (current bounding box.north) {$\scriptstyle G_1$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Gb}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]
        \node[state, alt=<7-8>{fill=redbg}] (v4) {$v_4$};
        \node[state, alt=<7-8>{fill=redbg}] (v5) [right=of v1] {$v_5$};

        \draw[->, bend left] (v4) edge (v5);
        \draw[->] (v4) edge (v5);
        \draw[->, bend right] (v4) edge (v5);

        \node[above, fill=none, overlay, visible on=<2->] at (current bounding box.north) {$\scriptstyle G_2$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[first-row]
      \color<5>{red}{v_1} & \color<5>{red}{v_2} & \color<5>{red}{v_3} & v_4 & v_5 \\
      \color<5>{red}{1}   & \color<5>{red}{1}   & \color<5>{red}{1}   & 0   & 0
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\VectorW}{
    \begin{bNiceMatrix}[first-row]
      v_1 & v_2 & v_3 & \color<8>{red}{v_4} & \color<8>{red}{v_5} \\
      0   & 0   & 0   & \color<8>{red}{1}   & \color<8>{red}{1}
    \end{bNiceMatrix}^\intercal
  }
  \begin{theorem}
    Let $\Set{G_1,\dotsc,G_k}$ be the connected components of a digraph $G$.
    \begin{align*}
      \Ga && \Gb
    \end{align*}
    \onslide<3->{Consider the associated \grn{classification vectors}
      $\Set{\bv_{G_1},\dotsc, \bv_{G_k}}$.}
    \begin{align*}
      \onslide<3->{\bv_{G_1}=\VectorV} && \onslide<6->{\bv_{G_2}=\VectorW}
    \end{align*}
    \onslide<9->{These vectors form a basis of $\Null(A^\intercal)$.}
    \onslide<10->{Here, $\Null(A^\intercal)=\Span\Set{\bv_{G_1}, \bv_{G_2}}$.}
  \end{theorem}

\end{frame}

\end{document}
