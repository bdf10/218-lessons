\documentclass[]{bmr}

\title{Linear Approximations}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Coordinate Functions}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Functionf}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<5->{1}{1}
        \hlEntry<6->[grn]{2}{1}
        \hlEntry<7->[red]{4}{1}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.75mm, visible on=<5->]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -- ++(5mm, 0)
          node[right] {$f_1\in\mathscr{C}(\mathbb{R}^n)$};
          \draw[<-, grn, thick, shorten <=0.75mm, visible on=<6->]
          ($ (row-2-|col-2)!0.5!(row-3-|col-2) $) -- ++(5mm, 0)
          node[right] {$f_2\in\mathscr{C}(\mathbb{R}^n)$};
          \draw[<-, red, thick, shorten <=0.75mm, visible on=<7->]
          ($ (row-4-|col-2)!0.5!(row-5-|col-2) $) -- ++(5mm, 0)
          node[right] {$f_m\in\mathscr{C}(\mathbb{R}^n)$};
        \end{tikzpicture}
      }
      ]
      f_1(x_1,\dotsc,x_n) \\
      f_2(x_1,\dotsc,x_n) \\
      \Vdots \\
      f_m(x_1,\dotsc,x_n)
    \end{bNiceMatrix}
  }
  \begin{definition}
    A \emph{\grn{function}} $\bv[f]$ from $\mathbb{R}^n$ to $\mathbb{R}^m$ is a
    rule
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(\bv[f]:
          \subnode{Rn}{\textcolor<2->{red}{\mathbb{R}^n}}
          \to
          \subnode{R}{\textcolor<3->{blu}{\mathbb{R}^m}}
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (Rn.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize ``domain''};

        \draw[<-, thick, blu, overlay, visible on=<3->] (R.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize ``target''};
      \end{tikzpicture}
    \]
    that associates every vector $\bv[x]\in\mathbb{R}^n$ a vector
    $\bv[f](\bv[x])\in\mathbb{R}^m$. \onslide<4->{Here, $\bv[f]$ looks like}
    \[
      \onslide<4->{\bv[f](x_1,\dotsc,x_n)=\Functionf}
    \]
    \onslide<8->{The scalar fields $f_1,f_2,\dotsc,f_n$ are the
      \emph{\grn{coordinate functions}} of $\bv[f]$.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Functionf}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<2->{1}{1}
        \hlEntry<3->[grn]{1}{2}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-2-|col-1)!0.5!(row-2-|col-2) $) |- ++(-2mm, -2.5mm)
          node[left] {$f_1(x, y, z)$};
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<3->]
          ($ (row-2-|col-2)!0.5!(row-2-|col-3) $) |- ++(2mm, -2.5mm)
          node[right] {$f_2(x, y, z)$};
        \end{tikzpicture}
      }
      ]
      xy & yz^3-x
    \end{bNiceMatrix}^\intercal
  }
  \begin{example}[$\boldsymbol{f}:\mathbb{R}^3\to\mathbb{R}^2$]%[$\bv[f]:\mathbb{R}^3\to\mathbb{R}^2$]
    $\bv[f](x, y, z)=\Functionf\vspace{1em}$
  \end{example}

  \newcommand{\Functiong}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<5->{1}{1}
        \hlEntry<6->[grn]{2}{1}
        \hlEntry<7->[red]{3}{1}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.75mm, visible on=<5->]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -- ++(5mm, 0)
          node[right] {$g_1(p, q, r, s)$};
          \draw[<-, grn, thick, shorten <=0.75mm, visible on=<6->]
          ($ (row-2-|col-2)!0.5!(row-3-|col-2) $) -- ++(5mm, 0)
          node[right] {$g_2(p, q, r, s)$};
          \draw[<-, red, thick, shorten <=0.75mm, visible on=<7->]
          ($ (row-3-|col-2)!0.5!(row-4-|col-2) $) -- ++(5mm, 0)
          node[right] {$g_3(p, q, r, s)$};
        \end{tikzpicture}
      }
      ]
      pq+r\\ p-s\\ e^{ps^2}
    \end{bNiceMatrix}
  }
  \begin{example}[$\boldsymbol{g}:\mathbb{R}^4\to\mathbb{R}^3$]<4->%[$\bv[f]:\mathbb{R}^3\to\mathbb{R}^2$]
    $\bv[g](p, q, r, s)=\Functiong$
  \end{example}

  \newcommand{\Functionh}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<9->{1}{1}
        \hlEntry<10->[grn]{1}{2}
        \hlEntry<11->[red]{1}{3}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<9->]
          ($ (row-2-|col-1)!0.5!(row-2-|col-2) $) |- ++(-2mm, -2.5mm)
          node[left] {$h_1(t)$};
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<10->]
          ($ (row-2-|col-2)!0.5!(row-2-|col-3) $) |- ++(-2mm, -2.5mm)
          node[left] {$h_2(t)$};
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<11->]
          ($ (row-2-|col-3)!0.5!(row-2-|col-4) $) |- ++(-1mm, -5mm)
          node[left] {$h_3(t)$};
        \end{tikzpicture}
      }
      ]
      \cos(t) & \sin(t) & t
    \end{bNiceMatrix}^\intercal
  }
  \begin{example}[$\boldsymbol{h}:\mathbb{R}\to\mathbb{R}^3$]<8->
    $\bv[h](t)=\Functionh$
  \end{example}

\end{frame}


\section{Jacobian Derivatives}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\pd}[2]{\sfrac{\partial f_{####1}}{\partial x_{####2}}}
  \newcommand{\Df}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlCol<2>{1}
        \hlCol<3>[grn]{2}
        \hlCol<4>[red]{4}
        \hlRow<5>{1}
        \hlRow<6>[grn]{2}
        \hlRow<7>[red]{4}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2>]
          ($ (row-5-|col-1)!0.5!(row-5-|col-2) $) |- ++(-2mm, -2.5mm)
          node[left] {$\sfrac{\partial \bv[f]}{\partial x_1}$};
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<3>]
          ($ (row-5-|col-2)!0.5!(row-5-|col-3) $) |- ++(-2mm, -2.5mm)
          node[left] {$\sfrac{\partial \bv[f]}{\partial x_2}$};
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<4>]
          ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-2mm, -2.5mm)
          node[left] {$\sfrac{\partial \bv[f]}{\partial x_n}$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<5>]
          ($ (row-1-|col-5)!0.5!(row-2-|col-5) $) -- ++(5mm, 0)
          node[right] {$\sfrac{\partial f_1}{\partial \bv[x]}$};
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<6>]
          ($ (row-2-|col-5)!0.5!(row-3-|col-5) $) -- ++(5mm, 0)
          node[right] {$\sfrac{\partial f_2}{\partial \bv[x]}$};
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<7>]
          ($ (row-4-|col-5)!0.5!(row-5-|col-5) $) -- ++(5mm, 0)
          node[right] {$\sfrac{\partial f_m}{\partial \bv[x]}$};
        \end{tikzpicture}
      }
      ]
      \pd{1}{1} & \pd{1}{2} & \Cdots & \pd{1}{n} \\
      \pd{2}{1} & \pd{2}{2} & \Cdots & \pd{2}{n} \\
      \Vdots    & \Vdots    & \Ddots & \Vdots    \\
      \pd{m}{1} & \pd{m}{2} & \Cdots & \pd{m}{n}
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{Jacobian derivative}} of $\bv[f]:\mathbb{R}^n\to\mathbb{R}^m$
    is the $m\times n$ matrix
    \[
      D\bv[f]
      =
      \Df
    \]
    \onslide<8->{The $(i, j)$ entry of $D\bv[f]$ is $\pd{i}{j}$.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Functionf}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlCol<2->{1}
        \hlCol<3->[grn]{2}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$f_1$};
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<3->]
          ($ (row-1-|col-2)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$f_2$};
        \end{tikzpicture}
      }
      ]
      x^2y & {xy-\sin(y)}
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\pd}[2]{\sfrac{\partial f_{####1}}{\partial ####2}}
  \newcommand{\DfFormula}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<5-6>{1}{1}
        \hlEntry<7-8>{1}{2}
        \hlEntry<9-10>[grn]{2}{1}
        \hlEntry<11-12>[grn]{2}{2}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      \pd{1}{x} & \pd{1}{y} \\
      \pd{2}{x} & \pd{2}{y}
    \end{bNiceMatrix}
  }
  \newcommand{\Df}{
    \begin{bNiceMatrix}[
      , margin
      , c
      , code-before = {
        \hlEntry<5-6>{1}{1}
        \hlEntry<7-8>{1}{2}
        \hlEntry<9-10>[grn]{2}{1}
        \hlEntry<11-12>[grn]{2}{2}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      \onslide<6->{2\,xy} & \onslide<8->{x^2}       \\
      \onslide<10->{y}     & \onslide<12->{x-\cos(y)}
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider $\bv[f](x, y)=\Functionf$. \onslide<4->{The Jacobian derivative is}
    \[
      \onslide<4->{D\bv[f] = \DfFormula = \Df}
    \]
    \onslide<13->{Note that $D\bv[f]$ is $2\times 2$ since
      $\bv[f]:\mathbb{R}^2\to\mathbb{R}^2$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Functiong}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlCol<2->{1}
        \hlCol<3->[grn]{2}
        \hlCol<4->[red]{3}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$g_1$};
          \draw[<-, grn, thick, shorten <=0.5mm, visible on=<3->]
          ($ (row-1-|col-2)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$g_2$};
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<4->]
          ($ (row-1-|col-3)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$g_3$};
        \end{tikzpicture}
      }
      ]
      u & u\cos(v) & u\sin(v)
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\pd}[2]{\sfrac{\partial g_{####1}}{\partial ####2}}
  \newcommand{\DgFormula}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<6-7>{1}{1}
        \hlEntry<8-9>{1}{2}
        \hlEntry<10-11>[grn]{2}{1}
        \hlEntry<12-13>[grn]{2}{2}
        \hlEntry<14-15>[red]{3}{1}
        \hlEntry<16-17>[red]{3}{2}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      \pd{1}{u} & \pd{1}{v} \\
      \pd{2}{u} & \pd{2}{v} \\
      \pd{3}{u} & \pd{3}{v}
    \end{bNiceMatrix}
  }
  \newcommand{\Dg}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlEntry<6-7>{1}{1}
        \hlEntry<8-9>{1}{2}
        \hlEntry<10-11>[grn]{2}{1}
        \hlEntry<12-13>[grn]{2}{2}
        \hlEntry<14-15>[red]{3}{1}
        \hlEntry<16-17>[red]{3}{2}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      \onslide<7->{1}       & \onslide<9->{0} \\
      \onslide<11->{\cos(v)} & \onslide<13->{-u\sin(v)} \\
      \onslide<15->{\sin(v)} & \onslide<17->{u\cos(v)}
    \end{bNiceMatrix}
  }

  \begin{example}
    Consider $\bv[g](u, v)=\Functiong$. \onslide<5->{The Jacobian derivative is}
    \[
      \onslide<5->{D\bv[g] = \DgFormula = \Dg}
    \]
    \onslide<18->{Note that $D\bv[g]$ is $3\times 2$ since
      $\bv[g]:\mathbb{R}^2\to\mathbb{R}^3$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Functionh}{
    \begin{bNiceMatrix}[
      % , margin
      , r
      % , code-before = {
      % \hlMat<2->
      % }
      %   , code-after = {
      %   \begin{tikzpicture}
      %     \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %     ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
      %     node[left] {$g_1$};
      %     \draw[<-, grn, thick, shorten <=0.5mm, visible on=<2->]
      %     ($ (row-1-|col-2)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %     node[left] {$g_2$};
      %     \draw[<-, red, thick, shorten <=0.5mm, visible on=<2->]
      %     ($ (row-1-|col-3)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
      %     node[left] {$g_3$};
      %   \end{tikzpicture}
      % }
      ]
      x_1 & 3\,x_1^2x_3 & 4\,x_2^2-22\,x_3 & x_3\sin(x_1)
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\pd}[2]{\sfrac{\partial h_{####1}}{\partial x_{####2}}}
  \newcommand{\DhFormula}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlCol<3-4>{1}
        \hlCol<5-6>[grn]{2}
        \hlCol<7-8>[red]{3}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      \pd{1}{1} & \pd{1}{2} & \pd{1}{3} \\
      \pd{2}{1} & \pd{2}{2} & \pd{2}{3} \\
      \pd{3}{1} & \pd{3}{2} & \pd{3}{3} \\
      \pd{4}{1} & \pd{4}{2} & \pd{4}{3}
    \end{bNiceMatrix}
  }
  \newcommand{\Dh}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlCol<3-4>{1}
        \hlCol<5-6>[grn]{2}
        \hlCol<7-8>[red]{3}
      }
      % , code-after = {
      % \begin{tikzpicture}
      %   \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2->]
      %   ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
      %   node[left] {$A$};
      % \end{tikzpicture}
      % }
      ]
      \onslide<4->{1}            & \onslide<6->{0}      & \onslide<8->{0}         \\
      \onslide<4->{6\,x_1x_3}    & \onslide<6->{0}      & \onslide<8->{3\,x_1^2}  \\
      \onslide<4->{0}            & \onslide<6->{8\,x_2} & \onslide<8->{-22}       \\
      \onslide<4->{x_3\cos(x_1)} & \onslide<6->{0}      & \onslide<8->{\sin(x_1)}
    \end{bNiceMatrix}
  }

  \begin{example}
    Consider the function $\bv[h]$ given by
    \[
      \bv[h](x_1, x_2, x_3)=\Functionh
    \]
    \onslide<2->{The Jacobian derivative of $\bv[h]$ is}
    \[
      \onslide<2->{D\bv[h] = \DhFormula = \Dh}
    \]
    \onslide<9->{Note that $D\bv[h]$ is $4\times 3$ since
    $\bv[h]:\mathbb{R}^3\to\mathbb{R}^4$.}
  \end{example}

\end{frame}


\section{Linear Approximations}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{local linearization}} of $\bv[f]:\mathbb{R}^n\to\mathbb{R}^m$
    at $P\in\mathbb{R}^n$ is
    \[
      \bv[L]_P(\bv[x]) = \bv[f](P)+D\bv[f](P)(\bv[x]-\bv[P])
    \]
    \onslide<2->{We use $\bv[L]_P$ to approximate values of $\bv[f]$ ``near''
      $P$ with}
    \begin{align*}
      \onslide<2->{\bv[f](\bv[P]+\Delta\bv[P])}
      &\onslide<2->{\approx} \onslide<3->{\bv[L]_P(\bv[P]+\Delta\bv[P])} \\
      &\onslide<3->{=} \onslide<4->{\bv[f](P)+D\bv[f](P)(\bv[P]+\Delta\bv[P]-\bv[P])} \\
      &\onslide<4->{=} \onslide<5->{\bv[f](P)+D\bv[f](P)\Delta\bv[P]}
    \end{align*}
    \onslide<6->{This is the idea behind \emph{\grn{linear approximations}}.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Df}{
    \begin{bNiceMatrix}[r]
      -5\,y & -5\,x-1 \\
      -10   & -3
    \end{bNiceMatrix}
  }
  \newcommand{\DfP}{
    \begin{bNiceMatrix}[r]
      10 & -6 \\
      -10 & -3
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider $\bv[f](x, y)=\nv{{-5\,xy-y} {-10\,x-3\,y}}$, which satisfies
    \begin{align*}
      \bv[f](1, -2) &= \nvc{12; -4} & D\bv[f] &= \Df & D\bv[f](1, -2) &= \DfP
    \end{align*}
    \pause We may approximate $\bv[f](1-\sfrac{1}{10}, -2+\sfrac{1}{3})$ with
    \[
      \bv[f](1-\sfrac{1}{10}, -2+\sfrac{1}{3})
      \approx
      \overset{\bv[f](1, -2)}{\nvc{12; -4}}
      +
      \overset{D\bv[f](1, -2)}{\DfP}
      \overset{\Delta\bv[P]}{\nvc{-\sfrac{1}{10}; \sfrac{1}{3}}}
      = \nvc{9; -4}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Dg}{
    \begin{bNiceMatrix}[r]
      y\cos(xy+1) & x\cos(xy+1) \\
      \cos(y+1)   & -x\sin(y+1) \\
      y^3         & 3\,xy^2
    \end{bNiceMatrix}
  }
  \newcommand{\DgP}{
    \begin{bNiceMatrix}[r]
      {} -1 & 1 \\
      {}  1 & 0 \\
      {} -1 & 3
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider $\bv[g](x, y)=\nv{\sin(xy+1) x\cos(y+1) xy^3}$, which satisfies
    \begin{gather*}
      \begin{align*}
        \bv[g](1, -1) &= \nvc{0; 1; -1} & D\bv[g] &= \Dg & D\bv[g](1, -1) &= \DgP
      \end{align*}
    \end{gather*}
    \pause We may approximate $\bv[g](1-\sfrac{1}{3}, -1+\sfrac{1}{4})$ with
    \[
      \bv[g](1-\sfrac{1}{3}, -1+\sfrac{1}{4})
      \approx
      \overset{\bv[g](1, -1)}{\nvc{0; 1; -1}}
      +
      \overset{D\bv[g](1, -1)}{\DgP}
      \overset{\Delta\bv[P]}{\nvc{\sfrac{-1}{3}; \sfrac{1}{4}}}
      = \nvc{\sfrac{7}{12}; \sfrac{2}{3}; \sfrac{1}{12}}
    \]
  \end{example}

\end{frame}





\end{document}
