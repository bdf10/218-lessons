\documentclass[]{bmr}

\title{Determinants I}

\usepackage{extarrows}
\def\cpm{%
  \mathchoice%
  {\xcpm\displaystyle{.2ex}{.53ex}}% displaystyle
  {\xcpm\textstyle{.2ex}{.53ex}}% textstyle
  {\xcpm\scriptstyle{.16ex}{.43ex}}% scriptstyle
  {\xcpm\scriptscriptstyle{.11ex}{.35ex}}% scriptscriptstyle
}
\def\xcpm#1#2#3{\mathbin{\ooalign{%
      \raise #2\hbox{\pdfliteral{0.0 0.5 0.0 rg}$#1+$\pdfliteral{0 g}}\cr
      \lower #3\hbox{\pdfliteral{1.0 0.0 0.0 rg}$#1-$\pdfliteral{0 g}}%
    }
  }
}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Determinants}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      4 & -9 & 3 &  7 \\
      2 &  1 & 0 & -5 \\
      4 & -3 & 4 &  2 \\
      0 & -6 & 0 &  5
    \end{bNiceMatrix}
  }
  \newcommand{\SubMatrixA}{
    \begin{bNiceMatrix}[r]
      \textcolor<1>{opq}{4}    & \textcolor<3->{opq}{-9}   & \textcolor<2>{opq}{3}    & \textcolor<0>{opq}{7}  \\
      \textcolor<1>{opq}{2}    & \textcolor<1,3->{opq}{1}  & \textcolor<1,2>{opq}{0}  & \textcolor<1>{opq}{-5} \\
      \textcolor<1,3->{opq}{4} & \textcolor<3->{opq}{-3}   & \textcolor<2,3->{opq}{4} & \textcolor<3->{opq}{2} \\
      \textcolor<1,2>{opq}{0}  & \textcolor<2,3->{opq}{-6} & \textcolor<2>{opq}{0}    & \textcolor<2>{opq}{5}
    \end{bNiceMatrix}
  }
  \begin{definition}
    We define $A_{ij}$ by deleting the \emph{\grn{$i$th row}} and
    \emph{\blu{$j$th column}} of $A$.
    \begin{align*}
      A=\MatrixA && A_{\only<1>{21}\only<2>{43}\only<3->{32}}=\SubMatrixA
    \end{align*}
    \onslide<4->{Here, $A$ is $4\times 4$ and each $A_{ij}$ is $3\times 3$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The \emph{\grn{determinant}} of a $1\times 1$ matrix is
    \[
      \det\begin{bNiceMatrix}a_{11}\end{bNiceMatrix} = a_{11}
    \]
    \onslide<2->{The \emph{\grn{determinant}} of an $n\times n$ matrix is}
    \[
      \begin{tikzpicture}[remember picture, outer sep=0, inner sep=0, visible on=<2->]
        \node {\(
          \displaystyle
          \det(A)
          =
          \sum_{k=1}^n
          \subnode{detdefpm1}{\color<3->{red}(-1)^{k+1}}
          \subnode{defdetak}{\color<4->{blu}a_{k1}}
          \subnode{defdetAk}{\color<5->{grn}\det(A_{k1})}
          \)};

        \draw[<-, thick, red, shorten <=0.5mm, shorten >=0.5mm, overlay, visible on=<3->]
        (detdefpm1.south) |- ++(1.5mm, -3.5mm) node[right] {$\scriptstyle\pm1$};

        \draw[<-, thick, blu, shorten <=0.5mm, shorten >=0.5mm, overlay, visible on=<4->]
        (defdetak.south) |- ++(1.5mm, -3.5mm) node[right] {\scriptsize\textnormal{$k$th entry in first column}};

        \draw[<-, thick, grn, shorten <=0.5mm, shorten >=0.5mm, overlay, visible on=<5->]
        (defdetAk.north) |- ++(1.5mm, 3.5mm) node[right] {\scriptsize\textnormal{$(n-1)\times(n-1)$ determinant}};
      \end{tikzpicture}
    \]
    \onslide<6->{When convenient, we write $\det(A)=\abs{A}$.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \newcommand{\DeterminantA}{
    \begin{vNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlDiag<3->[blu]
        \begin{tikzpicture}
          \draw[ultra thick, fill=grnbg, rounded corners, visible on=<4->]
          (row-1-|col-3)
          |- (row-2-|col-2)
          |- (row-3-|col-1)
          |- (row-2-|col-2)
          -- (row-1-|col-2)
          -- cycle;
        \end{tikzpicture}
      }
      ]
      a_{11} & a_{12} \\
      a_{21} & a_{22}
    \end{vNiceMatrix}
  }
  \newcommand{\DeterminantAaa}{
    \begin{vNiceMatrix}[r]
      \textcolor{opq}{a_{11}} & \textcolor{opq}{a_{12}} \\
      \textcolor{opq}{a_{21}} & a_{22}
    \end{vNiceMatrix}
  }
  \newcommand{\DeterminantAba}{
    \begin{vNiceMatrix}[r]
      \textcolor{opq}{a_{11}} & a_{12} \\
      \textcolor{opq}{a_{21}} & \textcolor{opq}{a_{22}}
    \end{vNiceMatrix}
  }
  \newcommand{\DetExample}{
    \begin{vNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlDiag<6->[blu]
        \begin{tikzpicture}
          \draw[ultra thick, fill=grnbg, rounded corners, visible on=<7->]
          (row-1-|col-3)
          |- (row-2-|col-2)
          |- (row-3-|col-1)
          |- (row-2-|col-2)
          -- (row-1-|col-2)
          -- cycle;
        \end{tikzpicture}
      }
      ]
      {}  3 & -4 \\
      {} -5 & -8
    \end{vNiceMatrix}
  }
  \begin{example}
    The determinant of a generic $2\times 2$ matrix is
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \DeterminantA
          = a_{11}\DeterminantAaa-a_{21}\DeterminantAba
          \onslide<2->{= \subnode{a11a22}{a_{11}a_{22}}-\subnode{a21a12}{a_{21}a_{12}}}
          \)};

        \begin{pgfonlayer}{background}
          \node[
          , overlay
          , draw
          , ultra thick
          , rounded corners
          , baseline=a11a22.base
          , fit=(a11a22)
          , fill=blubg
          , inner sep=0
          , visible on=<3->
          ] {};
        \end{pgfonlayer}

        \begin{pgfonlayer}{background}
          \node[
          , overlay
          , draw
          , ultra thick
          , rounded corners
          , baseline=a11a22.base
          , fit=(a21a12)
          , fill=grnbg
          , inner sep=0
          , visible on=<4->
          ] {};
        \end{pgfonlayer}
      \end{tikzpicture}
    \]
    \onslide<5->{For example, we have}
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \onslide<5->{\DetExample = \subnode{firstpart}{(3)(-8)}-\subnode{secondpart}{(-5)(-4)}}
          \onslide<8->{= -44}
          \)};

        \begin{pgfonlayer}{background}
          \node[
          , overlay
          , draw
          , ultra thick
          , rounded corners
          , baseline=a11a22.base
          , fit=(firstpart)
          , fill=blubg
          , inner sep=0
          , visible on=<6->
          ] {};
        \end{pgfonlayer}

        \begin{pgfonlayer}{background}
          \node[
          , overlay
          , draw
          , ultra thick
          , rounded corners
          , baseline=a11a22.base
          , fit=(secondpart)
          , fill=grnbg
          , inner sep=0
          , visible on=<7->
          ] {};
        \end{pgfonlayer}
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\GenericDet}{
    \begin{vNiceMatrix}[r, small]
      a_{11} & a_{12} & a_{13} \\
      a_{21} & a_{22} & a_{23} \\
      a_{31} & a_{32} & a_{33}
    \end{vNiceMatrix}
  }
  \newcommand{\GenericDetaa}{
    \begin{vNiceMatrix}[r, small]
      \textcolor{opq}{a_{11}} & \textcolor{opq}{a_{12}} & \textcolor{opq}{a_{13}} \\
      \textcolor{opq}{a_{21}} & a_{22}                  & a_{23}                  \\
      \textcolor{opq}{a_{31}} & a_{32}                  & a_{33}
    \end{vNiceMatrix}
  }
  \newcommand{\GenericDetba}{
    \begin{vNiceMatrix}[r, small]
      \textcolor{opq}{a_{11}} & a_{12}                  & a_{13}                  \\
      \textcolor{opq}{a_{21}} & \textcolor{opq}{a_{22}} & \textcolor{opq}{a_{23}} \\
      \textcolor{opq}{a_{31}} & a_{32}                  & a_{33}
    \end{vNiceMatrix}
  }
  \newcommand{\GenericDetca}{
    \begin{vNiceMatrix}[r, small]
      \textcolor{opq}{a_{11}} & a_{12}                  & a_{13}                  \\
      \textcolor{opq}{a_{21}} & a_{22}                  & a_{23}                  \\
      \textcolor{opq}{a_{31}} & \textcolor{opq}{a_{32}} & \textcolor{opq}{a_{33}}
    \end{vNiceMatrix}
  }
  \newcommand{\DetA}{
    \begin{vNiceMatrix}[r, small]
      {}  5 & -7 & 9 \\
      {} -2 & 11 & 4 \\
      {}  3 &  1 & 9
    \end{vNiceMatrix}
  }
  \newcommand{\DetAaa}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, visible on=<3->]
          \draw (row-4-|col-3) |- ++(-3mm, -2.5mm)
          node[left] {$\scriptstyle(11)(9)-(4)(1)=95$};
        \end{tikzpicture}
      }
      ]
      \textcolor{opq}{ 5} & \textcolor{opq}{-7}     & \textcolor{opq}{9}     \\
      \textcolor{opq}{-2} & \textcolor<3->{red}{11} & \textcolor<3->{red}{4} \\
      \textcolor{opq}{ 3} & \textcolor<3->{red}{ 1} & \textcolor<3->{red}{9}
    \end{vNiceMatrix}
  }
  \newcommand{\DetAba}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, visible on=<4->]
          \draw (row-4-|col-3) |- ++(3mm, -2.5mm)
          node[right] {$\scriptstyle(-7)(9)-(9)(1)=-72$};
        \end{tikzpicture}
      }
      ]
      \textcolor{opq}{ 5} & \textcolor<4->{blu}{-7} & \textcolor<4->{blu}{9} \\
      \textcolor{opq}{-2} & \textcolor{opq}{11}     & \textcolor{opq}{4}     \\
      \textcolor{opq}{ 3} & \textcolor<4->{blu}{1}  & \textcolor<4->{blu}{9}
    \end{vNiceMatrix}
  }
  \newcommand{\DetAca}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, visible on=<5->]
          \draw (row-1-|col-3.north) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle(-7)(4)-(9)(11)=-127$};
        \end{tikzpicture}
      }
      ]
      \textcolor{opq}{ 5} & \textcolor<5->{grn}{-7} & \textcolor<5->{grn}{9} \\
      \textcolor{opq}{-2} & \textcolor<5->{grn}{11} & \textcolor<5->{grn}{4} \\
      \textcolor{opq}{ 3} & \textcolor{opq}{1}      & \textcolor{opq}{9}
    \end{vNiceMatrix}
  }
  \begin{example}
    The determinant of a generic $3\times 3$ matrix is
    \[
      \scriptstyle
      \GenericDet
      = a_{11}\GenericDetaa-a_{21}\GenericDetba+a_{31}\GenericDetca
    \]
    \onslide<2->{For example, we have}
    \[
      \onslide<2->{\scriptstyle \DetA = 5\DetAaa-(-2)\DetAba+3\DetAca}
      \onslide<6->{= -50}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\DetA}{
    \begin{vNiceMatrix}[r, small]
      a_{11} & a_{12} & a_{13} & a_{14} \\
      a_{21} & a_{22} & a_{23} & a_{24} \\
      a_{31} & a_{32} & a_{33} & a_{34} \\
      a_{41} & a_{42} & a_{43} & a_{44}
    \end{vNiceMatrix}
  }
  \newcommand{\DetAa}{
    \begin{vNiceMatrix}[r, small]
      \textcolor<1->{opq}{a_{11}} & \textcolor<1->{opq}{a_{12}} & \textcolor<1->{opq}{a_{13}} & \textcolor<1->{opq}{a_{14}} \\
      \textcolor<1->{opq}{a_{21}} & \textcolor<0>{opq}{a_{22}}  & \textcolor<0>{opq}{a_{23}}  & \textcolor<0>{opq}{a_{24}}  \\
      \textcolor<1->{opq}{a_{31}} & \textcolor<0>{opq}{a_{32}}  & \textcolor<0>{opq}{a_{33}}  & \textcolor<0>{opq}{a_{34}}  \\
      \textcolor<1->{opq}{a_{41}} & \textcolor<0>{opq}{a_{42}}  & \textcolor<0>{opq}{a_{43}}  & \textcolor<0>{opq}{a_{44}}
    \end{vNiceMatrix}
  }
  \newcommand{\DetAb}{
    \begin{vNiceMatrix}[r, small]
      \textcolor<1->{opq}{a_{11}} & \textcolor<0>{opq}{a_{12}}  & \textcolor<0>{opq}{a_{13}}  & \textcolor<0>{opq}{a_{14}}  \\
      \textcolor<1->{opq}{a_{21}} & \textcolor<1->{opq}{a_{22}} & \textcolor<1->{opq}{a_{23}} & \textcolor<1->{opq}{a_{24}} \\
      \textcolor<1->{opq}{a_{31}} & \textcolor<0>{opq}{a_{32}}  & \textcolor<0>{opq}{a_{33}}  & \textcolor<0>{opq}{a_{34}}  \\
      \textcolor<1->{opq}{a_{41}} & \textcolor<0>{opq}{a_{42}}  & \textcolor<0>{opq}{a_{43}}  & \textcolor<0>{opq}{a_{44}}
    \end{vNiceMatrix}
  }
  \newcommand{\DetAc}{
    \begin{vNiceMatrix}[r, small]
      \textcolor<1->{opq}{a_{11}} & \textcolor<0>{opq}{a_{12}}  & \textcolor<0>{opq}{a_{13}}  & \textcolor<0>{opq}{a_{14}}  \\
      \textcolor<1->{opq}{a_{21}} & \textcolor<0>{opq}{a_{22}}  & \textcolor<0>{opq}{a_{23}}  & \textcolor<0>{opq}{a_{24}}  \\
      \textcolor<1->{opq}{a_{31}} & \textcolor<1->{opq}{a_{32}} & \textcolor<1->{opq}{a_{33}} & \textcolor<1->{opq}{a_{34}} \\
      \textcolor<1->{opq}{a_{41}} & \textcolor<0>{opq}{a_{42}}  & \textcolor<0>{opq}{a_{43}}  & \textcolor<0>{opq}{a_{44}}
    \end{vNiceMatrix}
  }
  \newcommand{\DetAd}{
    \begin{vNiceMatrix}[r, small]
      \textcolor<1->{opq}{a_{11}} & \textcolor<0>{opq}{a_{12}}  & \textcolor<0>{opq}{a_{13}}  & \textcolor<0>{opq}{a_{14}}  \\
      \textcolor<1->{opq}{a_{21}} & \textcolor<0>{opq}{a_{22}}  & \textcolor<0>{opq}{a_{23}}  & \textcolor<0>{opq}{a_{24}}  \\
      \textcolor<1->{opq}{a_{31}} & \textcolor<0>{opq}{a_{32}}  & \textcolor<0>{opq}{a_{33}}  & \textcolor<0>{opq}{a_{34}}  \\
      \textcolor<1->{opq}{a_{41}} & \textcolor<1->{opq}{a_{42}} & \textcolor<1->{opq}{a_{43}} & \textcolor<1->{opq}{a_{44}}
    \end{vNiceMatrix}
  }
  \begin{example}
    The determinant of a generic $4\times 4$ matrix is
    \begin{gather*}
      \scriptstyle
      \DetA
      = a_{11}\DetAa-a_{21}\DetAb+a_{31}\DetAc-a_{41}\DetAd
    \end{gather*}
    \onslide<2->{Calculating $\det(A)$ is hard!}
  \end{example}

\end{frame}


\section{Expansion Theorem}
\subsection{Theorem Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Minors}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$M_{ij}=\det(A_{ij})$};
        \node[above, blu] at (text.north) {\scriptsize$(i, j)$ Minor};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Cofactors}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$C_{ij}=(-1)^{i+j}\det(A_{ij})$};
        \node[above, grn] at (text.north) {\scriptsize$(i, j)$ Cofactor};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      3 & -5 & 8 \\
      0 & 14 & 5 \\
      2 &  4 & 3
    \end{bNiceMatrix}
  }
  \newcommand{\MinorAbc}{
    \begin{vNiceMatrix}[r, small]
      \textcolor<0>{opq}{3}  & \textcolor<0>{opq}{-5}  & \textcolor<1->{opq}{8} \\
      \textcolor<1->{opq}{0} & \textcolor<1->{opq}{14} & \textcolor<1->{opq}{5} \\
      \textcolor<0>{opq}{2}  & \textcolor<0>{opq}{ 4}  & \textcolor<1->{opq}{3}
    \end{vNiceMatrix}
  }
  \begin{definition}
    The \emph{\blu{minors}} and \emph{\grn{cofactors}} of $A$ are given by
    \begin{align*}
      \Minors && \Cofactors
    \end{align*}
    \onslide<2->{For example, we have}
    \begin{align*}
      \onslide<2->{\scriptstyle A=\MatrixA} && \onslide<3->{\scriptstyle M_{23}=\MinorAbc=22} && \onslide<4->{\scriptstyle C_{23}=(-1)^{2+3}\MinorAbc=-22}
    \end{align*}
    \onslide<5->{Cofactors allow us to elegantly write
      $\displaystyle\det(A)=\sum_{k=1}^na_{k1}C_{k1}$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    The signs $(-1)^{i+j}$ in each cofactor fit the following pattern.
    \[
      \begin{bNiceMatrix}
        \textcolor{grn}{+} & \textcolor{red}{-} & \textcolor{grn}{+} & \textcolor{red}{-} & \textcolor{grn}{+} & \Cdots & \cpm   \\
        \textcolor{red}{-} & \textcolor{grn}{+} & \textcolor{red}{-} & \textcolor{grn}{+} & \textcolor{red}{-} & \Cdots & \cpm   \\
        \textcolor{grn}{+} & \textcolor{red}{-} & \textcolor{grn}{+} & \textcolor{red}{-} & \textcolor{grn}{+} & \Cdots & \cpm   \\
        \textcolor{red}{-} & \textcolor{grn}{+} & \textcolor{red}{-} & \textcolor{grn}{+} & \textcolor{red}{-} & \Cdots & \cpm   \\
        \textcolor{grn}{+} & \textcolor{red}{-} & \textcolor{grn}{+} & \textcolor{red}{-} & \textcolor{grn}{+} & \Cdots & \cpm   \\
        \Vdots             & \Vdots             & \Vdots             & \Vdots             & \Vdots             & \Ddots & \Vdots \\
        \cpm               & \cpm               & \cpm               & \cpm               & \cpm               & \Cdots & \textcolor{grn}{+}
      \end{bNiceMatrix}
    \]
    \onslide<2->{This pattern follows from $\grn{(-1)^{\operatorname{even}}=+1}$
      and $\red{(-1)^{\operatorname{odd}}=-1}$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\RowExpansion}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\displaystyle\det(A)=\sum_{k=1}^na_{ik}C_{ik}$};
        \node[above, grn] at (text.north) {\scriptsize\textnormal{$i$th Row Expansion}};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\ColumnExpansion}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\displaystyle\det(A)=\sum_{k=1}^n a_{kj}C_{kj}$};
        \node[above, blu] at (text.north) {\scriptsize\textnormal{$j$th Column Expansion}};
      \end{tikzpicture}
    \end{array}
  }
  \begin{theorem}[The Cofactor Expansion Theorem]
    We can ``expand'' a determinant calculation about any row or column.
    \begin{align*}
      \RowExpansion && \ColumnExpansion
    \end{align*}
    \onslide<2->{This can make calculations easier!}
  \end{theorem}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\SecondRow}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-before = {\hlRow<2->[blu, th]{2}}
      ]
      {} -3                     & -8                      & -15                      \\
      {} \textcolor<4->{red}{7} &  \textcolor<4->{grn}{0} & \textcolor<4->{red}{-66} \\
      {} 80                     & 91                      &  -4
    \end{vNiceMatrix}
  }
  \newcommand{\SecondRowba}{
    \begin{vNiceMatrix}[
      , r
      , small
      ]
      \textcolor<1->{opq}{-3} & \textcolor<0>{opq}{-8} & \textcolor<0>{opq}{-15} \\
      \textcolor<1->{opq}{7}  & \textcolor<1->{opq}{0} & \textcolor<1->{opq}{-66} \\
      \textcolor<1->{opq}{80} & \textcolor<0>{opq}{91} & \textcolor<0>{opq}{-4}
    \end{vNiceMatrix}
  }
  \newcommand{\SecondRowbb}{
    \begin{vNiceMatrix}[
      , r
      , small
      ]
      \textcolor<0>{opq}{-3} & \textcolor<1->{opq}{-8} & \textcolor<0>{opq}{-15} \\
      \textcolor<1->{opq}{7} & \textcolor<1->{opq}{0}  & \textcolor<1->{opq}{-66} \\
      \textcolor<0>{opq}{80} & \textcolor<1->{opq}{91} & \textcolor<0>{opq}{-4}
    \end{vNiceMatrix}
  }
  \newcommand{\SecondRowbc}{
    \begin{vNiceMatrix}[
      , r
      , small
      ]
      \textcolor<0>{opq}{-3} & \textcolor<0>{opq}{-8} & \textcolor<1->{opq}{-15} \\
      \textcolor<1->{opq}{7} & \textcolor<1->{opq}{0} & \textcolor<1->{opq}{-66} \\
      \textcolor<0>{opq}{80} & \textcolor<0>{opq}{91} & \textcolor<1->{opq}{-4}
    \end{vNiceMatrix}
  }
  \newcommand{\ThirdColumn}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-before = {\hlCol<6->[blu, th]{3}}
      ]
      {} -3 & -8 & \textcolor<8->{grn}{-15} \\
      {}  7 &  0 & \textcolor<8->{red}{-66} \\
      {} 80 & 91 & \textcolor<8->{grn}{-4}
    \end{vNiceMatrix}
  }
  \newcommand{\ThirdColumnac}{
    \begin{vNiceMatrix}[
      , r
      , small
      ]
      \textcolor<1->{opq}{-3} & \textcolor<1->{opq}{-8} & \textcolor<1->{opq}{-15} \\
      \textcolor<0>{opq}{7}   & \textcolor<0>{opq}{0}   & \textcolor<1->{opq}{-66} \\
      \textcolor<0>{opq}{80}  & \textcolor<0>{opq}{91}  & \textcolor<1->{opq}{-4}
    \end{vNiceMatrix}
  }
  \newcommand{\ThirdColumnbc}{
    \begin{vNiceMatrix}[
      , r
      , small
      ]
      \textcolor<0>{opq}{-3} & \textcolor<0>{opq}{-8} & \textcolor<1->{opq}{-15} \\
      \textcolor<1->{opq}{7} & \textcolor<1->{opq}{0} & \textcolor<1->{opq}{-66} \\
      \textcolor<0>{opq}{80} & \textcolor<0>{opq}{91} & \textcolor<1->{opq}{-4}
    \end{vNiceMatrix}
  }
  \newcommand{\ThirdColumncc}{
    \begin{vNiceMatrix}[
      , r
      , small
      ]
      \textcolor<0>{opq}{-3}  & \textcolor<0>{opq}{-8}  & \textcolor<1->{opq}{-15} \\
      \textcolor<0>{opq}{7}   & \textcolor<0>{opq}{0}   & \textcolor<1->{opq}{-66} \\
      \textcolor<1->{opq}{80} & \textcolor<1->{opq}{91} & \textcolor<1->{opq}{-4}
    \end{vNiceMatrix}
  }
  \begin{example}
    Consider the second row expansion given by
    \[
      \scriptstyle
      \SecondRow
      =
      \onslide<3->{
        \textcolor<4->{red}{-7}\SecondRowba
        \textcolor<4->{grn}{+0}\SecondRowbb
        \textcolor<4->{red}{-(-66)}\SecondRowbc
      }
    \]
    \onslide<5->{The same determinant can be calculated with a third column
      expansion.}
    \[
      \scriptstyle
      \onslide<5->{
        \ThirdColumn
        =
      }
      \onslide<7->{
        \textcolor<8->{grn}{-15}\ThirdColumnac
        \textcolor<8->{red}{-(-66)}\ThirdColumnbc
        \textcolor<8->{grn}{+(-4)}\ThirdColumncc
      }
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\DetA}{
    \begin{vNiceMatrix}[r, small, code-before = {\hlCol<2->[blu, th]{3}}]
      {}  4 & -3 & 0                       & 7 \\
      {}  3 &  1 & 0                       & 0 \\
      {} -5 &  1 & \textcolor<4->{grn}{-2} & 2 \\
      {}  6 &  4 & 0                       & 0
    \end{vNiceMatrix}
  }
  \newcommand{\DetAa}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlCol<6->[blu, th]{4}
      }
      ]
      \onslide<1->{\textcolor<0>{opq}{4}}   & \onslide<1->{\textcolor<0>{opq}{-3}} & \onslide<-4>{\textcolor<1->{opq}{0}}  & \onslide<1->{\textcolor<8->{grn}{7}}  \\
      \onslide<1->{\textcolor<0>{opq}{3}}   & \onslide<1->{\textcolor<0>{opq}{1}}  & \onslide<-4>{\textcolor<1->{opq}{0}}  & \onslide<1->{\textcolor<0>{opq}{0}}  \\
      \onslide<-4>{\textcolor<1->{opq}{-5}} & \onslide<-4>{\textcolor<1->{opq}{1}} & \onslide<-4>{\textcolor<1->{opq}{-2}} & \onslide<-4>{\textcolor<1->{opq}{2}} \\
      \onslide<1->{\textcolor<0>{opq}{6}}   & \onslide<1->{\textcolor<0>{opq}{4}}  & \onslide<-4>{\textcolor<1->{opq}{0}}  & \onslide<1->{\textcolor<0>{opq}{0}}
    \end{vNiceMatrix}
  }
  \newcommand{\DetAb}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-after = {
        \begin{tikzpicture}[<-, red, thick, visible on=<10->]
          \draw ($ (row-4-|col-1)!0.5!(row-4-|col-3) $) |- ++(3mm, -2.5mm)
          node[right] {$\scriptstyle (3)(4)-(1)(6)=6$};
        \end{tikzpicture}
      }
      ]
      \onslide<-8>{\textcolor<1->{opq}{4}} & \onslide<-8>{\textcolor<1->{opq}{-3}} & \onslide<-8>{\textcolor<1->{opq}{7}}  \\
      \onslide<1->{\textcolor<10->{red}{3}} & \onslide<1->{\textcolor<10->{red}{1}}  & \onslide<-8>{\textcolor<1->{opq}{0}}  \\
      \onslide<1->{\textcolor<10->{red}{6}} & \onslide<1->{\textcolor<10->{red}{4}}  & \onslide<-8>{\textcolor<1->{opq}{0}}
    \end{vNiceMatrix}
  }
  \begin{example}
    Determinant calculations simplify if the matrix has lots of zeros.
    \[
      \scriptstyle
      \DetA =
      \onslide<3->{\textcolor<4->{grn}{-2}\DetAa =}
      \onslide<7->{(-2)\textcolor<8->{grn}{(7)}\DetAb=}
      \onslide<11->{(-2)(7)(6)=}
      \onslide<12->{-84}
    \]
  \end{example}

\end{frame}


\section{Properties}
\subsection{Triangular Matrices}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\DetA}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlUpper<2>[blu, th]
        \hlCol<3-9>[blu, th]{1}
        \hlDiag<10->[blu, th]
      }
      ]
      2 &  8 & 1            & -7            \\
      0 & -1 & \sfrac{3}{2} &  3            \\
      0 &  0 & 10           &  2            \\
      0 &  0 & 0            & -\sfrac{9}{5}
    \end{vNiceMatrix}
  }
  \newcommand{\DetAa}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlPortion<6->[blu, th]{2}{2}{4}{2}
      }
      ]
      \onslide<4>{\textcolor{opq}{2}} &  \onslide<4>{\textcolor{opq}{8}} & \onslide<4>{\textcolor{opq}{1}}  & \onslide<4>{\textcolor{opq}{-7}} \\
      \onslide<4>{\textcolor{opq}{0}} & -1                               & \sfrac{3}{2}                     &  3                               \\
      \onslide<4>{\textcolor{opq}{0}} &  0                               & 10                               &  2                               \\
      \onslide<4>{\textcolor{opq}{0}} &  0                               & 0                                & -\sfrac{9}{5}
    \end{vNiceMatrix}
  }
  \newcommand{\DetAb}{
    \begin{vNiceMatrix}[r, small]
      {} \onslide<7>{\textcolor{opq}{-1}} & \onslide<7>{\textcolor{opq}{\sfrac{3}{2}}} &  \onslide<7>{\textcolor{opq}{3}} \\
      {} \onslide<7>{\textcolor{opq}{ 0}} & 10                                         &  2                               \\
      {} \onslide<7>{\textcolor{opq}{ 0}} & 0                                          & -\sfrac{9}{5}
    \end{vNiceMatrix}
  }
  \begin{theorem}
    The determinant of a triangular matrix is the product of its diagonal.
    \[
      \scriptstyle
      \DetA =
      \onslide<4->{(2)\DetAa =}
      \onslide<7->{(2)(-1)\DetAb =}
      \onslide<9->{(2)(-1)(10)(-\sfrac{9}{5}) =}
      \onslide<11->{36}
    \]
  \end{theorem}

\end{frame}


\subsection{Algebraic Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Transposition}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\det(A^\intercal)=\det(A)$};
        \node[above, blu] at (text.north) {\scriptsize\textnormal{Transposition Rule}};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Products}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\det(AB)=\det(A)\det(B)$};
        \node[above, grn] at (text.north) {\scriptsize\textnormal{Product Rule}};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<5->[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, visible on=<5->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(1mm, 2.5mm)
          node[right] {$\scriptstyle\det=(-6)(2)=-12$};
        \end{tikzpicture}
      }
      ]
      {} -22 &   6 & -17 &   1 \\
      {}  28 &   7 &  -5 &   1 \\
      {} -38 & -93 & 147 & -13 \\
      {}   4 &   7 & -11 &   1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlUpper<3->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle\det=-6$};
        \end{tikzpicture}
      }
      ]
      1 &  4 & -3 &   1 \\
      0 & -3 &  3 &   1 \\
      0 &  0 &  2 & -13 \\
      0 &  0 &  0 &   1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixL}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlLower<4->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, visible on=<4->]
          \draw ($ (row-5-|col-1)!0.5!(row-5-|col-5) $) |- ++(3mm, -2.5mm)
          node[right] {$\scriptstyle\det=2$};
        \end{tikzpicture}
      }
      ]
      {} -1 &  0 &   0 & 0 \\
      {} -1 & -1 &   0 & 0 \\
      {}  7 & -1 &   2 & 0 \\
      {}  4 &  7 & -11 & 1
    \end{bNiceMatrix}
  }
  \begin{theorem}
    Determinants respect \blu{transposition} and \grn{products}.
    \begin{align*}
      \Transposition && \Products
    \end{align*}
    \pause For example, we have
    \[
      \MatrixA = \MatrixU\MatrixL
    \]
  \end{theorem}

\end{frame}


\section{Calculating Determinants}
\subsection{Row Reductions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Switch}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\det(B)=-\det(A)$};
        \node[above, grn] at (text.north) {$\scriptstyle A\xrightarrow{\bv[r]_i\leftrightarrow\bv[r]_j}B$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Scale}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\det(B)=c\cdot\det(A)$};
        \node[above, blu] at (text.north) {$\scriptstyle A\xrightarrow{c\cdot\bv[r]_i\to\bv[r]_i}B$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Add}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=redbg, draw, rounded corners, ultra thick]
        (text) {$\det(B)=\det(A)$};
        \node[above, red] at (text.north) {$\scriptstyle A\xrightarrow{\bv[r]_i+c\cdot\bv[r]_j\to\bv[r]_i}B$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{theorem}
    Determinants play nicely with row operations.
    \begin{align*}
      \Switch && \Scale && \Add
    \end{align*}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\DetA}{
    \begin{vNiceMatrix}[r, small]
      {}  1 & -2 & -2 & -3 \\
      {}  1 &  2 &  9 &  1 \\
      {} -1 &  2 &  2 &  1 \\
      {}  1 &  1 &  1 &  1
    \end{vNiceMatrix}
  }
  \newcommand{\DetAa}{
    \begin{vNiceMatrix}[r, small]
      1 & -2 & -2 & -3 \\
      0 &  4 & 11 &  4 \\
      0 &  0 &  0 & -2 \\
      0 &  3 &  3 &  4
    \end{vNiceMatrix}
  }
  \newcommand{\DetAb}{
    \begin{vNiceMatrix}[r, small]
      1 & -2 & -2             & -3 \\
      0 &  4 & 11             &  4 \\
      0 &  0 &  0             & -2 \\
      0 &  0 & -\sfrac{21}{4} & 1
    \end{vNiceMatrix}
  }
  \newcommand{\DetAc}{
    \begin{vNiceMatrix}[r, small, code-before={\hlDiag<5->[blu, th]}]
      1 & -2 & -2             & -3 \\
      0 &  4 & 11             &  4 \\
      0 &  0 & -\sfrac{21}{4} &  1 \\
      0 &  0 &  0             & -2
    \end{vNiceMatrix}
  }
  \newcommand{\StepA}{
    \begin{NiceArray}{rcrcl}[small]
      \bv[r]_2 &-& \bv[r]_1 &\to& \bv[r]_2 \\
      \bv[r]_3 &+& \bv[r]_1 &\to& \bv[r]_3 \\
      \bv[r]_4 &-& \bv[r]_1 &\to& \bv[r]_4
    \end{NiceArray}
  }
  \newcommand{\StepB}{
    \begin{NiceArray}{rcrcl}[small]
      \bv[r]_4 &-& (\sfrac{3}{4})\cdot\bv[r]_2 &\to& \bv[r]_4
    \end{NiceArray}
  }
  \newcommand{\StepC}{
    \begin{NiceArray}{c}[small]
      \bv[r]_3\leftrightarrow\bv[r]_4
    \end{NiceArray}
  }
  \begin{example}
    We can quickly calculate this $4\times 4$ determinant with row-reductions.
    \begin{align*}
      \DetA
      \onslide<2->{\xlongequal{\StepA}  \DetAa} \\
      \onslide<3->{\xlongequal{\StepB}  \DetAb} \\
      \onslide<4->{\xlongequal{\StepC} -\DetAc} \\
      \onslide<6->{= -(1)(4)(-\sfrac{21}{4})(-2)} \\
      \onslide<7->{= -42}
    \end{align*}
  \end{example}

\end{frame}


\subsection{$PA=LU$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\DetA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<9->[blu]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, visible on=<9->]
          \draw ($ (row-1-|col-1.north)!0.5!(row-1-|col-4.north) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle\det(A)=-9$};
        \end{tikzpicture}
      }
      ]
      {} -1 & 1 &  0 \\
      {}  1 & 2 & -3 \\
      {}  3 & 1 & -1
    \end{bNiceMatrix}
  }
  \newcommand{\DetP}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<8->[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, visible on=<8->]
          \draw ($ (row-4-|col-1.south)!0.5!(row-4-|col-4.south) $) |- ++(3mm, -2.5mm)
          node[right] {\scriptsize\textnormal{num swaps $=1$}};
        \end{tikzpicture}
      }
      ]
      0 & 0 & 1 \\
      0 & 1 & 0 \\
      1 & 0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\DetL}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlLower<6->[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, visible on=<6->]
          \draw ($ (row-4-|col-1.south)!0.5!(row-4-|col-4.south) $) |- ++(3mm, -2.5mm)
          node[right] {$\scriptstyle\det(L)=1$};
        \end{tikzpicture}
      }
      ]
      1             & 0            & 0 \\
      \sfrac{1}{3}  & 1            & 0 \\
      -\sfrac{1}{3} & \sfrac{4}{5} & 1
    \end{bNiceMatrix}
  }
  \newcommand{\DetU}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlUpper<7->[blu]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, visible on=<7->]
          \draw ($ (row-1-|col-1.north)!0.5!(row-1-|col-4.north) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle\det(U)=9$};
        \end{tikzpicture}
      }
      ]
      3 & 1            & -1            \\
      0 & \sfrac{5}{3} & -\sfrac{8}{3} \\
      0 & 0            &  \sfrac{9}{5}
    \end{bNiceMatrix}
  }
  \begin{theorem}
    The factorization $PA=LU$ gives $A=P^\intercal LU$ so that
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(\det(A)
          =
          \subnode{p}{\color<4->{red}\det(P)}\cdot
          \subnode{l}{\color<2->{grn}\det(L)}\cdot
          \subnode{u}{\color<3->{blu}\det(U)}
          \)};

        \draw[<-, thick, overlay, red, visible on=<4->] (p.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle(-1)^{\textnormal{num swaps}}$};
        \draw[<-, thick, overlay, grn, visible on=<2->] (l.south) |- ++(1mm, -6mm)
        node[right] {\scriptsize\textnormal{easy (lower triangular)}};
        \draw[<-, thick, overlay, blu, visible on=<3->] (u.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize\textnormal{easy (upper triangular)}};
      \end{tikzpicture}
    \]
    \onslide<5->{For example, we have}
    \[
      \onslide<5->{\DetP\DetA=\DetL\DetU}
    \]
  \end{theorem}

\end{frame}

\end{document}
