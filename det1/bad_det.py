from operator import ne
from functools import partial

# time with timeit('bad_det(A)', number=1, repeat=1)


def bad_det(A):
    if not A.is_square():
        raise TypeError('matrix must be square')
    det = 0
    try:
        for k, a1k in A[0].dict().items():
            colrange = filter(partial(ne, k), range(A.ncols()))
            rowrange = range(1, A.nrows())
            A1k = A.matrix_from_rows_and_columns(rowrange, colrange)
            det += (-1) ** k * a1k * bad_det(A1k)
        return det
    except IndexError:
        return 1
