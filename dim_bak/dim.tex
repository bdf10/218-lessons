\documentclass[]{bmr}

\title{Dimension}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Dimension}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\VectorVa}{
    \begin{bNiceMatrix}[r, small]
      1\\ -3\\ 5
    \end{bNiceMatrix}
  }
  \newcommand{\VectorVb}{
    \begin{bNiceMatrix}[r, small]
      -2\\ 8\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorWa}{
    \begin{bNiceMatrix}[r, small]
      -4\\ 14\\ -9
    \end{bNiceMatrix}
  }
  \newcommand{\VectorWb}{
    \begin{bNiceMatrix}[r, small]
      3\\ -11\\ 4
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXa}{
    \begin{bNiceMatrix}[r, small]
      -5\\ 19\\ -3
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXb}{
    \begin{bNiceMatrix}[r, small]
      -3\\ 11\\ -4
    \end{bNiceMatrix}
  }
  \begin{theorem}
    Every basis of a vector space has the same number of vectors!
    \[
      \begin{tikzpicture}[remember picture, visible on=<2->]
        \node
        {
          \(
          \subnode{V}{V}
          = \Span\Set*{\subnode{v1}{\VectorVa}, \subnode{v2}{\VectorVb}}
          = \Span\Set*{\subnode{w1}{\VectorWa}, \subnode{w2}{\VectorWb}}
          = \Span\Set*{\subnode{x1}{\VectorXa}, \subnode{x2}{\VectorXb}}
          \)
        };

        \draw[<-, thick, blu, overlay, visible on=<3->] (v1.north) |- ++(12mm, 2.5mm) node[right] (text1) {\scriptsize\textnormal{two vectors!}};
        \draw[<-, thick, blu, overlay, visible on=<3->] (v2.north) |- (text1);
        \draw[<-, thick, blu, overlay, visible on=<4->] (w1.north) |- ++(12mm, 2.5mm) node[right] (text2) {\scriptsize\textnormal{two vectors!}};
        \draw[<-, thick, blu, overlay, visible on=<4->] (w2.north) |- (text2);
        \draw[<-, thick, blu, overlay, visible on=<5->] (x1.north) |- ++(12mm, 2.5mm) node[right] (text3) {\scriptsize\textnormal{two vectors!}};
        \draw[<-, thick, blu, overlay, visible on=<5->] (x2.north) |- (text3);

        \draw[<-, thick, blu, overlay, visible on=<7->] (V.south) |- ++(1mm, -4mm) node[right] {$\scriptstyle\dim(V)=2$};
      \end{tikzpicture}
    \]
    \onslide<6->{This number is called the \grn{dimension} of $V$.}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlCol<3->[blu, th]{1}
        \hlCol<3->[blu, th]{2}
        \hlCol<3->[blu, th]{4}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, blu, visible on=<3->]
          \draw ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(3mm, -2.5mm)
          node[right] (text) {\scriptsize basis of $\Col(A)$};
          \draw ($ (row-5-|col-1)!0.5!(row-5-|col-2) $) |- (text);
          \draw ($ (row-5-|col-2)!0.5!(row-5-|col-3) $) |- (text);
        \end{tikzpicture}
      }
      ]
      {}  1 &  -5 &  14 & -23 & -15 \\
      {}  3 & -14 &  38 & -65 & -43 \\
      {} -4 &  24 & -72 & 109 &  69 \\
      {} -1 &   9 & -30 &  37 &  21
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlEntry[blu, th]{1}{1}
        \hlEntry[blu, th]{2}{2}
        \hlEntry[blu, th]{3}{4}
      }
      ]
      1 & 0 & -6 & 0 & -2 \\
      0 & 1 & -4 & 0 & -2 \\
      0 & 0 &  0 & 1 &  1 \\
      0 & 0 &  0 & 0 &  0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[r, small]
      1\\ 3\\ -4\\ -1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorW}{
    \begin{bNiceMatrix}[r, small]
      -5\\ -14\\ 24\\ 9
    \end{bNiceMatrix}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[r, small]
      -23\\ -65\\ 109\\ 37
    \end{bNiceMatrix}
  }
  \begin{example}
    To calculate $\dim(V)$, we find a basis and count the number of vectors.
    \[
      \onslide<2->{\rref\overset{A}{\MatrixA}=\MatrixR}
    \]
    \onslide<4->{Here, we have found a basis of $\Col(A)$.}
    \[
      \begin{tikzpicture}[remember picture, visible on=<5->]
        \node {
          \(
          \subnode{col}{\Col(A)}
          =\Span\Set*{
            \subnode{v}{\VectorV},
            \subnode{w}{\VectorW},
            \subnode{x}{\VectorX}
          }
          \)
        };

        \draw[<-, thick, blu, overlay, visible on=<6->] (x.north) |- ++(3mm, 2.5mm)
        node[right] (text) {\scriptsize\textnormal{three basis vectors!}};
        \draw[<-, thick, blu, overlay, visible on=<6->] (v.north) |- (text);
        \draw[<-, thick, blu, overlay, visible on=<6->] (w.north) |- (text);
        \draw[<-, thick, blu, overlay, visible on=<7->] (col.south) |- ++(1mm, -4mm)
        node[right] {$\scriptstyle \dim\Col(A)=3$};
      \end{tikzpicture}
    \]
    \onslide<8->{The dimension is not necessarily the same as the number of
      coordinates.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[r, small]
      1\\ 0\\ 0\\ \Vdots\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorW}{
    \begin{bNiceMatrix}[r, small]
      0\\ 1\\ 0\\ \Vdots\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[r, small]
      0\\ 0\\ \Vdots\\ 0\\ 1
    \end{bNiceMatrix}
  }
  \begin{example}
    Every vector in $\mathbb{R}^n$ is a linear combination of the columns of
    $I_n$.
    \[
      \begin{tikzpicture}[remember picture, visible on=<2->]
        \node {
          \(
          \subnode{Rn}{\mathbb{R}^n}
          =\Span\Set*{
            \subnode{e1}{\VectorV},
            \subnode{e2}{\VectorW},
            \dotsc,
            \subnode{en}{\VectorX}
          }
          \)
        };

        \draw[<-, thick, blu, overlay, visible on=<3->] (e1.north) |- ++(1mm, 2.5mm)
        node[right] {$\scriptstyle \bv[e]_1$};
        \draw[<-, thick, blu, overlay, visible on=<4->] (e2.north) |- ++(1mm, 2.5mm)
        node[right] {$\scriptstyle \bv[e]_2$};
        \draw[<-, thick, blu, overlay, visible on=<5->] (en.north) |- ++(1mm, 2.5mm)
        node[right] {$\scriptstyle \bv[e]_n$};
        \draw[<-, thick, blu, overlay, visible on=<6->] (Rn.south) |- ++(-1mm, -4mm)
        node[left] {$\scriptstyle \dim(\mathbb{R}^n)=n$};
      \end{tikzpicture}
    \]
    \onslide<7->{We call $\Set{\bv[e]_1,\bv[e]_2,\dotsc,\bv[e]_n}$ the
      \emph{\grn{standard basis}} of $\mathbb{R}^n$.}
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\DimensionZero}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<2->]
        \node[
        , fill=red
        , circle
        , inner sep=0
        , outer sep=0
        , minimum size=2mm
        ] (V) {};
        \node[red, below, visible on=<3->] at (V.south) {$\scriptstyle\dim(V)=0$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\DimensionOne}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , blu
        , visible on=<4->
        ]
        \coordinate (O) at (0, 0);
        \coordinate (v) at (30:1);

        \draw[ultra thick, ->] (O) -- (v);
        \draw[thick, <->, visible on=<5->] ($ -1*(v) $) -- ($ 2*(v) $)
        node[above, visible on=<6->] {$\scriptstyle\dim(V)=1$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\DimensionTwo}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<7->]

        \coordinate (O) at (0, 0);
        \coordinate (v) at (5:3);
        \coordinate (w) at (40:2.75);

        \draw[grn, thick, fill=grnbg, visible on=<8->] (O) -- (v) -- ($ (v)+(w) $) -- (w) -- cycle;

        \coordinate (O) at ($ 0.25*(v)+0.25*(w) $);

        \draw[grn, ultra thick, ->] (O) -- ++($ 0.5*(v) $);
        \draw[grn, ultra thick, ->] (O) -- ++($ 0.5*(w) $);

        \draw[opacity=0, visible on=<9->] (w) -- ($ (v)+(w) $) node[midway, above, sloped, grn, opacity=1] {$\scriptstyle\dim(V)=2$};

      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    We view $\dim(V)$ as a measurment of how ``complicated'' $V$ is.
    \begin{align*}
      \DimensionZero && \DimensionOne && \DimensionTwo
    \end{align*}
    \onslide<10->{The only zero-dimensional vector space is
      $V=\Span\Set{\bv[O]}$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Consider a list of vectors $\ell$ in a vector space $V$ given by
    \[
      \ell=\Set{\bv_1,\bv_2,\dotsc,\bv_k}
    \]
    If $k>\dim(V)$, then $\ell$ is linearly dependent.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\SpanAxiom}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\Span\Set{\bv_1,\dotsc,\bv_d}=V$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Spanning Axiom}};
    \end{tikzpicture}
  }
  \newcommand{\LinIndAxiom}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$\Set{\bv_1,\dotsc,\bv_d}$ linearly independent};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Linear Independence Axiom}};
    \end{tikzpicture}
  }
  \begin{theorem}
    Suppose $\dim(V)=d$ and consider $\beta=\Set{\bv_1,\bv_2,\dotsc,\bv_d}$ in
    $V$.
    \begin{align*}
      \SpanAxiom && \LinIndAxiom
    \end{align*}
    To check if $\beta$ is a basis, we only need to verify one of these two
    axioms.
  \end{theorem}

\end{frame}


\section{Fundamental Subspaces}
\subsection{Dimensions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\RowNull}{
    \begin{tikzpicture}[visible on=<6->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\dim\Col(A^\intercal)+\dim\Null(A)=n$};
    \end{tikzpicture}
  }
  \newcommand{\ColLNull}{
    \begin{tikzpicture}[visible on=<6->]
      \node[fill=redbg, draw, rounded corners, ultra thick]
      (text) {$\dim\Col(A)+\dim\Null(A^\intercal)=m$};
    \end{tikzpicture}
  }
  \begin{theorem}
    The dimensions of the four fundamental subspaces are as follows.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<1->
        ] {$\scriptstyle\underset{\onslide<5->{\rank(A)}}{\Col(A^\intercal)}$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<1->
        ] {$\scriptstyle\underset{\onslide<2->{\nullity(A)}}{\Null(A)}$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^n$};

        \begin{scope}[xshift=4.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<1->
          ] {$\scriptstyle\underset{\onslide<4->{\rank(A)}}{\Col(A)}$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<1->
          ] {$\scriptstyle\underset{\onslide<3->{\nullity(A^\intercal)}}{\Null(A^\intercal)}$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A$};

        % \draw[->, thick, visible on=<1->] (Rm.south west) to[bend left]
        % node[midway, below] {$\scriptstyle A^\intercal$} (Rn.south east);

        \draw[Point, blu, visible on=<1->]
        (Col.north) |- ++(3mm, 2.5mm)
        node[right, scale=0.6] {$
          \begin{aligned}
            \Col(A) &= \Span\Set{\textnormal{pivot columns of }A}                 \\
            {}      &= \Span\Set{\textnormal{nonzero rows of }\rref(A^\intercal)}
          \end{aligned}
          $};

        \draw[Point, blu, visible on=<1->]
        (Row.north) |- ++(-3mm, 2.5mm)
        node[left, scale=0.6] {$
          \begin{aligned}
            \Col(A^\intercal) &= \Span\Set{\textnormal{pivot columns of }A^\intercal} \\
            {}                &= \Span\Set{\textnormal{nonzero rows of }\rref(A)}
          \end{aligned}
          $};

        \draw[Point, red, visible on=<1->]
        (LNull.north) |- ++(2mm, 2.5mm)
        node[right, scale=0.55] {$\Null(A^\intercal)=\Span\Set{\textnormal{pivot sols to }A^\intercal\bv=\bv[O]}$};

        \draw[Point, grn, visible on=<1->]
        (Null.north) |- ++(-2mm, 2.5mm)
        node[left, scale=0.55] {$\Null(A)=\Span\Set{\textnormal{pivot sols to }A\bv=\bv[O]}$};

      \end{tikzpicture}
    \]
    \onslide<6->{This gives reinterpretations of the rank-nullity theorem.}
    \begin{align*}
      \RowNull && \ColLNull
    \end{align*}
    \onslide<7->{With one dimension, we can infer all dimensions!}
  \end{theorem}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlEntry<4->[blu, th]{1}{1}
        \hlEntry<4->[blu, th]{2}{2}
        \hlEntry<4->[blu, th]{3}{4}
      }
      ]
      7 & 3 & 11 &  2 \\
      0 & 5 & -2 & 15 \\
      0 & 0 &  0 & -9 \\
      0 & 0 &  0 &  0 \\
      0 & 0 &  0 &  0
    \end{bNiceMatrix}
  }
  \begin{example}
    This $A$ is in row echelon form, so we can quickly determine $\rank(A)=3$.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<1->
        ] {$\scriptstyle\underset{\onslide<6->{\dim=3}}{\Col(A^\intercal)}$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<1->
        ] {$\scriptstyle\underset{\onslide<7->{\dim=1}}{\Null(A)}$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^{\alt<3->{4}{n}}$};

        \begin{scope}[xshift=6cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<1->
          ] {$\scriptstyle\underset{\onslide<5->{\dim=3}}{\Col(A)}$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<1->
          ] {$\scriptstyle\underset{\onslide<8->{\dim=2}}{\Null(A^\intercal)}$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^{\alt<2->{5}{m}}$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\MatrixA$};

        % \draw[->, thick, visible on=<1->] (Rm.south west) to[bend left]
        % node[midway, below] {$\scriptstyle A^\intercal$} (Rn.south east);

      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Is it possible for a matrix to have these properties?
    \[
      \begin{tikzpicture}[remember picture]

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<1->
        ] {$\scriptstyle\underset{\onslide<4->{\dim\geq2}}{\Col(A^\intercal)}$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<1->
        ] {$\scriptstyle\underset{\onslide<6->{\dim\leq 2}}{\Null(A)}$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^{\alt<3->{4}{n}}$};

        \begin{scope}[xshift=4.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<1->
          ] {$\scriptstyle\underset{\onslide<5->{\dim\geq2}}{\Col(A)}$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<1->
          ] {$\scriptstyle\underset{\onslide<7->{\dim\geq2}}{\Null(A^\intercal)}$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^{\subnode{m}{\alt<2->{\color<8->{red}{3}}{m}}}$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above, alt=<10->{draw=red, ultra thick, cross out}] {$\scriptstyle A$};

        \draw[Point, blu, visible on=<1->]
        (Row.north) |- ++(-3mm, 2.5mm)
        node[left, scale=0.6] {$
          \nv{1 -3 2 4}, \nv{0 1 5 9}\in\Col(A^\intercal)
          $};

        \draw[Point, red, visible on=<1->]
        (LNull.north) |- ++(2mm, 2.5mm)
        node[right, scale=0.55] {$
          \nv{3 8 1}, \nv{2 5 3}\in\Null(A^\intercal)
          $};

        \draw[thick, <-, red, overlay, visible on=<8->] (m.north) |- ++(-1mm, 4.5mm)
        node[left] {$\scriptstyle 3<2+2$};

      \end{tikzpicture}
    \]
    \onslide<9->{This matrix cannot exist!}
  \end{example}

\end{frame}


\section{Geometric Multiplicity}
\subsection{Dimension of Eigenspaces}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\gm_A(\lambda)=\dim\mathcal{E}_A(\lambda)$
  \end{theorem}
  \begin{proof}<2->
    $\gm_A(\lambda)=\nullity(\lambda\cdot I_n-A)=\dim\Null(\lambda\cdot I_n-A)=\dim\mathcal{E}_A(\lambda)$
  \end{proof}

\end{frame}

\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {} 37 & -32 & 32 \\
      {} 32 & -27 & 32 \\
      {} -8 &   8 & -3
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixCharA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<3->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle -3\cdot I_3-A$};
        \end{tikzpicture}
      }
      ]
      {} -40 & 32 & -32 \\
      {} -32 & 24 & -32 \\
      {}   8 & -8 &   0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixCharAR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<4->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \rref(-3\cdot I_3-A)$};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 4 \\
      0 & 1 & 4 \\
      0 & 0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[
      , small
      , code-before = {\hlMat<5->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<5->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle \gm_A(-3)=1$};
        \end{tikzpicture}
      }
      ]
      4 & 4 & -1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\MatrixCharB}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<7->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<7->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle 5\cdot I_3-A$};
        \end{tikzpicture}
      }
      ]
      {} -32 & 32 & -32 \\
      {} -32 & 32 & -32 \\
      {}   8 & -8 &   8
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixCharBR}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<8->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<8->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \rref(5\cdot I_3-A)$};
        \end{tikzpicture}
      }
      ]
      1 & -1 & 1 \\
      0 &  0 & 0 \\
      0 &  0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorWa}{
    \begin{bNiceMatrix}[
      , small
      , code-before = {\hlMat<9->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<9->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(15mm, 2.5mm);
        \end{tikzpicture}
      }
      ]
      1 & 1 & 0
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\VectorWb}{
    \begin{bNiceMatrix}[
      , small
      , code-before = {\hlMat<9->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<9->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(3mm, 2.5mm)
          node[right] {$\scriptstyle \gm_A(5)=2$};
        \end{tikzpicture}
      }
      ]
      -1 & 0 & 1
    \end{bNiceMatrix}^\intercal
  }
  \begin{example}
    Consider the data
    \begin{align*}
      A=\MatrixA && \EVals(A)=\Set{-3, 5}
    \end{align*}
    \onslide<2->{The eigenspace $\mathcal{E}_A(-3)$ is given by}
    \[
      \onslide<2->{\mathcal{E}_A(-3)=\Null\MatrixCharA=\Null\MatrixCharAR=\Span\Set{\VectorV}}
    \]
    \onslide<6->{The eigenspace $\mathcal{E}_A(5)$ is given by}
    \[
      \onslide<6->{\mathcal{E}_A(5)=\Null\MatrixCharB=\Null\MatrixCharBR=\Span\Set{\VectorWa, \VectorWb}}
    \]
  \end{example}

\end{frame}

\section{Betti Numbers}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\BettiZero}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$h_0(G)=\textnormal{number of connected components}$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Zeroth Betti Number}};
    \end{tikzpicture}
  }
  \newcommand{\BettiOne}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$h_1(G)=\textnormal{circuit rank}$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{First Betti Number}};
    \end{tikzpicture}
  }
  \newcommand{\EulerCharacteristic}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=redbg, draw, rounded corners, ultra thick]
      (text) {$\chi(G)=(\textnormal{number of nodes})-(\textnormal{number of arrows})$};
      \node[above, red] at (text.north) {\scriptsize\textnormal{Euler Characteristic}};
    \end{tikzpicture}
  }
  \begin{definition}
    The \emph{\grn{Betti numbers}} of a directed graph $G$ are
    \begin{align*}
      \BettiZero && \BettiOne
    \end{align*}
    \pause The \emph{\grn{Euler characteristic}} is
    \[
      \EulerCharacteristic
    \]
  \end{definition}

\end{frame}


\subsection{Euler Characteristic}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\EulerCharacteristic}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\chi(G)=h_0(G)-h_1(G)$};
      % \node[above, grn] at (text.north) {\scriptsize\textnormal{Zeroth Betti Number}};
    \end{tikzpicture}
  }
  \begin{theorem}
    Let $A$ be the incidence matrix of a directed graph $G$.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<1->
        ] {$\scriptstyle\underset{\onslide<4->{\dim=n-h_1(G)}}{\Col(A^\intercal)}$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<1->
        ] {$\scriptstyle\underset{\onslide<2->{\dim=h_1(G)}}{\Null(A)}$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^n$};

        \begin{scope}[xshift=6cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<1->
          ] {$\scriptstyle\underset{\onslide<5->{\dim=m-h_0(G)}}{\Col(A)}$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<1->
          ] {$\scriptstyle\underset{\onslide<3->{\dim=h_0(G)}}{\Null(A^\intercal)}$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A$};

      \end{tikzpicture}
    \]
    \onslide<6->{Since $\rank(A)=\rank(A^\intercal)$, we have
      $n-h_1(G)=m-h_0(G)$, which gives}
    \[
      \onslide<7->{\EulerCharacteristic}
    \]
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]

        \node[state] (v1)                     {$v_1$};
        \node[state] (v2) [below=of v1]       {$v_2$};
        \node[state] (v3) [right=2cm of v1]   {$v_3$};
        \node[state] (v4) [right=2cm of v2]   {$v_4$};
        \node[state] (v5) [right=1.5cm of v4] {$v_5$};

        \draw[->] (v1) edge [bend right] node {$a_1$} (v2);
        \draw[->] (v2) edge [bend right] node {$a_2$} (v1);
        \draw[->] (v3) edge [bend right] node {$a_3$} (v4);
        \draw[->] (v3) edge [bend left]  node {$a_4$} (v4);
        \draw[->] (v4) edge              node {$a_6$} (v3);
        \draw[->] (v3) edge [bend left]  node {$a_5$} (v5);

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\EulerCharacteristic}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture]
        \node {\(\subnode{chi}{\color<2->{grn}\chi(G)}=\subnode{h0}{\color<3->{blu}h_0(G)}-\subnode{h1}{\color<4->{red}h_1(G)}\)};

        \draw[<-, thick, grn, overlay, visible on=<2->] (chi.north) |- ++(-3mm, 2.5mm)
        node[left] {$\scriptstyle 5-6=-1$};

        \draw[<-, thick, blu, overlay, visible on=<3->] (h0.north) |- ++(-3mm, 2.5mm)
        node[left] {$\scriptstyle 2$};

        \draw[<-, thick, red, overlay, visible on=<4->] (h1.north) |- ++(-3mm, 2.5mm)
        node[left] {$\scriptstyle 3$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    We can use the Euler characteristic to calculate $h_1(G)$.
    \begin{align*}
      \EulerCharacteristic && \myG
    \end{align*}
  \end{example}

\end{frame}

\end{document}
