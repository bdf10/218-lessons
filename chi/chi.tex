\documentclass[]{bmr}

\usepackage{booktabs}
\usepackage{extarrows}

\title{The Characteristic Polynomial}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{The Characteristic Polynomial}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixtI}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<4->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<4->]
          \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle t\cdot I_2$};
        \end{tikzpicture}
      }
      ]
      t & 0 \\ 0 & t
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<5->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<5->]
          \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      1 & -1 \\
      2 &  0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{vNiceMatrix}[
      , r
      , margin
      % , code-before = {\hlMat<2->}
      % , code-after = {
      % \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
      %   \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
      %   node[left] {$\scriptstyle A$};
      % \end{tikzpicture}
      % }
      ]
      t-1 & 1 \\
      -2  & t
    \end{vNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{characteristic polynomial}} of an $n\times n$ matrix $A$ is
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \chi_A(t) = \det(\subnode{charmat}{\textcolor<2->{red}{t\cdot I_n-A}})
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (charmat.north) |- ++(3mm, 2.5mm)
        node[right] {\scriptsize characteristic matrix!};
      \end{tikzpicture}
    \]
    \onslide<3->{For example, we have}
    \[
      \onslide<3->{\chi_A(t) =}
      \onslide<3->{\det\left(\MatrixtI-\MatrixA\right) =}
      \onslide<6->{\MatrixX =}
      \onslide<7->{t^2-t+2}
    \]
    \onslide<8->{Here, $A$ is $2\times 2$ and $\chi_A(t)$ is a \emph{\grn{monic
          polynomial with degree two}}.}
  \end{definition}

\end{frame}


\subsection{Basic Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixtI}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<3->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle t\cdot I_3$};
        \end{tikzpicture}
      }
      ]
      t & 0 & 0 \\
      0 & t & 0 \\
      0 & 0 & t
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<4->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} -1 &  1 &  1 \\
      {}  3 &  1 & -3 \\
      {} -1 & -1 &  3
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{vNiceMatrix}[
      , r
      , small
      % , code-before = {\hlMat<2->[blu, th]}
      % , code-after = {
      % \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
      %   \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
      %   node[left] {$\scriptstyle A$};
      % \end{tikzpicture}
      % }
      ]
      {} t+1 &  -1 &  -1 \\
      {}  -3 & t-1 &   3 \\
      {}   1 &   1 & t-3
    \end{vNiceMatrix}
  }
  \begin{theorem}
    Suppose $A$ is $n\times n$. Then $\chi_A(t)$ is monic with
    $\deg\chi_A(t)=n$.
    \[
      \begin{tikzpicture}[remember picture, visible on=<2->]
        \node {\(
          \chi_A(t)
          = \det\left(\MatrixtI-\MatrixA\right)
          = \MatrixX
          = \subnode{thisismonic}{\textcolor<5->{blu}{t}}^{\subnode{degreethree}{\textcolor<6->{red}{3}}} - 3t^{2} - 6t + 8
          \)};

        \draw[<-, thick, blu, overlay, visible on=<5->]
        (thisismonic.south) |- ++(2mm, -2.5mm) node[right] {\scriptsize monic};

        \draw[<-, thick, red, overlay, visible on=<6->]
        (degreethree.north) |- ++(2mm, 2.5mm) node[right] {$\scriptstyle\deg\chi_A(t)=3$};
      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $\chi_{A^\intercal}(t)=\chi_{A}(t)$
  \end{theorem}
  \begin{proof}<2->
    $
    \begin{aligned}
      \chi_{A^\intercal}(t)
      &= \det(t\cdot I_n-A^\intercal)           \\
      &\onslide<2->{=} \onslide<3->{\det(t\cdot I_n^\intercal-A^\intercal)} \\
      &\onslide<3->{=} \onslide<4->{\det((t\cdot I_n-A)^\intercal)}         \\
      &\onslide<4->{=} \onslide<5->{\det(t\cdot I_n-A)}                     \\
      &\onslide<5->{=} \onslide<6->{\chi_{A}(t)\qedhere}
    \end{aligned}
    $
  \end{proof}

\end{frame}


\section{Calculating Eigenvalues}
\subsection{Roots of $\chi_A(t)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Each root $\lambda$ of $\chi_A(t)$ satisfies
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \chi_A(\lambda) = \det(\subnode{charmatissingular}{\textcolor<2->{red}{\lambda\cdot I_n-A}}) = 0
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (charmatissingular.north) |- ++(3mm, 2.5mm)
        node[right] {\scriptsize\textnormal{$\lambda\cdot I_n-A$ is singular!}};
      \end{tikzpicture}
    \]
    \onslide<3->{The roots of $\chi_A(t)$ are the eigenvalues of $A$.}
  \end{theorem}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \newcommand{\MatrixtI}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle t\cdot I_2$};
        \end{tikzpicture}
      }
      ]
      t & 0 \\ 0 & t
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      1 & 1 \\
      1 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{vNiceMatrix}[
      , r
      , margin
      % , code-before = {\hlMat<2->}
      % , code-after = {
      % \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
      %   \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
      %   node[left] {$\scriptstyle A$};
      % \end{tikzpicture}
      % }
      ]
      t-1 &  -1 \\
      -1  & t-1
    \end{vNiceMatrix}
  }
  \begin{example}
    Consider the calculation
    \[
      \chi_A(t)
      = \det\left(\MatrixtI-\MatrixA\right)
      = \MatrixX
      = t\cdot(t-2)
    \]
    \onslide<3->{Here, $\EVals(A)=\Set{0, 2}$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}


  \newcommand{\MatrixtI}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle t\cdot I_2$};
        \end{tikzpicture}
      }
      ]
      t & 0 \\ 0 & t
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      1 & -1 \\
      1 &  2
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{vNiceMatrix}[
      , r
      , margin
      % , code-before = {\hlMat<2->}
      % , code-after = {
      % \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
      %   \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
      %   node[left] {$\scriptstyle A$};
      % \end{tikzpicture}
      % }
      ]
      t-1 &   1 \\
      -1  & t-2
    \end{vNiceMatrix}
  }
  \begin{example}
    Consider the calculation
    \[
      \chi_A(t)
      = \det\left(\MatrixtI-\MatrixA\right)
      \onslide<3->{= \MatrixX}
      \onslide<4->{= t^2-3\,t+3}
    \]
    \onslide<5->{To find the eigenvalues, we use the quadratic formula}
    \begin{align*}
      \onslide<5->{\lambda_1=\frac{3-\sqrt{3}\,i}{2}} && \onslide<5->{\lambda_2=\frac{3+\sqrt{3}\,i}{2}}
    \end{align*}
    \onslide<6->{Here, the eigenvalues are related by conjugation
      $\overline{\lambda_1}=\lambda_2$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixtI}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<2->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle t\cdot I_3$};
        \end{tikzpicture}
      }
      ]
      t & 0 & 0 \\
      0 & t & 0 \\
      0 & 0 & t
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<2->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} -1 &  1 &  1 \\
      {}  3 &  1 & -3 \\
      {} -1 & -1 &  3
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{vNiceMatrix}[
      , r
      , small
      % , code-before = {\hlMat<2->[blu, th]}
      % , code-after = {
      % \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
      %   \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
      %   node[left] {$\scriptstyle A$};
      % \end{tikzpicture}
      % }
      ]
      {} t+1 &  -1 &  -1 \\
      {}  -3 & t-1 &   3 \\
      {}   1 &   1 & t-3
    \end{vNiceMatrix}
  }
  \begin{example}
    Consider the calculation
    \[
      \chi_A(t)
      = \det\left(\MatrixtI-\MatrixA\right)
      = \MatrixX
      = (t+2)\cdot(t-1)\cdot(t-4)
    \]
    \onslide<3->{Here, $\EVals(A)=\Set{-2, 1, 4}$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MyA}{
    \begin{bNiceMatrix}[r, small]
      -3 & -2 & 3 \\
      -1 & -2 & 1 \\
      -1 & -2 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MyChi}{
    \begin{vNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<2->[red]{1}{2}
        \hlEntry<2->[red]{2}{1}
      }
      ]
      t + 3 & 2 & -3 \\
      1 & t + 2 & -1 \\
      1 & 2 & t - 1
    \end{vNiceMatrix}
  }
  \newcommand{\MyEa}{
    \begin{NiceArray}{rcrcr}[small]
      \br_2 &-& \br_3 &\to& \br_2 \\
      \br_1 &-& \br_3 &\to& \br_1 \\
    \end{NiceArray}
  }
  \newcommand{\MyChiRa}{
    \begin{vNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<4-5>{1}
        \hlRow<6-7>{2}
        \hlEntry<8->[red]{3}{1}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm]
          \draw[visible on=<4-5>] ($ (row-1-|col-4)!0.5!(row-2-|col-4) $) -| ++(17mm, 1.5mm)
          node[above, scale=0.75] {common factor of $(t+2)$};
          \draw[visible on=<6-7>] ($ (row-2-|col-4)!0.5!(row-3-|col-4) $) -| ++(20mm, 1.5mm)
          node[above, scale=0.75] {common factor of $t$};
        \end{tikzpicture}
      }
      ]
      \alt<5->{1}{t + 2} & 0 & \alt<5->{-1}{-t - 2} \\
      0 & \alt<7->{1}{t} & \alt<7->{-1}{-t} \\
      1 & 2 & t - 1
    \end{vNiceMatrix}
  }
  \newcommand{\MyChiRb}{
    \begin{vNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlEntry<10->[red, th]{3}{2}
      }
      ]
      1 & 0 & -1 \\
      0 & 1 & -1 \\
      0 & 2 &  t \\
    \end{vNiceMatrix}
  }
  \newcommand{\MyChiRc}{
    \begin{vNiceMatrix}[
      , r
      , small
      ]
      1 & 0 & -1 \\
      0 & 1 & -1 \\
      0 & 0 &  t+2 \\
    \end{vNiceMatrix}
  }

  \begin{example}
    The characteristic polynomial of $A=\MyA$ is
    \begin{align*}
      \chi_A(t)
      = \MyChi \\
      \onslide<3->{\xlongequal{\MyEa} \onslide<7->{t\cdot}\onslide<5->{(t+2)\cdot}\MyChiRa} \\
      \onslide<9->{\xlongequal{\scriptstyle\br_3-\br_1\to\br_3}t\cdot(t+2)\cdot\MyChiRb} \\
      \onslide<11->{\xlongequal{\scriptstyle\br_3-2\cdot\br_2\to\br_3}t\cdot(t+2)\cdot\MyChiRc} \\
      \onslide<12->{= t\cdot(t+2)^2}
    \end{align*}
  \end{example}
\end{frame}


\section{Algebraic Multiplicity}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    The fundamental theorem of algebra says that $\chi_A(t)$ factors as
    \[
      \begin{array}{c}
        \begin{tikzpicture}[remember picture]
          \node {\(
            \chi_A(t)
            =
            (t-\lambda_1)^{\subnode{defam1}{\textcolor<2->{red}{\am_A(\lambda_1)}}}
            (t-\lambda_2)^{\subnode{defam2}{\textcolor<2->{red}{\am_A(\lambda_2)}}}\dotsb
            (t-\lambda_k)^{\subnode{defamk}{\textcolor<2->{red}{\am_A(\lambda_k)}}}
            \)};

          \draw[<-, thick, red, overlay, visible on=<2->] (defamk.north) |- ++(1mm, 2.5mm)
          node[right] (am) {\scriptsize\textnormal{\emph{algebraic multiplicity}}};

          \draw[<-, thick, red, overlay, visible on=<2->] (defam2.north) |- (am);
          \draw[<-, thick, red, overlay, visible on=<2->] (defam1.north) |- (am);
        \end{tikzpicture}
      \end{array}
    \]
    where $\EVals(A)=\Set{\lambda_1,\dotsc,\lambda_k}$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      ]
      {}   9 & -16 & -64 \\
      {} -64 &  57 & 256 \\
      {}  16 & -16 & -71
    \end{bNiceMatrix}
  }
  \newcommand{\CharPoly}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture]
        \node {\(
          \chi_A(t)
          =
          (t + 7)^{\subnode{amAn7}{\textcolor<2->{red}{2}}} \cdot
          (t - 9)^{\subnode{amA9}{\onslide<2->{\textcolor<2->{red}{1}}}}
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (amAn7.north) |- ++(-3mm, 2.5mm)
        node[left]{$\scriptstyle\am_A(-7)=2$};

        \draw[<-, thick, red, overlay, visible on=<2->] (amA9.north) |- ++(3mm, 2.5mm)
        node[right]{$\scriptstyle\am_A(9)=1$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the matrix $A$ and its characteristic polynomial.
    \begin{align*}
      A=\MatrixA && \CharPoly
    \end{align*}
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\SizeFormula}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\am_A(\lambda_1)+\am_A(\lambda_2)+\dotsb+\am_A(\lambda_k)=n$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Size Formula}};
    \end{tikzpicture}
  }
  \newcommand{\GeoMultBound}{
    \begin{tikzpicture}[visible on=<3->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$1\leq\gm_A(\lambda)\leq \am_A(\lambda)$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Geometric Multiplicity Bounds}};
    \end{tikzpicture}
  }
  \begin{theorem}
    The eigenvalues of an $n\times n$ matrix $A$ satisfy
    \begin{align*}
      \SizeFormula && \GeoMultBound
    \end{align*}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\EigenData}{
    \begin{NiceArray}{rlc}\toprule
      \multicolumn{1}{c}{\blu{\lambda}} & \multicolumn{1}{c}{\blu{\gm_A(\lambda)}} & \blu{\am_A(\lambda)} \\ \midrule
      i                                & 1,\dotsc,4                               & 4                    \\
      -i                                & 1,\dotsc,4                               & 4                    \\
      -5                                & 1,\dotsc,5                               & 5                    \\
      9                                & 1,\dotsc,8                               & 8                    \\ \bottomrule
    \end{NiceArray}
  }
  \begin{example}
    Suppose the characteristic polynomial of an $n\times n$ matrix $A$ is
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \chi_A(t)
          =
          (t-i)^{\subnode{argento1}{\textcolor<2->{red}{4}}}\cdot
          (t+i)^{\subnode{argento2}{\textcolor<2->{red}{4}}}\cdot
          (t+5)^{\subnode{argento3}{\textcolor<2->{red}{5}}}\cdot
          (t-9)^{\subnode{argento4}{\textcolor<2->{red}{8}}}
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (argento4.north) |- ++(3mm, 2.5mm)
        node[right] (am) {$\scriptstyle n=4+4+5+8=21$};

        \draw[<-, thick, red, overlay, visible on=<2->] (argento1.north) |- (am);
        \draw[<-, thick, red, overlay, visible on=<2->] (argento2.north) |- (am);
        \draw[<-, thick, red, overlay, visible on=<2->] (argento3.north) |- (am);
      \end{tikzpicture}
    \]
    \onslide<3->{We can organize our ``eigendata'' into a table.}
    \[
      \onslide<3->{\EigenData}
    \]
  \end{example}

\end{frame}


\section{Trace and Determinant}
\subsection{Formulas}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixtI}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle t\cdot I_2$};
        \end{tikzpicture}
      }
      ]
      t & 0 \\ 0 & t
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<2->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      a & b \\
      c & d
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{vNiceMatrix}[
      , c
      , margin
      % , code-before = {\hlMat<2->}
      % , code-after = {
      % \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
      %   \draw (row-1-|col-2) |- ++(-3mm, 2.5mm)
      %   node[left] {$\scriptstyle A$};
      % \end{tikzpicture}
      % }
      ]
      t-a & -b \\
      -c  & t-d
    \end{vNiceMatrix}
  }
  \begin{example}
    The characteristic polynomial of a generic $2\times 2$ matrix is
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \chi_A(t)
          % = \det\left(\MatrixtI-\MatrixA\right)
          = \MatrixX
          = t^2-\subnode{trace2b2}{\textcolor<2->{red}{(a+d)}}\,t+\subnode{det2b2}{\textcolor<3->{blu}{(ad-bc)}}
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (trace2b2.north) |- ++(-3mm, 2.5mm)
        node[left]{$\scriptstyle\trace(A)$};

        \draw[<-, thick, blu, overlay, visible on=<3->] (det2b2.north) |- ++(3mm, 2.5mm)
        node[right]{$\scriptstyle\det(A)$};
      \end{tikzpicture}
    \]
    \onslide<4->{This gives $\chi_A(t)=t^2-\trace(A)\,t+\det(A)$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\ChiFormula}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=redbg, draw, rounded corners, ultra thick]
        (text) {$\chi_A(t) = t^n-\trace(A)\,t^{n-1}+\dotsb+(-1)^n\det(A)$};
        % \node[above, grn] at (text.north) {\scriptsize\textnormal{Trace Formula}};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\TraceFormula}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<2->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\scriptstyle\trace(A)=\am_A(\lambda_1)\cdot\lambda_1+\dotsb+\am_A(\lambda_k)\cdot\lambda_k$};
        \node[above, grn] at (text.north) {\scriptsize\textnormal{Trace Formula}};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\DetFormula}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<2->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\scriptstyle\det(A)=\lambda_1^{\am_A(\lambda_1)}\dotsb\lambda_k^{\am_A(\lambda_k)}$};
        \node[above, blu] at (text.north) {\scriptsize\textnormal{Determinant Formula}};
      \end{tikzpicture}
    \end{array}
  }
  \begin{theorem}
    The characteristic polynomial of every $n\times n$ matrix $A$ is of the form
    \[
      \ChiFormula
    \]
    \onslide<2->{The Vieta formulas then give a \grn{trace formula} and a
      \blu{determinant formula}.}
    \begin{align*}
      \TraceFormula && \DetFormula
    \end{align*}
    \onslide<2->{where $\EVals(A)=\Set{\lambda_1,\dotsc,\lambda_k}$.}
  \end{theorem}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that the characteristic polynomial of a matrix $A$ is given by
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \chi_A(t)
          =
          t^{\subnode{briantired}{\textcolor<2->{red}{7}}}
          \subnode{evieawake}{\textcolor<3->{blu}{-2}}\,t^6+
          7\,t^5-
          t^3
          -11\,t^2+
          t+
          \subnode{sloanieawake}{\textcolor<4->{grn}{5}}
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (briantired.north) |- ++(-3mm, 2.5mm)
        node[left] {\scriptsize $A$ is $7\times 7$};

        \draw[<-, thick, blu, overlay, visible on=<3->] (evieawake.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle\trace(A)=-(-2)=2$};

        \draw[<-, thick, grn, overlay, visible on=<4->] (sloanieawake.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle\det(A)=(-1)^7\cdot5=-5$};
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose that the characteristic polynomial of a matrix $A$ is given by
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \chi_A(t)=
          \subnode{myeigen1}{\textcolor<2->{blu}{(t+4)}}^{\subnode{myeigenmult1}{\textcolor<3->{red}{2}}}\cdot
          \subnode{myeigen2}{\textcolor<2->{blu}{(t-1)}}^{\subnode{myeigenmult2}{\textcolor<3->{red}{7}}}\cdot
          \subnode{myeigen3}{\textcolor<2->{blu}{(t-9)}}^{\subnode{myeigenmult3}{\textcolor<3->{red}{3}}}
          \)};

        \draw[<-, thick, blu, overlay, visible on=<2->] (myeigen1.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle\lambda_1=-4$};

        \draw[<-, thick, blu, overlay, visible on=<2->] (myeigen2.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle\lambda_2=1$};

        \draw[<-, thick, blu, overlay, visible on=<2->] (myeigen3.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle\lambda_3=9$};

        \draw[<-, thick, red, overlay, visible on=<3->] (myeigenmult1.north) |- ++(-2mm, 2.5mm)
        node[left, scale=0.5] {$\am_A(-4)=2$};

        \draw[<-, thick, red, overlay, visible on=<3->] (myeigenmult2.north) |- ++(-2mm, 2.5mm)
        node[left, scale=0.5] {$\am_A(1)=7$};

        \draw[<-, thick, red, overlay, visible on=<3->] (myeigenmult3.north) |- ++(-2mm, 2.5mm)
        node[left, scale=0.5] {$\am_A(9)=3$};

      \end{tikzpicture}
    \]
    \onslide<4->{We may calculate $\trace(A)$ and $\det(A)$ with}
    \begin{align*}
      \onslide<4->{\trace(A)= 2\,(-4)+7\,(1)+3\,(9)=26} && \onslide<5->{\det(A) = (-4)^2\cdot 1^7\cdot 9^3 = 11664}
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlDiag<3->[grn]}
      ]
      {} -71 & -52 & -16 &  148 \\
      {}  48 &  35 &  12 & -102 \\
      {}  16 &  13 &   9 &  -25 \\
      {} -16 & -13 &  -4 &   30
    \end{bNiceMatrix}
  }
  \newcommand{\Eigenvalues}{
    \begin{aligned}
      \lambda_1 &= -7 &
                        \lambda_2 &=  5 \\
      \lambda_3 &=  9 &
                        \lambda_4 &=  ?
    \end{aligned}
  }
  \begin{example}
    Consider the data
    \begin{align*}
      A=\MatrixA && \Eigenvalues
    \end{align*}
    \onslide<2->{We can find the missing $\lambda_4$ with our trace formula.}
    \[
      \begin{tikzpicture}[remember picture, visible on=<2->]
        \node {\(
          \subnode{mytrace}{\textcolor<3->{grn}{\trace(A)}}
          =
          \subnode{l1}{\textcolor<4->{blu}{\lambda_1}}+
          \subnode{l2}{\textcolor<5->{blu}{\lambda_2}}+
          \subnode{l3}{\textcolor<6->{blu}{\lambda_3}}+
          \subnode{l4}{\textcolor<7->{red}{\lambda_4}}
          \)};

        \draw[<-, thick, grn, overlay, visible on=<3->] (mytrace.north) |- ++(-2mm, 2.5mm)
        node[left] {$\scriptstyle3$};

        \draw[<-, thick, blu, overlay, visible on=<4->] (l1.north) |- ++(-2mm, 2.5mm)
        node[left] {$\scriptstyle-7$};
        \draw[<-, thick, blu, overlay, visible on=<5->] (l2.north) |- ++(-2mm, 2.5mm)
        node[left] {$\scriptstyle5$};
        \draw[<-, thick, blu, overlay, visible on=<6->] (l3.north) |- ++(-2mm, 2.5mm)
        node[left] {$\scriptstyle9$};

        \draw[<-, thick, red, overlay, visible on=<7->] (l4.south) |- ++(2mm, -2.5mm)
        node[right] {$\scriptstyle-4$};
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}

\end{document}
