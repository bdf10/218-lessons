\documentclass[]{bmr}

\title{Projections}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Geometric Motivation}
\subsection{Projecting to $z$-Axis}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixP}{
    \begin{bNiceMatrix}[r]
      0 & 0 & 0 \\
      0 & 0 & 0 \\
      0 & 0 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}[1][]{
    \begin{bNiceMatrix}[####1]
      v_1\\ v_2\\ v_3
    \end{bNiceMatrix}
  }
  \newcommand{\VectorPV}[1][]{
    \begin{bNiceMatrix}[c, ####1]
      0\\ 0\\ v_3
    \end{bNiceMatrix}
  }
  \newcommand{\Figure}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , scale=2
        ]

        \coordinate (O) at (0, 0, 0);

        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \coordinate (v)  at ($ 0.6*(e1) + 1.0*(e2) + 0.75*(e3) $);
        \coordinate (Pv) at ($ 0.0*(e1) + 0.0*(e2) + 0.75*(e3) $);
        \coordinate (Qv) at ($ 0.6*(e1) + 1.0*(e2) + 0.00*(e3) $);

        \draw[->] (O) -- (e1) node[pos=1.15, overlay] {$x$};
        \draw[->] (O) -- (e2) node[pos=1.10, overlay] {$y$};
        \draw[->] (O) -- (e3) node[pos=1.10, overlay] {$z$};

        \draw[visible on=<2->] ($ (Pv)+(O)!1mm!(e3) $) -- ++($ (O)!1mm!(Qv) $) -- ($ (Pv)+(O)!1mm!(Qv) $);
        \draw[dashed, red, visible on=<2->] (v) -- (Pv);

        \draw[->, blu] (O) -- (v)  node[right, overlay] {$\bv\onslide<4->{=\VectorV[small]}$};
        \draw[->, grn, visible on=<3->] (O) -- (Pv)
        node[midway, left, overlay] {$P\bv\alt<5->{=\VectorPV[small]}{}$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Question}
    What is the \emph{\grn{projection}} $P\bv$ of $\bv$ onto the $z$-axis?
    \begin{align*}
      \Figure && \onslide<7->{\overset{P}{\MatrixP}\overset{\bv}{\VectorV}=\VectorPV}
    \end{align*}
    \onslide<6->{This projection is obtained with multiplication by a matrix
      $P$.}
  \end{block}

\end{frame}


\subsection{Projecting to $xy$-Plane}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixP}{
    \begin{bNiceMatrix}[r]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}[1][]{
    \begin{bNiceMatrix}[####1]
      v_1\\ v_2\\ v_3
    \end{bNiceMatrix}
  }
  \newcommand{\VectorPV}[1][]{
    \begin{bNiceMatrix}[c, ####1]
      v_1\\ v_2\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\Figure}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , ultra thick
        , scale=2
        ]

        \coordinate (O) at (0, 0, 0);

        \coordinate (e2) at (1, 0, 0);
        \coordinate (e3) at (0, 1, 0);
        \coordinate (e1) at (0, 0, 1);

        \coordinate (v)  at ($ 0.6*(e1) + 1.0*(e2) + 0.75*(e3) $);
        \coordinate (Pv) at ($ 0.0*(e1) + 0.0*(e2) + 0.75*(e3) $);
        \coordinate (Qv) at ($ 0.6*(e1) + 1.0*(e2) + 0.00*(e3) $);

        \draw[->] (O) -- (e1) node[pos=1.15, overlay] {$x$};
        \draw[->] (O) -- (e2) node[pos=1.10, overlay] {$y$};
        \draw[->] (O) -- (e3) node[pos=1.10, overlay] {$z$};

        \draw[visible on=<2->] ($ (Qv)+(O)!1mm!(e3) $) -- ++($ (O)!1mm!(Qv) $) -- ($ (Qv)+(O)!1mm!(Qv) $);
        \draw[dashed, red, visible on=<2->] (v) -- (Qv);

        \draw[->, blu] (O) -- (v)  node[right, overlay] {$\scriptstyle \bv\onslide<4->{=\VectorV[small]}$};
        \draw[->, grn, visible on=<3->] (O) -- (Qv)
        node[midway, below, sloped, overlay] {$\scriptstyle P\bv\alt<5->{=\VectorPV[small]}{}$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Question}
    What is the \emph{\grn{projection}} $P\bv$ of $\bv$ onto the $xy$-plane?
    \begin{align*}
      \Figure && \onslide<7->{\overset{P}{\MatrixP}\overset{\bv}{\VectorV}=\VectorPV}
    \end{align*}
    \onslide<6->{This projection is obtained with multiplication by a matrix
      $P$.}
  \end{block}

\end{frame}


\section{Projection Formula}
\subsection{Derivation}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixX}{
    \begin{bNiceMatrix}[r]
      \bv_1 & \Cdots & \bv_d
    \end{bNiceMatrix}
  }
  \newcommand{\Figure}{
    \begin{tikzpicture}[ultra thick, line join=round, line cap=round]

      \coordinate (O)  at (0, 0);

      \coordinate (w1) at (4, 0);
      \coordinate (w2) at (45:2.5);

      \coordinate (sw) at ($ -0.25*(w1)-0.25*(w2) $);

      \coordinate (e3) at (0, 3);


      \coordinate (Pv) at ($ 0.5*(w1)+0.5*(w2) $);
      \coordinate (v)  at ($ (Pv)+(e3) $);

      \draw[grn, fill=grnbg]
      ($ -0.25*(w1)-0.25*(w2) $)
      to node[midway, below, overlay] (V) {$\scriptstyle V\onslide<4->{=\Span\Set{\bv_1,\dotsc,\bv_d}}$}
      ++(w1) -- ++(w2) -- ++($ -1*(w1) $) -- cycle;

      \draw[<-, thick, red, overlay, visible on=<5->] (V.east) -- ++(3mm, 0)
      node[right] {\scriptsize\textnormal{define $X=\MatrixX$ so $V=\Col(X)$}};

      \draw[visible on=<2->] ($ (Pv)+(O)!2mm!(e3) $) -- ++($ (O)!2mm!(Pv) $) -- ($ (Pv)+(O)!2mm!(Pv) $);

      \draw[visible on=<2->, alt=<7->{->}{dashed}, red] (Pv) -- (v) node[midway, right, overlay, scale=0.75]
      {$
        \begin{NiceArray}{l}
          \onslide<7->{\bv-X\widehat{\bv[x]}\alt<8->{\in\Null(X^\intercal)}{\perp\Col(X)}}            \\
          \alt<10->{X^\intercal X\widehat{\bv[x]}=X^\intercal\bv}{\onslide<9->{X^\intercal(\bv-X\widehat{\bv[x]})=\bv[O]}}    \\
          \onslide<11->{\alt<13->{P\bv}{\alt<12->{X\widehat{\bv[x]}}{\widehat{\bv[x]}}}=\alt<12->{X}{}(X^\intercal X)^{-1}X^\intercal\bv}
        \end{NiceArray}
        $};

      \draw[->, grn, visible on=<3->] (O) -- (Pv) node[midway, below, sloped] (Pveq) {$\scriptstyle P\bv\onslide<6->{=X\widehat{\bv[x]}}$};
      \draw[->, blu] (O) -- (v) node[overlay, above] {$\scriptstyle\bv$};

      \draw[<-, red, thick, overlay, visible on=<6->] (Pveq.south) |- ++(1.5cm, -2mm) node[right]
      {\scriptsize\textnormal{$P\bv\in V=\Col(X)$ so $P\bv=X\widehat{\bv[x]}$}};

    \end{tikzpicture}
  }
  \begin{block}{Question}
    What is the \emph{\grn{projection}} $P\bv$ of $\bv$ onto
    $V\subset\mathbb{R}^n$?
    \begin{align*}
      \Figure
    \end{align*}
    \onslide<14->{We can use $P=X(X^\intercal X)^{-1}X^\intercal$ to project
      $\bv$ onto $V$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixX}{
    \begin{bNiceMatrix}[r]
      \bv_1 & \Cdots & \bv_d
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{projection matrix}} onto $V$ is
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(P_V=X(\subnode{gramian}{\color<2->{blu}X^\intercal X})^{-1}X^\intercal\)};

        \draw[<-, thick, blu, overlay, visible on=<2->] (gramian.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize\textnormal{The Gramian!}};
      \end{tikzpicture}
    \]
    where $X$ is any matrix whose columns form a basis of $V$.
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{alertblock}{Warning}
    It is tempting to write
    \[
      \begin{tikzpicture}[remember picture]
        \node[alt=<5->{draw opacity=1}{draw opacity=0}, draw=red, ultra thick, cross out]
        {\(P_V = X(X^\intercal X)^{-1}X^\intercal = \subnode{a}{\color<2->{blu}XX^{-1}}\subnode{b}{\color<3->{grn}(X^\intercal)^{-1}X^\intercal}=I_n\)};

        \draw[<-, thick, blu, overlay, visible on=<2->] (a.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle I_n$};
        \draw[<-, thick, grn, overlay, visible on=<3->] (b.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle I_n$};
      \end{tikzpicture}
    \]
    \onslide<4->{The matrix $X$ is \emph{\red{not necessarily square}} so
      $X^{-1}$ generally does not exist.}
  \end{alertblock}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\VectorV}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1}
      ]
      -2 & -3 & 2
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorW}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1}
      ]
      3 & 4 & -3
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\MatrixX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol####1{1}
        \hlCol####1{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, blu, thick, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $)
          |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle X$};
        \end{tikzpicture}
      }
      ]
      {} -2 &  3 \\
      {} -3 &  4 \\
      {}  2 & -3
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXT}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow####1{1}
        \hlRow####1{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, blu, thick, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $)
          |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle X^\intercal$};
        \end{tikzpicture}
      }
      ]
      {} -2 & -3 &  2 \\
      {}  3 &  4 & -3
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixGI}{
    \begin{bNiceArray}{rr|rr}[
      , margin
      , code-before = {\hlPortion####1[grn]{1}{1}{2}{2}}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $)
          |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle X^\intercal X$};
        \end{tikzpicture}
      }
      ]
      {}  17 & -24 & 1 & 0 \\
      {} -24 &  34 & 0 & 1
    \end{bNiceArray}
  }
  \newcommand<>{\MatrixGIR}{
    \begin{bNiceArray}{rr|rr}[
      , margin
      , code-before = {\hlPortion####1[grn]{1}{3}{2}{4}}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-3)!0.5!(row-1-|col-5) $)
          |- ++(3mm, 2.5mm) node[right] {$\scriptstyle (X^\intercal X)^{-1}$};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 17 & 12            \\
      0 & 1 & 12 & \sfrac{17}{2}
    \end{bNiceArray}
  }
  \newcommand<>{\MatrixGi}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $)
          |- ++(-1mm, 5mm) node[left] {$\scriptstyle (X^\intercal X)^{-1}$};
        \end{tikzpicture}
      }
      ]
      17 & 12            \\
      12 & \sfrac{17}{2}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixP}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, red, thick, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $)
          |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle P_V$};
        \end{tikzpicture}
      }
      ]
      \onslide####1{\sfrac{1}{2} } & \onslide####1{0} & \onslide####1{-\sfrac{1}{2}} \\
      \onslide####1{            0} & \onslide####1{1} & \onslide####1{            0} \\
      \onslide####1{-\sfrac{1}{2}} & \onslide####1{0} & \onslide####1{\sfrac{1}{2} }
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider $V=\Span\Set*{\VectorV<3->, \VectorW<3->}$.
    \begin{align*}
      \onslide<2->{\MatrixX<4->} && \onslide<5->{\rref\MatrixGI<6->=\MatrixGIR<7->}
    \end{align*}
    \onslide<8->{The projection matrix onto $V$ is}
    \[
      \onslide<8->{\MatrixP<12->=\MatrixX<9->\MatrixGi<10->\MatrixXT<11->}
    \]
  \end{example}

\end{frame}


\section{Properties}
\subsection{General Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Symmetric}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$P^\intercal=P$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{$P$ Symmetric}};
    \end{tikzpicture}
  }
  \newcommand{\Idempotent}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$P^2=P$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{$P$ Idempotent}};
    \end{tikzpicture}
  }
  \newcommand{\VinV}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {\textnormal{$\bv\in V$ means $P\bv=\bv$}};
      % \node[above, grn] at (text.north) {\scriptsize\textnormal{Vectors in $V$}};
    \end{tikzpicture}
  }
  \newcommand{\VinVPerp}{
    \begin{tikzpicture}[visible on=<2->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {\textnormal{$\bv\in V^\perp$ means $P\bv=\bv[O]$}};
      % \node[above, blu] at (text.north) {\scriptsize\textnormal{Vectors in $V^\perp$}};
    \end{tikzpicture}
  }
  \newcommand{\Dimension}{
    \begin{tikzpicture}[visible on=<3->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\trace(P)=\dim(V)$};
      % (text) {$\trace(P)=\trace(X(X^\intercal X)^{-1}X^\intercal)=\trace((X^\intercal X)^{-1}(X^\intercal X))=\trace(I_d)=\dim(V)$};
      % \node[above, blu] at (text.north) {\scriptsize\textnormal{Vectors in $V^\perp$}};
    \end{tikzpicture}
  }
  \begin{theorem}
    Projection matrices are \grn{symmetric} and \blu{idempotent}.
    \begin{align*}
      \Symmetric && \Idempotent
    \end{align*}
    \onslide<2->{Projection matrices offer a \grn{classification of vectors}.}
    \begin{align*}
      \VinV && \VinVPerp
    \end{align*}
    \onslide<3->{Projection matrices are \grn{dimension-conscious}.}
    \[
      \Dimension
    \]
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      {}  7 & 44 & 119 & 164 \\
      {}  5 & 26 &  72 & 102 \\
      {} -2 & -8 & -23 & -34 \\
      {}  2 & 13 &  35 &  48
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixP}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlDiag<2->[blu, th]
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-5-|col-4)!0.5!(row-5-|col-5) $) |- ++(-1mm, -3mm)
          node[left] {$\scriptstyle \dim\Col(A)=\trace(P)=3$};
        \end{tikzpicture}
      }
      ]
      \sfrac{6}{7} &  \sfrac{1}{7} &  \sfrac{1}{7} &  \sfrac{2}{7} \\
      \sfrac{1}{7} &  \sfrac{6}{7} & -\sfrac{1}{7} & -\sfrac{2}{7} \\
      \sfrac{1}{7} & -\sfrac{1}{7} &  \sfrac{6}{7} & -\sfrac{2}{7} \\
      \sfrac{2}{7} & -\sfrac{2}{7} & -\sfrac{2}{7} &  \sfrac{3}{7}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixAFigure}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture]
        \node {\( \subnode{A}{\color<3->{blu}A} = \MatrixA\)};

        \draw[<-, thick, blu, overlay, visible on=<3->] (A.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle \rank(A)=3$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand<>{\PMatrix}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlMat####1[blu, th]
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle P_{\Col(A)}$};
        \end{tikzpicture}
      }
      ]
      \sfrac{6}{7} &  \sfrac{1}{7} &  \sfrac{1}{7} &  \sfrac{2}{7} \\
      \sfrac{1}{7} &  \sfrac{6}{7} & -\sfrac{1}{7} & -\sfrac{2}{7} \\
      \sfrac{1}{7} & -\sfrac{1}{7} &  \sfrac{6}{7} & -\sfrac{2}{7} \\
      \sfrac{2}{7} & -\sfrac{2}{7} & -\sfrac{2}{7} &  \sfrac{3}{7}
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle \bv_1$};
        \end{tikzpicture}
      }
      ]
      -1 \\ 6 \\ 5 \\ -6
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorPV}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-1mm, 3.5mm)
          node[left] {$\scriptstyle P\bv_1=\bv_1$};
        \end{tikzpicture}
      }
      ]
      -1 \\ 6 \\ 5 \\ -6
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorW}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle \bv_2$};
        \end{tikzpicture}
      }
      ]
      4\\ -7\\ -5\\ 1
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorPW}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-1mm, 3.5mm)
          node[left] {$\scriptstyle P\bv_2\neq\bv_2$};
        \end{tikzpicture}
      }
      ]
      2\\ -5\\-3 \\5
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following data.
    \begin{align*}
      \MatrixAFigure && P_{\Col(A)} = \MatrixP
    \end{align*}
    \onslide<4->{Whether of not $\bv\in\Col(A)$ hinges on whether or not
      $P\bv=\bv$.}
    \begin{align*}
      \onslide<4->{\PMatrix<5->\VectorV<6->=\VectorPV<7->} && \onslide<4->{\PMatrix<8->\VectorW<9->=\VectorPW<10->}
    \end{align*}
    \onslide<11->{Here, $\bv_1\in\Col(A)$ and $\bv_2\notin\Col(A)$}
  \end{example}

\end{frame}


\subsection{Eigendata}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\EigenOne}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\mathcal{E}_{P_V}(1)=V$};
        \node[above, grn] at (text.north) {$\scriptstyle\bv\in V\Leftrightarrow P\bv=\bv=1\cdot\bv$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\EigenZero}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\mathcal{E}_{P_V}(0)=V^\perp$};
        \node[above, blu] at (text.north) {$\scriptstyle\bv\in V^\perp\Leftrightarrow P\bv=\bv[O]=0\cdot\bv$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\GeometricOne}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<2->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\gm_{P_V}(1) = \dim(V)$};
        % \node[above, grn] at (text.north) {$\scriptstyle\bv\in V\Leftrightarrow P\bv=\bv$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\GeometricZero}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<2->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$\gm_{P_V}(0) = \dim(V^\perp) = n-\dim(V)$};
        % \node[above, blu] at (text.north) {$\scriptstyle\bv\in V^\perp\Leftrightarrow P\bv=\bv[O]$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{theorem}
    For $V\subset\mathbb{R}^n$, $P_V$ has $\EVals(P_V)=\Set{1, 0}$ with
    eigenspaces
    \begin{align*}
      \EigenOne && \EigenZero
    \end{align*}
    \onslide<2->{Consequently, the geometric multiplicities are}
    \begin{align*}
      \GeometricOne && \GeometricZero
    \end{align*}
  \end{theorem}

\end{frame}


\section{Orthogonality}
\subsection{Dimension One}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Figure}{
    \begin{array}{c}
      \begin{tikzpicture}[ultra thick, line join=round, line cap=round]

        \coordinate (O) at (0, 0);
        \coordinate (v) at (15:1);

        \draw[grn, very thick, <->, visible on=<3->] ($ -1*(v) $) -- ($ 1.5*(v) $)
        node[right, overlay] {$\scriptstyle V$};

        \draw[blu, ->, visible on=<2->] (O) -- (v)
        node[midway, below, sloped] {$\scriptstyle \bv_1$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Projection}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture, visible on=<4->]

        \node {
          \(
          \displaystyle
          P_V = \bv_1(\subnode{norm}{\color<5->{blu}\bv_1^\intercal\bv_1})^{-1}\bv_1^\intercal
          \onslide<6->{= \frac{1}{\norm{\bv_1}^2}\bv_1\bv_1^\intercal}
          \)
        };

        \draw[<-, thick, blu, overlay, visible on=<5->] (norm.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle \bv_1^\intercal\bv_1=\inner{\bv_1, \bv_1}=\norm{\bv_1}^2$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<8->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<8->]
          \draw ($ (row-2-|col-1)!0.5!(row-2-|col-5) $) |- ++(2mm, -2.5mm)
          node[right] {$\scriptstyle \bv_1$};
        \end{tikzpicture}
      }
      ]
      2 & -1 & 2 & -1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\OuterProduct}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<11->}
      ]
      {}  4 & -2 &  4 & -2 \\
      {} -2 &  1 & -2 &  1 \\
      {}  4 & -2 &  4 & -2 \\
      {} -2 &  1 & -2 &  1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixV}{
    \begin{bNiceMatrix}[r, small]
      2\\ -1\\ 2\\ -1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixVT}{
    \begin{bNiceMatrix}[r, small]
      2& -1& 2& -1
    \end{bNiceMatrix}
  }
  \begin{block}{Observation}
    Suppose $V$ is one-dimensional so $V=\Span\Set{\bv_1}$.
    \begin{align*}
      \Figure && \Projection
    \end{align*}
    \onslide<7->{For example, consider $V=\Span\Set*{\VectorV}$.}
    \[
      \begin{tikzpicture}[remember picture]
        \node[visible on=<9->] (eq) {\(
          \displaystyle
          P_V = \frac{1}{\subnode{normten}{\color<10->{blu}10}}\OuterProduct
          \)};

        \draw[<-, thick, blu, overlay, visible on=<10->] (normten.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle \norm{\bv_1}^2=10$};

        \draw[<-, thick, blu, overlay, shorten <=-4mm, visible on=<11->] (eq.east) -- ++(5mm, 0)
        node[right] {$\scriptstyle\bv_1\bv_1^\intercal=\MatrixV\MatrixVT$};
      \end{tikzpicture}
    \]
    \onslide<12->{Projecting when $\dim(V)=1$ is not so bad!}
  \end{block}

\end{frame}


\subsection{Formula}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Problem}
    Suppose $\dim(V)=d$ and consider the projection formula
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(P_V = X\subnode{G}{\color<2->{red}(X^\intercal X)^{-1}} X^\intercal\)};

        \draw[<-, thick, red, overlay, visible on=<2->] (G.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize\textnormal{Need to invert this $d\times d$ Gramian!}};
      \end{tikzpicture}
    \]
    \onslide<3->{When $\dim(V)$ is large, this problem is difficult and
      annoying.}
  \end{block}

  \begin{block}{Useful Idea}<4->
    Every vector space $V\subset\mathbb{R}^n$ satisfies
    \[
      \dim(V)+\dim(V^\perp)=n
    \]
    \onslide<5->{Large spaces have \emph{\grn{small orthogonal complements}}.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    $P_V+P_{V^\perp}=I_n$
  \end{theorem}

  \begin{block}{Advantage}<2->
    To project onto $V$, we can use
    \[
      P_V\bv = (I_n-P_{V^\perp})\bv = \bv-P_{V^\perp}\bv
    \]
    When $V$ is ``big'' it is easier to project onto $V^\perp$.
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\VectorVa}{
    \begin{bNiceMatrix}[
      , small
      , code-before = {\hlMat<6->[blu, th]}
      ]
      3 & 1 & 3 & 1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\SpaceV}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture]
        \node {\(\subnode{V}{\color<2->{red}V} = \Span\Set*{\nv[small]{1 -1 -1 1}, \nv[small]{1 0 -2 3}, \nv[small]{-1 7 -2 2}}\)};

        \draw[<-, thick, red, overlay, visible on=<2->] (V.south) |- ++(3mm, -2.5mm)
        node[right] {$\scriptstyle\dim(V)=3$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\SpaceVPerp}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture]
        \node[visible on=<3->] {\(\subnode{VPerp}{\color<4->{red}V^\perp} = \Span\Set*{\subnode{v1}{\VectorVa}}\)};

        \draw[<-, thick, red, overlay, visible on=<4->] (VPerp.south) |- ++(3mm, -2.5mm)
        node[right] {$\scriptstyle\dim(V^\perp)=4-3=1$};
        \draw[<-, thick, blu, overlay, shorten <=-1mm, visible on=<6->] (v1.north) |- ++(3mm, 1mm)
        node[right] {$\scriptstyle\bv_1$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\OuterProduct}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<9->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<9->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle \bv_1\bv_1^\intercal$};
        \end{tikzpicture}
      }
      ]
      9 & 3 & 9 & 3 \\
      3 & 1 & 3 & 1 \\
      9 & 3 & 9 & 3 \\
      3 & 1 & 3 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<10->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=<10->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\scriptstyle \bv$};
        \end{tikzpicture}
      }
      ]
      4 \\ 5 \\ 14 \\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorQV}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<11->[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=<11->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(2mm, 2.5mm)
          node[right] {$\scriptstyle P_{V^\perp}\bv$};
        \end{tikzpicture}
      }
      ]
      9 \\ 3 \\ 9 \\ 3
    \end{bNiceMatrix}
  }
  \newcommand{\RowPV}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<15->[blu]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<15->]
          \draw ($ (row-2-|col-1)!0.5!(row-2-|col-5) $) |- ++(-2mm, -2.5mm)
          node[left] {$\scriptstyle P_V\bv$};
        \end{tikzpicture}
      }
      ]
      -5 & 2 & 5 & -2
    \end{bNiceMatrix}^\intercal
  }
    \newcommand{\RowV}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<13->[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=<13->]
          \draw ($ (row-2-|col-1)!0.5!(row-2-|col-5) $) |- ++(-2mm, -2.5mm)
          node[left] {$\scriptstyle \bv$};
        \end{tikzpicture}
      }
      ]
      4 & 5 & 14 & 1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\RowPPerpV}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat<14->[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=<14->]
          \draw ($ (row-2-|col-1)!0.5!(row-2-|col-5) $) |- ++(-2mm, -2.5mm)
          node[left] {$\scriptstyle P_{V^\perp}\bv$};
        \end{tikzpicture}
      }
      ]
      9 & 3 & 9 & 3
    \end{bNiceMatrix}^\intercal
  }
  \begin{example}
    Let's project $\bv=\nv{4 5 14 1}$ onto $V\subset\mathbb{R}^4$ where
    \begin{align*}
      \SpaceV && \SpaceVPerp
    \end{align*}
    \onslide<5->{Here, it is easier to start by projecting onto $V^\perp$.}
    \[
      \begin{tikzpicture}[remember picture]
        \node[visible on=<7->] {\(\displaystyle
          \frac{1}{\subnode{norm}{\color<8->{blu}20}}
          \subnode{outer}{\OuterProduct}
          \subnode{vect}{\VectorV}
          =
          \subnode{QVect}{\VectorQV}
          \)};

        \draw[<-, thick, blu, overlay, visible on=<8->] (norm.west) -- ++(-3mm, 0)
        node[left] {$\scriptstyle \norm{\bv_1}^2=20$};
        % \draw[<-, thick, blu, shorten <=-1mm, overlay, visible on=<9->] ($ (outer.north)+(-10mm, 0) $) |- ++(-3mm, 1mm)
        % node[left] {$\scriptstyle \bv_1\bv_1^\intercal$};
        % \draw[<-, thick, grn, shorten <=-1mm, overlay, visible on=<10->] (vect.north east) |- ++(-2mm, 1mm)
        % node[left] {$\scriptstyle \bv$};
      \end{tikzpicture}
    \]
    \onslide<12->{The projection of $\bv$ onto $V$ is then}
    \[
      \onslide<12->{\RowPV = \RowV - \RowPPerpV}
    \]
  \end{example}

\end{frame}


\end{document}
