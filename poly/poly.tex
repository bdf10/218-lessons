\documentclass[]{bmr}

\title{Polynomial Algebra}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Polynomials}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{\grn{polynomial}} in a variable $t$ is an expression $f(t)$ of the
    form
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(f(t)=\subnode{leading}{\textcolor<2->{red}{c_n}}\,t^{\subnode{defdeg}{\textcolor<3->{red}{n}}}+c_{n-1}\,t^{n-1}+\dotsb+c_1\,t+\subnode{const}{\textcolor<4->{red}{c_0}}\)};

        \draw[<-, thick, red, overlay, visible on=<2->] (leading.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize ``leading coefficient'' $c_n\neq0$};

        \draw[<-, thick, red, overlay, visible on=<3->] (defdeg.north) |- ++(3mm, 2.5mm)
        node[right] {\scriptsize ``degree'' is the largest exponent};

        \draw[<-, thick, red, overlay, visible on=<4->] (const.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize ``constant coefficient''};
      \end{tikzpicture}
    \]
    \onslide<5->{We call $f(t)$ \emph{\grn{monic}} if $c_n=1$.}
  \end{definition}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}[Quadratic, $\deg=2$]
    \begin{tikzpicture}[remember picture]
      \node {\(f(t)=\subnode{quadratic}{\textcolor<2->{blu}{t^2}}-2\,t+3\)};

      \draw[<-, thick, blu, overlay, visible on=<2->] (quadratic.south) |- ++(3mm, -2.5mm)
      node[right] {\scriptsize monic $c_2=1$};
    \end{tikzpicture}
  \end{example}

  \begin{example}[Cubic, $\deg=3$]<3->
    \begin{tikzpicture}[remember picture]
      \node {\(f(t)=\subnode{cubic}{\textcolor<4->{red}{-5}}\,t^3+6\,t^2-t+5\)};

      \draw[<-, thick, red, overlay, visible on=<4->] (cubic.south) |- ++(3mm, -2.5mm)
      node[right] {\scriptsize not monic $c_3=-5$};
    \end{tikzpicture}
  \end{example}

  \begin{example}[Quartic, $\deg=4$]<5->
    \begin{tikzpicture}[remember picture]
      \node {\(f(t)=\subnode{quartic}{\textcolor<6->{red}{7}}\,t^4+3\,t^2-t\)};

      \draw[<-, thick, red, overlay, visible on=<6->] (quartic.south) |- ++(3mm, -2.5mm)
      node[right] {\scriptsize not monic $c_4=7$};
    \end{tikzpicture}
  \end{example}

  \begin{example}[Quintic, $\deg=5$]<7->
    \begin{tikzpicture}[remember picture]
      \node {\(f(t)=\subnode{quintic}{\textcolor<8->{blu}{t^5}}+t^4+t^3+t^2+t+1\)};

      \draw[<-, thick, blu, overlay, visible on=<8->] (quintic.south) |- ++(3mm, -2.5mm)
      node[right] {\scriptsize monic $c_5=1$};
    \end{tikzpicture}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Polyf}{
    \begin{array}{c}
      \begin{tikzpicture}[<-, thick, visible on=<2->]
        \node[alt=<3->{blu}{}] (f) {$f(t)=2\,t^2+7\,t-5$};

        \draw[blu, overlay, visible on=<3->] (f.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize $f\in\mathbb{R}[t]$ and $f\in\mathbb{C}[t]$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Polyg}{
    \begin{array}{c}
      \begin{tikzpicture}[<-, thick, visible on=<4->]
        \node[alt=<5->{red}{}] (g) {$g(t)=t^2+(6-4\,i)\,t-(1+i)$};

        \draw[red, overlay, visible on=<5->] (g.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize $g\notin\mathbb{R}[t]$ but $g\in\mathbb{C}[t]$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    We write $f\in\mathbb{R}[t]$ if the coefficients are in $\mathbb{R}$ and
    $f\in\mathbb{C}[t]$ if they are in $\mathbb{C}$.
    \begin{align*}
      \Polyf && \Polyg
    \end{align*}
    \onslide<6->{Polynomials $f\in\mathbb{R}[t]$ satisfy
      $f(\overline{t})=\overline{f(t)}$.}
  \end{definition}

\end{frame}


\subsection{Roots}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Roota}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture, baseline, visible on=<2->]
        \node {\(f(\subnode{yesroot1}{\textcolor<3->{blu}{i}})=0\)};

        \draw[<-, thick, blu, overlay, visible on=<3->] (yesroot1.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize $r=i$ root};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Rootb}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture, baseline, visible on=<2->]
        \node {\(f(\subnode{yesroot2}{\textcolor<4->{blu}{2}})=0\)};

        \draw[<-, thick, blu, overlay, visible on=<4->] (yesroot2.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize $r=2$ root};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\NotRoot}{
    \begin{array}{c}
      \begin{tikzpicture}[remember picture, baseline, visible on=<2->]
        \node {\(f(\subnode{noroot}{\textcolor<5->{red}{1}})=-2\)};

        \draw[<-, thick, red, overlay, visible on=<5->] (noroot.south) |- ++(2mm, -2.5mm)
        node[right] {\scriptsize $r=1$ not a root};
      \end{tikzpicture}
    \end{array}
  }
  \begin{definition}
    A \emph{\grn{root}} of a polynomial $f(t)$ is a scalar $r$ satisfying
    $f(r)=0$.
    \begin{align*}
      \onslide<2->{f(t)=t^3-2\,t^2+t-2} && \Roota && \NotRoot && \Rootb
    \end{align*}
    \onslide<6->{Every root is complex, but \emph{\red{some roots are not
          real}}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $r$ is a root of $f\in\mathbb{R}[t]$. Then $\overline{r}$ is
    also a root.
  \end{theorem}
  \begin{proof}<2->
    $f(\overline{r})=\overline{f(r)}=\overline{0}=0$
  \end{proof}

\end{frame}


\section{Fundamental Theorem of Algebra}
\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Fundamental Theorem of Algebra]
    Every monic polynomial uniquely factors as
    \[
      f(t) = (t-r_1)^{m_1}\cdot(t-r_2)^{m_2}\dotsb(t-r_k)^{m_k}
    \]
    where $r_1,r_2,\dotsc,r_k\in\mathbb{C}$ are distinct roots and
    \[
      \deg(f)=m_1+m_2+\dotsb+m_k
    \]
    The number $m_j$ is called the \grn{multiplicity} of $r_j$ as a root of $f$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the factorization
    \[
      \begin{tikzpicture}[remember picture]
        \node {$
          f(t)=
          (\subnode{exr1}{\textcolor<3->{blu}{t+2-3\,i}})^{\subnode{exm1}{\textcolor<2->{red}{3}}}\cdot
          (\subnode{exr2}{\textcolor<4->{blu}{t+2+3\,i}})^{\subnode{exm2}{\textcolor<2->{red}{3}}}\cdot
          (\subnode{exr3}{\textcolor<5->{blu}{t+7}})^{\subnode{exm3}{\textcolor<2->{red}{4}}}
          $};

        \draw[<-, thick, blu, overlay, visible on=<3->] (exr1.south) |- ++(1mm, -2.5mm)
        node[right] {$\scriptstyle r_1=-2+3\,i$};
        \draw[<-, thick, blu, overlay, visible on=<4->] (exr2.south) |- ++(1mm, -2.5mm)
        node[right] {$\scriptstyle r_2=-2-3\,i$};
        \draw[<-, thick, blu, overlay, visible on=<5->] (exr3.south) |- ++(1mm, -2.5mm)
        node[right] {$\scriptstyle r_3=-7$};

        \draw[<-, thick, red, overlay, visible on=<2->] (exm3.north) |- ++(3mm, 2.5mm)
        node[right] (exdeg) {$\scriptstyle \deg(f)=3+3+4=10$};
        \draw[<-, thick, red, overlay, visible on=<2->] (exm1.north) |- (exdeg);
        \draw[<-, thick, red, overlay, visible on=<2->] (exm2.north) |- (exdeg);
      \end{tikzpicture}
    \]
    \onslide<6->{Here, $\deg(f)=10$ with roots of multiplicities $m_1=m_2=3$ and
      $m_3=4$.}
  \end{example}

\end{frame}


\subsection{Formulas}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Quadratic Formula]
    $
    \displaystyle
    a\,t^2+b\,t+c
    =
    a\cdot
    \left(t-\frac{-b-\sqrt{b^2-4\,ac}}{2\,a}\right)\cdot
    \left(t-\frac{-b+\sqrt{b^2-4\,ac}}{2\,a}\right)
    $
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the factorization
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          -2\,t^2+8\,t-10
          =
          -2\cdot
          (t-(\subnode{quadr2}{\textcolor<2->{blu}{2+i}}))\cdot
          (t-(\subnode{quadr1}{\textcolor<3->{blu}{2-i}}))
          \)};

        \draw[<-, thick, blu, overlay, visible on=<2->] (quadr2.south) |- ++(-1mm, -3mm)
        node[left] {$\scriptstyle r_1=\frac{-8-\sqrt{8^2-4(-2)(-10)}}{2\,(-2)}$};
        \draw[<-, thick, blu, overlay, visible on=<3->] (quadr1.south) |- ++(1mm, -3mm)
        node[right] {$\scriptstyle r_2=\frac{-8+\sqrt{8^2-4(-2)(-10)}}{2\,(-2)}$};
      \end{tikzpicture}
    \]
    \onslide<4->{Note that the roots are related by conjugation
      $r_2=\overline{r_1}$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Cubic Formula]
    There is a cubic formula.
  \end{theorem}

  \begin{theorem}[Quartic Formula]<2->
    There is a quartic formula.
  \end{theorem}

  \begin{theorem}[Abel, 1824]<3->
    There is no quintic formula.
  \end{theorem}

\end{frame}


\section{Vieta's Formulas}
\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose we factor a general quadratic.
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          t^2+\subnode{exc1}{\textcolor<2->{blu}{c_1}}\,t+\subnode{exc0}{\textcolor<3->{red}{c_0}}
          =
          (t-r_1)\cdot(t-r_2)
          =
          t^2\subnode{vietac1}{\textcolor<2->{blu}{-(r_1+r_2)}}\,t+\subnode{vietac0}{\textcolor<3->{red}{r_1\cdot r_2}}
          \)};

        \draw[<->, thick, blu, visible on=<2->] (exc1.south) -- ++(0, -2.5mm) -| (vietac1.south)
        node[pos=0.25, below] {$\scriptstyle c_1=-(r_1+r_2)$};

        \draw[<->, thick, red, visible on=<3->] (exc0.north) -- ++(0, 2.5mm) -| (vietac0.north)
        node[pos=0.25, above] {$\scriptstyle c_0=r_1\cdot r_2$};
      \end{tikzpicture}
    \]
    \onslide<4->{The roots are related to the coefficients!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Vieta's Formulas]
    Suppose we factor a monic polynomial.
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          t^n+\subnode{vietacnmm}{\textcolor<2->{blu}{c_{n-1}}}\,t^{n-1}+\dotsb+\subnode{vietac0ex}{\textcolor<3->{red}{c_0}}
          =
          (t-r_1)^{m_1}\cdot(t-r_2)^{m_2}\dotsb(t-r_k)^{m_k}
          \)};

        \draw[<-, thick, blu, visible on=<2->] (vietacnmm.south) |- ++(3mm, -2.5mm)
        node[right] {$\scriptstyle c_{n-1}=-(m_1\cdot r_1+m_2\cdot r_2+\dotsb+m_k\cdot r_k)$};

        \draw[<-, thick, red, visible on=<3->] (vietac0ex.north) |- ++(3mm, 2.5mm)
        node[right] {$\scriptstyle c_0=(-1)^n\cdot r_1^{m_1}\cdot r_2^{m_2}\dotsb r_k^{m_k}$};
      \end{tikzpicture}
    \]
    \onslide<4->{These formulas relate the coefficients to the roots.}
  \end{theorem}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Consider the factorization
    \[
      \begin{tikzpicture}[remember picture]
        \node {$
          f(t)=
          (\subnode{root1}{\textcolor<3->{blu}{t+2-3\,i}})^{\subnode{mult1}{\textcolor<2->{red}{3}}}\cdot
          (\subnode{root2}{\textcolor<4->{blu}{t+2+3\,i}})^{\subnode{mult2}{\textcolor<2->{red}{3}}}\cdot
          (\subnode{root3}{\textcolor<5->{blu}{t+7}})^{\subnode{mult3}{\textcolor<2->{red}{4}}}
          $};

        \draw[<-, thick, blu, overlay, visible on=<3->] (root1.south) |- ++(1mm, -2.5mm)
        node[right] {$\scriptstyle r_1=-2+3\,i$};
        \draw[<-, thick, blu, overlay, visible on=<4->] (root2.south) |- ++(1mm, -2.5mm)
        node[right] {$\scriptstyle r_2=-2-3\,i$};
        \draw[<-, thick, blu, overlay, visible on=<5->] (root3.south) |- ++(1mm, -2.5mm)
        node[right] {$\scriptstyle r_3=-7$};

        \draw[<-, thick, red, overlay, visible on=<2->] (mult3.north) |- ++(3mm, 2.5mm)
        node[right] (exdeg) {$\scriptstyle \deg(f)=3+3+4=10$};
        \draw[<-, thick, red, overlay, visible on=<2->] (mult1.north) |- (exdeg);
        \draw[<-, thick, red, overlay, visible on=<2->] (mult2.north) |- (exdeg);
      \end{tikzpicture}
    \]
    \onslide<6->{The coefficients $c_9$ and $c_0$ are}
    \begin{align*}
      \onslide<6->{c_9} &\onslide<6->{=} \onslide<6->{-[3\cdot(-2+3\,i)+3\cdot(-2-3\,i)+4\cdot(-7)]        = 40}      \\
      \onslide<7->{c_0} &\onslide<7->{=} \onslide<7->{(-1)^{10}\cdot (-2+3\,i)^3\cdot(-2-3\,i)^3\cdot(-7)^4 = 5274997}
    \end{align*}
  \end{example}

\end{frame}

\end{document}
