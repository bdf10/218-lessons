\documentclass[]{bmr}

\title{The Spectral Theorem}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Part I}
\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Hermitian}{
    \begin{tikzpicture}[inner sep=0, outer sep=0]
      \node (n480f) {Hermitian};
    \end{tikzpicture}
  }
  \begin{theorem}[The Spectral Theorem, Part I]
    The eigenvalues of every Hermitian matrix $H$ are real.
  \end{theorem}
  \begin{proof}<2->
    Choose a unit vector $\bv\in\mathcal{E}_H(\lambda)$. \onslide<3->{Then}
    \begin{align*}
      \onslide<3->{\lambda} &\onslide<3->{\onslide<3->{=}} \onslide<3->{\lambda\cdot\inner{\bv, \bv}} &&\onslide<6->{=} \onslide<7->{\inner{H\bv, \bv}}                       \\
      {}                    &\onslide<3->{=} \onslide<4->{\inner{\bv, \lambda\cdot\bv}}               &&\onslide<7->{=} \onslide<8->{\inner{\lambda\cdot\bv, \bv}}            \\
      {}                    &\onslide<4->{=} \onslide<5->{\inner{\bv, H\bv}}                          &&\onslide<8->{=} \onslide<9->{\overline{\lambda}\cdot\inner{\bv, \bv}} \\
      {}                    &\onslide<5->{=} \onslide<6->{\inner{H^\ast\bv, \bv}}                     &&\onslide<9->{=} \onslide<10->{\overline{\lambda}}
    \end{align*}
    \onslide<11->{So $\overline{\lambda}=\lambda$, which means
      $\lambda\in\mathbb{R}$.\qedhere}
  \end{proof}
  % \begin{proof}<2->
  %   Choose a unit vector $\bv\in\mathcal{E}_H(\lambda)$. \onslide<3->{Then}
  %   \[
  %     \begin{tikzpicture}[remember picture, visible on=<3->]
  %       \node {\(
  %       \lambda=\lambda\cdot\inner{\bv, \bv}
  %       \onslide<4->{=\inner{\bv, \alt<5->{H}{\lambda\cdot}\bv}}
  %       \onslide<6->{=\inner{\alt<8->{\lambda\cdot}{\alt<7->{H}{H^\ast}}\bv, \bv}}
  %       \onslide<9->{=\overline{\lambda}\alt<10->{}{\cdot\inner{\bv, \bv}}}
  %       \)};
  %     \end{tikzpicture}
  %   \]
  %   \onslide<11->{So $\overline{\lambda}=\lambda$, which means
  %   $\lambda\in\mathbb{R}$.\qedhere}
  % \end{proof}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixHa}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {\scriptsize real-symmetric};
        \end{tikzpicture}
      }
      ]
      {}  2 & -9 \\
      {} -9 &  5
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixHb}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(3mm, 2.5mm)
          node[right] {\scriptsize Hermitian};
        \end{tikzpicture}
      }
      ]
      {}     -8 & 1-3\,i & -i \\
      {} 1+3\,i & 5      & 7+i \\
      {}      i &    7-i & 9
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixHc}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(3mm, 2.5mm)
          node[right] {\scriptsize real-symmetric};
        \end{tikzpicture}
      }
      ]
      {} 17 & 22 & 4 \\
      {} 22 & -5 & 9 \\
      {}  4 &  9 & 7
    \end{bNiceMatrix}
  }
  \begin{example}
    The eigenvalues of each of these matrices are all real.
    \begin{align*}
      \MatrixHa<2-> && \MatrixHb<3-> && \MatrixHc<4->
    \end{align*}
    \onslide<5->{Real-symmetric matrices are also Hermitian!}
  \end{example}


\end{frame}


\section{Part II}
\subsection{Statement}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\Sep}{
    &\scriptstyle\onslide####1{= }
  }
  \newcommand<>{\Fr}{\onslide####1{\frac{1}{\lambda_1-\lambda_2}\cdot}}
  \begin{theorem}[The Spectral Theorem, Part II]
    The eigenspaces of a Hermitian matrix $H$ are orthogonal to each other.
  \end{theorem}
  \begin{proof}<2->
    Consider $\bv_1\in\mathcal{E}_H(\lambda_1)$ and
    $\bv_2\in\mathcal{E}_H(\lambda_2)$ where $\lambda_1\neq\lambda_2$. \onslide<3->{Then}
    \begin{align*}\scriptstyle
      {} \onslide<3->{\inner{\bv_1, \bv_2}} \Sep<3->        \onslide<3->{\frac{\lambda_1-\lambda_2}{\lambda_1-\lambda_2}\cdot\inner{\bv_1, \bv_2}}    & \Sep<6-> \Fr<7->\onslide<7->{\Set{\inner{\lambda_1\cdot\bv_1, \bv_2}-\inner{H^\ast\bv_1, \bv_2}}}         \\
      {}                                    \Sep<3-> \Fr<4->\onslide<4->{\inner{\bv_1, \lambda_1\cdot\bv_2-\lambda_2\cdot\bv_2}}                      & \Sep<7-> \Fr<8->\onslide<8->{\Set{\inner{H\bv_1, \bv_2}-\inner{H\bv_1, \bv_2}}}                           \\
      {}                                    \Sep<4-> \Fr<5->\onslide<5->{\Set{\lambda_1\cdot\inner{\bv_1, \bv_2}-\inner{\bv_1, \lambda_2\cdot\bv_2}}} & \Sep<8->        \onslide<9->{0}                                                                           \\
      {}                                    \Sep<5-> \Fr<6->\onslide<6->{\Set{\inner{\overline{\lambda_1}\cdot\bv_1, \bv_2}-\inner{\bv_1, H\bv_2}}}
    \end{align*}
    \onslide<10->{This shows that $\bv_1\perp\bv_2$.} \onslide<11->{Hence
      $\mathcal{E}_H(\lambda_1)\perp\mathcal{E}_H(\lambda_2)$.\qedhere}
  \end{proof}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixS}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(3mm, 2.5mm)
          node[right] {\scriptsize real-symmetric!};
        \end{tikzpicture}
      }
      ]
      {}  4 & -1 & -1 \\
      {} -1 &  4 &  1 \\
      {} -1 &  1 &  4
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      ]
      1 & -1 & -1
    \end{bNiceMatrix}^{\scriptscriptstyle\intercal}
  }
  \newcommand<>{\VectorW}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      ]
      1 & 0 & 1
    \end{bNiceMatrix}^{\scriptscriptstyle\intercal}
  }
  \newcommand<>{\VectorX}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[red, th]}
      ]
      0 & 1 & -1
    \end{bNiceMatrix}^{\scriptscriptstyle\intercal}
  }
  \newcommand{\Eigen}{
    \begin{aligned}
      \onslide<2->{\mathcal{E}_S(6)} &\onslide<2->{= \Span\Set{\VectorV<5-6,8->}} \\
      \onslide<3->{\mathcal{E}_S(3)} &\onslide<3->{= \Span\Set{\VectorW<6->, \VectorX<9->}}
    \end{aligned}
  }
  \begin{example}
    This matrix $S$ has $\EVals(S)=\Set{3, 6}$.
    \begin{align*}
      S=\MatrixS<11-> && \Eigen
    \end{align*}
    \onslide<4->{Consider the following inner-products.}
    \begin{align*}
      \onslide<4->{\inner{\VectorV<5->, \VectorW<6->} = 0} && \onslide<7->{\inner{\VectorV<8->, \VectorX<9->} = 0}
    \end{align*}
    \onslide<10->{These calculations demonstrate that
      $\mathcal{E}_S(3)\perp\mathcal{E}_S(6)$.}
  \end{example}

\end{frame}


\section{Part III}
\subsection{Statement}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[The Spectral Theorem, Part III]
    Every Hermitian matrix is diagonalizable.
  \end{theorem}
  \begin{proof}<2->
    Hard.
  \end{proof}

\end{frame}

\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixHa}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {\scriptsize real-symmetric};
        \end{tikzpicture}
      }
      ]
      {}  2 & -9 \\
      {} -9 &  5
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixHb}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(3mm, 2.5mm)
          node[right] {\scriptsize Hermitian};
        \end{tikzpicture}
      }
      ]
      {}     -8 & 1-3\,i & -i \\
      {} 1+3\,i & 5      & 7+i \\
      {}      i &    7-i & 9
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixHc}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(3mm, 2.5mm)
          node[right] {\scriptsize real-symmetric};
        \end{tikzpicture}
      }
      ]
      {} 17 & 22 & 4 \\
      {} 22 & -5 & 9 \\
      {}  4 &  9 & 7
    \end{bNiceMatrix}
  }
  \begin{example}
    Each of these matrices is diagonalizable.
    \begin{align*}
      \MatrixHa<2-> && \MatrixHb<3-> && \MatrixHc<4->
    \end{align*}
    \onslide<5->{Real-symmetric matrices are also Hermitian!}
  \end{example}

\end{frame}


\section{Spectral Factorizations}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A matrix $H$ is Hermitian if and only if $H$ has a \grn{spectral
      factorization}.
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(%
          \subnode{Hioe}{H}
          =
          \subnode{Uioe}{\textcolor<2->{blu}{U}}
          \subnode{Dioe}{\textcolor<3->{red}{D}}
          U^\ast
          \)};

        \draw[<-, thick, blu, overlay, visible on=<2->] (Uioe.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize\textnormal{unitary $U^\ast=U^{-1}$}};

        \draw[<-, thick, red, overlay, visible on=<3->] (Dioe.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize\textnormal{real-diagonal}};
      \end{tikzpicture}
    \]
    \onslide<4->{Columns of $U$ can be obtained by applying Gram-Schmidt to each
      $\mathcal{E}_H(\lambda)$.}
  \end{theorem}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixS}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(3mm, 2.5mm)
          node[right] {\scriptsize real-symmetric!};
        \end{tikzpicture}
      }
      ]
      {}  4 & -1 & -1 \\
      {} -1 &  4 &  1 \\
      {} -1 &  1 &  4
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(3mm, 2.5mm)
          node[right, scale=0.75] {$\bv[q]_1=\left[\begin{array}{rrr}\sfrac{1}{\sqrt{3}} & -\sfrac{1}{\sqrt{3}} & -\sfrac{1}{\sqrt{3}}\end{array}\right]^\intercal$};
        \end{tikzpicture}
      }
      ]
      1 & -1 & -1
    \end{bNiceMatrix}^{\scriptscriptstyle\intercal}
  }
  \newcommand<>{\VectorW}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-2-|col-1)!0.5!(row-2-|col-4) $) |- ++(-3mm, -2.5mm)
          node[left, scale=0.75] {$\bv[q]_1=\left[\begin{array}{rrr}\sfrac{1}{\sqrt{2}} & 0 & \sfrac{1}{\sqrt{2}}\end{array}\right]^\intercal$};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 1
    \end{bNiceMatrix}^{\scriptscriptstyle\intercal}
  }
  \newcommand<>{\VectorX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-2-|col-1)!0.25!(row-2-|col-4) $) |- ++(3mm, -2.5mm)
          node[right, scale=0.55] {$\bv[q]_2=\left[\begin{array}{rrr}\sfrac{1}{\sqrt{6}} & \sfrac{2}{\sqrt{6}} & -\sfrac{1}{\sqrt{6}} \end{array}\right]^\intercal$};
        \end{tikzpicture}
      }
      ]
      0 & 1 & -1
    \end{bNiceMatrix}^{\scriptscriptstyle\intercal}
  }
  \newcommand{\Eigen}{
    \begin{aligned}
      \mathcal{E}_S(6) &= \Span\Set{\VectorV<3->} \\
      \mathcal{E}_S(3) &= \Span\Set{\VectorW<7->, \VectorX<11->}
    \end{aligned}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<4->{1}
        \hlCol<8->[grn]{2}
        \hlCol<12->[red]{3}
      }
      ]
      {} \onslide<4->{ \sfrac{1}{\sqrt{3}}} & \onslide<8->{ \sfrac{1}{\sqrt{2}}} & \onslide<12->{\sfrac{1}{\sqrt{6}}}  \\
      {} \onslide<4->{-\sfrac{1}{\sqrt{3}}} & \onslide<8->{0}                    & \onslide<12->{\sfrac{2}{\sqrt{6}}}  \\
      {} \onslide<4->{-\sfrac{1}{\sqrt{3}}} & \onslide<8->{\sfrac{1}{\sqrt{2}}} & \onslide<12->{-\sfrac{1}{\sqrt{6}}}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<5->{1}{1}
        \hlEntry<9->[grn]{2}{2}
        \hlEntry<13->[red]{3}{3}
      }
      ]
      {} \onslide<5->{6} &                 &                  \\
      {}                 & \onslide<9->{3} &                  \\
      {}                 &                 & \onslide<13->{3}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixUT}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<6->{1}
        \hlRow<10->[grn]{2}
        \hlRow<14->[red]{3}
      }
      ]
      {} \onslide<6->{\sfrac{1}{\sqrt{3}}}  & \onslide<6->{-\sfrac{1}{\sqrt{3}}}  & \onslide<6->{-\sfrac{1}{\sqrt{3}}} \\
      {} \onslide<10->{\sfrac{1}{\sqrt{2}}} & \onslide<10->{0}                    & \onslide<10->{\sfrac{1}{\sqrt{2}}} \\
      {} \onslide<14->{\sfrac{1}{\sqrt{6}}} & \onslide<14->{\sfrac{2}{\sqrt{6}}}  & \onslide<14->{-\sfrac{1}{\sqrt{6}}}
    \end{bNiceMatrix}
  }
  \begin{example}
    This matrix $S$ has $\EVals(S)=\Set{3, 6}$.
    \begin{align*}
      S=\MatrixS<15-> && \Eigen
    \end{align*}
    \onslide<2->{Applying Gram-Schmidt to the eigenspaces gives a spectral
      factorization.}
    \[
      \onslide<2->{
        S
        =
        \overset{U}{\MatrixU}
        \overset{D}{\MatrixD}
        \overset{U^\ast}{\MatrixUT}
      }
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixH}{
    \begin{bNiceMatrix}[
      , c
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(3mm, 2.5mm) node[right] {\scriptsize Hermitian};
        \end{tikzpicture}
      }
      ]
      {}3    & 1-i \\
      {} 1+i & 2
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.25!(row-1-|col-3) $) |- ++(3mm, 2.5mm)
          node[right, scale=0.75] {$\bv[q]_1=\left[\begin{array}{rr}\sfrac{1}{\sqrt{3}} & \sfrac{(-1-i)}{\sqrt{3}}\end{array}\right]^\intercal$};
        \end{tikzpicture}
      }
      ]
      1 & -1-i
    \end{bNiceMatrix}^{\scriptscriptstyle\intercal}
  }
  \newcommand<>{\VectorW}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-2-|col-1)!0.5!(row-2-|col-3) $) |- ++(3mm, -2.5mm)
          node[right, scale=0.75] {$\bv[q]_1=\left[\begin{array}{rr}\sfrac{2}{\sqrt{6}} & \sfrac{(1+i)}{\sqrt{6}}\end{array}\right]^\intercal$};
        \end{tikzpicture}
      }
      ]
      2 & 1+i
    \end{bNiceMatrix}^{\scriptscriptstyle\intercal}
  }
  \newcommand{\Eigen}{
    \begin{aligned}
      \mathcal{E}_H(1) &= \Span\Set{\VectorV<3->} \\
      \mathcal{E}_H(4) &= \Span\Set{\VectorW<7->}
    \end{aligned}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<4->{1}
        \hlCol<8->[grn]{2}
      }
      ]
      \onslide<4->{\sfrac{1}{\sqrt{3}}}      & \onslide<8->{\sfrac{2}{\sqrt{6}}}     \\
      \onslide<4->{\sfrac{(-1-i)}{\sqrt{3}}} & \onslide<8->{\sfrac{(1+i)}{\sqrt{6}}}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<5->{1}{1}
        \hlEntry<9->[grn]{2}{2}
      }
      ]
      {} \onslide<5->{1} &   \\
      {}                 & \onslide<9->{4}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixUT}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<6->{1}
        \hlRow<10->[grn]{2}
      }
      ]
      \onslide<6->{\sfrac{1}{\sqrt{3}}}  & \onslide<6->{\sfrac{(-1+i)}{\sqrt{3}}} \\
      \onslide<10->{\sfrac{2}{\sqrt{6}}} & \onslide<10->{\sfrac{(1-i)}{\sqrt{6}}}
    \end{bNiceMatrix}
  }
  \begin{example}
    This matrix $H$ has $\EVals(H)=\Set{1, 4}$.
    \begin{align*}
      H=\MatrixH<11-> && \Eigen
    \end{align*}
    \onslide<2->{This gives the spectral factorization}
    \[
      \onslide<2->{
        \overset{H}{\MatrixH<0>}
        =
        \overset{U}{\MatrixU}
        \overset{D}{\MatrixD}
        \overset{U^\ast}{\MatrixUT}
      }
    \]
  \end{example}

\end{frame}



\end{document}
