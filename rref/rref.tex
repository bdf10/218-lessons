\documentclass[]{bmr}

\title{Row Echelon Forms}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Reduced Row Echelon Form}
\subsection{Definition}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrr}[
      , margin
      , code-before={
        \hlRow[grn]<4>{4}
        \hlCol<13-15>[grn]{1}
        \hlCol<14-15>[grn]{3}
        \hlCol<15>[grn]{4}
        \hlEntry<6->{1}{1}
        \hlEntry<7->{2}{3}
        \hlEntry<8->{3}{4}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<10-11>[->, red, thick, shorten <=0.5mm]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ($ (row-2-|col-3)!0.5!(row-2-|col-4) $);
          \draw<11>[->, red, thick, shorten <=0.5mm]
          ($ (row-2-|col-4)!0.5!(row-3-|col-4) $) -| ($ (row-3-|col-4)!0.5!(row-3-|col-5) $);
        \end{tikzpicture}
      }
      ]
      1 & -4 & 0 & 0 & 1 \\
      0 &  0 & 1 & 0 & 2 \\
      0 &  0 & 0 & 1 & 7 \\
      0 &  0 & 0 & 0 & 0
    \end{bNiceArray}
  }
  \begin{definition}
    Suppose $R$ is an $m\times n$ matrix.
    \[
      R = \MatrixR
    \]
    \onslide<2->{We say $R$ is in \emph{\grn{reduced row echelon form}} if}
    \begin{enumerate}
    \item<3-> any zero-rows occur at the bottom
    \item<5-> the first \emph{nonzero} entry of the other rows is equal to $1$
      (called a \emph{\grn{pivot}})
    \item<9-> every pivot occurs to the right of the pivots above it
    \item<12-> all nonpivot entries in a column containing a pivot are equal to zero
    \end{enumerate}
    \onslide<16->{Here, we write $R=\rref(R)$.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrrrr}[
      , margin
      , code-before={
        \hlRow[grn]<2-3>{4}
        \hlRow[grn]<3>{5}
        \hlCol<9-11>[grn]{1}
        \hlCol<10-11>[grn]{3}
        \hlCol<11>[grn]{5}
        \hlEntry<4->{1}{1}
        \hlEntry<5->{2}{3}
        \hlEntry<6->{3}{5}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<7-8>[->, red, thick, shorten <=0.5mm]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ($ (row-2-|col-3)!0.5!(row-2-|col-4) $);
          \draw<8>[->, red, thick, shorten <=0.5mm]
          ($ (row-2-|col-4)!0.5!(row-3-|col-4) $) -| ($ (row-3-|col-5)!0.5!(row-3-|col-6) $);
        \end{tikzpicture}
      }
      ]
      1 & 2 & 0 & -5 & 0 &  1 & -1 \\
      0 & 0 & 1 &  3 & 0 & -1 &  1 \\
      0 & 0 & 0 &  0 & 1 & 19 & 17 \\
      0 & 0 & 0 &  0 & 0 &  0 &  0 \\
      0 & 0 & 0 &  0 & 0 &  0 &  0
    \end{bNiceArray}
  }
  \begin{example}
    Consider the matrix $R$ given by
    \[
      R = \MatrixR
    \]
    \onslide<12->{Here $R=\rref(R)$.}
  \end{example}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrr}[
      , margin
      , code-before={
        \hlCol<7-9>[grn]{1}
        \hlCol<8-9>[grn]{2}
        \hlEntry<9->[red]{2}{4}
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<4->{3}{4}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<5-6>[->, red, thick, shorten <=0.5mm]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ($ (row-2-|col-2)!0.5!(row-2-|col-3) $);
          \draw<6>[->, red, thick, shorten <=0.5mm]
          ($ (row-2-|col-3)!0.5!(row-3-|col-3) $) -| ($ (row-3-|col-4)!0.5!(row-3-|col-5) $);
          \draw<11->[red, ultra thick] (row-1-|col-1) -- (row-4-|col-6);
          \draw<11->[red, ultra thick] (row-4-|col-1) -- (row-1-|col-6);
        \end{tikzpicture}
      }
      ]
      1 & 0 & -11 & 0 & 9 \\
      0 & 1 &  14 & 2 & 5 \\
      0 & 0 &   0 & 1 & 6
    \end{bNiceArray}
  }
  \begin{example}
    Consider the matrix $R$ given by
    \[
      R = \MatrixR
    \]
    \onslide<10->{Here $R\neq\rref(R)$.}
  \end{example}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrrrr}[
      , margin
      , code-before={
        \hlRow<2-3>[grn]{5}
        \hlRow<3>[grn]{6}
        \hlCol<8-11>[grn]{1}
        \hlCol<9-11>[grn]{3}
        \hlCol<10-11>[grn]{5}
        \hlCol<11>[grn]{6}
        \hlEntry<4->{1}{1}
        \hlEntry<5->{2}{5}
        \hlEntry<6->{3}{3}
        \hlEntry<7->{4}{6}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<12->[->, red, thick, shorten <=0.5mm]
          ($ (row-2-|col-5)!0.5!(row-3-|col-5) $) -| ($ (row-3-|col-3)!0.5!(row-3-|col-4) $);
          \draw<14->[red, ultra thick] (row-1-|col-1) -- (row-7-|col-8);
          \draw<14->[red, ultra thick] (row-7-|col-1) -- (row-1-|col-8);
        \end{tikzpicture}
      }
      ]
      1 & 4 & 0 &  4 & 0 & 0 &  8 \\
      0 & 0 & 0 &  0 & 1 & 0 & -1 \\
      0 & 0 & 1 & -3 & 0 & 0 &  1 \\
      0 & 0 & 0 &  0 & 0 & 1 & -6 \\
      0 & 0 & 0 &  0 & 0 & 0 &  0 \\
      0 & 0 & 0 &  0 & 0 & 0 &  0
    \end{bNiceArray}
  }
  \begin{example}
    Consider the matrix $R$ given by
    \[
      R = \MatrixR
    \]
    \onslide<13->{Here $R\neq\rref(R)$.}
  \end{example}

\end{frame}


\section{Rank and Nullity}
\subsection{Definitions and Examples}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrr}[
      , margin
      , code-before={
        \hlCol<6->[grn]{1}
        \hlCol<6->[grn]{2}
        \hlCol<6->[grn]{4}
        \hlCol<7->[red]{3}
        \hlCol<7->[red]{5}
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<4->{3}{4}
      }
      , code-after={
        \tikz\draw[thick, grn, <-, shorten <=4pt, visible on=<6->] (1-1.north) |- ++(1.5, 4mm);
        \tikz\draw[thick, grn, <-, shorten <=4pt, visible on=<6->] (1-2.north) |- ++(1.5, 4mm);
        \tikz\draw[thick, grn, <-, shorten <=4pt, visible on=<6->] (1-4.north) |- ++(1.5, 4mm) node[right] {pivot columns};
        \tikz\draw[thick, red, <-, shorten <=5pt, visible on=<7->] (4-3.south) |- ++(1.5, -4mm);
        \tikz\draw[thick, red, <-, shorten <=5pt, visible on=<7->] (4-5.south) |- ++(0.75, -4mm)  node[right] {nonpivot columns};
      }
      ]
      1 & 0 & -3 & 0 & -9 \\
      0 & 1 &  7 & 0 & -4 \\
      0 & 0 &  0 & 1 &  1 \\
      0 & 0 &  0 & 0 &  0
    \end{bNiceArray}
  }
  \begin{definition}
    Suppose $R$ is in reduced row echelon form.
    \[
      R = \MatrixR
    \]
    \onslide<5->{Then $R$ has \emph{\grn{pivot columns}} and \emph{\red{nonpivot
          columns}}.}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrr}[
      , margin
      , code-before={
        \hlCol<5->[grn]{1}
        \hlCol<5->[grn]{2}
        \hlCol<5->[grn]{4}
        \hlCol<7->[red]{3}
        \hlCol<7->[red]{5}
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<4->{3}{4}
      }
      , code-after={
        \tikz\draw[thick, grn, <-, shorten <=4pt, visible on=<5->] (1-1.north) |- ++(1.5, 4mm);
        \tikz\draw[thick, grn, <-, shorten <=4pt, visible on=<5->] (1-2.north) |- ++(1.5, 4mm);
        \tikz\draw[thick, grn, <-, shorten <=4pt, visible on=<5->] (1-4.north) |- ++(1.5, 4mm) node[right] {$\rank(R)=3$};
        \tikz\draw[thick, red, <-, shorten <=5pt, visible on=<7->] (4-3.south) |- ++(1.5, -4mm);
        \tikz\draw[thick, red, <-, shorten <=5pt, visible on=<7->] (4-5.south) |- ++(0.75, -4mm)  node[right] {$\nullity(R)=2$};
      }
      ]
      1 & 0 & -3 & 0 & -9 \\
      0 & 1 &  7 & 0 & -4 \\
      0 & 0 &  0 & 1 &  1 \\
      0 & 0 &  0 & 0 &  0
    \end{bNiceArray}
  }
  \begin{definition}
    The \emph{\grn{rank}} is the number of pivot columns.
    \[
      R = \MatrixR
    \]
    \onslide<6->{The \emph{\red{nullity}} is the number of nonpivot columns.}
  \end{definition}

\end{frame}



% \begin{frame}{\secname}{\subsecname}

%   \newcommand{\MatrixR}{
%   \begin{bNiceMatrix}[
%     , r
%     , margin
%     , code-before={
%     \hlEntry<2->{1}{1}
%     \hlEntry<3->{2}{2}
%     \hlEntry<4->{3}{4}
%     \hlEntry<5->{4}{6}
%     \hlCol<7->[red]{3}
%     \hlCol<8->[red]{5}
%     \hlCol<9->[red]{7}
%     \hlCol<10->[red]{8}
%     \hlCol<11->[red]{9}
%     \hlCol<12->[red]{10}
%   }
%     ]
%     1 & 0 &  8 & 0 &  5 & 0 &  9 &  7 & -5 &  8 \\
%     0 & 1 & -3 & 0 & -4 & 0 & 11 &  6 & -2 &  0 \\
%     0 & 0 &  0 & 1 & 19 & 0 & -1 & -1 & -9 & 11 \\
%     0 & 0 &  0 & 0 &  0 & 1 &  5 &  1 & -2 & 17 \\
%     0 & 0 &  0 & 0 &  0 & 0 &  0 &  0 &  0 &  0 \\
%     0 & 0 &  0 & 0 &  0 & 0 &  0 &  0 &  0 &  0 \\
%     0 & 0 &  0 & 0 &  0 & 0 &  0 &  0 &  0 &  0
%   \end{bNiceMatrix}
% }
%   \newcommand{\RankNullity}{
%   \begin{aligned}
      %       \onslide<6->{\rank(R)}    &\onslide<6->{= 4} \\
      %       \onslide<13->{\nullity(R)} &\onslide<13->{= 6}
%   \end{aligned}
% }
%   \begin{example}
%     Consider the matrix $R=\rref(R)$ given by
%     \begin{align*}
%       R=\MatrixR && \RankNullity
%     \end{align*}
%   \end{example}

% \end{frame}


\subsection{The Rank-Nullity Theorem}
\begin{frame}{\secname}{\subsecname}

  \begin{theorem}[The Rank-Nullity Theorem]
    Suppose $R$ is $m\times n$ and $R=\rref(R)$. Then
    \[
      \rank(R)+\nullity(R)=n
    \]
    That is, the rank plus the nullity is the number of columns.
  \end{theorem}

\end{frame}


\begin{frame}{\secname}{\subsecname}


  \newcommand{\DiagramA}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<3->]
        \node (X) {$\mathbb{R}^5$};
        \node[right=of X] (Y) {$\mathbb{R}^9$};
        \draw[->] (X.east) -- (Y.west) node[midway, above, font=\scriptsize] (R) {$R$};
        \draw[<-, thick, red, overlay, visible on=<4->] (R.north) |- ++(-0.5, 5pt) node[left] {$9\times 5$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\DiagramB}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<6->]
        \node (X) {$\mathbb{R}^{12}$};
        \node[right=of X] (Y) {$\mathbb{R}^7$};
        \draw[->] (X.east) -- (Y.west) node[midway, above, font=\scriptsize] (R) {$R$};
        \draw[<-, thick, red, overlay, visible on=<7->] (R.north) |- ++(-0.5, 5pt) node[left] {$7\times 12$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\DiagramC}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<9->]
        \node (X) {$\mathbb{R}^{\alt<11->{8}{?}}$};
        \node[right=of X] (Y) {$\mathbb{R}^{6}$};
        \draw[->] (X.east) -- (Y.west) node[midway, above, font=\scriptsize] (R) {$R$};
        \draw[<-, thick, red, overlay, visible on=<10->] (R.north) |- ++(-0.5, 5pt) node[left] {$6\times \alt<11->{8}{?}$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    The rank-nullity theorem states that
    \[
      \rank(R)+\nullity(R)=\text{number of columns of }R
    \]
    \onslide<2->{With two of these numbers we can infer the third.}
    \begin{align*}
      \DiagramA && \onslide<3->{\rank(R)} &\onslide<3->{= 2}                      & \onslide<3->{\nullity(R)} &\onslide<3->{=} \onslide<5->{5-2=3} \\
      \DiagramB && \onslide<6->{\rank(R)} &\onslide<6->{=} \onslide<8->{12-9 = 3} & \onslide<6->{\nullity(R)} &\onslide<6->{= 9}                   \\
      \DiagramC && \onslide<9->{\rank(R)} &\onslide<9->{= 5}                      & \onslide<9->{\nullity(R)} &\onslide<9->{= 3}
    \end{align*}

  \end{example}

\end{frame}


\section{Column Relations}
\subsection{Definition}
\begin{frame}{\secname}{\subsecname}

  \begin{definition}
    The nonpivot columns are \emph{\grn{linear combinations}} of the pivot
    columns.
    \[
      \begin{bNiceMatrix}[
        , r
        , margin
        , code-before={
          \hlEntry<2->{1}{1}
          \hlEntry<3->{2}{2}
          \hlEntry<4->{3}{4}
          \hlCol<5->[red]{3}
          \hlCol<6->[red]{5}
        }
        , code-after={
          \begin{tikzpicture}
            \draw<5->[<-, red, thick, shorten <=0.5mm]
            ($ (row-5-|col-3)!0.5!(row-5-|col-4) $) |- ++(-5mm,-2mm)
            node[left, scale=0.75] {\(\Col_3=-3\cdot\Col_1+7\cdot\Col_2\)};
            \draw<6->[<-, red, thick, shorten <=0.5mm]
            ($ (row-5-|col-5)!0.5!(row-5-|col-6) $) |- ++(3mm,-2mm)
            node[right, scale=0.75] {\(\Col_5=-9\cdot\Col_1-4\cdot\Col_2+\Col_4\)};
          \end{tikzpicture}
        }
        ]
        1 & 0 & -3 & 0 & -9 \\
        0 & 1 &  7 & 0 & -4 \\
        0 & 0 &  0 & 1 &  1 \\
        0 & 0 &  0 & 0 &  0
      \end{bNiceMatrix}
    \]
    \onslide<7->{We call these equations \emph{\grn{column relations}}.}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{3}
        \hlEntry<4->{3}{4}
        \hlCol<5->[red]{2}
        \hlCol<6->[red]{5}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<5->[<-, red, thick, shorten <=0.5mm]
          ($ (row-5-|col-2)!0.5!(row-5-|col-3) $) |- ++(-5mm,-2mm)
          node[left, scale=0.75] {\(\Col_2=5\cdot\Col_1\)};
          \draw<6->[<-, red, thick, shorten <=0.5mm]
          ($ (row-5-|col-5)!0.5!(row-5-|col-6) $) |- ++(3mm,-2mm)
          node[right, scale=0.75] {\(\Col_5=3\cdot\Col_1-8\cdot\Col_3+9\cdot\Col_4\)};
        \end{tikzpicture}
      }
      ]
      1     & 5     & 0     & 0     &  3    \\
      0     & 0     & 1     & 0     & -8    \\
      0     & 0     & 0     & 1     &  9    \\
      0     & 0     & 0     & 0     &  0
    \end{bNiceMatrix}
  }
  \begin{example}
    Each nonpivot column contributes a column relation.
    \[
      \MatrixR
    \]
    \onslide<7->{The number of column relations is equal to the nullity.}
  \end{example}

\end{frame}


\subsection{Solving $R\bv[x]=\bv[O]$}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{3}
        \hlEntry<4->{3}{4}
        \hlCol<5->[red]{2}
        \hlCol<7->[red]{5}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<5->[<-, red, thick, shorten <=0.5mm]
          ($ (row-4-|col-2)!0.5!(row-4-|col-3) $) |- ++(-5mm,-2mm)
          node[left, scale=0.75] {\(\Col_2=5\cdot\Col_1\)};
          \draw<7->[<-, red, thick, shorten <=0.5mm]
          ($ (row-4-|col-5)!0.5!(row-4-|col-6) $) |- ++(3mm,-2mm)
          node[right, scale=0.75] {\(\Col_5=3\cdot\Col_1-8\cdot\Col_3+9\cdot\Col_4\)};
        \end{tikzpicture}
      }
      ]
      1     & 5     & 0     & 0     &  3    \\
      0     & 0     & 1     & 0     & -8    \\
      0     & 0     & 0     & 1     &  9    \\
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixRBottom}{
    \begin{bNiceMatrix}[
      , margin
      , columns-width=0.75em
      , code-before = {
        \hlMat####1
      }
      ]
      \Block{3-3}{R} & &\\[1em]
      &&\\[1em]
      &&
    \end{bNiceMatrix}
  }
  \newcommand{\ColumnRelations}{
    \begin{NiceArray}{r}[
      , margin
      , code-before = {
        \hlRow<11->[red]{1}
        \hlRow<14->[red]{2}
      }
      ]
      \onslide<6->{-5\cdot\Col_1+\Col_2 = \bv[O]} \\
      \onslide<8->{-3\cdot\Col_1+8\cdot\Col_3-9\cdot\Col_4+\Col_5 = \bv[O]}
    \end{NiceArray}
  }
  \newcommand<>{\PivotA}{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat####1[red]
      }
      ]
      -5\\ 1\\ 0\\ 0\\ 0
    \end{bNiceMatrix}
  }
  \newcommand<>{\PivotB}{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat####1[red]
      }
      ]
      -3\\ 0\\ 8\\ -9\\ 1
    \end{bNiceMatrix}
  }
  \newcommand<>{\Zero}{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat####1[grn]
      }
      ]
      0\\ 0\\ 0
    \end{bNiceMatrix}
  }
  \begin{block}{Observation}
    Suppose we ``rearrange'' each column relation.
    \begin{align*}
      \MatrixR && \ColumnRelations
    \end{align*}
    \onslide<9->{Organizing the coefficients into vectors gives solutions to
      $R\bv[x]=\bv[O]$.}
    \begin{align*}
      \onslide<9->{\MatrixRBottom<10->\PivotA<11->=\Zero<12->} && \onslide<9->{\MatrixRBottom<13->\PivotB<14->=\Zero<15->}
    \end{align*}
  \end{block}

\end{frame}


\section{Row Echelon Form}
\subsection{Definition and Examples}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrr}[
      , margin
      , code-before={
        \hlRow[grn]<4>{4}
        \hlColPart<13-15>[grn]{2}{1}
        \hlColPart<14-15>[grn]{3}{3}
        \hlEntry<15>[grn]{4}{4}
        \hlEntry<6->{1}{1}
        \hlEntry<7->{2}{3}
        \hlEntry<8->{3}{4}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<10-11>[->, red, thick, shorten <=0.5mm]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ($ (row-2-|col-3)!0.5!(row-2-|col-4) $);
          \draw<11>[->, red, thick, shorten <=0.5mm]
          ($ (row-2-|col-4)!0.5!(row-3-|col-4) $) -| ($ (row-3-|col-4)!0.5!(row-3-|col-5) $);
        \end{tikzpicture}
      }
      ]
      9 & -4 & 8 & -2 &  1 \\
      0 &  0 & 3 &  4 & 12 \\
      0 &  0 & 0 &  6 &  7 \\
      0 &  0 & 0 &  0 &  0
    \end{bNiceArray}
  }
  \begin{definition}
    Suppose $R$ is an $m\times n$ matrix.
    \[
      R = \MatrixR
    \]
    \onslide<2->{We say $R$ is in \emph{\grn{row echelon form}} if}
    \begin{enumerate}
    \item<3-> any zero-rows occur at the bottom
    \item<5-> the first \emph{nonzero} entry of the other rows is called a
      \emph{\grn{pivot}}
    \item<9-> every pivot occurs to the right of the pivots above it
    \item<12-> all entries \emph{below} a pivot are equal to zero
    \end{enumerate}
    \onslide<16->{If $R\neq\rref(R)$, we call this \emph{\grn{nonreduced row
          echelon form}}.}
  \end{definition}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrrrr}[
      , margin
      , code-before={
        \hlRow[grn]<2-3>{4}
        \hlRow[grn]<3>{5}
        \hlColPart<9-11>[grn]{2}{1}
        \hlColPart<10-11>[grn]{3}{3}
        \hlColPart<11>[grn]{4}{5}
        \hlEntry<4->{1}{1}
        \hlEntry<5->{2}{3}
        \hlEntry<6->{3}{5}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<7-8>[->, red, thick, shorten <=0.5mm]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ($ (row-2-|col-3)!0.5!(row-2-|col-4) $);
          \draw<8>[->, red, thick, shorten <=0.5mm]
          ($ (row-2-|col-4)!0.5!(row-3-|col-4) $) -| ($ (row-3-|col-5)!0.5!(row-3-|col-6) $);
        \end{tikzpicture}
      }
      ]
      5 & 8 &  7 & -5 & -1 &  4 & -3 \\
      0 & 0 & -2 &  3 &  7 & -1 &  2 \\
      0 & 0 &  0 &  0 & 14 & 19 & 17 \\
      0 & 0 &  0 &  0 &  0 &  0 &  0 \\
      0 & 0 &  0 &  0 &  0 &  0 &  0
    \end{bNiceArray}
  }
  \begin{example}
    Consider the matrix $R$ given by
    \[
      R = \MatrixR
    \]
    \onslide<12->{Here $R$ is in row echelon form.}
  \end{example}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrr}[
      , margin
      , code-before={
        \hlColPart<7-9>[grn]{2}{1}
        \hlColPart<8-9>[grn]{3}{2}
        \hlEntry<9>[grn]{4}{4}
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<4->{3}{4}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<5-6>[->, red, thick, shorten <=0.5mm]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ($ (row-2-|col-2)!0.5!(row-2-|col-3) $);
          \draw<6>[->, red, thick, shorten <=0.5mm]
          ($ (row-2-|col-3)!0.5!(row-3-|col-3) $) -| ($ (row-3-|col-4)!0.5!(row-3-|col-5) $);
        \end{tikzpicture}
      }
      ]
      2 & 7 & -11 &  6 & 9 \\
      0 & 3 &  14 &  2 & 5 \\
      0 & 0 &   0 & -4 & 6 \\
      0 & 0 &   0 &  0 & 0
    \end{bNiceArray}
  }
  \begin{example}
    Consider the matrix $R$ given by
    \[
      R = \MatrixR
    \]
    \onslide<10->{Here $R$ is in row echelon form.}
  \end{example}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrrrr}[
      , margin
      , code-before={
        \hlRow<2-3>[grn]{5}
        \hlRow<3>[grn]{6}
        \hlColPart<8-11>[grn]{2}{1}
        \hlColPart<9-11>[grn]{4}{3}
        \hlColPart<10-11>[grn]{3}{5}
        \hlColPart<11>[grn]{5}{6}
        \hlEntry<4->{1}{1}
        \hlEntry<5->{2}{5}
        \hlEntry<6->{3}{3}
        \hlEntry<7->{4}{6}
      }
      , code-after={
        \begin{tikzpicture}
          \draw<12->[->, red, thick, shorten <=0.5mm]
          ($ (row-2-|col-5)!0.5!(row-3-|col-5) $) -| ($ (row-3-|col-4)!0.5!(row-3-|col-3) $);
          \draw<14->[red, ultra thick] (row-1-|col-1) -- (row-7-|col-8);
          \draw<14->[red, ultra thick] (row-7-|col-1) -- (row-1-|col-8);
        \end{tikzpicture}
      }
      ]
      5 & 4 & 9 &  4 & 7 & -3 & 13 \\
      0 & 0 & 0 &  0 & 5 &  2 & -1 \\
      0 & 0 & 8 & -3 & 0 &  7 &  1 \\
      0 & 0 & 0 &  0 & 0 & 11 & -6 \\
      0 & 0 & 0 &  0 & 0 &  0 &  0 \\
      0 & 0 & 0 &  0 & 0 &  0 &  0
    \end{bNiceArray}
  }
  \begin{example}
    Consider the matrix $R$ given by
    \[
      R = \MatrixR
    \]
    \onslide<13->{Here $R$ is \emph{not} in row echelon form.}
  \end{example}

\end{frame}


\subsection{Rank and Nullity}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrr}[
      , margin
      , code-before={
        \hlCol<5->[grn]{1}
        \hlCol<5->[grn]{2}
        \hlCol<5->[grn]{4}
        \hlCol<6->[red]{3}
        \hlCol<6->[red]{5}
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<4->{3}{4}
      }
      , code-after={
        \tikz\draw[thick, grn, <-, shorten <=4pt, visible on=<5->] (1-1.north) |- ++(1.5, 4mm);
        \tikz\draw[thick, grn, <-, shorten <=4pt, visible on=<5->] (1-2.north) |- ++(1.5, 4mm);
        \tikz\draw[thick, grn, <-, shorten <=4pt, visible on=<5->] (1-4.north) |- ++(1.5, 4mm) node[right] {$\rank(R)=3$};
        \tikz\draw[thick, red, <-, shorten <=5pt, visible on=<6->] (4-3.south) |- ++(1.5, -4mm);
        \tikz\draw[thick, red, <-, shorten <=5pt, visible on=<6->] (4-5.south) |- ++(0.75, -4mm)  node[right] {$\nullity(R)=2$};
      }
      ]
      7 & 4 & -3 & 8 & -9 \\
      0 & 3 &  7 & 4 & -4 \\
      0 & 0 &  0 & 6 &  1 \\
      0 & 0 &  0 & 0 &  0
    \end{bNiceArray}
  }
  \begin{definition}
    The concepts of \emph{\grn{rank}} and \emph{\grn{nullity}} carry-over.
    \[
      R = \MatrixR
    \]
    \onslide<7->{Unfortunately, the column relations are much more difficult to
      decypher!}
  \end{definition}

\end{frame}



\end{document}