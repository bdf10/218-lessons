\documentclass[]{bmr}

\usepackage{tikz-cd}

\title{Matrix-Vector Products}

% \includeonlyframes{current}

\begin{document}

\maketitle

\begin{frame}
  \tableofcontents
\end{frame}


\section{Linear Combinations}
\subsection{Definition}

\begin{frame}{\secname}{\subsecname}

  \begin{definition}
    A \egrn{linear combination} of $\bv_1,\bv_2,\dotsc,\bv_m\in\mathbb{R}^n$ is
    any expression of the form
    \[
      \begin{tikzpicture}[remember picture]
        \node {
          \(
          \displaystyle
          \subnode{c1}{\textcolor<2->{red}{c_1}} \cdot\subnode{v1}{\bv_1}
          +\subnode{c2}{\textcolor<2->{red}{c_2}}\cdot\subnode{v2}{\bv_2}
          +\dotsb
          +\subnode{cm}{\textcolor<2->{red}{c_m}}\cdot\subnode{vm}{\bv_m}
          \)
        };

        \draw[<-, thick, red, overlay, visible on=<2->]
        (c1.south) |- ++(-3mm, -2.5mm)
        node[left] {\small scalars (called \emph{weights})};

        \draw[<-, thick, red, overlay, visible on=<2->]
        (c2.south) |- ++(-15mm, -2.5mm);

        \draw[<-, thick, red, overlay, visible on=<2->]
        (cm.south) |- ++(-25mm, -2.5mm);
      \end{tikzpicture}
    \]
    \onslide<3->{For example, consider the linear combination}
    \newcommand<>{\MyScalar}[2]{
      \begin{tikzpicture}[baseline]
        \node[anchor=base] (c) {$####1$};
        \node[anchor=base, fill=redbg, draw=black, rounded corners, ultra thick, overlay, visible on=####3] (c) {$####1$};
        \draw[<-, red, thick, shorten <=0.5mm, overlay, visible on=####3]
        (c.south) |- ++(-2mm, -3mm) node[left] {\(####2\)};
      \end{tikzpicture}
    }
    \newcommand<>{\MyVector}[2]{
      \begin{bNiceArray}{r}[
        , code-before = {
          \hlMat####3
        }
        , code-after = {
          \begin{tikzpicture}
            \draw[<-, blu, thick, shorten <=0.5mm, visible on=####3]
            ($ (row-4-|col-1)!0.5!(row-4-|col-2) $) |- ++(-2mm, -3mm)
            node[left] {\(####2\)};
          \end{tikzpicture}
        }
        ]
        ####1
      \end{bNiceArray}
    }
    \[
      \onslide<3->{
        \MyScalar<5->{-3}{c_1}\cdot\MyVector<4->{2\\-1\\4}{\bv_1}
        +\MyScalar<5->{6}{c_2}\cdot\MyVector<4->{3\\0\\1}{\bv_2}
        +\MyScalar<5->{4}{c_3}\cdot\MyVector<4->{0\\1\\-1}{\bv_3}
        +\MyScalar<5->{2}{c_4}\cdot\MyVector<4->{6\\0\\-3}{\bv_4}
        \onslide<6->{= \MyVector<7->{24\\7\\-16}{\bv[b]}}
      }
    \]
    \onslide<8->{Here, $\bv[b]$ is a linear combination of
      $\bv_1, \bv_2, \bv_3, \bv_4\in\mathbb{R}^3$.}
  \end{definition}

\end{frame}

\subsection{Examples}
\begin{frame}{\secname}{\subsecname}

  \newcommand<>{\LinComb}{
    \begin{bNiceMatrix}[
      , code-before = {
        \hlRow####1[red]{2}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-2-|col-2)!0.5!(row-3-|col-2) $) -| ++(18mm, 1.5mm)
          node[above] {\emph{always zero!}};
        \end{tikzpicture}
      }
      ]
      2\,c_1-c_2\\ 0\\ 3\,c_1+5\,c_2
    \end{bNiceMatrix}
  }
  \begin{example}
    Generic combinations of $\bv_1=\nv{2 0 3}$ and $\bv_2=\nv{{-1} 0 5}$ look
    like
    \[
      c_1\cdot\nvc{2;0;3}
      +c_2\cdot\nvc{-1;0;5}
      =\LinComb<2->
    \]
    \onslide<3->{For example, $\nv{2 1 3}$ cannot be expressed as such a
      combination.}
  \end{example}

\end{frame}


\section{Matrix-Vector Products}
\subsection{Definition}

\begin{frame}{\secname}{\subsecname}

  \begin{alertblock}{Problem}
    Linear combinations are extremely important, but clumsy to work with.
    \[
      \begin{tikzpicture}[visible on=<1->]
        \node[fill=redbg, draw, rounded corners, ultra thick]
        (text) {$c_1\cdot\bv_1+c_2\cdot\bv_2+\dotsb+c_m\cdot\bv_m$};
        \node[above, red] at (text.north) {\scriptsize\textnormal{Important But Clumsy}};
      \end{tikzpicture}
    \]
    \onslide<2->{\egrn{Matrix-vector products} encode this data into a single
      expression $A\bv$.}
  \end{alertblock}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\AvA}{
    \begin{tikzpicture}[baseline, remember picture]
      \node[anchor=base, inner sep=0] {\(\subnode{matA}{A}\in\mathbb{R}^{\subnode{numrows}{\textcolor<12->{blu}{m}}\times \subnode{ncols}{\textcolor<2-3>{blu}{n}}}\)};

      \draw[thick, <-, shorten <=1.5mm, blu, overlay, visible on=<2-3>]
      (ncols.north) |- ++(1.5mm, 8mm) node[right] {\small num cols of $A$ is $n$};

      \draw[thick, <-, shorten <=1.5mm, blu, overlay, visible on=<12->]
      (numrows.north) |- ++(3mm, 4mm) node[right] {\small num rows of $A$ is $m$};
    \end{tikzpicture}
  }
  \newcommand{\Avv}{
    \begin{tikzpicture}[baseline, remember picture]
      \node[anchor=base, inner sep=0] {\(\subnode{vecv}{\bv}\in\mathbb{R}^{\subnode{ncoords}{\textcolor<3>{red}{n}}}\)};

      \draw[thick, <-, shorten <=1.5mm, red, overlay, visible on=<3>]
      (ncoords.north) |- ++(3mm, 4mm) node[right] {\small num coords in $\bv$ is $n$};
    \end{tikzpicture}
  }
  \newcommand{\AvAv}{
    \begin{tikzpicture}[baseline]
      \node[anchor=base, inner sep=0] (AvAv) {\(\textcolor<12->{blu}{A\bv}\)};

      \draw[thick, <-, shorten <=1.5mm, blu, overlay, visible on=<12->]
      (AvAv.south) |- ++(3mm, -4mm) node[right] {\small num coords of $A\bv$ is $m$ ($A\bv\in\mathbb{R}^m$)};
    \end{tikzpicture}
  }
  \newcommand{\MatA}{
    \begin{bNiceArray}{rrrr}[
      , margin
      , code-before = {
        \hlMat<1>
        \hlCol<2-3,5->{1}
        \hlCol<2-3,6->{2}
        \hlCol<2-3,7->{4}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<1>]
          ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2-3>]
          ($ (row-5-|col-2)!0.5!(row-5-|col-1) $) -- ++(0, -4mm);
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2-3>]
          ($ (row-5-|col-3)!0.5!(row-5-|col-2) $) -- ++(0, -4mm);
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2-3>]
          ($ (row-5-|col-5)!0.5!(row-5-|col-4) $) |- ++(-5mm, -6mm)
          node[left, fill=white] {\small num cols  of $A$ is $n$};
        \end{tikzpicture}
      }
      ]
      a_{11} & a_{12} & \Cdots & a_{1n} \\
      a_{21} & a_{22} & \Cdots & a_{2n} \\
      \Vdots & \Vdots & \Ddots & \Vdots \\
      a_{m1} & a_{m2} & \Cdots & a_{mn}
    \end{bNiceArray}
  }
  \newcommand{\Vecv}{
    \begin{bNiceArray}{r}[
      , code-before = {
        \hlMat[red]<1>
        \hlRow<3,8->[red]{1}
        \hlRow<3,9->[red]{2}
        \hlRow<3,10->[red]{4}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<1>]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv$};
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<3>]
          ($ (row-2-|col-2)!0.5!(row-3-|col-2) $) -| ++(3mm, -1mm);
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<3>]
          ($ (row-4-|col-2)!0.5!(row-5-|col-2) $) -| ++(3mm, -1mm);
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<3>]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ++(3mm, -20mm)
          node[below] {\small num coords in $\bv$ is $n$};
        \end{tikzpicture}
      }
      ]
      c_1\\ c_2\\ \Vdots\\ c_n
    \end{bNiceArray}
  }
  \newcommand<>{\MyScalar}[1]{
    \begin{tikzpicture}[baseline]
      \node[anchor=base] (c) {$c_####1$};
      \node[anchor=base, fill=redbg, draw=black, rounded corners, ultra thick, overlay, visible on=####2] (c) {$c_####1$};
    \end{tikzpicture}
  }
  \newcommand<>{\MyVector}[1]{
    \begin{bNiceArray}{r}[
      , code-before = {
        \hlMat####2
      }
      ]
      a_{1####1}\\a_{2####1}\\ \Vdots\\a_{m####1}
    \end{bNiceArray}
  }
  \begin{definition}
    The \egrn{matrix-vector product $A\bv$} of $\AvA$ and $\Avv$ is
    \[
      \MatA\Vecv
      \onslide<4->{=\MyScalar<8->{1}\cdot\MyVector<5->{1}+\MyScalar<9->{2}\cdot\MyVector<6->{2}+\dotsb+\MyScalar<10->{n}\cdot\MyVector<7->{n}}
    \]
    \onslide<11->{The vector $\AvAv$ is the combination of columns of $A$ using
      the weights in $\bv$.}
  \end{definition}

\end{frame}

\subsection{Examples}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatA}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat<1>
        \hlCol<2-3,5->{1}
        \hlCol<2-3,6->{2}
        \hlCol<2-3,7->{3}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<1>]
          ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2-3>]
          ($ (row-5-|col-2)!0.5!(row-5-|col-1) $) -- ++(0, -4mm);
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2-3>]
          ($ (row-5-|col-3)!0.5!(row-5-|col-2) $) -- ++(0, -4mm);
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<2-3>]
          ($ (row-5-|col-4)!0.5!(row-5-|col-3) $) |- ++(-3mm, -6mm)
          node[left, fill=white] {\small num cols  of $A$ is $3$};
        \end{tikzpicture}
      }
      ]
      1 & 0 & 2\\
      0 & 3 & 4\\
      2 & 1 & 0\\
      4 & 5 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\Vecv}{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat<1>[red]
        \hlRow<3,8->[red]{1}
        \hlRow<3,9->[red]{2}
        \hlRow<3,10->[red]{3}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<1>]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv$};
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<3>]
          ($ (row-2-|col-2)!0.5!(row-3-|col-2) $) -| ++(3mm, -1mm);
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<3>]
          ($ (row-3-|col-2)!0.5!(row-4-|col-2) $) -| ++(3mm, -1mm);
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<3>]
          ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ++(3mm, -15mm)
          node[below] {\small num coords in $\bv$ is $3$};
        \end{tikzpicture}
      }
      ]
      -2\\3\\5
    \end{bNiceMatrix}
  }
  \newcommand<>{\MyScalar}[1]{
    \begin{tikzpicture}[baseline]
      \node[anchor=base] (c) {$####1$};
      \node[anchor=base, fill=redbg, draw=black, rounded corners, ultra thick, overlay, visible on=####2] (c) {$####1$};
    \end{tikzpicture}
  }
  \newcommand<>{\MyVector}[1]{
    \begin{bNiceArray}{r}[
      , code-before = {
        \hlMat####2
      }
      ]
      ####1
    \end{bNiceArray}
  }
  \newcommand<>{\MyAv}[1]{
    \begin{bNiceArray}{r}[
      , code-before = {
        \hlMat####2
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=####2]
          ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(2mm, 2.5mm)
          node[right] {$A\bv\in\mathbb{R}^4$};
        \end{tikzpicture}
      }
      ]
      8\\29\\-1\\12
    \end{bNiceArray}
  }
  \begin{example}
    This $4\times 3$ matrix $A$ can be multiplied by any vector
    $\bv\in\mathbb{R}^3$.
    \[
      \MatA\Vecv
      \onslide<4->{
        =
        \MyScalar<8->{-2}\cdot\MyVector<5->{1\\0\\2\\4}
        +\MyScalar<9->{3}\cdot\MyVector<6->{0\\3\\1\\5}
        +\MyScalar<10->{5}\cdot\MyVector<7->{2\\4\\0\\1}
      }
      \onslide<11->{
        =
        \MyAv<13->
      }
    \]
    \onslide<12->{Since $A$ has four rows, this product produces a vector
      $A\bv\in\mathbb{R}^4$.}
  \end{example}

\end{frame}

\begin{frame}{\secname}{\subsecname}

  \newcommand<>{\MyScalar}[1]{
    \begin{tikzpicture}[baseline]
      \node[anchor=base] (c) {$####1$};
      \node[anchor=base, fill=redbg, draw=black, rounded corners, ultra thick, overlay, visible on=####2] (c) {$####1$};
    \end{tikzpicture}
  }
  \newcommand<>{\MyVector}[1]{
    \begin{bNiceArray}{r}[
      , code-before = {
        \hlMat####2
      }
      ]
      ####1
    \end{bNiceArray}
  }
  \newcommand{\MatA}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat<{2,20}>
        \hlCol<3-19>{1}
        \hlCol<5-19>{2}
        \hlCol<7-19>{3}
        \hlCol<9-19>{4}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<{2,20}>]
          ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
        \end{tikzpicture}
      }
      ]
      \onslide<4->{4} & \onslide<6->{1} & \onslide<8->{0} & \onslide<10->{1} \\
      \onslide<4->{2} & \onslide<6->{6} & \onslide<8->{0} & \onslide<10->{0} \\
      \onslide<4->{3} & \onslide<6->{4} & \onslide<8->{1} & \onslide<10->{1} \\
      \onslide<4->{0} & \onslide<6->{2} & \onslide<8->{3} & \onslide<10->{7} \\
      \onslide<4->{9} & \onslide<6->{0} & \onslide<8->{1} & \onslide<10->{5}
    \end{bNiceMatrix}
  }
  \newcommand{\Vecv}{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat<{2,20}>[red]
        \hlRow<11-19>[red]{1}
        \hlRow<13-19>[red]{2}
        \hlRow<15-19>[red]{3}
        \hlRow<17-19>[red]{4}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<{2,20}>]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv$};
        \end{tikzpicture}
      }
      ]
      \onslide<12->{-5}\\ \onslide<14->{3}\\ \onslide<16->{8}\\ \onslide<18->{5}
    \end{bNiceMatrix}
  }
  \begin{example}
    We will often convert linear combinations into matrix-vector products.
    \[
      \MyScalar<11->{-5}\cdot\MyVector<3->{4\\2\\3\\0\\9}
      +\MyScalar<13->{3}\cdot\MyVector<5->{1\\6\\4\\2\\0}
      +\MyScalar<15->{8}\cdot\MyVector<7->{0\\0\\1\\3\\1}
      +\MyScalar<17->{5}\cdot\MyVector<9->{1\\0\\1\\7\\5}
      \onslide<2->{
        =
        \MatA
        \Vecv
      }
    \]
    \onslide<19->{We have converted the ugly
      $c_1\cdot\bv_1+\dotsb+c_4\cdot\bv_4$ to the beautiful $A\bv$.}
  \end{example}

\end{frame}


\begin{frame}{\secname}{\subsecname}

  \newcommand{\MatA}{
    \begin{bNiceMatrix}[
      , margin
      , c
      , code-before = {
        \hlMat<1>
        \hlCol<4->{1}
        \hlCol<5->{2}
        \hlCol<6->{3}
        \hlCol<7->{4}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[black, line cap=round, rounded corners, thick, shorten <=2mm, shorten >=2mm, visible on=<3->]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) --
          ($ (row-3-|col-1)!0.5!(row-3-|col-2) $);
          \draw[black, line cap=round, rounded corners, thick, shorten <=2mm, shorten >=2mm, visible on=<3->]
          ($ (row-4-|col-1)!0.5!(row-4-|col-2) $) --
          ($ (row-6-|col-1)!0.5!(row-6-|col-2) $);
          \draw[black, line cap=round, rounded corners, thick, shorten <=2mm, shorten >=2mm, visible on=<3->]
          ($ (row-1-|col-2)!0.5!(row-1-|col-3) $) --
          ($ (row-3-|col-2)!0.5!(row-3-|col-3) $);
          \draw[black, line cap=round, rounded corners, thick, shorten <=2mm, shorten >=2mm, visible on=<3->]
          ($ (row-4-|col-2)!0.5!(row-4-|col-3) $) --
          ($ (row-6-|col-2)!0.5!(row-6-|col-3) $);
          \draw[black, line cap=round, rounded corners, thick, shorten <=2mm, shorten >=2mm, visible on=<3->]
          ($ (row-1-|col-3)!0.5!(row-1-|col-4) $) --
          ($ (row-3-|col-3)!0.5!(row-3-|col-4) $);
          \draw[black, line cap=round, rounded corners, thick, shorten <=2mm, shorten >=2mm, visible on=<3->]
          ($ (row-4-|col-3)!0.5!(row-4-|col-4) $) --
          ($ (row-6-|col-3)!0.5!(row-6-|col-4) $);
          \draw[black, line cap=round, rounded corners, thick, shorten <=2mm, shorten >=2mm, visible on=<3->]
          ($ (row-1-|col-4)!0.5!(row-1-|col-5) $) --
          ($ (row-3-|col-4)!0.5!(row-3-|col-5) $);
          \draw[black, line cap=round, rounded corners, thick, shorten <=2mm, shorten >=2mm, visible on=<3->]
          ($ (row-4-|col-4)!0.5!(row-4-|col-5) $) --
          ($ (row-6-|col-4)!0.5!(row-6-|col-5) $);
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<4-8>]
          ($ (row-6-|col-1)!0.5!(row-6-|col-2) $) |- ++(-2mm, -3mm)
          node[left, scale=0.75] {$c_1=1$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<5-8>]
          ($ (row-6-|col-2)!0.5!(row-6-|col-3) $) |- ++(-2mm, -3mm)
          node[left, scale=0.75] {$c_2=0$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<6-8>]
          ($ (row-6-|col-3)!0.5!(row-6-|col-4) $) |- ++(-2mm, -3mm)
          node[left, scale=0.75] {$c_3=0$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<7-8>]
          ($ (row-6-|col-4)!0.5!(row-6-|col-5) $) |- ++(-2mm, -5mm)
          node[left, scale=0.75] {$c_4=-1$};
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=<1>]
          ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
        \end{tikzpicture}
      }
      ]
      &   &   &   \\
      &   &   &   \\
      \onslide<3->{\Col_1} & \onslide<3->{\Col_2} & \onslide<3->{\Col_3} & \onslide<3->{\Col_4} \\
      &   &   &   \\
      &   &   &   \\
    \end{bNiceMatrix}
  }
  \newcommand{\Vecv}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat<1>[red]
        \hlRow<2->[red]{1}
        \hlRow<2-3,5->[red]{2}
        \hlRow<2-3,6->[red]{3}
        \hlRow<2-3,7->[red]{4}
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, red, thick, shorten <=0.5mm, visible on=<1>]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$\bv$};
        \end{tikzpicture}
      }
      ]
      1\\ 0\\ 0\\ -1
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following matrix-vector product.
    \[
      \MatA\Vecv \onslide<8->{= \Col_1-\Col_4}
    \]
    \onslide<9->{Here, we simply subtract the last column from the first
      column!}
  \end{example}

\end{frame}



\section{Properties}
\subsection{Compatibility}
\begin{frame}{\secname}{\subsecname}


  \newcommand{\Aava}{
    \begin{tikzpicture}
      \node[visible on=<2->, cross out, draw=red, ultra thick] {$\bAutoNiceMatrix{4-2}{\ast}\bAutoNiceMatrix{4-1}{\ast}$};
      \node {$\bAutoNiceMatrix{4-2}{\ast}\bAutoNiceMatrix{4-1}{\ast}$};
    \end{tikzpicture}
  }
  \newcommand{\Abvb}{
    \begin{tikzpicture}
      \node[visible on=<2->, cross out, draw=red, ultra thick] {$\bAutoNiceMatrix{3-3}{\ast}\bAutoNiceMatrix{2-1}{\ast}$};
      \node {$\bAutoNiceMatrix{3-3}{\ast}\bAutoNiceMatrix{2-1}{\ast}$};
    \end{tikzpicture}
  }
  \begin{alertblock}{Warning}
    The product $A\bv$ only makes sense if $A\in\mathbb{R}^{m\times n}$ and
    $\bv\in\mathbb{R}^n$.
    \begin{align*}
      \Aava && \Abvb
    \end{align*}
    \onslide<3->{Be careful when you write $A\bv$!}
  \end{alertblock}

\end{frame}

\subsection{Algebraic}
\begin{frame}{\secname}{\subsecname}

  \newcommand{\LinearityRule}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$A(c_1\cdot\bv_1+c_2\cdot\bv_2)=c_1\cdot A\bv_1+c_2\cdot A\bv_2$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Linearity Rule}};
    \end{tikzpicture}
  }
  \newcommand{\IdentityRule}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=blubg, draw, rounded corners, ultra thick]
      (text) {$I_n\bv=\bv$};
      \node[above, blu] at (text.north) {\scriptsize\textnormal{Identity Rule}};
    \end{tikzpicture}
  }
  \newcommand{\ZeroRule}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=redbg, draw, rounded corners, ultra thick]
      (text) {$A\bv[O]_n=\bv[O]_m$};
      \node[above, red] at (text.north) {\scriptsize\textnormal{Zero Rule}};
    \end{tikzpicture}
  }
  \newcommand{\InOut}{
    \begin{tikzpicture}[baseline, remember picture, inner sep=0]
      \node[anchor=base] {\(\subnode{InOutA}{\textcolor<3-5>{blu}{A}}\subnode{InOutv}{\textcolor<4-5>{red}{\bv}}=\subnode{InOutb}{\textcolor<5>{grn}{\bv[b]}}\)};

      \draw[thick, <-, shorten <=1.5mm, blu, overlay, visible on=<3-5>]
      (InOutA.south) |- ++(-3mm, -4mm) node[left, scale=0.75] {$A\in\mathbb{R}^{m\times n}$};

      \draw[thick, <-, shorten <=1.5mm, red, overlay, visible on=<4-5>]
      (InOutv.south) |- ++(-3mm, -8mm) node[left, scale=0.75] {``input'' $\bv\in\mathbb{R}^{n}$};

      \draw[thick, <-, shorten <=1.5mm, grn, overlay, visible on=<5>]
      (InOutb.south) |- ++(3mm, -4mm) node[right, scale=0.75] {``output'' $\bv[b]\in\mathbb{R}^{m}$};
    \end{tikzpicture}
  }
  \newcommand{\ArrowNotation}{
    \begin{tikzpicture}[visible on=<6->]
      \node[fill=grnbg, draw, rounded corners, ultra thick]
      (text) {$\begin{tikzcd}[ampersand replacement=\&]\mathbb{R}^n\ar[r, "A"] \& \mathbb{R}^m\end{tikzcd}$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Arrow Notation}};
    \end{tikzpicture}
  }
  \begin{theorem}
    Matrix-vector multiplictation respects the \grn{linearity}, \blu{identity},
    and \red{zero} rules.
    \begin{align*}
      \LinearityRule && \IdentityRule && \ZeroRule
    \end{align*}
    \onslide<2->{Thinking in terms of inputs and outputs \InOut}
    \onslide<6->{inspires \grn{arrow notation}}
    \[
      \ArrowNotation
    \]
    \onslide<7->{Writing $\mathbb{R}^n\xrightarrow{A}\mathbb{R}^m$ is a
      sophisticated way of declaring that $A$ is $m\times n$.}
  \end{theorem}

\end{frame}




\section{Eigenvectors}
\subsection{Definition}
\begin{frame}{\secname}{\subsecname}

  \newcommand<>{\MatA}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
        \end{tikzpicture}
      }
      ]
      {}  3 &  3 &  3 \\
      {} -8 & -2 & -4 \\
      {}  2 & -4 & -2
    \end{bNiceMatrix}
  }
  \newcommand<>{\Vec}[3]{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat####4[####1]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=####4]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$####2$};
        \end{tikzpicture}
      }
      ]
      ####3
    \end{bNiceMatrix}
  }
  \newcommand<>{\VecAw}[3]{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat####4[####1]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=####4]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$####2$};
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=<8->]
          ($ (row-4-|col-1)!0.5!(row-4-|col-2) $) |- ++(-2mm, -2.5mm)
          node[left, scale=0.75] {$A\bv[w]\neq\lambda\cdot\bv[w]$};
        \end{tikzpicture}
      }
      ]
      ####3
    \end{bNiceMatrix}
  }
  \newcommand<>{\VecAv}[3]{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat####4[####1]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=####4]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$####2$};
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=<6->]
          ($ (row-4-|col-1)!0.5!(row-4-|col-2) $) |- ++(-2mm, -2.5mm)
          node[left, scale=0.75] {$A\bv=2\cdot\bv$};
        \end{tikzpicture}
      }
      ]
      ####3
    \end{bNiceMatrix}
  }
  \begin{example}
    If $A$ is square, then both $\bv$ and $A\bv$ are vectors in $\mathbb{R}^n$.
    \begin{align*}
      \MatA<1->\Vec<2->{red}{\bv[w]}{1\\-1\\0} &= \VecAw<3->{red}{A\bv[w]}{0\\-6\\6} & \MatA<1->\Vec<4->{grn}{\bv[v]}{0\\1\\-1} &= \VecAv<5->{grn}{A\bv[v]}{0\\2\\-2}
    \end{align*}
    \onslide<7->{We call $\bv$ an \egrn{eigenvector} of $A$ with
      \egrn{associated eigenvalue $\lambda=2$}.}
  \end{example}

\end{frame}

\begin{frame}{\secname}{\subsecname}

  \newcommand{\EigenvectorEquation}{
    \begin{tikzpicture}[visible on=<1->]
      \node[fill=grnbg, draw, rounded corners, ultra thick, inner sep=5mm]
      (text) {$A\bv=\lambda\cdot\bv$};
      \node[above, grn] at (text.north) {\scriptsize\textnormal{Eigenvector Equation}};
    \end{tikzpicture}
  }
  \newcommand<>{\EigenSpace}{
    \begin{tikzpicture}[baseline]
      \node[anchor=base, inner sep=0] (EALambda)
      {\(\textcolor####1{grn}{\bv\in\mathcal{E}_A(\lambda)}\)};

      \draw[thick, <-, shorten <=1.5mm, grn, overlay, visible on=####1]
      (EALambda.south) |- ++(-3mm, -4mm) node[left, scale=0.75]
      {writing $\bv\in\mathcal{E}_A(\lambda)$ declares that $A\bv=\lambda\cdot\bv$};
    \end{tikzpicture}
  }
  \begin{definition}
    We call $\bv\neq\bv[O]$ an \egrn{eigenvector} of $A$ with \egrn{associated
      eigenvalue $\lambda$} if
    \[
      \EigenvectorEquation
    \]
    \onslide<2->{We communicate that this equation is satisfied by writing
      $\EigenSpace<3->$.}
  \end{definition}

\end{frame}



\subsection{Example}
\begin{frame}{\secname}{\subsecname}

  \newcommand<>{\MatA}{
    \begin{bNiceMatrix}[
      , margin
      , r
      , code-before = {
        \hlMat####1
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, blu, thick, shorten <=0.5mm, visible on=####1]
          ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-2mm, 2.5mm)
          node[left] {$A$};
        \end{tikzpicture}
      }
      ]
      {}  3 &  3 &  3 \\
      {} -8 & -2 & -4 \\
      {}  2 & -4 & -2
    \end{bNiceMatrix}
  }
  \newcommand<>{\Vec}[3]{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat####4[####1]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=####4]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$####2$};
        \end{tikzpicture}
      }
      ]
      ####3
    \end{bNiceMatrix}
  }
  \newcommand<>{\VecAw}[3]{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat####4[####1]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=####4]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$####2$};
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=<10->]
          ($ (row-4-|col-1)!0.5!(row-4-|col-2) $) |- ++(-2mm, -2.5mm)
          node[left, scale=0.75] {$A\bv[w]\neq\lambda\cdot\bv[w]$};
        \end{tikzpicture}
      }
      ]
      ####3
    \end{bNiceMatrix}
  }
  \newcommand<>{\VecAv}[3]{
    \begin{bNiceMatrix}[
      , r
      , code-before = {
        \hlMat####4[####1]
      }
      , code-after = {
        \begin{tikzpicture}
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=####4]
          ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 2.5mm)
          node[left] {$####2$};
          \draw[<-, ####1, thick, shorten <=0.5mm, visible on=<8->]
          ($ (row-4-|col-1)!0.5!(row-4-|col-2) $) |- ++(-2mm, -2.5mm)
          node[left, scale=0.75] {$A\bv=2\cdot\bv$};
        \end{tikzpicture}
      }
      ]
      ####3
    \end{bNiceMatrix}
  }
  \newcommand<>{\EigenSpaceYes}{
    \begin{tikzpicture}[baseline]
      \node[anchor=base, inner sep=0] (EALambda) {\(\textcolor####1{grn}{\bv\in\mathcal{E}_A(2)}\)};

      \draw[thick, <-, shorten <=1.5mm, grn, overlay, visible on=####1]
      (EALambda.south) |- ++(2mm, -8mm) node[right, scale=0.75]
      {$\lambda=2$ makes $A\bv=\lambda\cdot\bv$ work};
    \end{tikzpicture}
  }
  \newcommand<>{\EigenSpaceNo}{
    \begin{tikzpicture}[baseline]
      \node[anchor=base, inner sep=0] (EALambda)
      {\(\textcolor####1{red}{\bv[w]\notin\mathcal{E}_A(\lambda)}\)};

      \draw[thick, <-, shorten <=1.5mm, red, overlay, visible on=####1]
      (EALambda.south) |- ++(3mm, -4mm) node[right, scale=0.75]
      {no scalar $\lambda$ makes $A\bv[w]=\lambda\cdot\bv[w]$ work};
    \end{tikzpicture}
  }
  \begin{example}
    Consider our previous calculations.
    \begin{align*}
      \MatA<1->\Vec<2->{red}{\bv[w]}{1\\-1\\0} &= \VecAw<3->{red}{A\bv[w]}{0\\-6\\6} & \MatA<1->\Vec<4->{grn}{\bv[v]}{0\\1\\-1} &= \VecAv<5->{grn}{A\bv[v]}{0\\2\\-2}
    \end{align*}
    \onslide<6->{Here, \EigenSpaceYes<7-> but \EigenSpaceNo<9->.}
  \end{example}

\end{frame}


\end{document}
