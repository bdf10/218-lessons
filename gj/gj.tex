\documentclass[]{bmr}

\usepackage{pifont}
\usepackage{booktabs}


\title{Gau\ss-Jordan Elimination}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Reduced Row Echelon Form}
\subsection{Elementary Row Operations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    There are three types of \emph{\grn{elementary row operations}}.
  \end{definition}

  \begin{exampleblock}{Row Switching $\bv[r]_i\leftrightarrow\bv[r]_j$}<2->
    We may switch the $i$th row with the $j$th row.
  \end{exampleblock}

  \begin{exampleblock}{Row Scaling $c\cdot \bv[r]_i\to\bv[r]_i$}<3->
    We may multiply the $i$th row $\bv[r]_i$ by a \emph{nonzero scalar} $c\neq 0$.
  \end{exampleblock}

  \begin{exampleblock}{Row Addition $\bv[r]_i+c\cdot\bv[r]_j\to\bv[r]_i$}<4->
    We can add $c\cdot\bv[r]_j$ to the $i$th row (leaving $\bv[r]_j$ unchanged).
  \end{exampleblock}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<2->{1}
        \hlRow<2->[grn]{3}
      }
      ]
      -4 &  3 &  1 &  0 \\
      -1 &  5 &  2 &  3 \\
      0  & -1 & -1 & -1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<2>{1}
        \hlRow<2>[grn]{3}
        \hlRow<3->{1}
        \hlRow<3->[grn]{3}
      }
      ]
      \alt<3->{0}{-4}  & \alt<3->{-1}{3} & \alt<3->{-1}{1} & \alt<3->{-1}{0} \\
      -1 &  5 &  2 &  3 \\
      \alt<3->{-4}{0}  & \alt<3->{3}{-1} & \alt<3->{1}{-1} & \alt<3->{0}{-1} \\
    \end{bNiceMatrix}
  }
  \begin{example}[Row Switching]
    $\MatrixA\xrightarrow{\bv[r]_1\leftrightarrow\bv[r]_3}\MatrixB$
  \end{example}

  \newcommand{\MatrixC}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlRow<5->{2}}
      ]
      9  &  1 \\
      -5 & -7 \\
      8  &  5
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlRow<5->{2}}
      ]
      \phantom{-}9  &  \phantom{-}1 \\
      \alt<6->{15}{-5} & \alt<6->{21}{-7} \\
      8  &  5
    \end{bNiceMatrix}
  }
  \begin{example}[Row Scaling]<4->
    $\MatrixC\xrightarrow{-3\cdot\bv[r]_2\to\bv[r]_2}\MatrixD$
  \end{example}

  \newcommand{\MatrixE}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlRow<8->{3}}
      ]
      2  & -1 & -1 &  -1 &  0 \\
      1  & 1  & -1 & -13 &  2 \\
      -1 & -1 & 0  &   0 & -1 \\
      0  & -1 & 3  &  -2 & -1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixF}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before={\hlRow<8->{3}\hlRow<9->[grn]{1}}
      ]
      2  & -1 & -1 &  -1 & 0 \\
      1  &  1 & -1 & -13 & 2 \\
      \alt<10->{-9}{-1} &  \alt<10->{3}{-1} &  \alt<10->{4}{0} &   \alt<10->{4}{0} & \alt<10->{-1}{-1} \\
      0  & -1 &  3 &  -2 & -1
    \end{bNiceMatrix}
  }
  \begin{example}[Row Addition]<7->
    $\MatrixE\xrightarrow{\bv[r]_3-4\cdot\bv[r]_1\to\bv[r]_3}\MatrixF$
  \end{example}

\end{frame}


\subsection{Existence and Uniqueness of $\rref(A)$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlEntry<3->[red]{1}{1}}
      ]
      -9 & 18 & 20 \\
      -5 & 10 & 11
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<5>[red]{1}{1}
        \hlEntry<6->[grn]{1}{1}
        \hlEntry<7->[red]{2}{1}
      }
      ]
      1  & -2 & -\sfrac{20}{9} \\
      -5 & 10 & 11
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixC}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry{1}{1}
        \hlEntry<9>[red]{2}{1}
        \hlEntry<10>[grn]{2}{1}
        \hlEntry<12->[red]{2}{3}
      }
      ]
      1 & -2 & -\sfrac{20}{9} \\
      0 &  0 & -\sfrac{1}{9}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<14->{1}{1}
        \hlEntry<14>[red]{2}{3}
        \hlEntry<15->[grn]{2}{3}
        \hlEntry<16->[red]{1}{3}
      }
      ]
      1 & -2 & -\sfrac{20}{9} \\
      0 &  0 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixE}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<18>[red]{1}{3}
        \hlEntry<19>[grn]{1}{3}
        \hlEntry<18->{1}{1}
        \hlEntry<18->{2}{3}
      }
      ]
      1 & -2 & 0 \\
      0 &  0 & 1
    \end{bNiceMatrix}
  }
  \begin{theorem}
    Every matrix $A$ can be row-reduced to one and only one $\rref(A)$.
    \begin{align*}
      \onslide<2->{\overset{A}{\MatrixA}}
      \onslide<4->{\xrightarrow{(-\sfrac{1}{9})\cdot\bv[r]_1\to\bv[r]_1}}\onslide<5->{\MatrixB} \\
      \onslide<8->{\xrightarrow{\bv[r]_2+5\cdot\bv[r]_1\to\bv[r]_2}}\onslide<9->{\MatrixC} \\
      \onslide<13->{\xrightarrow{-9\cdot\bv[r]_2\to\bv[r]_2}}\onslide<14->{\MatrixD} \\
      \onslide<17->{\xrightarrow{\bv[r]_1+(\sfrac{20}{9})\cdot\bv[r]_2\to\bv[r]_1}}\onslide<18->{\overset{\onslide<20->{\rref(A)}}{\MatrixE}}
    \end{align*}
  \end{theorem}

\end{frame}


\section{The Gau\ss-Jordan Algorithm}
\subsection{Description}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The Gau\ss-Jordan Algorithm}
    To determine $\rref(A)$, start with $i=j=1$.
    \begin{description}[<+->]
    \item[Step 1] If $a_{ij}=0$, switch $\Row_i$ with the first row below whose
      $j$th entry is nonzero. If no such row exists, increase $j$ by one and
      repeat this step.
    \item[Step 2] Scale $\Row_i$ by $\sfrac{1}{a_{ij}}$ to produce a pivot.
    \item[Step 3] Eliminate \emph{all} nonpivot entries in the $j$th column by
      subtracting suitable multiples of $\Row_i$.
    \item[Step 4] Increase $i$ and $j$ by one and return to Step 1.
    \end{description}
    \onslide<+->{The algorithm terminates after the last row or column is
      processed.}
  \end{block}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<2->[red]{1}{1}
      }
      ]
      -4 &  20 & -12 \\
      -1 &   5 &  -3 \\
      5  & -25 &  16
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<4>[red]{1}{1}
        \hlEntry<5->[grn]{1}{1}
        \hlColPart<6->[red]{2}{1}
      }
      ]
      1  &  -5 &  3 \\
      -1 &   5 & -3 \\
      5  & -25 & 16
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixC}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry{1}{1}
        \hlColPart<8>[red]{2}{1}
        \hlColPart<9>[grn]{2}{1}
        \hlEntry<11>[red]{2}{2}
        \hlColPart<12>[red]{2}{2}
        \hlEntry<13>[red]{2}{3}
        \hlColPart<14->[red]{2}{3}
      }
      ]
      1  & -5 & 3 \\
      0  &  0 & 0 \\
      0  &  0 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry{1}{1}
        \hlColPart<16>[red]{2}{3}
        \hlColPart<17>[grn]{2}{3}
        \hlEntry<18->{2}{3}
        \hlEntry<19->[red]{1}{3}
      }
      ]
      1  & -5 & 3 \\
      0  &  0 & 1 \\
      0  &  0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixE}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry{1}{1}
        \hlEntry{2}{3}
        \hlEntry<21>[red]{1}{3}
        \hlEntry<22>[grn]{1}{3}
      }
      ]
      1  & -5 & 0 \\
      0  &  0 & 1 \\
      0  &  0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\StepX}{
    \begin{NiceArray}{rcrcl}[small]
      \bv[r]_2 &+&       \bv[r]_1 &\to& \bv[r]_2 \\
      \bv[r]_3 &-& 5\cdot\bv[r]_1 &\to& \bv[r]_3
    \end{NiceArray}
  }
  \begin{example}
    $
    \begin{aligned}
      \overset{A}{\MatrixA}
      \onslide<3->{\xrightarrow{(-\sfrac{1}{4})\cdot\bv[r]_1\to\bv[r]_1}}\onslide<4->{\MatrixB} \\
      \onslide<7->{\xrightarrow{\StepX}}\onslide<8->{\MatrixC} \\
      \onslide<15->{\xrightarrow{\bv[r]_2\leftrightarrow\bv[r]_3}}\onslide<16->{\MatrixD} \\
      \onslide<20->{\xrightarrow{\bv[r]_1-3\cdot\bv[r]_2\to\bv[r]_1}}\onslide<21->{\overset{\onslide<23->{\rref(A)}}{\MatrixE}}
    \end{aligned}
    $
  \end{example}

\end{frame}


% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \tikzstyle{th}=[thin, rounded corners=0.5mm]

%   \newcommand{\MatrixA}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , small
%       , code-before = {
%         \hlEntry<2->[red, th]{1}{1}
%       }
%       ]
%       -2 &  8 &  5 \\
%       -1 &  4 &  2 \\
%       1  & -4 & -3 \\
%       0  &  0 & -5
%     \end{bNiceMatrix}
%   }
%   \newcommand{\MatrixB}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , small
%       , code-before = {
%         \hlEntry<4>[red, th]{1}{1}
%         \hlEntry<5->[grn, th]{1}{1}
%         \hlPortion<6->[red, th]{2}{1}{3}{1}
%       }
%       ]
%       1  & -4 & -\sfrac{5}{2} \\
%       -1 &  4 &             2 \\
%       1  & -4 &            -3 \\
%       0  &  0 &            -5
%     \end{bNiceMatrix}
%   }
%   \newcommand{\MatrixC}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , small
%       , code-before = {
%         \hlEntry[blu, th]{1}{1}
%         \hlPortion<8>[red, th]{2}{1}{3}{1}
%         \hlPortion<9>[grn, th]{2}{1}{3}{1}
%         \hlEntry<10>[red, th]{2}{2}
%         \hlPortion<11>[red, th]{2}{2}{4}{2}
%         \hlEntry<12->[red, th]{2}{3}
%       }
%       ]
%       1 & -4 & -\sfrac{5}{2} \\
%       0 &  0 & -\sfrac{1}{2} \\
%       0 &  0 & -\sfrac{1}{2} \\
%       0 &  0 &            -5
%     \end{bNiceMatrix}
%   }
%   \newcommand{\MatrixD}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , small
%       , code-before = {
%         \hlEntry[blu, th]{1}{1}
%         \hlEntry<14>[red, th]{2}{3}
%         \hlEntry<15->[grn, th]{2}{3}
%         \hlEntry<16->[red, th]{1}{3}
%         \hlPortion<16->[red, th]{3}{3}{4}{3}
%       }
%       ]
%       1 & -4 & -\sfrac{5}{2} \\
%       0 &  0 &             1 \\
%       0 &  0 & -\sfrac{1}{2} \\
%       0 &  0 &            -5
%     \end{bNiceMatrix}
%   }
%   \newcommand{\MatrixE}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , small
%       , code-before = {
%         \hlEntry[blu, th]{1}{1}
%         \hlEntry[blu, th]{2}{3}
%         \hlEntry<18>[red, th]{1}{3}
%         \hlPortion<18>[red, th]{3}{3}{4}{3}
%         \hlEntry<19>[grn, th]{1}{3}
%         \hlPortion<19>[grn, th]{3}{3}{4}{3}
%       }
%       ]
%       1 & -4 & 0 \\
%       0 &  0 & 1 \\
%       0 &  0 & 0 \\
%       0 &  0 & 0
%     \end{bNiceMatrix}
%   }
%   \newcommand{\StepX}{
%     \begin{NiceArray}{rcrcl}[small]
%       \bv[r]_2 &+& \bv[r]_1 &\to& \bv[r]_2 \\
%       \bv[r]_3 &-& \bv[r]_1 &\to& \bv[r]_3
%     \end{NiceArray}
%   }
%   \newcommand{\StepY}{
%     \begin{NiceArray}{rcrcl}[small]
%       \bv[r]_1 &+& (\sfrac{5}{2})\cdot\bv[r]_2 &\to& \bv[r]_1 \\
%       \bv[r]_3 &+& (\sfrac{1}{2})\cdot\bv[r]_2 &\to& \bv[r]_3 \\
%       \bv[r]_4 &+&              5\cdot\bv[r]_2 &\to& \bv[r]_4
%     \end{NiceArray}
%   }
%   \begin{example}
%     $
%     \begin{aligned}
%       \overset{A}{\MatrixA}
%       \onslide<3->{\xrightarrow{(-\sfrac{1}{2})\cdot\bv[r]_1\to\bv[r]_1}}\onslide<4->{\MatrixB} \\
%       \onslide<7->{\xrightarrow{\StepX}}\onslide<8->{\MatrixC} \\
%       \onslide<13->{\xrightarrow{-2\cdot\bv[r]_2\to\bv[r]_2}}\onslide<14->{\MatrixD} \\
%       \onslide<17->{\xrightarrow{\StepY}}\onslide<18->{\overset{\onslide<20->{\rref(A)}}{\MatrixE}}
%     \end{aligned}
%     $
%   \end{example}

% \end{frame}


% \begin{frame}

%   \frametitle{\secname}
%   \framesubtitle{\subsecname}

%   \newcommand{\MatrixA}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , code-before = {
%         \hlEntry<2>[red]{1}{1}
%         \hlPortion<3->[red]{1}{1}{2}{1}
%       }
%       ]
%       0  &  0 & -1 &   1 \\
%       1  & -3 & -3 &   4 \\
%       -4 & 12 & 17 & -21
%     \end{bNiceMatrix}
%   }
%   \newcommand{\MatrixB}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , code-before = {
%         \hlPortion<5>[red]{1}{1}{2}{1}
%         \hlEntry<6->{1}{1}
%         \hlEntry<7->[red]{3}{1}
%       }
%       ]
%       1  & -3 & -3 &   4 \\
%       0  &  0 & -1 &   1 \\
%       -4 & 12 & 17 & -21
%     \end{bNiceMatrix}
%   }
%   \newcommand{\MatrixC}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , code-before = {
%         \hlEntry<9->{1}{1}
%         \hlEntry<9>[red]{3}{1}
%         \hlEntry<10>[grn]{3}{1}
%         \hlEntry<11>[red]{2}{2}
%         \hlPortion<12>[red]{2}{2}{3}{2}
%         \hlEntry<13->[red]{2}{3}
%       }
%       ]
%       1 & -3 & -3 &  4 \\
%       0 &  0 & -1 &  1 \\
%       0 &  0 &  5 & -5
%     \end{bNiceMatrix}
%   }
%   \newcommand{\MatrixD}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , code-before = {
%         \hlEntry<15->{1}{1}
%         \hlEntry<15>[red]{2}{3}
%         \hlEntry<16->[grn]{2}{3}
%         \hlEntry<17->[red]{1}{3}
%         \hlEntry<17->[red]{3}{3}
%       }
%       ]
%       1 & -3 & -3 &  4 \\
%       0 &  0 &  1 & -1 \\
%       0 &  0 &  5 & -5
%     \end{bNiceMatrix}
%   }
%   \newcommand{\MatrixE}{
%     \begin{bNiceMatrix}[
%       , r
%       , margin
%       , code-before = {
%         \hlEntry<19->{1}{1}
%         \hlEntry<19->{2}{3}
%         \hlEntry<19>[red]{1}{3}
%         \hlEntry<19>[red]{3}{3}
%         \hlEntry<20>[grn]{1}{3}
%         \hlEntry<20>[grn]{3}{3}
%       }
%       ]
%       1 & -3 & 0 &  1 \\
%       0 &  0 & 1 & -1 \\
%       0 &  0 & 0 &  0
%     \end{bNiceMatrix}
%   }
%   \newcommand{\StepX}{
%     \begin{NiceArray}{rcrcl}[small]
%       \bv[r]_1 &+& 3\cdot\bv[r]_2 &\to& \bv[r]_1 \\
%       \bv[r]_3 &-& 5\cdot\bv[r]_2 &\to& \bv[r]_3
%     \end{NiceArray}
%   }
%   \begin{example}
%     $
%     \begin{aligned}
%       \overset{A}{\MatrixA}
%       \onslide<4->{\xrightarrow{\bv[r]_1\leftrightarrow\bv[r]_1}}\onslide<5->{\MatrixB}    \\
%       \onslide<8->{\xrightarrow{\bv[r]_3+4\cdot\bv[r]_1\to\bv[r]_3}}\onslide<9->{\MatrixC} \\
%       \onslide<14->{\xrightarrow{-\bv[r]_2\to\bv[r]_2}}\onslide<15->{\MatrixD}             \\
%       \onslide<18->{\xrightarrow{\StepX}}\onslide<19->{\overset{\onslide<21->{\rref(A)}}{\MatrixE}}
%     \end{aligned}
%     $
%   \end{example}

% \end{frame}


\section{Rank and Nullity}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

    \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<5->[grn]{1}
        \hlCol<6->[grn]{3}
        \hlCol<7->[grn]{5}
        \hlCol<10->[red]{2}
        \hlCol<11->[red]{4}
      }
      , code-after = {
        \begin{tikzpicture}[thick, grn, ->, shorten >=3pt]
          \node[above right=0.25mm and 5mm, visible on=<8->]
          (pivot) at (1-5.north east) {\emph{pivot columns}};
          \draw[visible on=<8->] (pivot) -| (1-1.north);
          \draw[visible on=<8->] (pivot) -| (1-3.north);
          \draw[visible on=<8->] (pivot) -| (1-5.north);
          \node[red, below=0.5mm, anchor=north west, visible on=<12->]
          (nonpivot) at (4-5.south) {\emph{nonpivot columns}};
          \draw[red, visible on=<12->] (nonpivot) -| (4-2.south);
          \draw[red, visible on=<12->] (nonpivot) -| (4-4.south);
        \end{tikzpicture}
      }
      ]
      -3 &  12 & 10 & -18 &  16 \\
      -2 &   8 &  5 &  -7 &  10 \\
      -4 &  16 & 12 & -20 &  21 \\
      3  & -12 & -3 &  -3 & -14
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{3}
        \hlEntry<4->{3}{5}
      }
      ]
      1 & -4 & 0 & -4 & 0 \\
      0 &  0 & 1 & -3 & 0 \\
      0 &  0 & 0 &  0 & 1 \\
      0 &  0 & 0 &  0 & 0
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{pivot columns}} of $A$ are where the pivots end up in
    $\rref(A)$.
    \[
      \rref\MatrixA = \MatrixR
    \]
    \onslide<9->{The \emph{\red{nonpivot columns}} of $A$ are the other columns.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<5->[grn]{1}
        \hlCol<6->[grn]{3}
        \hlCol<7->[grn]{5}
        \hlCol<10->[red]{2}
        \hlCol<11->[red]{4}
      }
      , code-after = {
        \begin{tikzpicture}[thick, grn, ->, shorten >=3pt]
          \node[above right=0.25mm and 5mm, visible on=<8->]
          (pivot) at (1-5.north east) {$\rank(A)=3$};
          \draw[visible on=<8->] (pivot) -| (1-1.north);
          \draw[visible on=<8->] (pivot) -| (1-3.north);
          \draw[visible on=<8->] (pivot) -| (1-5.north);
          \node[red, below=0.5mm, anchor=north west, visible on=<12->]
          (nonpivot) at (4-5.south) {$\nullity(A)=2$};
          \draw[red, visible on=<12->] (nonpivot) -| (4-2.south);
          \draw[red, visible on=<12->] (nonpivot) -| (4-4.south);
        \end{tikzpicture}
      }
      ]
      -3 &  12 & 10 & -18 &  16 \\
      -2 &   8 &  5 &  -7 &  10 \\
      -4 &  16 & 12 & -20 &  21 \\
      3  & -12 & -3 &  -3 & -14
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{3}
        \hlEntry<4->{3}{5}
      }
      ]
      1 & -4 & 0 & -4 & 0 \\
      0 &  0 & 1 & -3 & 0 \\
      0 &  0 & 0 &  0 & 1 \\
      0 &  0 & 0 &  0 & 0
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{rank}} is the number of pivot columns.
    \[
      \rref\MatrixA = \MatrixR
    \]
    \onslide<9->{The \emph{\red{nullity}} is the number of nonpivot columns.}
  \end{definition}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\RankNullity}{
    \begin{array}{c}
      \textnormal{\grn{\scriptsize Rank-Nullity Theorem}} \\
      \rank(A) + \nullity(A) = n
    \end{array}
  }
  \newcommand{\RankTranspose}{
    \begin{array}{c}
      \textnormal{\grn{\scriptsize Rank-Transpose Theorem}} \\
      \rank(A)=\rank(A^\intercal)
    \end{array}
  }
  \newcommand{\RankGramian}{
    \begin{array}{c}
      \textnormal{\grn{\scriptsize Rank-Gramian Theorem}} \\
      \rank(A)=\rank(A^\intercal A)
    \end{array}
  }
  \begin{theorem}[Rank Theorems]
    Every $m\times n$ matrix $A$ satisfies
    \begin{align*}
      \RankNullity && \onslide<2->{\RankTranspose} && \onslide<3->{\RankGramian}
    \end{align*}
    \onslide<4->{These first two equations can be represented pictorially.}
    \[
      \begin{tikzpicture}[visible on=<4->]

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        , font=\scriptsize
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]

        \node (Row) [
        , LeftSpace
        , fill=blubg
        , anchor=south west
        ] {$\rank(A)$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        ] {$\nullity(A)$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^n$};

        \begin{scope}[xshift=5cm]
          \node(Col) [
          , RightSpace
          , fill=blubg
          , anchor=south east
          ] {$\rank(A)$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          ] {$\nullity(A^\intercal)$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west) node[midway, above] {$A$};

      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{example}
    Suppose $A$ is $8\times 13$ with $\nullity(A)=6$.
    \[
      \begin{tikzpicture}[visible on=<2->]

        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        , font=\scriptsize
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]

        \node (Row) [
        , LeftSpace
        , fill=blubg
        , anchor=south west
        ] {$\rank(A)=\onslide<3->{7}$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        ] {$\nullity(A)=6$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^{13}$};

        \begin{scope}[xshift=6cm]
          \node(Col) [
          , RightSpace
          , fill=blubg
          , anchor=south east
          ] {$\rank(A)=\onslide<4->{7}$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          ] {$\nullity(A^\intercal)=\onslide<5->{1}$};

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^{8}$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west) node[midway, above] {$A$};

      \end{tikzpicture}
    \]
    \onslide<6->{We also know that $A^\intercal A$ is $13\times 13$ with
      $\rank(A^\intercal A)=7$.}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}[Rank Descriptors]
    We have colloquialisms that describe the rank of an $m\times n$ matrix $A$.
    \begin{description}[<+->][full column rank]
    \item[\emph{\grn{full column rank}}] $\rank(A)=n$
    \item[\emph{\grn{full row rank}}] $\rank(A)=m$
    \item[\emph{\grn{full rank}}] $\rank(A)=m$ or $\rank(A)=n$
    \item[\emph{\grn{rank deficient}}] $\rank(A)<m$ and $\rank(A)<n$
    \item[\emph{\grn{nonsingular}}] $m=n$ and $\rank(A)=n$
    \item[\emph{\grn{singular}}] $m=n$ and $\rank(A)<n$
    \end{description}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \tikzstyle{th}=[thin, rounded corners=0.5mm]
  \newcommand{\rrefA}{
    \begin{bNiceMatrix}[
      , small
      , r
      , code-before={\hlEntry[blu, th]{1}{1}\hlEntry[blu, th]{2}{2}}
      ]
      1 & 0 & -1 \\
      0 & 1 &  4
    \end{bNiceMatrix}
  }
  \newcommand{\rrefB}{
    \begin{bNiceMatrix}[
      , small
      , r
      , code-before={\hlEntry[blu, th]{1}{1}\hlEntry[blu, th]{2}{3}}
      ]
      1 & 3 & 0 \\
      0 & 0 & 1 \\
      0 & 0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\rrefC}{
    \begin{bNiceMatrix}[
      , small
      , r
      , code-before={\hlEntry[blu, th]{1}{1}\hlEntry[blu, th]{2}{2}\hlEntry[blu, th]{3}{3}}
      ]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\rrefD}{
    \begin{bNiceMatrix}[
      , small
      , r
      , code-before={\hlEntry[blu, th]{1}{1}}
      ]
      1 & 3 \\
      0 & 0 \\
      0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\ya}{\textnormal{\grn{\ding{51}}}}
  \newcommand{\na}{\textnormal{\red{\ding{55}}}}
  \begin{example}
    Suppose the reduced row echelon forms of $A$, $B$, $C$, and $D$ are
    \begin{gather*}
      \begin{align*}
        {\scriptstyle\rref(A)=}\rrefA && {\scriptstyle\rref(B)=}\rrefB && {\scriptstyle\rref(C)=}\rrefC && {\scriptstyle\rref(D)=}\rrefD
      \end{align*}
    \end{gather*}
    \pause This is enough information to apply our rank descriptors.
    \[
      \begin{NiceArray}{rcccc}
        \toprule
        {}                            & A   & B   & C   & D   \\ \midrule
        \textnormal{full column rank} & \na & \na & \ya & \na \\
        \textnormal{full row rank}    & \ya & \na & \ya & \na \\
        \textnormal{full rank}        & \ya & \na & \ya & \na \\
        \textnormal{rank deficient}   & \na & \ya & \na & \ya \\
        \textnormal{nonsingular}      & \na & \na & \ya & \na \\
        \textnormal{singular}         & \na & \ya & \na & \na \\ \bottomrule
      \end{NiceArray}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<8->[red]{2}
        \hlCol<9->[red]{4}
      }
      , code-after = {
        \begin{tikzpicture}[->, thick, red, shorten >=2mm]
          \node[below left=3mm and 2mm, anchor=north east, visible on=<8->]
          (col2) at (3-2.south) {$\scriptstyle\Col_2=3\cdot\Col_1$};
          \draw[visible on=<8->] (col2.east) -| (3-2.south);
          \node[below left=7.5mm and 2mm, anchor=north east, visible on=<9->]
          (col4) at (3-4.south) {$\scriptstyle\Col_4=-2\cdot\Col_1+5\cdot\Col_3$};
          \draw[visible on=<9->] (col4.east) -| (3-4.south);
        \end{tikzpicture}
      }
      ]
      2  &  6 &  0 & -4  \\
      -3 & -9 & -4 & -14 \\
      1  &  3 &  7 &  33
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{3}
        \hlCol<4->[red]{2}
        \hlCol<5->[red]{4}
      }
      , code-after = {
        \begin{tikzpicture}[->, thick, red, shorten >=2mm]
          \node[below right=7.5mm and 2mm, anchor=north west, visible on=<6->]
          (col2) at (3-2.south) {$\scriptstyle\Col_2=3\cdot\Col_1$};
          \draw[visible on=<6->] (col2.west) -| (3-2.south);
          \node[below right=3mm and 2mm, anchor=north west, visible on=<7->]
          (col4) at (3-4.south) {$\scriptstyle\Col_4=-2\cdot\Col_1+5\cdot\Col_3$};
          \draw[visible on=<7->] (col4.west) -| (3-4.south);
        \end{tikzpicture}
      }
      ]
      1 & 3 & 0 & -2 \\
      0 & 0 & 1 &  5 \\
      0 & 0 & 0 &  0
    \end{bNiceMatrix}
  }
  \begin{theorem}
    The matrix $A$ has the same column relations as $\rref(A)$.
    \[
      \rref\MatrixA = \MatrixR
    \]
  \end{theorem}

\end{frame}


\section{Solving Linear Systems}
\subsection{Rouch\'e-Capelli Theorem}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceArray}{rr|r}[
      , margin
      , first-row
      , code-before = {
        \hlEntry<2->{1}{1}
        \hlEntry<3->[red]{2}{1}
      }
      ]
      x & y &   \\
      1 & 2 & 3 \\
      3 & 4 & 0
    \end{bNiceArray}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceArray}{rr|r}[
      , margin
      , first-row
      , code-before = {
        \hlEntry{1}{1}
        \hlEntry<5>[red]{2}{1}
        \hlEntry<6>[grn]{2}{1}
        \hlEntry<7->[red]{2}{2}
      }
      ]
      x &  y &    \\
      1 &  2 &  3 \\
      0 & -2 & -9
    \end{bNiceArray}
  }
  \newcommand{\MatrixC}{
    \begin{bNiceArray}{rr|r}[
      , margin
      , first-row
      , code-before = {
        \hlEntry{1}{1}
        \hlEntry<9>[red]{2}{2}
        \hlEntry<10->[grn]{2}{2}
        \hlEntry<11->[red]{1}{2}
      }
      ]
      x & y &               \\
      1 & 2 &  3            \\
      0 & 1 & \sfrac{9}{2}
    \end{bNiceArray}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceArray}{rr|r}[
      , margin
      , first-row
      , code-before = {
        \hlEntry{1}{1}
        \hlEntry{2}{2}
        \hlEntry<13>[red]{1}{2}
        \hlEntry<14>[grn]{1}{2}
      }
      ]
      x & y &               \\
      1 & 0 & -6            \\
      0 & 1 & \sfrac{9}{2}
    \end{bNiceArray}
  }
  \begin{block}{Observation}
    Row operations on an augmented matrix represent \emph{\grn{equation
        arithmetic}}.
    \begin{align*}
      \MatrixA
      \onslide<4->{\xrightarrow{\bv[r]_2-3\cdot\bv[r]_1\to\bv[r]_2}} \onslide<5->{\MatrixB}      \\
      \onslide<8->{\xrightarrow{(-\sfrac{1}{2})\cdot\bv[r]_2\to\bv[r]_2}} \onslide<9->{\MatrixC} \\
      \onslide<12->{\xrightarrow{\bv[r]_1-2\cdot\bv[r]_2\to\bv[r]_1}} \onslide<13->{\MatrixD}
    \end{align*}
    \onslide<15->{The system has a \emph{\grn{unique solution}} given by
      $\bv[x]=\nv{{-6} {\sfrac{9}{2}}}$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Inconsistent}{
    \begin{array}{c}
      \textnormal{\red{\scriptsize Inconsistent (No Solutions)}} \\
      \rref
      \begin{bNiceArray}{rr|r}[margin]
        3 & 9 & -13 \\
        1 & 3 &  -4
      \end{bNiceArray}
                =
                \begin{bNiceArray}{rr|r}[margin, code-before={\hlEntry{1}{1}}\hlEntry[red]{2}{3}]
                  1 & 3 & 0 \\
                  0 & 0 & 1
                \end{bNiceArray} \\
      \red{\scriptstyle\rank(A)<\rank[A\mid\bv[b]]}
    \end{array}
  }
  \newcommand{\ConsistentOne}{
    \begin{array}{c}
      \textnormal{\blu{\scriptsize Consistent (One Solution)}} \\
      \rref
      \begin{bNiceArray}{rr|r}[margin]
        5 & 2 & 11 \\
        3 & 1 &  6
      \end{bNiceArray}
                =
                \begin{bNiceArray}{rr|r}[margin, code-before={\hlEntry{1}{1}}\hlEntry{2}{2}]
                  1 & 0 & 1 \\
                  0 & 1 & 3
                \end{bNiceArray} \\
      \blu{\scriptstyle{\rank(A)=\rank[A\mid\bv[b]]} \ \&\ {\nullity(A)=0}}
    \end{array}
  }
  \newcommand{\ConsistentInfinite}{
    \begin{array}{c}
      \textnormal{\grn{\scriptsize Consistent (Infinite Solutions)}} \\
      \rref
      \begin{bNiceArray}{rr|r}[margin]
        4 & 12 & 8 \\
        3 &  9 & 6
      \end{bNiceArray}
                 =
                 \begin{bNiceArray}{rr|r}[margin, code-before={\hlEntry{1}{1}}]
                   1 & 3 & 2 \\
                   0 & 0 & 0
                 \end{bNiceArray} \\
      \grn{\scriptstyle{\rank(A)=\rank[A\mid\bv[b]]} \ \&\ {\nullity(A)>0}}
    \end{array}
  }
  \begin{block}{Rouch\"e-Capelli Theorem}
    Every system $[A\mid\bv[b]]$ falls into one of the following three categories.
    \gcenter{%
      $\Inconsistent$\\
      $\ConsistentOne$ && $\ConsistentInfinite$
    }%
    Moreover, $[A\mid\bv[b]]$ and $\rref[A\mid\bv[b]]$ have the same solutions.
  \end{block}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceArray}{rrrr|r}[first-row]
      x_1 & x_2 & x_3 & x_4 &     \\
      3   & -14 &  40 & -20 &  34 \\
      2   &  -9 &  26 & -13 &  22 \\
      -3  &  11 & -34 &  17 & -27
    \end{bNiceArray}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrr|r}[
      , margin
      , first-row
      , code-before={
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<4->[red]{3}{5}
      }
      ]
      x_1 & x_2 & x_3 & x_4 &   \\
      1   &   0 &   4 &  -2 & 0 \\
      0   &   1 &  -2 &   1 & 0 \\
      0   &   0 &   0 &   0 & 1
    \end{bNiceArray}
  }
  \begin{example}
    Suppose we apply the Gau\ss-Jordan algorithm to the following system.
    \[
      \rref\MatrixA=\MatrixR
    \]
    \onslide<5->{The system is \emph{\red{inconsistent}} and has \emph{\red{no
          solutions}}.}
  \end{example}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceArray}{rrrr|r}[first-row]
      x_1 & x_2 & x_3 & x_4 &     \\
      4   & -15 &  72 & -57 & 428 \\
      3   & -11 &  53 & -42 & 316 \\
      3   &  -9 &  45 & -35 & 267 \\
      2   & -12 &  54 & -43 & 313
    \end{bNiceArray}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrr|r}[
      , margin
      , first-row
      , code-before={
        \hlEntry<2->{1}{1}
        \hlEntry<3->{2}{2}
        \hlEntry<4->{3}{4}
        \hlCol<6->[red]{3}
      }
      ]
      x_1 & x_2 & x_3 & x_4 &    \\
      1   &   0 &   3 &   0 &  5 \\
      0   &   1 &  -4 &   0 &  7 \\
      0   &   0 &   0 &   1 & -9 \\
      0   &   0 &   0 &   0 &  0
    \end{bNiceArray}
  }
  \newcommand{\Solution}{
    \begin{bNiceMatrix}[r]
      x_1\\ x_2\\ x_3\\ x_4
    \end{bNiceMatrix}
  }
  \newcommand{\SolutionSub}{
    \begin{bNiceMatrix}[r]
      \onslide<10->{5-3\,c_1}\\ \onslide<11->{7+4\,c_1}\\ \onslide<9->{c_1}\\ \onslide<12->{-9}
    \end{bNiceMatrix}
  }
  \newcommand{\Naked}{
    \begin{bNiceMatrix}[r]
      \onslide<14->{5}\\ \onslide<14->{7}\\ \onslide<14->{0}\\ \onslide<14->{-9}
    \end{bNiceMatrix}
  }
  \newcommand{\NullA}{
    \begin{bNiceMatrix}[r]
      \onslide<15->{-3}\\ \onslide<15->{4}\\ \onslide<15->{1}\\ \onslide<15->{0}
    \end{bNiceMatrix}
  }
  \begin{example}
    Applying the Gau\ss-Jordan algorithm here shows this system is
    \emph{\grn{consistent}}.
    \[
      \rref\MatrixA=\MatrixR
    \]
    \onslide<5->{Here, $\nullity(A)=1>0$.} \onslide<7->{There are
      \emph{\grn{infinitely many solutions}} given by}
    \[
      \onslide<7->{\bv[x] = \Solution =}
      \onslide<8->{\SolutionSub =}
      \onslide<13->{\Naked + c_1\cdot\NullA}
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \tikzstyle{th}=[thin, rounded corners=0.5mm]
  \newcommand{\MatrixA}{
    \begin{bNiceArray}{rrrrr|r}[first-row]
      x_1 & x_2 & x_3 & x_4 &  x_5 &      \\
      -5  & -35 &  74 & 159 &  775 & 1127 \\
      -1  &  -4 &  10 &  20 &   92 &  145 \\
      -2  &  -6 &  17 &  32 &  142 &  236 \\
      -3  & -16 &  39 &  83 &  389 &  589 \\
      2   &   8 & -21 & -47 & -216 & -325 \\
      3   &  20 & -43 & -92 & -446 & -653
    \end{bNiceArray}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrrr|r}[
      % , small
      , margin
      , first-row
      , code-before={
        \hlEntry<2->{1}{1}
        \hlEntry<2->{2}{2}
        \hlEntry<2->{3}{3}
        \hlEntry<2->{4}{4}
        \hlEntry<2->{5}{5}
      }
      ]
      x_1 & x_2 & x_3 & x_4 & x_5 &    \\
      1   &   0 &   0 &   0 &   0 &  7 \\
      0   &   1 &   0 &   0 &   0 & -6 \\
      0   &   0 &   1 &   0 &   0 &  4 \\
      0   &   0 &   0 &   1 &   0 &  9 \\
      0   &   0 &   0 &   0 &   1 & -1 \\
      0   &   0 &   0 &   0 &   0 &  0
    \end{bNiceArray}
  }
  \begin{example}
    This system is consistent and $\nullity(A)=0$.
    \begin{gather*}
      \rref\MatrixA=\MatrixR
    \end{gather*}
    \onslide<3->{The only solution is $\bv[x]=\nv{7 -6 4 9 -1}$.}
  \end{example}

\end{frame}



\end{document}
