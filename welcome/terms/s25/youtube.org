#+title: Math 218 Welcome Video (Fall 2024)

Welcome to Math 218 (Fall 2024)

RESOURCES
Lecture slides: https://gitlab.oit.duke.edu/bdf10/218-lessons/-/raw/master/welcome/terms/f24/218f24-welcome.pdf
Course site: https://bfitz.xyz

OVERVIEW
0:00 - Personnel
1:05 - Structure and Policies (Schedule)
2:17 - Structure and Policies (Grades)
5:02 - SDAO Accommodations
5:12 - Resources (Course Webpage)
5:35 - Resources (Textbook)
6:04 - Resources (Getting Help)
6:49 - Expectations (Academic)
8:48 - Expectations (Conduct)
10:43 - Course Webpage
