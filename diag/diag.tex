\documentclass[]{bmr}

\title{Diagonalization}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Similar Matrices}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[grn, th, visible on=####1]}
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} -32 & -30 &   4 & 12 \\
      {}  27 &  15 & -27 & 15 \\
      {} -30 & -44 & -32 & 49 \\
      {} -13 & -31 & -41 & 50
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixX}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[red, th, visible on=####1]}
      , code-after = {
        \begin{tikzpicture}[red, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle X$};
        \end{tikzpicture}
      }
      ]
      1 & 0 &  2 &  3 \\
      0 & 1 & -2 & -4 \\
      2 & 2 &  1 &  1 \\
      2 & 2 &  0 & -1
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixB}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[blu, th, visible on=####1]}
      , code-after = {
        \begin{tikzpicture}[blu, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle B$};
        \end{tikzpicture}
      }
      ]
      {}  1 & -3 & -1 & -2 \\
      {}  1 & -3 & -1 &  1 \\
      {}  1 &  1 & -1 &  3 \\
      {} -1 &  1 &  1 &  4
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXi}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[red, th, visible on=####1]}
      , code-after = {
        \begin{tikzpicture}[red, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle X^{-1}$};
        \end{tikzpicture}
      }
      ]
      {} -1 & -2 & -2 &  3 \\
      {}  0 &  1 &  2 & -2 \\
      {}  4 &  4 &  1 & -3 \\
      {} -2 & -2 &  0 &  1
    \end{bNiceMatrix}
  }
  \begin{definition}
    We say that $A$ is \emph{\grn{similar}} to $B$ if $A=XBX^{-1}$ for some $X$.
    \[
      \onslide<2->{
        \MatrixA<3->
        =
        \MatrixX<5->
        \MatrixB<4->
        \MatrixXi<5->
      }
    \]
    \onslide<6->{We write $A\sim B$ to indicate that $A$ and $B$ are similar.}
  \end{definition}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Suppose that $A\sim B$ so $A=XBX^{-1}$. Then
    \begin{description}[<+->]
    \item[rank] $\rank(A)=\rank(B)$
    \item[char-poly] $\chi_A(t)=\chi_B(t)$
    \item[trace] $\trace(A)=\trace(B)$
    \item[det] $\det(A)=\det(B)$
    \item[e-vals] $\EVals(A)=\EVals(B)$
    \item[alg-mults] $\am_A(\lambda)=\am_B(\lambda)$
    \item[geo-mults] $\gm_A(\lambda)=\gm_B(\lambda)$
    \item[e-spaces] $\mathcal{E}_A(\lambda)=X\cdot\mathcal{E}_B(\lambda)$
    \item[powers] $A^k=XB^kX^{-1}$
    \end{description}
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<3,7,9,11,13>
        \hlDiag<5>
      }
      , code-after = {
        \begin{tikzpicture}[blu, <-, thick, shorten <=0.5mm]
          \coordinate (s) at ($ (row-4-|col-1)!0.5!(row-4-|col-4) $);
          \coordinate (d) at ($ (row-4-|col-3)!0.5!(row-4-|col-4) $);
          \draw[visible on=<3>]   (s) |- ++(2.5mm, -3mm)   node[right] {$\scriptstyle\rank(A)=2$};
          \draw[visible on=<5>]   (d) |- ++(2.5mm, -3mm)   node[right] {$\scriptstyle\trace(A)=-3$};
          \draw[visible on=<7>]   (s) |- ++(2.5mm, -3mm)   node[right] {$\scriptstyle\det(A)=0$};
          \draw[visible on=<9>]   (s) |- ++(2.5mm, -3mm)   node[right] {$\scriptstyle\gm_A(0)=1$};
          \draw[visible on=<11>]  (s) |- ++(2.5mm, -3mm)   node[right] {$\scriptstyle\gm_A(-3)=1$};
          \draw[visible on=<13>] (s) |- ++(2.5mm, -2.5mm) node[right] {$\scriptstyle\chi_A(t)=t^2\cdot(t+3)$};
        \end{tikzpicture}
      }
      ]
      {}  74 & -52 & 112 \\
      {} 131 & -92 & 198 \\
      {}  10 &  -7 &  15
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} -1 & 2 &  -8 \\
      {}  1 & 3 & -14 \\
      {}  1 & 0 &  -1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<2-3>[grn]
        \hlDiag<4-5>[grn]
        \hlLower<6-13>[grn]
        \hlEntry<8-9>[grn]{1}{1}
        \hlEntry<8-9>[grn]{2}{2}
        \hlEntry<10-11>[grn]{3}{3}
      }
      , code-after = {
        \begin{tikzpicture}[grn, <-, thick, shorten <=0.5mm]
          \coordinate (s) at ($ (row-4-|col-1)!0.5!(row-4-|col-4) $);
          \coordinate (d) at ($ (row-4-|col-3)!0.5!(row-4-|col-4) $);
          \draw[visible on=<2-3>]   (s) |- ++(2.5mm, -3mm) node[right] {$\scriptstyle\rank(B)=2$};
          \draw[visible on=<4-5>]   (d) |- ++(2.5mm, -3mm) node[right] {$\scriptstyle\trace(B)=-3$};
          \draw[visible on=<6-7>]   (s) |- ++(2.5mm, -3mm) node[right] {$\scriptstyle\det(B)=0$};
          \draw[visible on=<8-9>]   (s) |- ++(2.5mm, -3mm) node[right] {$\scriptstyle\gm_B(0)=1$};
          \draw[visible on=<10-11>] (d) |- ++(2.5mm, -3mm) node[right] {$\scriptstyle\gm_B(-3)=1$};
          \draw[visible on=<12-13>]   (s) |- ++(2.5mm, -3mm) node[right] {$\scriptstyle\chi_B(t)=t^2\cdot(t+3)$};
        \end{tikzpicture}
      }
      ]
      0 & 0 &  0 \\
      1 & 0 &  0 \\
      2 & 1 & -3
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixXi}{
    \begin{bNiceMatrix}[
      , r
      ]
      {}  -3 & 2 &  -4 \\
      {} -13 & 9 & -22 \\
      {}  -3 & 2 &  -5
    \end{bNiceMatrix}
  }
  \newcommand<>{\VectorBb}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1}
      ]
      0 & 3 & 1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand<>{\VectorBa}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat####1}
      ]
      0 & 0 & 1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\VectorAb}{
    \begin{bNiceMatrix}
      -2 & -5 & -1
    \end{bNiceMatrix}^\intercal
  }
  \newcommand{\VectorAa}{
    \begin{bNiceMatrix}
      -8 & -14 & -1
    \end{bNiceMatrix}^\intercal
  }

  \begin{example}
    Consider $A\sim B$ where
    \[
      \overset{A}{\MatrixA}
      =
      \overset{X}{\MatrixX}
      \overset{B}{\MatrixB}
      \overset{X^{-1}}{\MatrixXi}
    \]
    \onslide<14->{The eigenvalues of $B$ are $\EVals(B)=\Set{-3, 0}$ and the
      eigenspaces are}
    \begin{align*}
      \onslide<14->{\mathcal{E}_B(-3) = \Span\Set{\VectorBa<17-18>}} && \onslide<15->{\mathcal{E}_B(0) = \Span\Set{\VectorBb<20->}}
    \end{align*}
    \onslide<16->{The equation $\mathcal{E}_A(\lambda)=X\cdot\mathcal{E}_B(\lambda)$ gives}
    \begin{align*}
      \onslide<16->{\mathcal{E}_A(-3)} &\onslide<16->{= \Span\Set{X\cdot\VectorBa<17-18>}} & \onslide<19->{\mathcal{E}_A(0)} &\onslide<19->{= \Span\Set{X\cdot\VectorBb<20->}} \\
                                       &\onslide<18->{= \Span\Set{\VectorAa}}              &                                 &\onslide<21->{= \Span\Set{\VectorAb}}
    \end{align*}
  \end{example}

\end{frame}


\section{Diagonalizable Matrices}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[grn, th, visible on=####1]}
      , code-after = {
        \begin{tikzpicture}[grn, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {} -13 & -8 & 16 & -12 \\
      {}  32 & 31 & -8 &  -8 \\
      {}   8 & 16 & 19 & -28 \\
      {}  24 & 32 & 16 & -33
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixX}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[red, th, visible on=####1]}
      , code-after = {
        \begin{tikzpicture}[red, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle X$};
        \end{tikzpicture}
      }
      ]
      1 & 0 &  2 &  3 \\
      0 & 1 & -2 & -4 \\
      2 & 2 &  1 &  1 \\
      2 & 2 &  0 & -1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlMat[blu, th, visible on=<4-6>]
        \hlDiag[blu, th, visible on=<7->]
      }
      , code-after = {
        \begin{tikzpicture}[blu, thick, <-, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle D$};
        \end{tikzpicture}
      }
      ]
      {} -5 &  0 & 0 & 0 \\
      {}  0 & -1 & 0 & 0 \\
      {}  0 &  0 & 3 & 0 \\
      {}  0 &  0 & 0 & 7
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXi}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat[red, th, visible on=####1]}
      , code-after = {
        \begin{tikzpicture}[red, thick, <-, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle X^{-1}$};
        \end{tikzpicture}
      }
      ]
      {} -1 & -2 & -2 &  3 \\
      {}  0 &  1 &  2 & -2 \\
      {}  4 &  4 &  1 & -3 \\
      {} -2 & -2 &  0 &  1
    \end{bNiceMatrix}
  }
  \begin{definition}
    We say that $A$ is \emph{\grn{diagonalizable}} if $A$ is similar to a
    diagonal matrix.
    \[
      \onslide<2->{
        \MatrixA<3->
        =
        \MatrixX<5->
        \MatrixD
        \MatrixXi<5->
      }
    \]
    \onslide<6->{The diagonal entries of $D$ are the eigenvalues of $A$!}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, grn, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      {}  62 &  84 \\
      {} -45 & -61
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, red, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle X$};
        \end{tikzpicture}
      }
      ]
      {}  4 &  7 \\
      {} -3 & -5
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlDiag####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, blu, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle D$};
        \end{tikzpicture}
      }
      ]
      {} -1 & 0 \\
      {}  0 & 2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXi}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, red, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle X^{-1}$};
        \end{tikzpicture}
      }
      ]
      {} -5 & -7 \\
      {}  3 &  4
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixAn}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, grn, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A^n$};
        \end{tikzpicture}
      }
      ]
      {} -20\cdot(-1)^n+21\cdot 2^n & -28\cdot(-1)^n+28\cdot 2^n \\
      {}  15\cdot(-1)^n-15\cdot 2^n &  21\cdot(-1)^n-20\cdot 2^n
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXsm}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, red, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle X$};
        \end{tikzpicture}
      }
      ]
      {}  4 &  7 \\
      {} -3 & -5
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixDn}{
    \begin{bNiceMatrix}[
      , small
      , code-before = {\hlDiag####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, blu, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle D^n$};
        \end{tikzpicture}
      }
      ]
      {} (-1)^n & 0   \\
      {}  0     & 2^n
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixXism}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm, red, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle X^{-1}$};
        \end{tikzpicture}
      }
      ]
      {} -5 & -7 \\
      {}  3 &  4
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the diagonalization
    \[
      \MatrixA<2->
      =
      \MatrixX<4->
      \MatrixD<3->
      \MatrixXi<4->
    \]
    \onslide<5->{The $n$th power of $A$ is}
    \[
      \onslide<5->{
        \scriptstyle
        \MatrixAn<6->
        =
        \MatrixXsm<8->
        \MatrixDn<7->
        \MatrixXism<8->
      }
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlUpper<2->}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      0 & 1 \\
      0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {
        \hlEntry<3->[red]{1}{1}
        \hlEntry<3->[red]{2}{2}
      }
      ]
      \alt<4->{0}{\lambda_1} & 0        \\
      0                      & \alt<4->{0}{\lambda_2}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{bNiceMatrix}[margin]
      \Block{2-2}{X} & \\
      &
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixXi}{
    \begin{bNiceMatrix}[margin]
      \Block{2-2}{X^{-1}} & \\
      &
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixZ}{
    \begin{bNiceMatrix}
      0 & 0 \\
      0 & 0
    \end{bNiceMatrix}
  }
  \begin{alertblock}{Warning}
    Some matrices are \emph{\red{not diagonalizable}}.
    \[
      \MatrixA
      \alt<6->{\neq}{=}
      X
      \MatrixD
      X^{-1}
      \onslide<5->{=\MatrixZ}
    \]
    \onslide<7->{This $A$ is not similar to any diagonal matrix.}
  \end{alertblock}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \overset{A}{
      \begin{bNiceMatrix}[r]
        {}   4 &  11 &  11 \\
        {} -33 & -40 & -33 \\
        {}  33 &  33 &  26
      \end{bNiceMatrix}
    }
  }
  \newcommand{\MatrixX}{
    \overset{X}{
      \begin{bNiceMatrix}[
        , r
        , margin
        , code-before = {
          \hlCol<5->{1}
          \hlCol<5->{2}
          \hlCol<8->[grn]{3}
        }
        ]
        {}  1 & -1 &  1 \\
        {} -1 &  2 & -3 \\
        {}  0 & -1 &  3
      \end{bNiceMatrix}
    }
  }
  \newcommand{\MatrixD}{
    \overset{D}{
      \begin{bNiceMatrix}[
        , r
        , margin
        , code-before = {
          \hlDiagN<6->{2}
          \hlEntry<9->[grn]{3}{3}
        }
        ]
        {} -7 &  0 & 0 \\
        {}  0 & -7 & 0 \\
        {}  0 &  0 & 4
      \end{bNiceMatrix}
    }
  }
  \newcommand{\MatrixXi}{
    \overset{X^{-1}}{
      \begin{bNiceMatrix}[
        , r
        ]
        3 & 2 & 1 \\
        3 & 3 & 2 \\
        1 & 1 & 1
      \end{bNiceMatrix}
    }
  }
  \begin{example}
    Consider the diagonalization
    \[
      \MatrixA
      =
      \MatrixX
      \MatrixD
      \MatrixXi
    \]
    \onslide<2->{The eigenspaces of $D$ are}
    \begin{align*}
      \onslide<2->{\mathcal{E}_D(-7) = \Span\Set{\nv{1 0 0}, \nv{0 1 0}}} && \onslide<2->{\mathcal{E}_D(4) = \Span\Set{\nv{0 0 1}}}
    \end{align*}
    \onslide<3->{The eigenspaces of $A$ are then}
    \begin{align*}
      \onslide<3->{\mathcal{E}_A(-7) = \Span\Set{\nv[small, code-before={\hlMat<4->[blu, th]}]{1 -1 0}, \nv[small, code-before={\hlMat<4->[blu, th]}]{-1 2 -1}}} && \onslide<3->{\mathcal{E}_A(4) = \Span\Set{\nv[small, code-before={\hlMat<7->[grn, th]}]{1 -3 3}}}
    \end{align*}
  \end{example}

\end{frame}


\section{Diagonalizability}
\subsection{Criteria}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}[Tests for Diagonalizability]
    An $n\times n$ matrix $A$ is diagonalizable if and only if
    \[
      \begin{tikzpicture}[remember picture]
        \node
        {%
          \(
          \gm_A(\subnode{l1}{\textcolor<2->{red}{\lambda_1}})+
          \gm_A(\subnode{l2}{\textcolor<2->{red}{\lambda_2}})+
          \dotsb+
          \gm_A(\subnode{lk}{\textcolor<2->{red}{\lambda_k}})
          =
          n
          \)
        };

        \draw[<-, red, thick, overlay, visible on=<2->] (lk.north) |- ++(3mm, 2.5mm)
        node[right] (evals) {$\scriptstyle \EVals(A)=\Set{\lambda_1, \lambda_2, \dotsc, \lambda_k}$};

        \draw[<-, red, thick, overlay, visible on=<2->] (l1.north) |- (evals);
        \draw[<-, red, thick, overlay, visible on=<2->] (l2.north) |- (evals);
      \end{tikzpicture}
    \]
    or (equivalently) if each eigenvalue $\lambda$ satisfies
    $\gm_A(\lambda)=\am_A(\lambda)$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{array}{c}
      \begin{tikzpicture}[outer sep=0, inner sep=0]
        \node[alt=<5->{draw opacity=1}{draw opacity=0}, draw=red, ultra thick, cross out]
        {%
          \(
          \begin{bNiceMatrix}[
            , r
            ]
            {} -2 & -11 & -11 \\
            {} -1 &  -3 &  -1 \\
            {}  1 &  12 &  10
          \end{bNiceMatrix}
          \)
        };
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\EValsA}{
    \begin{NiceArray}{r}[
      , code-after = {
        \begin{tikzpicture}[<-, thick, red]
          \draw[visible on=<2->] ($ (row-1-|col-2)!0.5!(row-2-|col-2) $) -| ++(2mm, 2mm)
          node[above] {$\scriptstyle \gm_A(-2)=1$};
          \draw[visible on=<3->] ($ (row-2-|col-2)!0.5!(row-3-|col-2) $) -- ++(3mm, 0)
          node[right] {$\scriptstyle \gm_A(9)=1$};
        \end{tikzpicture}
      }
      ]
      \mathcal{E}_A(-2) = \Span\Set{\begin{bmatrix}0 & 1 & -1\end{bmatrix}^\intercal} \\
      \mathcal{E}_A(9)  = \Span\Set{\begin{bmatrix}1 & 0 & -1\end{bmatrix}^\intercal}
    \end{NiceArray}
  }
  \begin{example}
    This $3\times 3$ matrix $A$ has $\EVals(A)=\Set{-2, 9}$.
    \begin{align*}
      A=\MatrixA && \EValsA
    \end{align*}
    \onslide<4->{Here, $\gm_A(-2)+\gm_A(9)=2<3$, so $A$ is \emph{\red{not diagonalizable}}.}
  \end{example}

\end{frame}


\subsection{Method}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixD}{
    \overset{D}{
      \begin{bNiceMatrix}
        {} \lambda_1 &        &           \\
        {}           & \Ddots &           \\
        {}           &        & \lambda_n
      \end{bNiceMatrix}
    }
  }
  \begin{theorem}
    Suppose that $A$ is diagonalizable so that
    \[
      \begin{tikzpicture}[remember picture]
        \node
        {%
          \(
          A
          =
          \subnode{X}{\textcolor<2->{blu}{X}}
          \MatrixD
          X^{-1}
          \)
        };

        \draw[<-, thick, blu, overlay, visible on=<2->] (X.south) |- ++(-3mm, -2.5mm)
        node[left] {$\scriptstyle\Col_j\in\mathcal{E}_A(\lambda_j)$};
      \end{tikzpicture}
    \]
    Then the columns of $X$ are linearly independent eigenvectors of $A$.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} -2 &  5 & -5 \\
      {} -5 &  8 & -5 \\
      {}  5 & -5 &  8
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixX}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<5->{1}
        \hlCol<7->{2}
        \hlCol<10->[grn]{3}
      }
      ]
      {} \onslide<5->{ 1} & \onslide<7->{0} & \onslide<10->{ 1} \\
      {} \onslide<5->{ 0} & \onslide<7->{1} & \onslide<10->{ 1} \\
      {} \onslide<5->{-1} & \onslide<7->{1} & \onslide<10->{-1}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixXa}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol{1}
        \hlCol[grn]{2}
        \hlCol{3}
      }
      ]
      {}  1 &  1 & 0 \\
      {}  0 &  1 & 1 \\
      {} -1 & -1 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixXb}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol[grn]{1}
        \hlCol{2}
        \hlCol{3}
      }
      ]
      {}  1 &  1 & 0 \\
      {}  1 &  0 & 1 \\
      {} -1 & -1 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixXc}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol[grn]{1}
        \hlCol{2}
        \hlCol{3}
      }
      ]
      {}  1 & 0 &  1 \\
      {}  1 & 1 &  0 \\
      {} -1 & 1 & -1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlDiagN<8->[blu]{2}
        \hlEntry<11->[grn]{3}{3}
      }
      ]
      {} 3 &   &   \\
      {}   & 3 &   \\
      {}   &   & 8
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixDa}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry{1}{1}
        \hlEntry[grn]{2}{2}
        \hlEntry{3}{3}
      }
      ]
      {} 3 &   &   \\
      {}   & 8 &   \\
      {}   &   & 3
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixDb}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry[grn]{1}{1}
        \hlDiagM{2}
      }
      ]
      {} 8 &   &   \\
      {}   & 3 &   \\
      {}   &   & 3
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the matrix $A$, which diagonalizes as $A=XDX^{-1}$.
    \begin{align*}
      A=\MatrixA && X=\only<-11>{\MatrixX}\only<12>{\MatrixXa}\only<13>{\MatrixXb}\only<14>{\MatrixXc} && D=\only<-11>{\MatrixD}\only<12>{\MatrixDa}\only<13->{\MatrixDb}
    \end{align*}
    \onslide<2->{The eigenspaces of $A$ are}
    \begin{align*}
      \onslide<2->{\mathcal{E}_A(3) &= \Span\Set{\nv[margin, code-before = {\hlMat<4->}]{1 0 -1}, \nv[margin, code-before = {\hlMat<6->}]{0 1 1}}} \\
      \onslide<2->{\mathcal{E}_A(8) &= \Span\Set{\nv[margin, code-before = {\hlMat<9->[grn]}]{1 1 -1}}}
    \end{align*}
    \onslide<3->{These eigenvectors can be used to construct the columns of $X$. }
  \end{example}

\end{frame}

\end{document}
