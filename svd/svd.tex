\documentclass[]{bmr}

\usepackage{sgtx}

\title{SVD}

\usepackage{caption}
\usepackage{xfp}
\ExplSyntaxOn
\NewDocumentCommand{\filesize}{O{B}m}
{
  \fpeval{ round( \pdffilesize { #2 } / \fp_use:c { c_brian_#1_fp } , 2) }
  \,#1
}

\fp_const:Nn \c_brian_B_fp { 1 }
\fp_const:Nn \c_brian_KiB_fp { 1024 }
\fp_const:Nn \c_brian_MiB_fp { 1024*1024 }
\fp_const:Nn \c_brian_GiB_fp { 1024*1024*1024 }
\ExplSyntaxOff

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}


\section{Image Compression}
\subsection{Cat Picture}
\newcommand{\mymocha}[2]{
  \begin{frame}

    \frametitle{\secname}
    \framesubtitle{\subsecname}

    \begin{figure}[h]
      \centering
      \includegraphics[height=.725\textheight]{./mocha/#1}
      \caption*{#2 (\filesize[KiB]{./mocha/#1})}
      \label{fig:color}
    \end{figure}

  \end{frame}
}

% \mymocha{mocha.jpg}{Uncompressed Color Image}
\mymocha{mocha-grayed.jpg}{Uncompressed Gray Image}
\mymocha{mocha-compressed-500.jpg}{$N=500$ Compression}
\mymocha{mocha-compressed-400.jpg}{$N=400$ Compression}
\mymocha{mocha-compressed-300.jpg}{$N=300$ Compression}
\mymocha{mocha-compressed-200.jpg}{$N=200$ Compression}
\mymocha{mocha-compressed-100.jpg}{$N=100$ Compression}
\mymocha{mocha-compressed-50.jpg}{$N=50$ Compression}
\mymocha{mocha-compressed-25.jpg}{$N=25$ Compression}
\mymocha{mocha-compressed-10.jpg}{$N=10$ Compression}
\mymocha{mocha-compressed-5.jpg}{$N=5$ Compression}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{table}[h]
    \centering
    \begin{tabular}{lr}
      Image & Size \\ \hline
      % Uncompressed (Color) & \filesize[KiB]{./mocha/mocha.jpg} \\
      Uncompressed         & \filesize[KiB]{./mocha/mocha-grayed.jpg} \\
      Compressed ($N=500$) & \filesize[KiB]{./mocha/mocha-compressed-500.jpg} \\
      Compressed ($N=400$) & \filesize[KiB]{./mocha/mocha-compressed-400.jpg} \\
      Compressed ($N=300$) & \filesize[KiB]{./mocha/mocha-compressed-300.jpg} \\
      Compressed ($N=200$) & \filesize[KiB]{./mocha/mocha-compressed-200.jpg} \\
      Compressed ($N=100$) & \filesize[KiB]{./mocha/mocha-compressed-100.jpg} \\
      Compressed ($N=50$)  & \filesize[KiB]{./mocha/mocha-compressed-50.jpg} \\
      Compressed ($N=25$)  & \filesize[KiB]{./mocha/mocha-compressed-25.jpg} \\
      Compressed ($N=10$)  & \filesize[KiB]{./mocha/mocha-compressed-10.jpg} \\
      Compressed ($N=5$)   & \filesize[KiB]{./mocha/mocha-compressed-5.jpg}
    \end{tabular}
  \end{table}

\end{frame}


\subsection{Factorizations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Rectangle}{
    \begin{array}{c}
      \begin{tikzpicture}
        \pgfmathsetmacro{\Width}{24mm}
        \pgfmathsetmacro{\Height}{9/16 * \Width}
        \node[fill=red, minimum width=\Width,minimum height=\Height] (rect)  {};
        \node[above] at (rect.north) {\scriptsize $3840$ pixels};
        \node[left, rotate=90, anchor=south] at (rect.west) {\scriptsize $2160$ pixels};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceArray}{cWc{1cm}c}[
      , margin
      , code-before = {\hlMat<3->[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-1mm, 3.5mm)
          node[left] {$\scriptstyle3840\cdot2160=8294400$};
          \draw[visible on=<6->] ($ (row-4-|col-1)!0.5!(row-4-|col-4) $) |- ++(3mm, -2.5mm)
          node[right] {$\scriptstyle\rank=1$};
        \end{tikzpicture}
      }
      ]
      r      & \Cdots & r \\
      \Vdots & \Ddots & \Vdots \\
      r      & \Cdots & r
    \end{bNiceArray}
  }
  \newcommand{\Factor}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<2->]
        \node[alt=<4->{fill=redbg, ultra thick, draw, rounded corners}{}]
        (f)
        {\(
          \begin{bNiceMatrix}[c]
            1\\ \Vdots\\ 1
          \end{bNiceMatrix}
          \begin{bNiceMatrix}[c]
            r & \Cdots & r
          \end{bNiceMatrix}
          \)};

        \draw[<-, thick, red, overlay, shorten <=0.5mm, visible on=<4->] (f.north) |- ++(1mm, 3mm)
        node[right] {$\scriptstyle 3840+2160=6000$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    A 4k UHD moniter has resolution $2160\times 3840$.
    \[
      \Rectangle
      \leftrightsquigarrow
      \MatrixR
      =
      \Factor
    \]
    \onslide<5->{By factoring, we ``compress'' our data without sacrificing
      quality!}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Sweden}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , scale=0.333
        ]
        % http://www.schemecolor.com/sweden-flag-colors.php

        % Name: Medium Persian Blue
        % Hex: #006AA7
        % RGB: (0, 106, 167)
        % CMYK: 1, 0.365, 0, 0.345

        % Name: Philippine Yellow
        % Hex: #FECC00
        % RGB: (254, 204, 0)
        % CMYK: 0, 0.196, 1, 0.003

        % \definecolor{sblu}{HTML}{006AA7}
        % \definecolor{syel}{HTML}{FECC00}

        \fill[sblu] (0, 0) rectangle (16, 10);
        \fill[syel] (5, 10) rectangle (7, 0);
        \fill[syel] (0, 4) rectangle (16, 6);

        \path (0, 10) -- (5, 10) node[midway, above] {$\scriptstyle5\,n$};
        \path (5, 10) -- (7, 10) node[midway, above] {$\scriptstyle2\,n$};
        \path (7, 10) -- (16, 10) node[midway, above] {$\scriptstyle9\,n$};

        \path (0, 0) -- (0, 4) node[midway, left] {$\scriptstyle4\,n$};
        \path (0, 4) -- (0, 6) node[midway, left] {$\scriptstyle2\,n$};
        \path (0, 6) -- (0, 10) node[midway, left] {$\scriptstyle4\,n$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\MatrixS}{
    \NiceMatrixOptions{parallelize-diags=false}
    \begin{bNiceArray}{cccccccWc{2cm}c}[
      , small
      , code-before = {
        \hlPortion<2->[sblu, th]{1}{1}{3}{3}
        \hlPortion<2->[sblu, th]{1}{7}{3}{9}
        \begin{tikzpicture}[hl, th, visible on=<2->]
          \draw[fill=syelbg]
          (row-1-|col-4) |-
          (row-4-|col-1) |-
          (row-7-|col-4) |-
          (row-10-|col-7) |-
          (row-7-|col-10) |-
          (row-4-|col-7) --
          (row-1-|col-7) --
          cycle;
        \end{tikzpicture}
        \hlPortion<2->[sblu, th]{7}{1}{9}{3}
        \hlPortion<2->[sblu, th]{7}{7}{9}{9}
      }
      , code-after = {
        \begin{tikzpicture}[visible on=<3->]
          \draw[<-, thick, sblu, shorten <=0.5mm]
          ($ (row-1-|col-1)!0.5!(row-1-|col-10) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle\rank=2$};
        \end{tikzpicture}
      }
      ]
      b      & \Cdots & b      & y      & \Cdots & y      & b      & \Cdots & b      \\
      \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots \\
      b      & \Cdots & b      & y      & \Cdots & y      & b      & \Cdots & b      \\
      y      & \Cdots & y      & y      & \Cdots & y      & y      & \Cdots & y      \\
      \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots \\
      y      & \Cdots & y      & y      & \Cdots & y      & y      & \Cdots & y      \\
      b      & \Cdots & b      & y      & \Cdots & y      & b      & \Cdots & b      \\
      \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots \\
      b      & \Cdots & b      & y      & \Cdots & y      & b      & \Cdots & b
    \end{bNiceArray}
  }
  \begin{example}
    The Swedish flag is slightly more complicated than a solid image.
    \[
      \Sweden\leftrightsquigarrow\MatrixS
    \]
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixS}{
    \NiceMatrixOptions{parallelize-diags=false}
    \begin{bNiceArray}{ccccccccc}[
      , small
      , code-before = {
        \hlPortion[sblu, th]{1}{1}{3}{3}
        \hlPortion[sblu, th]{1}{7}{3}{9}
        \begin{tikzpicture}[hl, th]
          \draw[fill=syelbg]
          (row-1-|col-4) |-
          (row-4-|col-1) |-
          (row-7-|col-4) |-
          (row-10-|col-7) |-
          (row-7-|col-10) |-
          (row-4-|col-7) --
          (row-1-|col-7) --
          cycle;
        \end{tikzpicture}
        \hlPortion[sblu, th]{7}{1}{9}{3}
        \hlPortion[sblu, th]{7}{7}{9}{9}
      }
      ]
      b      & \Cdots & b      & y      & \Cdots & y      & b      & \Cdots & b      \\
      \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots \\
      b      & \Cdots & b      & y      & \Cdots & y      & b      & \Cdots & b      \\
      y      & \Cdots & y      & y      & \Cdots & y      & y      & \Cdots & y      \\
      \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots \\
      y      & \Cdots & y      & y      & \Cdots & y      & y      & \Cdots & y      \\
      b      & \Cdots & b      & y      & \Cdots & y      & b      & \Cdots & b      \\
      \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots & \Vdots & \Ddots & \Vdots \\
      b      & \Cdots & b      & y      & \Cdots & y      & b      & \Cdots & b
    \end{bNiceArray}
  }
  \newcommand{\MatrixUa}{
    \begin{bNiceMatrix}[
      , small
      ]
      1\\ \Vdots\\ 1\\ 0\\ \Vdots\\ 0\\ 1\\ \Vdots\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixVa}{
    \begin{bNiceMatrix}[
      , small
      , code-before = {
        \hlPortion[sblu, th]{1}{1}{1}{3}
        \hlPortion[syelbg, th]{1}{4}{1}{6}
        \hlPortion[sblu, th]{1}{7}{1}{9}
      }
      ]
      b & \Cdots & b & y & \Cdots & y & b & \Cdots & b
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixUb}{
    \begin{bNiceMatrix}[small]
      0\\ \Vdots\\ 0\\ 1\\ \Vdots\\ 1\\ 0\\ \Vdots\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixVb}{
    \begin{bNiceMatrix}[
      , small
      , code-before = {
        \hlPortion[syelbg, th]{1}{1}{1}{3}
      }
      ]
      y & \Cdots & y
    \end{bNiceMatrix}
  }
  \begin{example}
    When $\rank=2$, our ``compression'' requires two terms.
    \[
      \MatrixS
      \alt<3->{\approx}{=}
      \MatrixUa
      \MatrixVa
      \onslide<-2>{
        +
        \MatrixUb
        \MatrixVb
      }
    \]
    \onslide<2->{We can \emph{approximate} our image by dropping a term.}
  \end{example}

\end{frame}


\section{SVD}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{definition}
    A \emph{\grn{singular value decomposition}} is a factorization of the form
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(
          \subnode{j4809}{\textcolor<2->{red}{A}}
          =
          \subnode{j4808}{\textcolor<3->{blu}{U}}
          \subnode{j4807}{\textcolor<4->{red}{\Sigma}}
          \subnode{j4806}{\textcolor<5->{grn}{V}}^\ast
          \)};

        \draw[<-, thick, red, overlay, visible on=<2->] (j4809.south) |- ++(-3mm, -2.5mm)
        node[left] {\scriptsize $m\times n$ with rank $r$};

        \draw[<-, thick, blu, overlay, visible on=<3->] (j4808.south) |- ++(-1mm, -6mm)
        node[left] {\scriptsize $m\times r$ with $U^\ast U=I_r$};

        \draw[<-, thick, red, overlay, visible on=<4->] (j4807.south) |- ++(1mm, -10mm)
        node[right, scale=0.5] (m) {$\Sigma=\begin{bNiceMatrix} \sigma_1 & & \\ & \Ddots & \\ & & \sigma_r\end{bNiceMatrix}$};

        \draw[<-, thick, grn, overlay, visible on=<5->] (j4806.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize $n\times r$ with $V^\ast V=I_r$};

        \node[below] at (m.west) {\phantom{m}};
      \end{tikzpicture}
    \]
    \onslide<6->{The scalars $\sigma_1\geq\dotsb\geq\sigma_r>0$ are the
      \emph{\grn{singular values}} of $A$ and the sum}
    \[
      \onslide<6->{
        A
        \approx
        \sigma_1\cdot\bv[u]_1\bv[v]_1^\ast
        +
        \dotsb
        +
        \sigma_k\cdot\bv[u]_k\bv[v]_k^\ast
      }
    \]
    \onslide<6->{is the \emph{\grn{rank $k$ approximation}} of $A$.}
  \end{definition}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      3 & 2 &  2 \\
      2 & 3 & -2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixAs}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      3 & 2 &  2 \\
      2 & 3 & -2
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<3-5>
        \hlCol<9-13,17->{1}
        \hlCol<12-13>{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle U$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}} &  \sfrac{1}{\sqrt{2}} \\
      \sfrac{1}{\sqrt{2}} & -\sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlDiag<4-5>[red]
        \hlEntry<8-13,16->[red]{1}{1}
        \hlEntry<11-13>[red]{2}{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \Sigma$};
        \end{tikzpicture}
      }
      ]
      {} 5 &   \\
      {}   & 3
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixVT}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<5>[grn]
        \hlRow<10-13,18->[grn]{1}
        \hlRow<13>[grn]{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=<5->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle V^\ast$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}}  &  \sfrac{1}{\sqrt{2}}  & 0 \\
      \sfrac{1}{\sqrt{18}} & -\sfrac{1}{\sqrt{18}} & \sfrac{4}{\sqrt{18}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\ColUa}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[u]_1$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}}\\ \sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\ColUb}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[u]_2$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}}\\ -\sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\RowVa}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[v]_1^\ast$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}} & \sfrac{1}{\sqrt{2}} & 0
    \end{bNiceMatrix}
  }
  \newcommand<>{\RowVb}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[v]_2^\ast$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{18}} & -\sfrac{1}{\sqrt{18}} & \sfrac{4}{\sqrt{18}}
    \end{bNiceMatrix}
  }
  \newcommand{\RankOne}{
    \begin{bNiceMatrix}[r, small]
      \sfrac{5}{2} & \sfrac{5}{2} & 0 \\
      \sfrac{5}{2} & \sfrac{5}{2} & 0
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the singular value decomposition
    \[
      \MatrixA<2->
      =
      \MatrixU
      \MatrixS
      \MatrixVT
    \]
    \onslide<6->{Our ``compressed'' sum is}
    \[
      \begin{tikzpicture}[remember picture, visible on=<6->]
        \node{\(\scriptstyle
          \MatrixAs<7->
          =
          \subnode{s9991}{5}\cdot
          \ColUa<9->
          \RowVa<10->
          +
          \subnode{s9992}{3}\cdot
          \ColUb<12->
          \RowVb<13->
          \)};

        \hlNodeth<8->[red]{s9991}
        \hlNodeth<11->[red]{s9992}
      \end{tikzpicture}
    \]
    \onslide<14->{The rank one approximation of $A$ is}
    \[
      \begin{tikzpicture}[remember picture, visible on=<14->]
        \node{\(\scriptstyle
          \MatrixAs<15->
          \approx
          \subnode{s81}{5}\cdot
          \ColUa<17->
          \RowVa<18->
          \onslide<19->{
            =
            \RankOne
          }
          \)};

        \hlNodeth<16->[red]{s81}
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}

\section{Properties}
\subsection{Singular Vectors}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\CalcA}{
    \begin{tikzpicture}[remember picture, visible on=<2->]
      \node{\(AV = U\Sigma \subnode{vastv}{\textcolor<3>{red}{\onslide<2-3>{V^\ast V}}}\)};

      \draw<3>[<-, thick, red, overlay] (vastv.south) |- ++(-3mm, -3mm)
      node[left, red] {$\scriptstyle V^\ast V=I_r$};
    \end{tikzpicture}
  }
  \newcommand{\CalcB}{
    \begin{tikzpicture}[remember picture, visible on=<5->]
      \node{\(A^\ast U = V\subnode{sigmaast}{\alt<7->{\Sigma}{\textcolor<6>{red}{\Sigma^\ast}}}
        \subnode{uastu}{\onslide<5-8>{\textcolor<8>{red}{U^\ast U}}}\)};

      \draw<6>[<-, thick, red, overlay] (sigmaast.south) |- ++(-3mm, -3mm)
      node[left, red] {$\scriptstyle \Sigma^\ast=\Sigma$};

      \draw<8>[<-, thick, red, overlay] (uastu.south) |- ++(-3mm, -3mm)
      node[left, red] {$\scriptstyle U^\ast U=I_r$};
    \end{tikzpicture}
  }


  \newcommand{\RightSing}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<10->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$A\bv_j=\sigma_j\cdot\bu_j$};
        % \node[above, grn] at (text.north) {Right Sing Vector};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\LeftSing}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<10->]
        \node[fill=blubg, draw, rounded corners, ultra thick]
        (text) {$A^\ast\bu_j=\sigma_j\cdot\bv_j$};
        % \node[above, blu] at (text.north) {Left Sing Vector};
      \end{tikzpicture}
    \end{array}
  }

  \newcommand{\Spectral}{
    \begin{tikzpicture}[remember picture, baseline=(sp.base), inner sep=0pt]
      \node(sp){\(A^\ast A = V\alt<14->{\Sigma^2}{\Sigma} \alt<13->{}{\textcolor<12>{red}{\subnode{uastuagain}{U^\ast U}}}\alt<14->{}{\Sigma} V^\ast\)};

      \draw<12>[<-, thick, red, overlay] (uastuagain.south) |- ++(-3mm, -3mm)
      node[left, red] {$\scriptstyle U^\ast U=I_r$};
    \end{tikzpicture}
  }

  \newcommand{\EigenStuff}{
    \begin{tikzpicture}[remember picture, visible on=<15->]
      \node {\(\subnode{gramianA}{\textcolor<17->{grn}{A^\ast A}}\subnode{vja}{\textcolor<16->{red}{\bv_j}} = \subnode{myl}{\textcolor<18->{blu}{\sigma_j^2}}\cdot \subnode{vjb}{\textcolor<16->{red}{\bv_j}}\)};

      \draw<16->[<-, thick, red, overlay] (vja.south) |- ++(2cm, -2mm)
      node[right, red, scale=0.75] {$j$th col of $V$ is an eigenvector of};

      \draw<16->[<-, thick, red, overlay] (vjb.south) |- ++(2mm, -2mm);
      % node[right, red, scale=0.75] {$j$th col of $V$ is an eigenvector of};

      \draw<17->[<-, thick, grn, overlay] (gramianA.south) |- ++(-3mm, -2mm)
      node[left, grn, scale=0.75] {the Gramian of $A$};

      \draw<18->[<-, thick, blu, overlay] (myl.north) |- ++(3mm, 2mm)
      node[right, blu, scale=0.75] {with eigenvalue $\sigma_j^2$};
    \end{tikzpicture}
  }

  \begin{theorem}
    Consider a singular value decomposition $A=U\Sigma V^\ast$.
    \begin{align*}
      \CalcA && \CalcB
    \end{align*}
    \onslide<10->{Cols of $V$ are \emph{\grn{right singular vectors}} and cols of
      $U$ are \emph{\blu{left singular vectors}}.}
    \begin{align*}
      \RightSing && \LeftSing
    \end{align*}
    \onslide<11->{Moreover, \Spectral } \onslide<15->{is a spectral factorization and}
    \[
      \EigenStuff
    \]
    \onslide<19->{The singular values of $A$ are the square roots of the
      eigenvalues of $A^\ast A$.}
  \end{theorem}

  % Consider a singular value decomposition
  % \[
  %   A
  %   =
  %   \MatrixU<2->
  %   \MatrixS<3->
  %   \MatrixVT<4->
  % \]
  % \onslide<5->{The columns of $U$ and $V$ are related by the formulas}
  % \begin{align*}
  %   \onslide<5->{A\bv_j = \sigma_j\cdot\bv[u]_j} && \onslide<5->{A^\ast\bv[u]_j=\sigma_j\cdot\bv_j} && \onslide<6->{A^\ast A\bv_j = \sigma_j^2\cdot\bv_j}
  % \end{align*}
  % \onslide<7->{Moreover, $A^\ast=V\Sigma U^\ast$ is a singular value
  % decomposition of $A^\ast$.}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      3 & 2 &  2 \\
      2 & 3 & -2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixAs}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      3 & 2 &  2 \\
      2 & 3 & -2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixAT}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle A^\ast$};
        \end{tikzpicture}
      }
      ]
      3 &  2 \\
      2 &  3 \\
      2 & -2
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<3-5>
        \hlCol<10,13-14>{1}
        \hlCol<18,21-22>{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle U$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}} &  \sfrac{1}{\sqrt{2}} \\
      \sfrac{1}{\sqrt{2}} & -\sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixS}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlDiag<4->[red]
        % \hlEntry<6->[red]{1}{1}
        % \hlEntry<6->[red]{2}{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \Sigma$};
        \end{tikzpicture}
      }
      ]
      {} 5 &   \\
      {}   & 3
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixVT}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlMat<5>[grn]
        \hlRow<9-10,14>[grn]{1}
        \hlRow<17-18,22>[grn]{2}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=<5->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle V^\ast$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}}  &  \sfrac{1}{\sqrt{2}}  & 0 \\
      \sfrac{1}{\sqrt{18}} & -\sfrac{1}{\sqrt{18}} & \sfrac{4}{\sqrt{18}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\ColUa}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[u]_1$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}}\\ \sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\ColUb}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv[u]_2$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}}\\ -\sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\ColVa}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv_1$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}} \\ \sfrac{1}{\sqrt{2}} \\ 0
    \end{bNiceMatrix}
  }
  \newcommand<>{\ColVb}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat####1[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \bv_2$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{18}} \\ -\sfrac{1}{\sqrt{18}} \\ \sfrac{4}{\sqrt{18}}
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the singular value decomposition
    \[
      \MatrixA<2->
      =
      \MatrixU
      \MatrixS
      \MatrixVT
    \]
    \onslide<6->{The columns of $U$ and $V$ are related by the formulas}
    \begin{align*}
      \onslide<7->{\scriptstyle\MatrixAs<8->\ColVa<9->=5\cdot\ColUa<10->}    && \onslide<11->{\scriptstyle\MatrixAT<12->\ColUa<13->=5\cdot\ColVa<14->} \\[1em]
      \onslide<15->{\scriptstyle\MatrixAs<16->\ColVb<17->=3\cdot\ColUb<18->} && \onslide<19->{\scriptstyle\MatrixAT<20->\ColUb<21->=3\cdot\ColVb<22->}
    \end{align*}
  \end{example}

\end{frame}


\subsection{Projections}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    An SVD can be used to project onto the four fundamental subspaces.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\PerpLength}{0.25}
        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , visible on=<1->
        ] {$\scriptstyle\Col(A^\ast)$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        , visible on=<1->
        ] {$\scriptstyle\Null(A)$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^n$};

        \draw[LeftSpace, visible on=<1->] (0, 0) rectangle (-\PerpLength,\PerpLength);

        \begin{scope}[xshift=6cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , visible on=<1->
          ] {$\scriptstyle\Col(A)$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          , visible on=<1->
          ] {$\scriptstyle\Null(A^\ast)$};

          \draw[RightSpace, visible on=<1->] (0, 0) rectangle (\PerpLength,\PerpLength);

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A=U\Sigma V^\ast$};

        % \draw[->, thick, visible on=<1->] (Rm.south west) to[bend left]
        % node[midway, below] {$\scriptstyle A^\ast$} (Rn.south east);

        \draw[Point, blu, visible on=<2->]
        (Col.north) |- ++(3mm, 2.5mm)
        node[right, scale=1.00] {$\scriptstyle P_{\Col(A)}=UU^\ast$};

        \draw[Point, blu, visible on=<3->]
        (Row.north) |- ++(-3mm, 2.5mm)
        node[left, scale=1.00] {$\scriptstyle P_{\Col(A^\ast)}=VV^\ast$};

        \draw[Point, red, visible on=<4->]
        (LNull.north) |- ++(2mm, 2.5mm)
        node[right, scale=1.00] {$\scriptstyle P_{\Null(A^\ast)}=I_m-UU^\ast$};

        \draw[Point, grn, visible on=<5->]
        (Null.north) |- ++(-2mm, 2.5mm)
        node[left, scale=1.00] {$\scriptstyle P_{\Null(A)}=I_n-VV^\ast$};

      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle A$};
        \end{tikzpicture}
      }
      ]
      3 & 2 &  2 \\
      2 & 3 & -2
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1}
      , code-after = {
        \begin{tikzpicture}[<-, thick, blu, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle U$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}} &  \sfrac{1}{\sqrt{2}} \\
      \sfrac{1}{\sqrt{2}} & -\sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixS}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlDiag####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle \Sigma$};
        \end{tikzpicture}
      }
      ]
      {} 5 &   \\
      {}   & 3
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixVT}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle V^\ast$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}}  &  \sfrac{1}{\sqrt{2}}  & 0                    \\
      \sfrac{1}{\sqrt{18}} & -\sfrac{1}{\sqrt{18}} & \sfrac{4}{\sqrt{18}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixV}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, grn, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-3) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle V$};
        \end{tikzpicture}
      }
      ]
      \sfrac{1}{\sqrt{2}}  &  \sfrac{1}{\sqrt{18}} \\
      \sfrac{1}{\sqrt{2}}  & -\sfrac{1}{\sqrt{18}} \\
      0                    &  \sfrac{4}{\sqrt{18}}
    \end{bNiceMatrix}
  }
  \newcommand<>{\MatrixP}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlMat####1[red]}
      , code-after = {
        \begin{tikzpicture}[<-, thick, red, shorten <=0.5mm, visible on=####1]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm) node[left] {$\scriptstyle P_{\Col(A^\ast)}$};
        \end{tikzpicture}
      }
      ]
      \sfrac{5}{9} &  \sfrac{4}{9} &  \sfrac{2}{9} \\
      \sfrac{4}{9} &  \sfrac{5}{9} & -\sfrac{2}{9} \\
      \sfrac{2}{9} & -\sfrac{2}{9} &  \sfrac{8}{9}
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the singular value decomposition
    \[
      \MatrixA<2->
      =
      \MatrixU<2->
      \MatrixS<2->
      \MatrixVT<2->
    \]
    \onslide<3->{Projection onto $\Col(A^\ast)$ is given by}
    \[
      \onslide<3->{
        \MatrixP<4->
        =
        \MatrixV<4->
        \MatrixVT<4->
      }
    \]
  \end{example}

\end{frame}


\section{Calculating SVD}
\subsection{Procedure}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{The SVD Algorithm}
    The following steps compute $A=U\Sigma V^\ast$.
    \begin{description}[<+->][Step 1]
    \item[Step 1] Order the positive eigenvalues of $A^\ast A$ as
      \[
        \lambda_1\geq\lambda_2\geq\dotsb\geq\lambda_r
      \]
      accounting for algebraic multiplicity.
    \item[Step 2] Use the spectral theorem to factor $A^\ast A$ as
      \[
        A^\ast A=VDV^\ast
      \]
      where $D=\operatorname{diag}(\lambda_1, \lambda_2, \dotsc, \lambda_r)$.
    \item[Step 3] Define $\Sigma=\sqrt{D}$ and $U=AV\Sigma^{-1}$.
    \end{description}
  \end{block}

\end{frame}

\subsection{Example}
\begin{sagesilent}
  A = matrix.column([(0, 1, 1), (1, 1, 0)])
  D = diagonal_matrix([3, 1])
  Sigma = diagonal_matrix(map(sqrt, D.diagonal()))
\end{sagesilent}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\myV}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} \\
        \frac{1}{\sqrt{2}} & \frac{-1}{\sqrt{2}}
      \end{array}
    \right]
  }
  \newcommand{\myVT}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{2}} & \frac{1}{\sqrt{2}} \\
        \frac{1}{\sqrt{2}} & \frac{-1}{\sqrt{2}}
      \end{array}
    \right]
  }
  \newcommand{\mySigmai}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{3}} & 0 \\
        0                  & 1
      \end{array}
    \right]
  }
  \newcommand{\myU}{
    \left[
      \begin{array}{rr}
        \frac{1}{\sqrt{6}} & \frac{-1}{\sqrt{2}} \\
        \frac{2}{\sqrt{6}} & 0                   \\
        \frac{1}{\sqrt{6}} & \frac{1}{\sqrt{2}}
      \end{array}
    \right]
  }
  \begin{example}
    Consider the data
    \begin{gather*}
      \overset{A^\ast}{\sage{A.T}}\overset{A}{\sage{A}}
      = \sage{A.T*A}
      = \overset{V}{\myV}\overset{D}{\sage{D}}\overset{V^\ast}{\myVT}
    \end{gather*}
    \onslide<2->{We then define}
    \begin{gather*}
      \begin{align*}
        \onslide<3->{\Sigma} &\onslide<3->{= \sage{Sigma}} & \onslide<4->{\overset{U}{\myU}} &\onslide<4->{= \overset{A}{\sage{A}}\overset{V}{\myV}\overset{\Sigma^{-1}}{\mySigmai}}
      \end{align*}
    \end{gather*}
    \onslide<5->{This gives our singular value decomposition
      $A=U\Sigma V^\ast$.}
    % \[
    %   \overset{A}{\sage{A}}
    %   =
    %   \overset{U}{\myU}
    %   \overset{\Sigma}{\sage{Sigma}}
    %   \overset{V^\ast}{\myVT}
    % \]
  \end{example}

\end{frame}


\end{document}
