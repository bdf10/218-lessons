% \documentclass[12pt]{article}
\documentclass[17pt]{extarticle}

\usepackage{mth}
\usepackage{sgtx}
\usepackage{fullpage}
\usepackage{extarrows}
\usepackage{resizegather}

\theoremstyle{definition}
\newtheorem*{problem}{Problem}

\title{Singular Value Decomposition Example}
\author{Math 218D-2}
\date{}

\begin{document}

\maketitle

\begin{sagesilent}
  A = matrix(ZZ, [[1, 0, 4], [0, 1, 1], [0, 1, 1], [-2, 5, -3]])
  S = A.T*A
\end{sagesilent}

\begin{problem}
  Consider the matrix $A$ given by
  \[
    A = \sage{A}
  \]
  Find a singular value decomposition $A=U\Sigma V^\ast$. Use your SVD to
  calculate the low-rank approximations of $A$.
\end{problem}

\begin{proof}[Solution]
  First, note that
  \[
    \overset{A^\ast A}{\sage{S}}
    =
    \overset{A^\intercal}{\sage{A.T}}
    \overset{A}{\sage{A}}
  \]
  The characteristic polynomial of $A^\ast A$ is
  \begin{sagesilent}
    var('t')
    chi = t*identity_matrix(S.nrows())-S
    from functools import partial
    e = partial(elementary_matrix, S.nrows())
    chi1 = e(row1=2, row2=1, scale=1)*chi
    chi2 = e(row1=2, scale=1/(t-14))*chi1
    chi3 = e(row1=0, row2=2, scale=10)*e(row1=1, row2=2, scale=-13)*chi2
    chi4 = chi3.matrix_from_rows_and_columns([0, 1], [0, 1])
  \end{sagesilent}
  \newcommand{\StepA}{
    \begin{NiceArray}{rcrcr}[small]
      \bv[r]_1 &+& 10\cdot\bv[r]_3 &\to& \bv[r]_1 \\
      \bv[r]_2 &-& 13\cdot\bv[r]_3 &\to& \bv[r]_2
    \end{NiceArray}
  }
  \begin{align*}
    \chi_{A^\ast A}(t)
    &= \sage{detmat(chi)}                                          \\
    &\xlongequal{\bv[r]_3+\bv[r]_2\to\bv[r]_3} \sage{detmat(chi1)} \\
    &= (t-14)\cdot\sage{detmat(chi2)}                              \\
    &\xlongequal{\StepA}(t-14)\cdot\sage{detmat(chi3)}             \\
    &= (t-14)\cdot\sage{detmat(chi4)}                              \\
    &= (t-14)\cdot\Set{\sage{chi4.det()}}                          \\
    &= (t-14)\cdot\Set{\sage{expand(chi4.det())}}                  \\
    &= (t-14)\cdot\sage{factor(expand(chi4.det()))}
  \end{align*}
  The eigenspaces of the positive eigenvalues of $A^\ast A$ are
  \begin{sagesilent}
    l1, l2 = 45, 14
    v1 = vector([1, -2, 2])
    v2 = vector([0, 1, 1])
    D = diagonal_matrix([l1, l2])
  \end{sagesilent}
  \begin{align*}
    \mathcal{E}_{A^\ast A}(\sage{l1}) &= \Null\sage{chi(t=l1)} = \Span\Set*{\frac{1}{\sage{v1.norm()}}\cdot\sage{v1.column()}} \\
    \mathcal{E}_{A^\ast A}(\sage{l2}) &= \Null\sage{chi(t=l2)} = \Span\Set*{\frac{1}{\sage{v2.norm()}}\cdot\sage{v2.column()}}
  \end{align*}
  This gives the factorization
  \newcommand{\MatrixV}{
    \begin{bNiceMatrix}[r]
      {} \sfrac{ 1}{3} & 0                   \\
      {} \sfrac{-2}{3} & \sfrac{1}{\sqrt{2}} \\
      {} \sfrac{ 2}{3} & \sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixVT}{
    \begin{bNiceMatrix}[r]
      {} \sfrac{ 1}{3} & \sfrac{-2}{3}       & \sfrac{ 2}{3}       \\
      {} 0             & \sfrac{1}{\sqrt{2}} & \sfrac{1}{\sqrt{2}}
    \end{bNiceMatrix}
  }
  \[
    \overset{A^\ast A}{\sage{S}}
    =
    \overset{V}{\MatrixV}
    \overset{D}{\sage{D}}
    \overset{V^\ast}{\MatrixVT}
  \]
  Now, the formulas $\Sigma=\sqrt{D}$ and $U=AV\Sigma^{-1}$ give
  \newcommand{\MatrixSigma}{
    \begin{bNiceMatrix}[r]
      {} \sqrt{45} &           \\
      {}           & \sqrt{14}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixSigmai}{
    \begin{bNiceMatrix}[r]
      {} \sfrac{1}{\sqrt{45}} &           \\
      {}                      & \sfrac{1}{\sqrt{14}}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[r]
      {} \sfrac{ 3}{\sqrt{45}} & \sfrac{2}{\sqrt{7}} \\
      {} 0                     & \sfrac{1}{\sqrt{7}} \\
      {} 0                     & \sfrac{1}{\sqrt{7}} \\
      {} \sfrac{-6}{\sqrt{45}} & \sfrac{1}{\sqrt{7}}
    \end{bNiceMatrix}
  }
  \begin{align*}
    \Sigma &= \MatrixSigma \\
    \overset{U}{\MatrixU} &= \overset{A}{\sage{A}}\overset{V}{\MatrixV}\overset{\Sigma^{-1}}{\MatrixSigmai}
  \end{align*}
  This gives the singular value decomposition
  \[
    \overset{A}{\sage{A}}
    =
    \overset{U}{\MatrixU}
    \overset{\Sigma}{\MatrixSigma}
    \overset{V^\ast}{\MatrixVT}
  \]
  Our ``compressed'' sum is
  \[
    \overset{A}{\sage{A}}
    = \sqrt{45}\cdot\nvc{{\sfrac{3}{\sqrt{45}}}; {0}; {0}; {\sfrac{-6}{\sqrt{45}}}}\nvc{{\sfrac{1}{3}} {\sfrac{-2}{3}} {\sfrac{2}{3}}}+\sqrt{14}\cdot\nvc{{\sfrac{2}{\sqrt{7}}}; {\sfrac{1}{\sqrt{7}}}; {\sfrac{1}{\sqrt{7}}}; {\sfrac{1}{\sqrt{7}}}}\nvc{0 {\sfrac{1}{\sqrt{2}}} {\sfrac{1}{\sqrt{2}}}}
  \]
  The ``rank one'' approximation is
  \begin{sagesilent}
    A1 = sqrt(45)*vector([3/sqrt(45), 0, 0, -6/sqrt(45)]).outer_product(vector([1/3, -2/3, 2/3]))
  \end{sagesilent}
  \[
    \overset{A}{\sage{A}}
    \approx \sqrt{45}\cdot\nvc{{\sfrac{3}{\sqrt{45}}}; {0}; {0}; {\sfrac{-6}{\sqrt{45}}}}\nvc{{\sfrac{1}{3}} {\sfrac{-2}{3}} {\sfrac{2}{3}}}
    = \sage{A1}\qedhere
  \]
\end{proof}

\end{document}
