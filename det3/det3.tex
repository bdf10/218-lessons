\documentclass[]{bmr}

\title{Determinants III}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Area in $\mathbb{R}^2$}
\subsection{Orientations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\Positive}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , ->
      , visible on=####1
      ]

      \coordinate (O) at (0, 0);
      \coordinate (v) at (15:2.5);
      \coordinate (w) at (55:2);

      \draw[blu] (O) -- (v) node[midway, below, sloped] {$\scriptstyle\bv$};
      \draw[red] (O) -- (w) node[midway, above, sloped] {$\scriptstyle\bv[w]$};

      \draw (15:1) arc(15:55:1) node[midway, right] {$\scriptstyle\theta$};

      \node[above, grn] at (current bounding box.north) {Positively Oriented};
      \node[below] at (current bounding box.south)
      {\scriptsize counterclockwise rotation};

    \end{tikzpicture}
  }
  \newcommand<>{\Negative}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , ->
      , visible on=####1
      ]

      \coordinate (O) at (0, 0);
      \coordinate (w) at (15:2.5);
      \coordinate (v) at (55:2);

      \draw[blu] (O) -- (v) node[midway, above, sloped] {$\scriptstyle\bv$};
      \draw[red] (O) -- (w) node[midway, below, sloped] {$\scriptstyle\bv[w]$};

      \draw (55:1) arc(55:15:1) node[pos=0.25, right] {$\scriptstyle\theta$};

      \node[above, red] at (current bounding box.north) {Negatively Oriented};
      \node[below] at (current bounding box.south)
      {\scriptsize clockwise rotation};

    \end{tikzpicture}
  }
  \newcommand<>{\Colinear}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , ->
      , visible on=####1
      ]

      \coordinate (O) at (0, 0);
      \coordinate (v) at (15:1.25);
      \coordinate (w) at (15:-1);

      \draw[blu] (O) -- (v) node[midway, above, sloped] {$\scriptstyle\bv$};
      \draw[red] (O) -- (w) node[midway, above, sloped] {$\scriptstyle\bv[w]$};

      % \draw (55:1) arc(55:15:1) node[pos=0.25, right] {$\scriptstyle\theta$};

      \node[above, blu] at (current bounding box.north) {Colinear};
      \node[below] at (current bounding box.south)
      {\scriptsize linearly dependent};

    \end{tikzpicture}
  }
  \begin{definition}
    Every list of two vectors $\Set{\bv,\bv[w]}$ in $\mathbb{R}^2$ falls into
    one of three categories.
    \begin{align*}
      \Positive<2-> && \Negative<3-> && \Colinear<4->
    \end{align*}
    \onslide<5->{The order of the list matters when establishing orientation.}
  \end{definition}

\end{frame}


\subsection{Area}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $\mathcal{P}$ be the parallelogram formed by
    $\bv,\bv[w]\in\mathbb{R}^2$.
    \[
      \begin{tikzpicture}[line join=round, line cap=round, ultra thick]

        \coordinate (O) at (0, 0);
        \coordinate (v) at (5:3);
        \coordinate (w) at (65:2.5);

        \fill[draw, fill=grnbg]
        (O) -- ++(v) -- ++(w) -- (w) -- cycle;

        \draw[->, blu] (O) -- (v)
        node[midway, sloped, below] {$\scriptstyle\bv$};

        \draw[->, red] (O) -- (w)
        node[midway, sloped, above] {$\scriptstyle\bv[w]$};

        \node at ($ 0.5*(v)+0.5*(w) $) {$\mathcal{P}$};

      \end{tikzpicture}
    \]
    The orientation of $\Set{\bv, \bv[w]}$ is determined by the sign of
    \[
      \det\begin{bNiceMatrix}\bv & \bv[w]\end{bNiceMatrix}
    \]
    and the area of $\mathcal{P}$ is the absolute value of this determinant.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Parallelogram}{
    \begin{array}{c}
      \begin{tikzpicture}[line join=round, line cap=round, ultra thick]

        \coordinate (O) at (0, 0);
        \coordinate (w) at (5:3);
        \coordinate (v) at (65:2.5);

        \fill[draw, fill=grnbg]
        (O) -- ++(v) -- ++(w) -- (w) -- cycle;

        \draw[->, blu] (O) -- (v)
        node[midway, sloped, above] {$\scriptstyle\bv=\nv[small]{-391 760}$};

        \draw[->, red] (O) -- (w)
        node[midway, sloped, below] {$\scriptstyle\bv[w]=\nv[small]{881 696}$};

        \node at ($ 0.5*(v)+0.5*(w) $) {$\mathcal{P}$};

      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      ]
      {} -391 & 881 \\
      {}  760 & 696
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the $\mathcal{P}$ formed by $\bv=\nv{-391 760}$ and
    $\bv[w]=\nv{881 696}$.
    \begin{align*}
      \Parallelogram && \onslide<2->{\det\MatrixA=-941696}
    \end{align*}
    \onslide<3->{Here, $\Set{\bv, \bv[w]}$ is \emph{negatively oriented} and
      $\area(\mathcal{P})=941696$.}
  \end{example}

\end{frame}


\section{Volume in $\mathbb{R}^3$}
\subsection{Orientations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\Positive}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , ->
      , visible on=####1
      ]

      \node (p) {\includegraphics[width=0.2\textwidth]{rho.pdf}};

      \node[above, grn] at (current bounding box.north) {Positively Oriented};
      \node[below] at (current bounding box.south)
      {\scriptsize right hand order};

    \end{tikzpicture}
  }
  \newcommand<>{\Negative}{
    \begin{tikzpicture}[
      , line join=round
      , line cap=round
      , ultra thick
      , ->
      , visible on=####1
      ]

      \node (p) {\includegraphics[width=0.2\textwidth]{lho.pdf}};

      \node[above, red] at (current bounding box.north) {Negatively Oriented};
      \node[below] at (current bounding box.south)
      {\scriptsize left hand order};

    \end{tikzpicture}
  }
  \newcommand<>{\Coplanar}{
    \begin{tikzpicture}[
      , ultra thick
      , line join=round
      , line cap=round
      , ->
      , visible on=####1
      ]

      \coordinate (O)  at (0, 0);

      \coordinate (w1) at (5:2.75);
      \coordinate (w2) at (50:2);

      \coordinate (sw) at ($ -0.25*(w1)-0.25*(w2) $);

      \coordinate (e3) at (0, 3);

      \draw[grn, fill=grnbg]
      ($ -0.25*(w1)-0.25*(w2) $) -- ++(w1) -- ++(w2) -- ++($ -1*(w1) $) -- cycle;

      \draw[blu] (O) -- ($ 0.5*(w1) $) node[midway, sloped, below] {$\scriptstyle\bv$};
      \draw[red] (O) -- ($ 0.5*(w2) $) node[midway, sloped, above] {$\scriptstyle\bv[w]$};
      \draw[grn] (O) -- ($ 0.5*(w1)+0.5*(w2) $) node[midway, sloped, above] {$\scriptstyle\bv[x]$};

      \node[above, blu] at (current bounding box.north) {Coplanar};
      \node[below] at (current bounding box.south)
      {\scriptsize linearly dependent};

    \end{tikzpicture}
  }
  \begin{definition}
    Every list $\Set{\bv, \bv[w], \bv[x]}$ in $\mathbb{R}^3$ falls into one of
    three categories.
    \begin{align*}
      \Negative<2-> && \Positive<3-> && \Coplanar<4->
    \end{align*}
    \onslide<5->{The order of the list matters when establishing orientation.}
  \end{definition}

\end{frame}

\subsection{Volume}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    Let $\mathcal{P}$ be the parallelepiped formed by
    $\bv,\bv[w],\bv[x]\in\mathbb{R}^3$.
    \[
      \begin{tikzpicture}
        \begin{scope}[
          , x={(2cm, 0cm)}
          , y={({cos(150)*1cm}, {sin(150)*1cm})}%30
          , z={({cos(70)*1.5cm}, {sin(70)*1.5cm})}
          , line join=round
          , line cap=round
          , fill opacity=0.5
          , ultra thick
          ]

          \draw[fill=blubg] (0,0,0) -- (0,0,1) -- (0,1,1) -- (0,1,0) -- cycle;
          \draw[fill=redbg] (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
          \draw[fill=grnbg] (0,1,0) -- (1,1,0) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=grnbg] (1,0,0) -- (1,0,1) -- (1,1,1) -- (1,1,0) -- cycle;
          \draw[fill=redbg] (0,0,1) -- (1,0,1) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=blubg] (0,0,0) -- (1,0,0) -- (1,0,1) -- (0,0,1) -- cycle;

          \draw[blu, ->, fill opacity=1] (0, 0, 0) -- (1, 0, 0) node[midway, below, sloped] {$\scriptstyle\bv$};
          \draw[red, ->, fill opacity=1] (0, 0, 0) -- (0, 1, 0) node[midway, below, sloped] {$\scriptstyle\bv[w]$};
          \draw[grn, ->, fill opacity=1] (0, 0, 0) -- (0, 0, 1) node[midway, above, sloped] {$\scriptstyle\bv[x]$};
        \end{scope}
      \end{tikzpicture}
    \]
    The orientation of $\Set{\bv,\bv[w],\bv[x]}$ is determined by the sign of
    \[
      \det\begin{bNiceMatrix}\bv & \bv[w] & \bv[x]\end{bNiceMatrix}
    \]
    and the volume of $\mathcal{P}$ is the absolute value of this determinant.
  \end{theorem}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\ParallelepipedA}{
    \begin{array}{c}
      \begin{tikzpicture}
        \begin{scope}[
          , x={(2cm, 0cm)}
          , y={({cos(150)*1cm}, {sin(150)*1cm})}%30
          , z={({cos(70)*1.5cm}, {sin(70)*1.5cm})}
          , line join=round
          , line cap=round
          , fill opacity=0.5
          , ultra thick
          ]

          \draw[fill=blubg] (0,0,0) -- (0,0,1) -- (0,1,1) -- (0,1,0) -- cycle;
          \draw[fill=redbg] (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
          \draw[fill=grnbg] (0,1,0) -- (1,1,0) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=grnbg] (1,0,0) -- (1,0,1) -- (1,1,1) -- (1,1,0) -- cycle;
          \draw[fill=redbg] (0,0,1) -- (1,0,1) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=blubg] (0,0,0) -- (1,0,0) -- (1,0,1) -- (0,0,1) -- cycle;

          \draw[blu, ->, fill opacity=1] (0, 0, 0) -- (1, 0, 0) node[midway, below, sloped] {$\scriptstyle\bv$};
          \draw[red, ->, fill opacity=1] (0, 0, 0) -- (0, 1, 0) node[midway, below, sloped] {$\scriptstyle\bv[w]$};
          \draw[grn, ->, fill opacity=1] (0, 0, 0) -- (0, 0, 1) node[midway, above, sloped] {$\scriptstyle\bv[x]$};
        \end{scope}

        \CrossOut<4->
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\ParallelepipedB}{
    \begin{array}{c}
      \begin{tikzpicture}
        \begin{scope}[
          , x={(2cm, 0cm)}
          , y={({cos(150)*1cm}, {sin(150)*1cm})}%30
          , z={({cos(70)*1.5cm}, {sin(70)*1.5cm})}
          , line join=round
          , line cap=round
          , fill opacity=0.5
          , ultra thick
          ]

          \draw[fill=blubg] (0,0,0) -- (0,0,1) -- (0,1,1) -- (0,1,0) -- cycle;
          \draw[fill=redbg] (0,0,0) -- (1,0,0) -- (1,1,0) -- (0,1,0) -- cycle;
          \draw[fill=grnbg] (0,1,0) -- (1,1,0) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=grnbg] (1,0,0) -- (1,0,1) -- (1,1,1) -- (1,1,0) -- cycle;
          \draw[fill=redbg] (0,0,1) -- (1,0,1) -- (1,1,1) -- (0,1,1) -- cycle;
          \draw[fill=blubg] (0,0,0) -- (1,0,0) -- (1,0,1) -- (0,0,1) -- cycle;

          \draw[red, ->, fill opacity=1] (0, 0, 0) -- (1, 0, 0) node[midway, below, sloped] {$\scriptstyle\bv[w]$};
          \draw[blu, ->, fill opacity=1] (0, 0, 0) -- (0, 1, 0) node[midway, below, sloped] {$\scriptstyle\bv$};
          \draw[grn, ->, fill opacity=1] (0, 0, 0) -- (0, 0, 1) node[midway, above, sloped] {$\scriptstyle\bv[x]$};
        \end{scope}
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Vectors}{
    \begin{aligned}
      \bv    &= \nv{3 8 4} \\
      \bv[w] &= \nv{9 7 1} \\
      \bv[x] &= \nv{6 5 5}
    \end{aligned}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      3 & 9 & 6 \\
      8 & 7 & 5 \\
      4 & 1 & 5
    \end{bNiceMatrix}
  }
  \begin{example}
    Which of these figures is correct and what is the volume?
    \begin{align*}
      \ParallelepipedA && \ParallelepipedB && \Vectors
    \end{align*}
    \onslide<2->{We can solve this problem with a determinant}
    \[
      \begin{tikzpicture}[remember picture, visible on=<2->]
        \node {\(\det\MatrixA=\subnode{neg}{\textcolor<3->{red}{-210}}\)};

        \draw[thick, <-, red, overlay, visible on=<3->] (neg.south) |- ++(3mm, -2.5mm)
        node[right] {\scriptsize negative orientation};
      \end{tikzpicture}
    \]
    \onslide<5->{Here, we have $\volume(\mathcal{P})=210$.}
  \end{example}

\end{frame}

\end{document}
