\documentclass[]{bmr}

\title{Orthogonality}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Fundamental Subspaces}
\subsection{Orthogonality Relations}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{Observation}
    Suppose $\bv\in\Col(A^\intercal)$ and $\bv[w]\in\Null(A)$.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\PerpLength}{0.25}
        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        ] {$\scriptstyle\Col(A^\intercal)$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        ] {$\scriptstyle\Null(A)$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^n$};

        \draw[LeftSpace, visible on=<8->] (0, 0) rectangle (-\PerpLength,\PerpLength);

        \draw[ultra thick, ->, blu, visible on=<2->]
        (0, 0) -- (Row.north west) node[midway, left] {$\scriptstyle\bv$};

        \draw[ultra thick, ->, grn, visible on=<2->]
        (0, 0) -- (Null.north west) node[midway, sloped, above] {$\scriptstyle\bv[w]$};

        \begin{scope}[xshift=4.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          ] {$\scriptstyle\Col(A)$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          ] {$\scriptstyle\Null(A^\intercal)$};

          \draw[RightSpace, visible on=<10->] (0, 0) rectangle (\PerpLength,\PerpLength);

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A$};

      \end{tikzpicture}
    \]
    \onslide<3->{The inner product of $\bv$ and $\bv[w]$ is then}
    \[
      \begin{tikzpicture}[visible on=<3->, remember picture]
        \node {\(
          \inner{\subnode{v}{\color<4>{blu}\bv}, \bv[w]}=
          \inner{\subnode{ATx}{\color<4>{blu}\color<5>{red}A^\intercal\bv[x]}, \bv[w]}=
          \inner{\bv[x], \subnode{Aw}{\color<5>{red}\color<6>{grn}A\bv[w]}}=
          \inner{\bv[x], \subnode{O}{\color<6>{grn}\bv[O]}}=
          0
          \)};

        \draw[thick, blu, ->, overlay, visible on=<4>] (v.south) -- ++(0mm, -2mm) -| (ATx.south);
        \node[below=1.5mm, blu, visible on=<4>, overlay] at ($ (v.south)!0.5!(ATx.south) $)
        {\scriptsize$\bv\in\Col(A^\intercal)$ so $\bv=A^\intercal\bv[x]$};

        \draw[thick, red, ->, overlay, visible on=<5>] (ATx.south) -- ++(0mm, -2mm) -| (Aw.south);
        \node[below=1.5mm, red, visible on=<5>, overlay] at ($ (ATx.south)!0.5!(Aw.south) $)
        {\scriptsize adjoint property};

        \draw[thick, grn, ->, overlay, visible on=<6>] (Aw.south) -- ++(0mm, -2mm) -| (O.south);
        \node[below=1.5mm, grn, visible on=<6>, overlay] at ($ (Aw.south)!0.5!(O.south) $)
        {\scriptsize$\bv[w]\in\Null(A)$ so $A\bv[w]=\bv[O]$};

      \end{tikzpicture}
    \]
    \onslide<7->{This means $\Null(A)\perp\Col(A^\intercal)$.}
    \onslide<9->{Additionally, $\Null(A^\intercal)\perp\Col(A)$.}
  \end{block}

\end{frame}


\subsection{Orthogonal Complements}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    The fundamental subspaces pair off into \grn{orthogonal complements}.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\PerpLength}{0.25}
        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        ] {$\scriptstyle\underset{\onslide<2->{\Null(A)^\perp}}{\Col(A^\intercal)}$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        ] {$\scriptstyle\underset{\onslide<2->{\Col(A^\intercal)^\perp}}{\Null(A)}$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^n$};

        \draw[LeftSpace, visible on=<1->] (0, 0) rectangle (-\PerpLength,\PerpLength);

        \begin{scope}[xshift=4.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          ] {$\scriptstyle\underset{\onslide<5->{\Null(A^\intercal)^\perp}}{\Col(A)}$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          ] {$\scriptstyle\underset{\onslide<5->{\Col(A)^\perp}}{\Null(A^\intercal)}$};

          \draw[RightSpace, visible on=<1->] (0, 0) rectangle (\PerpLength,\PerpLength);

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^m$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\scriptstyle A$};

        \draw[Point, blu, visible on=<6->]
        (Col.north) |- ++(3mm, 2.5mm) node[right] (ColPerp)
        {\scriptsize\textnormal{$\bv\in\Col(A)$ means $\bv\perp\bv[w]$}};
        \node[below, blu, overlay, visible on=<6->] at (ColPerp)
        {\scriptsize\textnormal{for $\bv[w]\in\Null(A^\intercal)$}};

        \draw[Point, blu, visible on=<3->]
        (Row.north) |- ++(-3mm, 2.5mm) node[left] (RowPerp)
        {\scriptsize\textnormal{$\bv\in\Col(A^\intercal)$ means $\bv\perp\bv[w]$}};
        \node[below, blu, overlay, visible on=<3->] at (RowPerp)
        {\scriptsize\textnormal{for $\bv[w]\in\Null(A)$}};

        \draw[Point, red, visible on=<7->]
        (LNull.north) |- ++(2mm, 2.5mm) node[right] (LNullPerp)
        {\scriptsize\textnormal{$\bv\in\Null(A^\intercal)$ means $\bv\perp\bv[w]$}};
        \node[below, red, overlay, visible on=<7->] at (LNullPerp)
        {\scriptsize\textnormal{when $\bv[w]\in\Col(A)$}};

        \draw[Point, grn, visible on=<4->]
        (Null.north) |- ++(-2mm, 2.5mm) node[left] (NullPerp)
        {\scriptsize\textnormal{$\bv\in\Null(A)$ means $\bv\perp\bv[w]$}};
        \node[below, grn, overlay, visible on=<4->] at (NullPerp)
        {\scriptsize\textnormal{when $\bv[w]\in\Col(A^\intercal)$}};

      \end{tikzpicture}
    \]
  \end{theorem}

\end{frame}


\section{Using Orthogonality}
\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      1 &  -4 &  -4 &  -9 \\
      1 &  -4 &  -3 &  -7 \\
      5 & -20 & -24 & -53
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixM}{
    \begin{bNiceArray}{rr|r}[small]
      1 &  4 & 3 \\
      1 &  3 & 5 \\
      5 & 24 & 1
    \end{bNiceArray}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rr|r}[small]
      1 & 0 & 0 \\
      0 & 1 & 0 \\
      0 & 0 & 1
    \end{bNiceArray}
  }
  \newcommand{\Inefficient}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<7->]
        \node[fill=redbg, draw, rounded corners, ultra thick]
        (text) {$\rref\MatrixM=\MatrixR$};
        \node[above, red, overlay] at (text.north) {\scriptsize Inefficient};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Efficient}{
    \begin{array}{c}
      \begin{tikzpicture}[visible on=<8->]
        \node[fill=grnbg, draw, rounded corners, ultra thick]
        (text) {$\inner*{\nv[small]{3 5 1}, \nv[small]{-9 4 1}}=-6\neq 0$};
        \node[above, grn, overlay] at (text.north) {\scriptsize Efficient};
      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Here, we have bases of $\Col(A)$ and $\Null(A^\intercal)$.
    \[
      \begin{tikzpicture}

        \pgfmathsetmacro{\PerpLength}{0.25}
        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        ] {$\scriptstyle\underset{\onslide<4->{\dim=2}}{\Col(A^\intercal)}$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        ] {$\scriptstyle\underset{\onslide<5->{\dim=2}}{\Null(A)}$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^4$};

        \draw[LeftSpace, visible on=<1->] (0, 0) rectangle (-\PerpLength,\PerpLength);

        \begin{scope}[xshift=6.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          ] {$\scriptstyle\underset{\onslide<3->{\dim=2}}{\Col(A)}$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          ] {$\scriptstyle\underset{\onslide<2->{\dim=1}}{\Null(A^\intercal)}$};

          \draw[RightSpace, visible on=<1->] (0, 0) rectangle (\PerpLength,\PerpLength);

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^3$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above, overlay] {$\MatrixA$};

        \draw[Point, blu] (Col.north) |- ++(3mm, 2.5mm) node[right]
        {$\scriptstyle\beta=\Set*{\nv[small]{1 1 5}, \nv[small]{4 3 24}}$};

        \draw[Point, red] (LNull.north) |- ++(2mm, 2.5mm) node[right]
        {$\scriptstyle\beta=\Set*{\nv[small]{-9 4 1}}$};

      \end{tikzpicture}
    \]
    \onslide<6->{To determine if
      $\color<9->{red}\nv{3 5 1}\alt<9->{\notin}{\in}\Col(A)$, we have two
      options.}
    \begin{align*}
      \Inefficient && \Efficient
    \end{align*}
  \end{example}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {} -2 & -3 \\
      {}  6 &  9 \\
      {} -1 & -2 \\
      {}  0 & -1
    \end{bNiceMatrix}
  }
  \begin{example}
    Let $V=\Span\Set*{\nv{-2 6 -1 0}, \nv{-3 9 -2 -1}}$.
    \[
      \begin{tikzpicture}[visible on=<2->]

        \pgfmathsetmacro{\PerpLength}{0.25}
        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        , fill=blubg
        , minimum height=1.25cm
        , minimum width=1.5cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        ] {$\scriptstyle\underset{\onslide<6->{\dim=2}}{\Col(A^\intercal)}$};

        \node (Null) [
        , LeftSpace
        , fill=grnbg
        , anchor=north east
        ] {$\scriptstyle\underset{\onslide<7->{\dim=0}}{\Null(A)}$};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^2$};

        \draw[LeftSpace, visible on=<1->] (0, 0) rectangle (-\PerpLength,\PerpLength);

        \begin{scope}[xshift=5.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          ] {$\scriptstyle\underset{\onslide<4->{\dim=2}}{\Col(A)}$};

          \node(LNull) [
          , RightSpace
          , fill=redbg
          , anchor=north west
          ] {$\scriptstyle\underset{\onslide<5->{\dim=2}}{\Null(A^\intercal)}$};

          \draw[RightSpace, visible on=<1->] (0, 0) rectangle (\PerpLength,\PerpLength);

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^4$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above] {$\MatrixA$};

        \draw[Point, blu, visible on=<3->] (Col.north) |- ++(2mm, 2.5mm) node[right, scale=0.80]
        {$\scriptstyle V=\Span\Set*{\nv[small]{-2 6 -1 0}, \nv[small]{-3 9 -2 -1}}$};

        \draw[Point, red, visible on=<9->] (LNull.north) |- ++(1.5mm, 3.5mm) node[right, scale=0.70]
        {$\scriptstyle V^\perp=\Span\Set*{\nv[small]{3 1 0 0}, \nv[small]{1 0 -2 1}}$};

      \end{tikzpicture}
    \]
    \onslide<8->{Finding a basis of $V^\perp$ is the same as finding a basis of
      $V^\perp=\Null(A^\intercal)$.}
  \end{example}

\end{frame}


\subsection{$EA=R$}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixE}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<7-9>[grn]{3}
        \hlRow<10-12>[grn]{4}
      }
      ]
      {}  1 &  1 &  -8 &  24 \\
      {} -1 &  0 &   5 & -17 \\
      {}  3 & -1 & -11 &  41 \\
      {} -1 &  1 &  -3 &   6
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlCol<7,10>[grn]{1}
        \hlCol<8,11>[grn]{2}
        \hlCol<9,12>[grn]{3}
      }
      , code-after = {
        \begin{tikzpicture}[blu, thick, <-, visible on=<5->]
          \node[below=-2pt] at ($ (row-5-|col-2.south)!0.5!(row-5-|col-3.south) $)
          {\scriptsize$4\times 3$ $(r=2)$};
        \end{tikzpicture}
      }
      ]
      {} -3 &  12 & -17 \\
      {} 12 & -48 &  49 \\
      {} 13 & -52 &  58 \\
      {}  4 & -16 &  18
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlEntry<3->{1}{1}
        \hlEntry<4->{2}{3}
        \hlEntry<7>[red]{3}{1}
        \hlEntry<8>[red]{3}{2}
        \hlEntry<9>[red]{3}{3}
        \hlEntry<10>[red]{4}{1}
        \hlEntry<11>[red]{4}{2}
        \hlEntry<12>[red]{4}{3}
      }
      ]
      1 & -4 & 0 \\
      0 &  0 & 1 \\
      0 &  0 & 0 \\
      0 &  0 & 0
    \end{bNiceMatrix}
  }
  \begin{block}{Recall}
    Suppose that $A$ is $m\times n$ with $\rank(A)=r$ and consider $EA=R$.
    \[
      \onslide<2->{\overset{E}{\MatrixE}\overset{A}{\MatrixA}=\overset{R}{\MatrixR}}
    \]
    \onslide<6->{The last $m-r$ rows of $E$ are \emph{orthogonal} to every
      column of $A$.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}
  \newcommand{\MatrixE}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<5->[grn]{3}
        \hlRow<5->[grn]{4}
      }
      ]
      {}  1 &  1 &  -8 &  24 \\
      {} -1 &  0 &   5 & -17 \\
      {}  3 & -1 & -11 &  41 \\
      {} -1 &  1 &  -3 &   6
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r]
      {} -3 &  12 & -17 \\
      {} 12 & -48 &  49 \\
      {} 13 & -52 &  58 \\
      {}  4 & -16 &  18
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[r]
      1 & -4 & 0 \\
      0 &  0 & 1 \\
      0 &  0 & 0 \\
      0 &  0 & 0
    \end{bNiceMatrix}
  }
  \begin{theorem}
    Suppose $EA=R$ where $A$ is $m\times n$ with $\rank(A)=r$.
    \[
      \onslide<2->{\overset{E}{\MatrixE}\overset{A}{\MatrixA}=\overset{R}{\MatrixR}}
    \]
    \onslide<3->{The last $m-r$ rows of $E$ form a basis of
      $\Null(A^\intercal)$.} \onslide<4->{Here, we have}
    \[
      \onslide<4->{\Null(A^\intercal)=\Span\Set*{
          \nv[margin, code-before = {\hlMat<6->[grn]}]{3 -1 -11 41},
          \nv[margin, code-before = {\hlMat<6->[grn]}]{-1 1 -3 6}
      }}
    \]
  \end{theorem}

\end{frame}


\end{document}
