\documentclass[]{bmr}

\usepackage{pdfpages}

\title{$PA=LU$ Factorizations}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{$PA=LU$ Factorizations}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand<>{\MatrixP}{
    \begin{tikzpicture}[thick, inner sep=0pt, <-]
      \node (P) {$\color####1{grn}P$};
      \draw[overlay, grn, visible on=####1]
      (P.south) |- ++(-4mm, -3mm) node[left]
      {$m\times m$ \emph{permutation} matrix};
    \end{tikzpicture}
  }
  \newcommand<>{\MatrixA}{
    \begin{tikzpicture}[thick, inner sep=0pt, <-]
      \node (A) {$\color####1{blu}A$};
      \draw[overlay, blu, visible on=####1]
      (A.south) |- ++(-4mm, -8mm) node[left]
      {$m\times n$};
    \end{tikzpicture}
  }
  \newcommand<>{\MatrixL}{
    \begin{tikzpicture}[thick, inner sep=0pt, <-]
      \node (L) {$\color####1{red}L$};
      \draw[overlay, red, visible on=####1]
      (L.south) |- ++(4mm, -8mm) node[right]
      {$m\times m$ lower triangular};
    \end{tikzpicture}
  }
  \newcommand<>{\MatrixU}{
    \begin{tikzpicture}[thick, inner sep=0pt, <-]
      \node (U) {$\color####1{blu}U$};
      \draw[overlay, blu, visible on=####1]
      (U.south) |- ++(4mm, -3mm) node[right]
      {$m\times n$ row echelon form};
    \end{tikzpicture}
  }
  \begin{definition}
    A \emph{\grn{$PA=LU$ factorization}} is an equation of the form
    \[
      \MatrixP<5->\MatrixA<2->=\MatrixL<3->\MatrixU<4->
    \]
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixI}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {}
      ]
      1 & 0 & 0 & 0 \\
      0 & 1 & 0 & 0 \\
      0 & 0 & 1 & 0 \\
      0 & 0 & 0 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixPA}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {
        \hlRow<2>[blu]{2}
        \hlRow<2>[grn]{4}
        \hlRow<3>[grn]{2}
        \hlRow<3>[blu]{4}
      }
      ]
      1 & 0 & 0 & 0 \\
      \alt<3->{0}{0} & \alt<3->{0}{1} & \alt<3->{0}{0} & \alt<3->{1}{0} \\
      0 & 0 & 1 & 0 \\
      \alt<3->{0}{0} & \alt<3->{1}{0} & \alt<3->{0}{0} & \alt<3->{0}{1}
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixP}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {
        \hlRow<5>[blu]{1}
        \hlRow<5>[grn]{4}
        \hlRow<6>[grn]{1}
        \hlRow<6>[blu]{4}
        \hlMat<7->[red]
      }
      ]
      \alt<6->{0}{1} & \alt<6->{1}{0} & \alt<6->{0}{0} & \alt<6->{0}{0} \\
      0 & 0 & 0 & 1 \\
      0 & 0 & 1 & 0 \\
      \alt<6->{1}{0} & \alt<6->{0}{1} & \alt<6->{0}{0} & \alt<6->{0}{0}
    \end{bNiceMatrix}
  }
  \begin{definition}
    A \emph{\grn{permutation matrix $P$}} is obtained from row-swaps on $I_n$.
    \[
      \overset{I_4}{\MatrixI}
      \xrightarrow{\bv[r]_2\leftrightarrow\bv[r]_4}\MatrixPA
      \onslide<4->{\xrightarrow{\bv[r]_1\leftrightarrow\bv[r]_4}\overset{P}{\MatrixP}}
    \]
    \onslide<8->{Every permutation matrix is \emph{\grn{orthogonal}}, meaning
      $P^{-1}=P^\intercal$.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixP}{
    \begin{bNiceMatrix}[light-syntax]
      0 0 1;
      1 0 0;
      0 1 0;
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, light-syntax]
      {-15} 30 -10 -5;
      { 25} 28  34 35;
      {  5} -7   4  3;
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixL}[1][0]{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlLower<####1>}
      ]
      { 1} & 0  & 0 \\
      {-3} & 1  & 0 \\
      { 5} & 7  & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}[1][0]{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {\hlUpper<####1>[grn]}
      ]
      5 & -7 & 4 &  3 \\
      0 &  9 & 2 &  4 \\
      0 &  0 & 0 & -8
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixPT}{
    \begin{bNiceMatrix}[light-syntax]
      0 1 0;
      0 0 1;
      1 0 0;
    \end{bNiceMatrix}
  }
  \begin{example}
    Consider the following $PA=LU$ factorization
    \[
      \overset{P}{\MatrixP}
      \overset{A}{\MatrixA}
      =
      \overset{L}{\MatrixL[2-]}
      \overset{U}{\MatrixU[3-]}
    \]
    \onslide<4->{Since $P^{-1}=P^\intercal$, we can easily rewrite our
      factorization as $A=P^\intercal LU$.}
    \[
      \onslide<4->{
        \overset{A}{\MatrixA} =
        \overset{P^{-1}=P^\intercal}{\MatrixPT} \overset{L}{\MatrixL}
        \overset{U}{\MatrixU}
      }
    \]
  \end{example}

\end{frame}


\subsection{Properties}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixP}{
    \begin{bNiceMatrix}[r, small, light-syntax]
      1 0 0 0;
      0 0 0 1;
      0 1 0 0;
      0 0 1 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlCol<3->[grn, thin, rounded corners=0.5mm]{1}
        \hlCol<3->[grn, thin, rounded corners=0.5mm]{2}
        \hlCol<3->[grn, thin, rounded corners=0.5mm]{4}
        \hlCol<4->[red, thin, rounded corners=0.5mm]{3}
        \hlCol<4->[red, thin, rounded corners=0.5mm]{5}
      }
      ]
      {}  7 &   4 &  -3 &   8 &  -9 \\
      {} 35 &  35 &  20 &  66 & -64 \\
      {} 14 & -13 & -55 & -66 &   1 \\
      {} 21 &  15 &  -2 &  28 & -31
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixL}{
    \begin{bNiceMatrix}[r, small, light-syntax]
      1  0  0 0;
      3  1  0 0;
      5  5  1 0;
      2 -7 -9 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {
        \hlCol<3->[grn, thin, rounded corners=0.5mm]{1}
        \hlCol<3->[grn, thin, rounded corners=0.5mm]{2}
        \hlCol<3->[grn, thin, rounded corners=0.5mm]{4}
        \hlCol<4->[red, thin, rounded corners=0.5mm]{3}
        \hlCol<4->[red, thin, rounded corners=0.5mm]{5}
        \hlEntry<2->[blu, thin, rounded corners=0.5mm]{1}{1}
        \hlEntry<2->[blu, thin, rounded corners=0.5mm]{2}{2}
        \hlEntry<2->[blu, thin, rounded corners=0.5mm]{3}{4}
      }
      ]
      7 & 4 & -3 & 8 & -9 \\
      0 & 3 &  7 & 4 & -4 \\
      0 & 0 &  0 & 6 &  1 \\
      0 & 0 &  0 & 0 &  0
    \end{bNiceMatrix}
  }
  \newcommand{\APicture}{
    \begin{tikzpicture}[visible on=<6->]

      \pgfmathsetmacro{\SpaceRotate}{20}
      \tikzstyle{Space} = [
      , draw
      , ultra thick
      , outer sep=0pt
      , font=\scriptsize
      ]
      \tikzstyle{LeftSpace} = [
      , Space
      , rotate=\SpaceRotate
      ]
      \tikzstyle{RightSpace} = [
      , Space
      , rotate=-\SpaceRotate
      ]

      \node (Row) [
      , LeftSpace
      , fill=blubg
      , anchor=south west
      ] {$\rank(A)=\onslide<7->{3}$};

      \node (Null) [
      , LeftSpace
      , fill=grnbg
      , anchor=north east
      ] {$\nullity(A)=\onslide<8->{2}$};
      \node[right] (Rn) at (Row.east) {$\mathbb{R}^5$};

      \begin{scope}[xshift=6cm]
        \node(Col) [
        , RightSpace
        , fill=blubg
        , anchor=south east
        ] {$\rank(A)=\onslide<7->{3}$};

        \node(LNull) [
        , RightSpace
        , fill=redbg
        , anchor=north west
        ] {$\nullity(A^\intercal)=\onslide<9->{1}$};

        \node[left] (Rm) at (Col.west) {$\mathbb{R}^4$};
      \end{scope}

      \draw[->, thick] (Rn.east) -- (Rm.west) node[midway, above] {$A$};

    \end{tikzpicture}
  }
  \begin{block}{Observation}
    In $PA=LU$, $A$ and $U$ have the same \emph{\grn{pivot}} and
    \emph{\red{nonpivot}} column locations.
    \[
      \overset{P}{\MatrixP}
      \overset{A}{\MatrixA}
      =
      \overset{L}{\MatrixL}
      \overset{U}{\MatrixU}
    \]
    \onslide<5->{Consequently, $\rank(A)=\rank(U)$ and
      $\nullity(A)=\nullity(U)$.}
    \[
      \APicture
    \]
  \end{block}

\end{frame}


\section{Solving Systems}
\subsection{Procedure}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\Ux}{
    \begin{tikzpicture}[baseline=(Ux.base), inner sep=0, thick, <-]
      \node (Ux) {$\color<5->{red}U\bv[x]$};
      \draw[overlay, red, visible on=<5->] (Ux.north) |- ++(-6mm, 3mm)
      node[left] {$U\bv[x]=\bv[y]$};
    \end{tikzpicture}
  }
  \newcommand{\Lower}{
    \begin{tikzpicture}[baseline=(L.base), inner sep=0, thick, <-]
      \node (L) {$\color<7->{grn}L$};
      \draw[overlay, grn, visible on=<7->] (L.south) |- ++(2mm, -5mm)
      node[right] {\emph{lower triangular}};
    \end{tikzpicture}
  }
  \newcommand{\Upper}{
    \begin{tikzpicture}[baseline=(U.base), inner sep=0, thick, <-]
      \node (U) {$\color<9->{blu}U$};
      \draw[overlay, blu, visible on=<9->] (U.south) |- ++(2mm, -5mm) node[right] {\emph{row echelon form}};
    \end{tikzpicture}
  }
  \begin{block}{Observation}
    Suppose $PA=LU$ and consider the system $A\bv[x]=\bv[b]$.
    \[
      \onslide<2->{\alt<4->{L\Ux}{\onslide<3->{P}A\bv[x]}=\onslide<3->{P}\bv[b]}
    \]
    \onslide<6->{First, solve $\Lower\bv[y]=P\bv[b]$ for $\bv[y]$.}
    \onslide<8->{Then, solve $\Upper\bv[x]=\bv[y]$ for $\bv[x]$.}
  \end{block}

\end{frame}


\subsection{Example}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \tikzstyle{th}=[thin, rounded corners=0.5mm]
  \newcommand{\MatrixP}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<4>[grn, th]}
      ]
      0 & 1 & 0 \\
      1 & 0 & 0 \\
      0 & 0 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      {} -9 &  7 &  1 \\
      {}  3 & -2 &  1 \\
      {}  6 &  1 & 27
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixL}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<3-4>[blu, th]}
      ]
      {}  1 & 0 & 0 \\
      {} -3 & 1 & 0 \\
      {}  2 & 5 & 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixU}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<12-13>[blu, th]}
      ]
      3 & -2 & 1 \\
      0 &  1 & 4 \\
      0 &  0 & 5
    \end{bNiceMatrix}
  }
  \newcommand{\VectorB}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<4>[grn, th]}
      ]
      0\\ -3\\ -1
    \end{bNiceMatrix}
  }
  \newcommand{\LyPb}{
    \begin{NiceArray}{rcrcrcrcrcr}[
      , small
      , code-before = {
        \hlPortion<3-4>[blu, th]{1}{1}{3}{5}
        \hlCol<4>[grn, th]{7}
        \hlPortion<5-6>[blu, th]{1}{1}{1}{7}
        \hlPortion<7-8>[blu, th]{2}{1}{2}{7}
        \hlPortion<9-10>[blu, th]{3}{1}{3}{7}
        \hlCol<13>[grn, th]{11}
      }
      , code-after = {
        \begin{tikzpicture}[thick, <-, shorten <=1mm]
          \draw[blu, visible on=<3-4>] (row-4-|col-2.south) |- ++(-6mm, -3mm)
          node[left] {$\scriptstyle L\bv[y]$};
          \draw[grn, visible on=<4>] ($ (row-4-|col-7.south)!0.5!(row-4-|col-8.south) $)
          |- ++(4mm, -3mm) node[right] {$\scriptstyle P\bv[b]$};
        \end{tikzpicture}
      }
      ]
      {}     y_1 & &        & &     &=& -3 &\onslide<6->{\to} & \onslide<6->{y_1}  &\onslide<6->{=} & \onslide<6->{-3}  \\
      {} -3\,y_1 &+&    y_2 & &     &=&  0 &\onslide<8->{\to} & \onslide<8->{y_2}  &\onslide<8->{=} & \onslide<8->{-9}  \\
      {}  2\,y_1 &+& 5\,y_2 &+& y_3 &=& -1 &\onslide<10->{\to}& \onslide<10->{y_3} &\onslide<10->{=}& \onslide<10->{50}
    \end{NiceArray}
  }
  \newcommand{\Uxy}{
    \begin{NiceArray}{rcrcrcrcrcr}[
      , small
      , code-before = {
        \hlPortion<12-13>[blu, th]{1}{1}{3}{5}
        \hlCol<13>[grn, th]{7}
        \hlPortion<18-19>[blu, th]{1}{1}{1}{7}
        \hlPortion<16-17>[blu, th]{2}{1}{2}{7}
        \hlPortion<14-15>[blu, th]{3}{1}{3}{7}
      }
      , code-after = {
        \begin{tikzpicture}[thick, <-, shorten <=1mm]
          \draw[blu, visible on=<12-13>] (row-4-|col-2.south) |- ++(-6mm, -3mm)
          node[left] {$\scriptstyle U\bv[x]$};
          \draw[grn, visible on=<13>] ($ (row-4-|col-7.south)!0.5!(row-4-|col-8.south) $)
          |- ++(4mm, -3mm) node[right] {$\scriptstyle \bv[y]$};
        \end{tikzpicture}
      }
      ]
      {} 3\,x_1 &-& 2\,x_2 &+&    x_3 &=& -3 &\onslide<19->{\to}& \onslide<19->{x_1} &\onslide<19->{=}& \onslide<19->{-37} \\
      {}        & &    x_2 &+& 4\,x_3 &=& -9 &\onslide<17->{\to}& \onslide<17->{x_2} &\onslide<17->{=}& \onslide<17->{-49} \\
      {}        & &        & & 5\,x_3 &=& 50 &\onslide<15->{\to}& \onslide<15->{x_3} &\onslide<15->{=}& \onslide<15->{10}
    \end{NiceArray}
  }
  \begin{example}
    Consider the $PA=LU$ factorization and the vector $\bv[b]$ given by
    \begin{align*}
      \overset{P}{\MatrixP}\overset{A}{\MatrixA}=\overset{L}{\MatrixL}\overset{U}{\MatrixU} && \bv[b]=\VectorB
    \end{align*}
    \onslide<2->{To solve $A\bv[x]=\bv[b]$, first solve $L\bv[y]=P\bv[b]$ for
      $\bv[y]$.} \onslide<11->{Then solve $U\bv[x]=\bv[y]$ for $\bv[x]$.}
    \begin{align*}
      \onslide<2->{\LyPb} && \onslide<11->{\Uxy}
    \end{align*}
    \onslide<20->{Here, the only solution to $A\bv[x]=\bv[b]$ is $\bv[x]=\nv{-37 -49 10}$.}
  \end{example}

\end{frame}


\section{Calculating $PA=LU$}
\subsection{Description}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{block}{$PA=LU$ Factorization Algorithm}
    Suppose $A$ is $m\times n$. Start with $i=j=1$, $L=\bv[O]_{m\times m}$, and
    $P=I_m$.
    \begin{description}[<+->]
    \item[Step 1] If $a_{ij}=0$, find the smallest $k>i$ where
      $a_{kj}\neq0$. Perform $\bv[r]_i\leftrightarrow\bv[r]_k$ on $A$, $P$, and
      $L$. If no $k$ exists, increase $j$ by one and repeat this step.
    \item[Step 2] Eliminate all entries \emph{below} $a_{ij}$ by subtracting
      suitible multiples $m_{kj}$ of $\Row_i$. In $L$, put $\ell_{kj}=-m_{kj}$.
    \item[Step 3] Increase $i$ and $j$ by one and return to Step 1.
    \item[Step 4] When the last row or column of $A$ is processed, put ones on
      the diagonal of $L$.
    \end{description}
    \onslide<+->{This reduces $A$ to row echelon form $U$ and we have $PA=LU$.}
  \end{block}

\end{frame}

\subsection{Example}

{
  \setbeamercolor{background canvas}{bg=}
  \includepdf[pages=-, fitpaper=true]{palu-ex.pdf}
}


\end{document}

