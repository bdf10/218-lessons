\documentclass[]{bmr}

\title{Null Spaces}

\begin{document}

\maketitle

\begin{frame}
  \frametitle{Overview}
  \tableofcontents
\end{frame}

\section{Null Spaces}
\subsection{Definition}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {}  3 &  2 &  -1 \\
      {} -8 &  4 & -44 \\
      {}  2 & -9 &  51 \\
      {}  1 &  6 & -27
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[r, small]
      3\\ -5\\ -1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorO}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<6->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<6->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 4mm)
          node[left] {$\scriptstyle \bv\in\Null(A)$};
        \end{tikzpicture}
      }
      ]
      0\\ 0\\ 0\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorW}{
    \begin{bNiceMatrix}[r, small]
      2\\ -1\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorAW}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<7->[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, red, thick, shorten <=0.5mm, visible on=<7->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 4mm)
          node[left] {$\scriptstyle \bv[w]\notin\Null(A)$};
        \end{tikzpicture}
      }
      ]
      3\\ -64\\ 64\\ -31
    \end{bNiceMatrix}
  }
  \begin{definition}
    The \emph{\grn{null space}} of an $m\times n$ matrix $A$ is
    \[
      \begin{tikzpicture}[thick, remember picture]
        \node {$\Null(A) = \Set{\subnode{all}{\color<2->{blu}\bv\in\mathbb{R}^n}\subnode{st}{\color<3->{red}\given}\subnode{null}{\color<4->{grn}A\bv=\bv[O]}}$};
        \draw[<-, overlay, blu, visible on=<2->] (all.south) |- ++(-5mm, -2mm) node[left] {\scriptsize all $\bv\in\mathbb{R}^n$};
        \draw[<-, overlay, red, visible on=<3->] (st.north) |- ++(5mm, 2mm) node[right] {\scriptsize that satisfy};
        \draw[<-, overlay, grn, visible on=<4->] (null.south) |- ++(5mm, -2mm) node[right] {\scriptsize this equation};
      \end{tikzpicture}
    \]
    \onslide<5->{We write $\bv\in\Null(A)$ to indicate that $A\bv=\bv[O]$.}
    \begin{align*}
      \onslide<5->{\overset{A}{\MatrixA}\overset{\bv}{\VectorV}=\VectorO} && \onslide<5->{\overset{A}{\MatrixA}\overset{\bv[w]}{\VectorW}=\VectorAW}
    \end{align*}
    \onslide<8->{We also refer to $\Null(A)$ as the \emph{\grn{kernel}} of $A$.}
  \end{definition}

\end{frame}


\subsection{Examples}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}
  
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {} -1 &  2 & -11 & -74 \\
      {}  1 & -2 &  -1 & -10 \\
      {} -8 & 16 &   1 &  31
    \end{bNiceMatrix}
  }
  \newcommand{\VectorYesNull}{
    \begin{bNiceMatrix}[r, small]
      1\\ 5\\ 21\\ -3
    \end{bNiceMatrix}
  }
  \newcommand{\VectorNoNull}{
    \begin{bNiceMatrix}[r, small]
      1\\ -2\\ 1\\ 4
    \end{bNiceMatrix}
  }
  \newcommand{\VectorYesNullOutput}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<3->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 6mm)
          node[left] {$\scriptstyle \bv[w]_2\in\Null(A)$};
        \end{tikzpicture}
      }
      ]
      0\\ 0\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorNoNullOutput}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<2->[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, red, thick, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(-2mm, 6mm)
          node[left] {$\scriptstyle \bv[w]_1\notin\Null(A)$};
        \end{tikzpicture}
      }
      ]
      -312\\ -36\\ 85
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixM}{
    \begin{bNiceArray}{rrrr|r}[
      , small
      , code-before = {
        \hlPortion<5->[blu, th]{1}{1}{3}{4}
        \hlCol<6->[grn, th]{5}
      }
      , code-after = {
        \begin{tikzpicture}[<-, thick, shorten <=0.5mm]
          \draw[blu, visible on=<5->] ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle A$};
          \draw[grn, visible on=<6->] ($ (row-1-|col-5)!0.5!(row-1-|col-6) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \bv[O]$};
        \end{tikzpicture}
      }
      ]
      {} -1 &  2 & -11 & -74 & 0 \\
      {}  1 & -2 &  -1 & -10 & 0 \\
      {} -8 & 16 &   1 &  31 & 0
    \end{bNiceArray}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceArray}{rrrr|r}[small]
      1 & -2 & 0 & -3 & 0 \\
      0 &  0 & 1 &  7 & 0 \\
      0 &  0 & 0 &  0 & 0
    \end{bNiceArray}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[r, small]
      x_1\\ x_2\\ x_3\\ x_4
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXs}{
    \begin{bNiceMatrix}[r, small]
      2\,c_1+3\,c_2\\ c_1\\ -7\,c_2\\ c_2
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXa}{
    \begin{bNiceMatrix}[r, small]
      2\\ 1\\ 0\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXb}{
    \begin{bNiceMatrix}[r, small]
      3\\ 0\\ -7\\ 1
    \end{bNiceMatrix}
  }
  \begin{example}
    Whether or not $\bv[x]\in\Null(A)$ hinges on whether or not
    $A\bv[x]=\bv[O]$.
    \begin{align*}
      \overset{A}{\MatrixA}\overset{\bv[w]_1}{\VectorNoNull}=\VectorNoNullOutput && \overset{A}{\MatrixA}\overset{\bv[w]_2}{\VectorYesNull}=\VectorYesNullOutput
    \end{align*}
    \onslide<4->{Describing all $\bv[x]\in\Null(A)$ requires one to solve
      $A\bv[x]=\bv[O]$.}
    \begin{align*}
      \onslide<4->{\rref\MatrixM=\MatrixR} && \onslide<7->{\overset{\bv[x]}{\VectorXs} = c_1\cdot\VectorXa+c_2\cdot\VectorXb}
    \end{align*}
    \onslide<8->{Here, $\Null(A)$ consists of linear combinations of
      $\Set{\nv[small]{2 1 0 0}, \nv[small]{3 0 -7 1}}$.}
  \end{example}

\end{frame}


\section{Properties}
\subsection{Subset Language}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat<4->}
      , code-after = {
        \begin{tikzpicture}[<-, blu, thick, shorten <=0.5mm, visible on=<4->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-5) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle \Null(A)\subset\mathbb{R}^4$};
        \end{tikzpicture}
      }
      ]
      \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixB}{
    \begin{bNiceMatrix}[
      , margin
      , code-before = {\hlMat<5->[grn]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<5->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-7) $) |- ++(-3mm, 3mm)
          node[left] {$\scriptstyle \Null(B)\subset\mathbb{R}^6$};
        \end{tikzpicture}
      }
      ]
      \ast & \ast & \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast & \ast & \ast \\
      \ast & \ast & \ast & \ast & \ast & \ast
    \end{bNiceMatrix}
  }
  \begin{definition}
    For an $m\times n$ matrix $A$, the equation $A\bv=\bv[O]$ only makes sense
    if $\bv\in\mathbb{R}^n$.
    \[
      \onslide<2->{\Null(A)\subset\mathbb{R}^n\xrightarrow{A}\mathbb{R}^m}
    \]
    \onslide<2->{Writing $\Null(A)\subset\mathbb{R}^n$ indicates that vectors in $\Null(A)$
    have $n$ coordinates.}
    \begin{align*}
      \onslide<3->{A = \MatrixA} && \onslide<3->{B = \MatrixB}
    \end{align*}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {} -3 &  6 \\
      {}  2 & -4 \\
      {} -1 &  2 \\
      {} -2 &  4
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixR}{
    \begin{bNiceMatrix}[r, small]
      1 & -2 \\
      0 &  0 \\
      0 &  0 \\
      0 &  0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorX}{
    \begin{bNiceMatrix}[r]
      2\,c_1 \\ c_1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorXa}{
    \begin{bNiceMatrix}[r]
      2\\ 1
    \end{bNiceMatrix}
  }
  \begin{example}
    This matrix $A$ is $4\times 2$, so $\Null(A)\subset\mathbb{R}^2$.
    \begin{align*}
      \rref\MatrixA = \MatrixR && \bv[x] = \VectorX = c_1\cdot\VectorXa
    \end{align*}
    \onslide<2->{The vectors in $\Null(A)$ are the scalar multiples of
      $\nv{2 1}$.}
    \[
      \begin{tikzpicture}[ultra thick, line join=round, line cap=round, scale=0.75, visible on=<2->]
        \draw[<->] (-5, 0) -- (5, 0);
        \draw[<->] (0, -2) -- (0, 2);

        \draw[<->, grn, visible on=<4->] (-4, -2) -- (4, 2) node[right, overlay] {$\Null(A)$};
        \draw[->, blu, visible on=<3->] (0, 0) -- (2, 1) node[midway, sloped, above] {$\nv[small]{2 1}$};
      \end{tikzpicture}
    \]
  \end{example}

\end{frame}


\subsection{Cycles in Digraphs}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\DigraphG}{
    \begin{array}{c}
      \begin{tikzpicture}[grph]
        \node[state, alt=<{5-8,12-17}>{fill=redbg}] (v1) {$v_1$};
        \node[state, alt=<{3-8,10-17}>{fill=redbg}] (v2) [above right=1cm and 2.5cm of v1] {$v_2$};
        \node[state, alt=<{8,15-17}>{fill=redbg}] (v3) [below right=0.75cm and 2.5cm of v1] {$v_3$};

        \draw[->, bend left, alt=<{4-8,11-17}>{red}] (v1) edge node {$a_1$} (v2);
        \draw[->, bend left, alt=<0>{red}] (v2) edge node {$a_2$} (v1);
        \draw[->, bend right, alt=<{7-8,14-17}>{red}] (v1) edge node {$a_3$} (v3);
        \draw[->, bend right=66, alt=<17>{red}] (v3) edge node {$a_4$} (v2);
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\VectorP}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<2-5>[red]{1}
        \hlRow<6-8>[red]{3}
      }
      ]
      -1\\ 0\\ 1\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorC}{
    \begin{bNiceMatrix}[
      , r
      , margin
      , code-before = {
        \hlRow<9-12>[red]{1}
        \hlRow<13-15>[red]{3}
        \hlRow<16-17>[red]{4}
      }
      ]
      -1\\ 0\\ 1\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {} -1 &  1 & -1 &  0 \\
      {}  1 & -1 &  0 &  1 \\
      {}  0 &  0 &  1 & -1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorPb}{
    \begin{bNiceMatrix}[r, small]
      -1\\ 0\\ 1\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorCb}{
    \begin{bNiceMatrix}[r, small]
      -1\\ 0\\ 1\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorAP}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<19->[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, red, thick, shorten <=0.5mm, visible on=<19->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(2mm, 4mm)
          node[right] {$\scriptstyle \bv_p\notin\Null(A)$};
        \end{tikzpicture}
      }
      ]
      0\\ -1\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorAC}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<20->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<20->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(2mm, 4mm)
          node[right] {$\scriptstyle \bv_c\in\Null(A)$};
        \end{tikzpicture}
      }
      ]
      0\\ 0\\ 0
    \end{bNiceMatrix}
  }
  \begin{block}{Recall}
    Paths in a digraph can be represented numerically by \emph{\grn{path
        vectors}}.
    \begin{align*}
      \DigraphG && \bv_{p}=\VectorP && \bv_{c}=\VectorC
    \end{align*}
    \onslide<18->{Path vectors can be multiplied by the incidence matrix.}
    \begin{align*}
      \onslide<18->{\overset{A}{\MatrixA}\overset{\bv_p}{\VectorPb}=\VectorAP} && \onslide<18->{\overset{A}{\MatrixA}\overset{\bv_c}{\VectorCb}=\VectorAC}
    \end{align*}
    \onslide<21->{Of these two paths, only $c$ is a cycle.}
  \end{block}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \begin{theorem}
    A path $p$ is a cycle if and only if $\bv_p\in\Null(A)$.
  \end{theorem}

\end{frame}


\section{Eigenspaces}
\subsection{Definitions}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\EigenVectorA}{
    \begin{tikzpicture}[remember picture, visible on=<3->]
      \node {$(\lambda\cdot \subnode{I}{I_n}-\subnode{A}{A})\subnode{v}{\bv} = \bv[O]$};
      % \draw[->, thick, overlay, blu, visible on=<4->] (v.south) to[out=-90, in=-90] (I.south);
      % \draw[->, thick, overlay, red, visible on=<5->] (v.south) to[out=-90, in=-65] (A.south);
      \draw[->, thick, overlay, blu, visible on=<4->] (v.south) -- ++(0, -3mm) -| (I.south);
      \draw[->, thick, overlay, red, visible on=<5->] (v.south) -- ++(0, -1.5mm) -| (A.south);
    \end{tikzpicture}
  }
  \newcommand{\EigenVectorB}{
    \begin{tikzpicture}[remember picture, visible on=<6->]
      \node {$\subnode{lv}{\lambda\cdot\bv}-A\bv = \subnode{O}{\bv[O]}$};
      \draw[->, thick, overlay, red, visible on=<7->] (lv.south) -- ++(0, -2mm) -| (O.south);
    \end{tikzpicture}
  }
  \newcommand{\EigenVectorC}{
    \begin{tikzpicture}[ultra thick, rounded corners, visible on=<8->]
      \node[alt=<10->{draw, fill=blubg}] {$\only<8>{-}A\bv=\only<8>{-}\lambda\cdot\bv$};
    \end{tikzpicture}
  }
  \begin{definition}
    The \emph{\grn{eigenspace}} of $A$ associated to an eigenvalue $\lambda$ is
    \[
      \mathcal{E}_A(\lambda) = \Null(\lambda\cdot I_n-A)
    \]
    \onslide<2->{Each vector $\bv\in\mathcal{E}_A(\lambda)$ satisfies}
    \begin{align*}
      \EigenVectorA && \EigenVectorB && \EigenVectorC
    \end{align*}
    \onslide<11->{Vectors $\bv\in\mathcal{E}_A(\lambda)$ satisfy
      $A\bv=\lambda\cdot\bv$ and are called \emph{\grn{eigenvectors}}.}
  \end{definition}

\end{frame}


\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      {} -17 &  27 &  9 \\
      {} -13 &  21 &  7 \\
      {}   9 & -15 & -5
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      -3\\ -2\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorW}{
    \begin{bNiceMatrix}[
      , r
      , small
      ]
      -4\\ -4\\ 5
    \end{bNiceMatrix}
  }
  \newcommand{\VectorAV}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<2->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<2->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(2mm, 4mm)
          node[right] {$\scriptstyle A\bv=-2\cdot\bv$};
        \end{tikzpicture}
      }
      ]
      6\\ 4\\ -2
    \end{bNiceMatrix}
  }
  \newcommand{\VectorAW}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<3->[red, th]}
      , code-after = {
        \begin{tikzpicture}[<-, red, thick, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-2) $) |- ++(2mm, 4mm)
          node[right] {$\scriptstyle A\bv[w]\neq \lambda\cdot\bv[w]$};
        \end{tikzpicture}
      }
      ]
      5\\ 3\\ -1
    \end{bNiceMatrix}
  }
  \begin{example}
    To verify if $\bv$ is an eigenvector, we need only check
    $A\bv=\lambda\cdot\bv$.
    \begin{align*}
      \overset{A}{\MatrixA}\overset{\bv}{\VectorV}=\VectorAV && \overset{A}{\MatrixA}\overset{\bv[w]}{\VectorW}=\VectorAW
    \end{align*}
    \onslide<4->{Here, we find that $\bv\in\mathcal{E}_A(-2)$ and that $\bv[w]$
      is \emph{not} and eigenvector of $A$.}
  \end{example}

\end{frame}

\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {}  9 & -21 & -28 \\
      {} 14 & -40 & -56 \\
      {} -7 &  21 &  30
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixXa}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<3->[grn, th]}
      , code-after = {
        \begin{tikzpicture}[<-, grn, thick, shorten <=0.5mm, visible on=<3->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle -5\cdot I_3-A$};
        \end{tikzpicture}
      }
      ]
      {} -14 &  21 &  28 \\
      {} -14 &  35 &  56 \\
      {}   7 & -21 & -35
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixXaR}{
    \begin{bNiceMatrix}[r, small]
      1 & 0 & 1 \\
      0 & 1 & 2 \\
      0 & 0 & 0
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixXb}{
    \begin{bNiceMatrix}[
      , r
      , small
      , code-before = {\hlMat<7->[blu, th]}
      , code-after = {
        \begin{tikzpicture}[<-, blu, thick, shorten <=0.5mm, visible on=<7->]
          \draw ($ (row-1-|col-1)!0.5!(row-1-|col-4) $) |- ++(-3mm, 2.5mm)
          node[left] {$\scriptstyle 2\cdot I_3-A$};
        \end{tikzpicture}
      }
      ]
      {}  -7 &  21 &  28 \\
      {} -14 &  42 &  56 \\
      {}   7 & -21 & -28
    \end{bNiceMatrix}
  }
  \newcommand{\MatrixXbR}{
    \begin{bNiceMatrix}[r, small]
      1 & -3 & -4 \\
      0 &  0 &  0 \\
      0 &  0 &  0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorV}{
    \begin{bNiceMatrix}[r, small]
      -1\\ -2\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\VectorWa}{
    \begin{bNiceMatrix}[r, small]
      3\\ 1\\ 0
    \end{bNiceMatrix}
  }
  \newcommand{\VectorWb}{
    \begin{bNiceMatrix}[r, small]
      4\\ 0\\ 1
    \end{bNiceMatrix}
  }
  \newcommand{\EvalA}{
    \begin{array}{c}
      \begin{tikzpicture}[rounded corners, ultra thick, visible on=<4->]
        \node[alt=<5->{draw, fill=grnbg}] (v) {$\bv = c_1\cdot\VectorV$};
        \draw[thick, overlay, grn, <-, shorten <=0.5mm, visible on=<5->] (v.east) -- ++ (5mm, 0)
        node[right] {$\scriptstyle \mathcal{E}_A(-5)$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\EvalB}{
    \begin{array}{c}
      \begin{tikzpicture}[rounded corners, ultra thick, visible on=<8->]
        \node[alt=<9->{draw, fill=blubg}] (v) {$\bv = c_1\cdot\VectorWa+c_2\cdot\VectorWb$};
        \draw[thick, overlay, blu, <-, shorten <=0.5mm, visible on=<9->] (v.east) -- ++ (5mm, 0)
        node[right] {$\scriptstyle \mathcal{E}_A(2)$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{example}
    Consider the following data.
    \begin{align*}
      A=\MatrixA && \EVals(A)=\Set{-5, 2}
    \end{align*}
    \onslide<2->{For $\lambda=-5$, the eigenvectors live in
      $\mathcal{E}_A(-5)=\Null(-5\cdot I_3-A)$.}
    \begin{align*}
      \onslide<2->{\rref\MatrixXa = \MatrixXaR} && \EvalA
    \end{align*}
    \onslide<6->{For $\lambda=2$, the eigenvectors live in
      $\mathcal{E}_A(2)=\Null(2\cdot I_3-A)$.}
    \begin{align*}
      \onslide<6->{\rref\MatrixXb = \MatrixXbR} && \EvalB
    \end{align*}
  \end{example}

\end{frame}


\subsection{Geometry}
\begin{frame}

  \frametitle{\secname}
  \framesubtitle{\subsecname}

  \pgfmathsetmacro{\FigScale}{0.4}
  \newcommand{\MatrixA}{
    \begin{bNiceMatrix}[r, small]
      {}  1 & 2 \\
      {} -1 & 4
    \end{bNiceMatrix}
  }
  \newcommand{\Domain}{
    \begin{array}{c}
      \begin{tikzpicture}[ultra thick, scale=\FigScale]
        \draw[<->] (-1, 0) -- (4, 0);
        \draw[<->] (0, -1) -- (0, 4);

        \draw[->, blu, visible on=<3->] (0, 0) -- (2, 0)
        node[below, overlay] {$\scriptstyle \bv=\nvc[small]{2; 0}$};
        \draw[->, blu, visible on=<5->] (0, 0) -- (1, 1)
        node[right] {$\scriptstyle \bv[w]=\nvc[small]{1; 1}$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Target}{
    \begin{array}{c}
      \begin{tikzpicture}[ultra thick, scale=\FigScale]
        \draw[<->] (-1, 0) -- (5, 0);
        \draw[<->] (0, -2) -- (0, 3);

        \draw[->, grn, visible on=<4->] (0, 0) -- (2, -2) node[right, overlay]
        {$\scriptstyle A\bv=\nvc[small]{2; -2}$};
        \draw[->, grn, visible on=<6->] (0, 0) -- (3, 3)  node[right, overlay]
        {$\scriptstyle A\bv[w]=\nvc[small]{3; 3}$};
      \end{tikzpicture}
    \end{array}
  }
  \newcommand{\Super}{
    \begin{array}{c}
      \begin{tikzpicture}[ultra thick, scale=\FigScale]
        \draw[<->] (-1, 0) -- (5, 0);
        \draw[<->] (0, -2) -- (0, 3);

        \draw[->, grn] (0, 0) -- (2, -2) node[right, overlay]
        {$\scriptstyle A\bv\neq\lambda\cdot\bv$};
        \draw[->, grn] (0, 0) -- (3, 3)  node[right, overlay]
        {$\scriptstyle A\bv[w]=3\cdot\bv[w]$};

        \draw[->, blu] (0, 0) -- (2, 0) node[below] {$\scriptstyle \bv$};
        \draw[->, blu] (0, 0) -- (1, 1) node[above] {$\scriptstyle \bv[w]$};
      \end{tikzpicture}
    \end{array}
  }
  \begin{block}{Observation}
    Eigenvectors satisfy $A\bv=\lambda\cdot\bv$, so the output is
    \emph{parallel} to the input.
    \[
      \onslide<2->{\Domain\xrightarrow{A=\MatrixA}\Target}
    \]
    \onslide<7->{Superimposing illustrates that $\bv[w]\in\mathcal{E}_A(3)$ but
      that $\bv$ is not an eigenvector.}
    \[
      \onslide<7->{\Super}
    \]
  \end{block}

\end{frame}

\end{document}